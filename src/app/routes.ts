import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: '', loadChildren: './home/home.module#HomeModule', pathMatch: 'full' },
  {
    path: 'species',
    loadChildren: './taxon/taxon.module#TaxonModule'
  },
  {
    path: 'close-by',
    loadChildren: './close-by/close-by.module#CloseByModule'
  },
  {
    path: 'observations',
    loadChildren: './document/document.module#DocumentModule'
  },
  {
    path: 'user',
    loadChildren: './user/user.module#UserModule'
  },
  {
    path: 'instructions',
    loadChildren: './instructions/instructions.module#InstructionsModule'
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'
  },
  // { path: '**', component: NotFoundPageComponent },
];
