import { ActionReducer, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import * as fromCloseBy from './reducers/close-by-reducers';
import * as fromRoot from '../../core/store';
import { localStorageSync } from 'ngrx-store-localstorage';

export interface CloseByState {
  closeBy: fromCloseBy.State;
}

export interface State extends fromRoot.State {
  'closeBy': CloseByState;
}

export const reducers = {
  closeBy: fromCloseBy.reducer
};

export function localStorageSyncReducer(reducer: ActionReducer<State>): ActionReducer<any, any> {
  return localStorageSync({
    keys: [
      'closeBy'
    ],
    rehydrate: true,
    removeOnUndefined: true,
    restoreDates: false
  })(reducer);
}

export const metaReducers: MetaReducer<State>[] = [localStorageSyncReducer];

export const getCloseByFeatureState = createFeatureSelector<CloseByState>('closeBy');

export const getCloseByState = createSelector(getCloseByFeatureState, (state: CloseByState) => state.closeBy);

export const getCloseByFilters = createSelector(getCloseByState, fromCloseBy.getFilters);
