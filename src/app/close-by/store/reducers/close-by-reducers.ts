import * as closeByAction from '../actions/close-by-actions';
import { LajiApi } from '../../../shared/service/laji-api.service';

export interface State {
  filters: LajiApi.WarehouseQuery;
}

export const initialState: State = {
  filters: {}
};

export function reducer(
  state = initialState,
  action: closeByAction.Actions
): State {
  switch (action.type) {
    case closeByAction.UPDATE_FILTERS:
      return {
        ...state,
        filters: action.payload
      };
    default: {
      return state;
    }
  }
}

export const getFilters = (state: State) => state.filters;
