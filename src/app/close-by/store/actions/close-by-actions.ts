import { Action } from '@ngrx/store';
import { LajiApi } from '../../../shared/service/laji-api.service';

export const UPDATE_FILTERS = '[CloseBy] update filters';

export class UpdateFiltersAction implements Action {
  readonly type = UPDATE_FILTERS;

  constructor(public payload: LajiApi.WarehouseQuery) {}
}


export type Actions
  = UpdateFiltersAction;
