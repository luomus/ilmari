import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CloseByRoutingModule } from './close-by-routing.module';
import { CloseByContainerComponent } from './close-by-container/close-by-container.component';
import { SharedModule } from '../shared/shared.module';
import { CloseByMapComponent } from './close-by-map/close-by-map.component';
import { NguiMapModule } from '@ngui/map';
import { environment } from '../../environments/environment';
import { CloseByFiltersComponent } from './close-by-filters/close-by-filters.component';
import { CloseByTableComponent } from './close-by-table/close-by-table.component';
import { MatPaginatorModule } from '@angular/material';
import { CloseByListComponent } from './close-by-list/close-by-list.component';
import { CloseByStatsComponent } from './close-by-stats/close-by-stats.component';
import { CloseByMapInfoComponent } from './close-by-map-info/close-by-map-info.component';
import { CloseByStatsContainerComponent } from './close-by-stats-container/close-by-stats-container.component';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from './store';
import { DistanceComponent } from './close-by-table/distance/distance.component';

@NgModule({
  imports: [
    CommonModule,
    MatPaginatorModule,
    SharedModule,
    NguiMapModule.forRoot({
      apiUrl: 'https://maps.google.com/maps/api/js?v=3.33&libraries=visualization,places,drawing&key=' + environment.mapsToken
    }),
    StoreModule.forFeature('closeBy', reducers as any, {metaReducers}),
    CloseByRoutingModule
  ],
  declarations: [
    CloseByContainerComponent,
    CloseByMapComponent,
    CloseByFiltersComponent,
    CloseByTableComponent,
    CloseByListComponent,
    CloseByStatsComponent,
    CloseByMapInfoComponent,
    CloseByStatsContainerComponent,
    DistanceComponent
  ]
})
export class CloseByModule { }
