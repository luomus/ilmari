import { ChangeDetectionStrategy, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import * as fromRoot from '../../core/store';
import * as fromUser from '../../user/store';
import * as fromCloseBy from '../store';
import * as closeByActions from '../store/actions/close-by-actions';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CloseByFiltersComponent } from '../close-by-filters/close-by-filters.component';
import { LajiApi } from '../../shared/service/laji-api.service';
import { CloseByMapComponent } from '../close-by-map/close-by-map.component';
import { MatDrawer } from '@angular/material';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'ilm-close-by-container',
  templateUrl: './close-by-container.component.html',
  styleUrls: ['./close-by-container.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseByContainerComponent implements OnInit {

  @ViewChild(CloseByFiltersComponent) filters: CloseByFiltersComponent;
  @ViewChild(CloseByMapComponent) map: CloseByMapComponent;
  @ViewChild(MatDrawer) drawer: MatDrawer;

  isMobile$: Observable<boolean>;
  isMobile: boolean;
  filters$: Observable<LajiApi.WarehouseQuery>;
  personToken$: Observable<string>;
  filterCnt = 0;
  viewBounds: any;

  constructor(
    private store: Store<fromRoot.State>
  ) {
    this.isMobile$ = this.store.select(fromRoot.getIsMobile).pipe(
      tap((isMobile) => this.isMobile = isMobile)
    );
    this.filters$ = this.store.select(fromCloseBy.getCloseByFilters).pipe(
      tap(filters => {
        let remove = 0;
        if (filters.qualityIssues) {
          remove++;
        }
        this.filterCnt = Object.keys(filters).length - remove;
      })
    );
    this.personToken$ = this.store.select(fromUser.getPersonToken);
  }

  ngOnInit() {
  }

  update() {
    this.store.dispatch(new closeByActions.UpdateFiltersAction(this.filters.getFilters()));
  }

  clear() {
    this.filters.clear();
  }

  mapViewportChanged(bounds) {
    this.viewBounds = bounds;
  }

}
