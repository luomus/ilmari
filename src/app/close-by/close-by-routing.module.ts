import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CloseByContainerComponent } from './close-by-container/close-by-container.component';

const routes: Routes = [
  {path: '', component: CloseByContainerComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CloseByRoutingModule { }
