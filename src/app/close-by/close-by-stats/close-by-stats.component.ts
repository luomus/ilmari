import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { StatsDaoService } from '../service/stats-dao.service';
import { LajiApi } from '../../shared/service/laji-api.service';
import { ResultItem } from '../service/table-dao.interface';

@Component({
  selector: 'ilm-close-by-stats',
  templateUrl: './close-by-stats.component.html',
  styleUrls: ['./close-by-stats.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseByStatsComponent {

  @Input() pageSize = 30;
  @Input() _query: LajiApi.WarehouseQuery;

  listQuery: LajiApi.WarehouseQuery;
  showList = false;

  constructor(public statsDaoService: StatsDaoService) { }

  @Input()
  set query(query: LajiApi.WarehouseQuery) {
    this._query = query;
    this.showList = false;
  }

  rowClick(row: ResultItem) {
    this.listQuery = {...this._query, target: row.species};
    this.showList = true;
  }

  closeList() {
    this.showList = false;
  }

}
