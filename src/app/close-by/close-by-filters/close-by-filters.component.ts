import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { LajiApi } from '../../shared/service/laji-api.service';
import { LocationService } from '../../shared/service/location.service';
import { debounceTime, map } from 'rxjs/operators';
import { of, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'ilm-close-by-filters',
  templateUrl: './close-by-filters.component.html',
  styleUrls: ['./close-by-filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseByFiltersComponent implements OnInit, OnDestroy {

  _filters: LajiApi.WarehouseQuery = {};

  dateStart: string;
  dateEnd: string;

  mine: boolean;
  changeSub: Subscription;

  expandedTime = false;
  expandedArea = false;
  expandedOwn = false;
  expandedName = false;

  @Input() personToken: string;

  @Input() viewBounds: any;
  @Output() change = new EventEmitter<LajiApi.WarehouseQuery>();

  private _change = new Subject();

  constructor(
    private locationService: LocationService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.changeSub = this._change.pipe(
      debounceTime(1000)
    ).subscribe(() => {
      this.change.emit({
        ...this._filters,
        coordinates: this._filters.coordinates ? this.locationService.coordinatesWithSystem(this._filters.coordinates) : undefined
      });
    });
  }

  ngOnDestroy() {
    this.changeSub.unsubscribe();
  }

  dateChange(spot: 'start'|'end', event) {
    if (spot === 'start') {
      this.dateStart = this.toIsoDate(event);
    } else {
      this.dateEnd = this.toIsoDate(event);
    }
  }

  setTime(diff) {
    const d1 = new Date();
    const d2 = new Date();
    if (diff === '-1y') {
      d2.setFullYear(d2.getFullYear(), 0, 1);
      this.dateEnd = this.toIsoDate(d1);
      this.dateStart = this.toIsoDate(d2);
    } else if (diff === '-2y') {
      d1.setFullYear(d1.getFullYear() - 1, 11, 31);
      d2.setFullYear(d2.getFullYear() - 1, 0, 1);
      this.dateEnd = this.toIsoDate(d1);
      this.dateStart = this.toIsoDate(d2);
    } else if (diff === '-1w') {
      d2.setFullYear(d2.getFullYear(), d2.getMonth(), d2.getDate() - 7);
      this.dateEnd = this.toIsoDate(d1);
      this.dateStart = this.toIsoDate(d2);
    }
    this.update();
  }

  setLocation(grid) {
    if (grid === '1km' || grid === '10km') {
      of(this.viewBounds).pipe(
        map(bounds => {
          const center = bounds.getCenter();
          return this.locationService.convertToWgsToYkj({longitude: center.lng(), latitude: center.lat(), accuracy: 1, altitude: 0, altitudeAccuracy: 0, heading: 0, speed: 0});
        })
      ).subscribe(pos => {
        const lat = '' + pos.latitude;
        const lng = '' + pos.longitude;
        const length = grid === '1km' ? 4 : 3;
        this._filters = {
          ...this._filters,
          coordinates: lat.slice(0, length) + ':' + lng.slice(0, length)
        };
        this.cdr.markForCheck();
      });
    } else {
      this._filters = {
        ...this._filters,
        coordinates: this.locationService.coordinatesFromBounds(this.viewBounds)
      };
    }
    this.update();
  }

  clear() {
    this._filters = {};
    this.dateStart = '';
    this.dateEnd = '';
    this.mine = false;
    this.cdr.markForCheck();
    this.update();
  }

  getFilters(): LajiApi.WarehouseQuery {
    const result: LajiApi.WarehouseQuery = {
      ...this._filters,
      coordinates: this._filters.coordinates ? this.locationService.coordinatesWithSystem(this._filters.coordinates) : undefined
    };
    if (this.dateStart || this.dateEnd) {
      result.time = this.dateStart <= this.dateEnd ?
        `${this.dateStart}/${this.dateEnd}` :
        `${this.dateEnd}/${this.dateStart}`;
    } else {
      result.time = undefined;
    }
    result.editorOrObserverPersonToken = this.mine ? this.personToken : undefined;
    result.qualityIssues = this.mine ? 'BOTH' : undefined;
    Object.keys(result).forEach(key => result[key] === undefined || result[key] === '' ? delete result[key] : '');

    return result;
  }

  @Input()
  set filters(filters: LajiApi.WarehouseQuery) {
    this._filters = {...filters};
    if (filters.time) {
      this.parseTimeFilter(filters.time);
    } else {
      this.dateStart = '';
      this.dateEnd = '';
    }
    this.mine = !!filters.editorOrObserverPersonToken;
    this.initExpanded();
  }

  updateValue(key: string, value) {
    this._filters[key] = value;
    this.update();
  }

  update() {
    this._change.next();
  }

  private initExpanded() {
    this.expandedOwn = this.mine;
    this.expandedTime = !!(this.dateStart || this.dateEnd);
    this.expandedArea = !!this._filters.coordinates;
    this.expandedName = !!(this._filters.target || this._filters.informalTaxonGroupId);
  }

  private parseTimeFilter(time: string) {
    const parts = time.split('/', 2);
    if (parts.length === 2) {
      this.dateStart = parts[0];
      this.dateEnd = parts[1];
    } else {
      this.dateStart = parts[0];
    }
  }

  private toIsoDate(date: Date) {
    if (date instanceof Date) {
      return date.getFullYear() +
        '-' + ('' + (date.getMonth() + 1)).padStart(2, '0') +
        '-' + ('' + (date.getDate())).padStart(2, '0');
    }
    return date;
  }

}
