import { Component, Input, OnInit } from '@angular/core';
import { LajiApi } from '../../shared/service/laji-api.service';

@Component({
  selector: 'ilm-close-by-stats-container',
  templateUrl: './close-by-stats-container.component.html',
  styleUrls: ['./close-by-stats-container.component.scss']
})
export class CloseByStatsContainerComponent implements OnInit {

  @Input() query: LajiApi.WarehouseQuery;

  constructor() { }

  ngOnInit() {
  }

}
