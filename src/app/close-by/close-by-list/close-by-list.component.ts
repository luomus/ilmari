import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ListDaoService } from '../service/list-dao.service';
import { LajiApi } from '../../shared/service/laji-api.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'ilm-close-by-list',
  templateUrl: './close-by-list.component.html',
  styleUrls: ['./close-by-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseByListComponent implements OnInit {

  @Input() pageSize = 30;
  @Input() isMobile: boolean;
  @Input() query: LajiApi.WarehouseQuery;

  constructor(public listDaoService: ListDaoService) { }

  ngOnInit() {
  }

  onRowClick(row) {
    const url = environment.viewer
      .replace('%uri%', row.documentId)
      .replace('%highlight%', row.unitId)
      .replace('#', '%23');
    const win = window.open(url, '_blank');
    win.focus();
  }
}
