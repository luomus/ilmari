import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ResultItem, TableDaoInterface } from '../service/table-dao.interface';
import { merge, of, Subject, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap, tap } from 'rxjs/operators';
import { LajiApi } from '../../shared/service/laji-api.service';

@Component({
  selector: 'ilm-close-by-table',
  templateUrl: './close-by-table.component.html',
  styleUrls: ['./close-by-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseByTableComponent implements OnDestroy {

  displayedColumns = ['species', 'count', 'individuals'];
  dataSource = new MatTableDataSource();
  resultsLength = 0;
  isLoadingResults = true;

  @Input() pageSize;
  @Output() rowClick = new EventEmitter<ResultItem>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  private dataSub: Subscription;
  private sortSub: Subscription;
  private source: TableDaoInterface;
  private _isMobile: boolean;
  private _query: LajiApi.WarehouseQuery;
  private queryChange = new Subject();

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnDestroy() {
    if (this.dataSub) {
      this.dataSub.unsubscribe();
      this.sortSub.unsubscribe();
    }
  }

  @Input()
  set query(query: LajiApi.WarehouseQuery) {
    this._query = query;
    this.queryChange.next();
  }

  @Input()
  set isMobile(mobile: boolean) {
    this._isMobile = mobile;
    if (!this.source) {
      return;
    }
    this.initCols();
  }

  @Input()
  set database(source: TableDaoInterface) {
    this.source = source;
    this.initCols();
    if (this.dataSub) {
      this.dataSub.unsubscribe();
      this.sortSub.unsubscribe();
    }
    this.sortSub = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.dataSub = merge(this.sort.sortChange, this.paginator.page, this.queryChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return source.fetch(
            this._query, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.resultsLength = data.total;

          return data.results;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          return of([]);
        }),
        tap(() => this.cdr.markForCheck())
      ).subscribe(data => this.dataSource.data = data);
  }

  private initCols() {
    if (this._isMobile && this.source.mobileColumns) {
      this.displayedColumns = this.source.mobileColumns;
    } else {
      this.displayedColumns = this.source.columns;
    }
  }

}
