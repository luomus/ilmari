import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ilm-distance',
  templateUrl: './distance.component.html',
  styleUrls: ['./distance.component.scss']
})
export class DistanceComponent implements OnInit {

  _distance: number;
  _distanceType: 'm'|'km' = 'm';

  constructor() { }

  ngOnInit() {
  }

  @Input()
  set distance(dist: number) {
    if (dist >= 1000) {
      this._distance = +(dist / 1000).toFixed(0);
      this._distanceType = 'km';
    } else {
      this._distance = dist;
      this._distanceType = 'm';
    }
  }

}
