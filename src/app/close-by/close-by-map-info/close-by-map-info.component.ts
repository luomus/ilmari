import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { LajiApi } from '../../shared/service/laji-api.service';

@Component({
  selector: 'ilm-close-by-map-info',
  templateUrl: './close-by-map-info.component.html',
  styleUrls: ['./close-by-map-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseByMapInfoComponent {

  @Input() query: LajiApi.WarehouseQuery;

  constructor() { }

}
