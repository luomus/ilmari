import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { LajiApi, LajiApiService } from '../../shared/service/laji-api.service';
import { catchError, debounceTime, map, tap } from 'rxjs/operators';
import { of, Subject, Subscription } from 'rxjs';
import { LocationService } from '../../shared/service/location.service';
import LatLngBounds = google.maps.LatLngBounds;
import LatLng = google.maps.LatLng;

@Component({
  selector: 'ilm-close-by-map',
  templateUrl: './close-by-map.component.html',
  styleUrls: ['./close-by-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseByMapComponent implements OnInit, OnChanges, OnDestroy {

  @Input() query: LajiApi.WarehouseQuery = {};
  @Output() boundsChanged = new EventEmitter<LatLngBounds>();
  position: LatLng[];
  map: any;
  displayMarker = false;
  infoQuery: LajiApi.WarehouseQuery;
  isLoadingResults = false;
  bound = new Subject();
  boundSub: Subscription;
  pointsSub: Subscription;
  currentBounds: LatLngBounds;

  constructor(
    private lajiApiService: LajiApiService,
    private locationService: LocationService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.boundSub = this.bound.pipe(
      debounceTime(500)
    ).subscribe(() => {
      if (!this.map) {
        return;
      }
      this.boundsChanged.emit(this.map.getBounds());
      this.fetchMapData();
    });
  }

  ngOnChanges() {
    this.fetchMapData(true);
  }

  ngOnDestroy() {
    this.boundSub.unsubscribe();
    if (this.pointsSub) {
      this.pointsSub.unsubscribe();
    }
  }

  initMap(mapElem) {
    this.map = mapElem;
  }

  fetchMapData(queryChange = false) {
    if (!this.map) {
      return;
    }
    const bounds = this.map.getBounds();
    if (!bounds ||
      (!queryChange && this.currentBounds && this.currentBounds.contains(bounds.getNorthEast()) && this.currentBounds.contains(bounds.getSouthWest())) ||
      !queryChange && this.query.coordinates
    ) {
      return;
    }
    this.currentBounds = this.extendBounds(bounds);
    const coordinates = this.query.coordinates ? this.query.coordinates : this.locationService.coordinatesWithSystem(this.locationService.coordinatesFromBounds(this.currentBounds));

    this.isLoadingResults = true;
    this.pointsSub = this.lajiApiService.getList(
      LajiApi.Endpoint.warehouseAggregate,
      {
        ...this.query,
        aggregateBy: 'gathering.conversions.wgs84CenterPoint.lat,gathering.conversions.wgs84CenterPoint.lon',
        taxonRankId: 'MX.species,MX.subspecies',
        coordinates: coordinates,
        coordinateAccuracyMax: 100,
        includeNonValidTaxa: false,
        pageSize: 10000,
        page: 1
      }
    ).pipe(
      map(result => result.results.map(item => new google.maps.LatLng(
        parseFloat(item.aggregateBy['gathering.conversions.wgs84CenterPoint.lat']),
        parseFloat(item.aggregateBy['gathering.conversions.wgs84CenterPoint.lon'])
      ))),
      catchError(() => of([])),
      tap(() => this.isLoadingResults = false)
    ).subscribe(points => {
      this.position = points;
      this.cdr.markForCheck();
    });
  }

  onClick({target: marker}) {
    this.displayMarker = true;
    this.infoQuery = {
      ...this.query,
      wgs84CenterPoint: [
        marker.getPosition().lat(),
        marker.getPosition().lng(),
        'WGS84'
      ].join(':')
    };

    marker.nguiMapComponent.openInfoWindow('iw', marker);
  }

  onClose() {
    this.displayMarker = false;
  }

  positionTrackBy(idx, pos: LatLng) {
    return pos.lat() + ':' + pos.lng();
  }

  private extendBounds(bounds: LatLngBounds): LatLngBounds {
    const ne = bounds.getNorthEast();
    const sw = bounds.getSouthWest();
    const height = Math.abs(ne.lat() - sw.lat());
    const width = Math.abs(ne.lng() - sw.lng());
    bounds.extend({lat: ne.lat() + height, lng: ne.lng() + width});
    bounds.extend({lat: ne.lat() - height, lng: ne.lng() - width});
    bounds.extend({lat: sw.lat() + height, lng: sw.lng() + width});
    bounds.extend({lat: sw.lat() - height, lng: sw.lng() - width});
    return bounds;
  }

}
