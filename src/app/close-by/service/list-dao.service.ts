import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { Result, ResultItem, TableDaoInterface } from './table-dao.interface';
import { Observable, of } from 'rxjs';
import { LajiApi, LajiApiService } from '../../shared/service/laji-api.service';
import { map, tap } from 'rxjs/operators';
import { PagedResult } from '../../models/paged-result';

@Injectable({
  providedIn: 'root'
})
export class ListDaoService implements TableDaoInterface {

  cache: any;
  cacheKey: string;

  constructor(
    private lajiApiService: LajiApiService,
    @Inject(LOCALE_ID) private locale: string
  ) { }

  columns: string[] = ['species', 'location', 'locality', 'coordinateAccuracy', 'displayDateTime', 'team', 'count'];
  mobileColumns: string[] = ['species', 'location', 'coordinateAccuracy', 'displayDateTime', 'count'];

  sorts = {
    'species': ['unit.taxonVerbatim'],
    'count': ['unit.abundanceString'],
    'location': [
      'gathering.interpretations.countryDisplayname',
      'gathering.interpretations.biogeographicalProvinceDisplayname',
      'gathering.interpretations.municipalityDisplayname'
    ],
    'coordinateAccuracy': ['gathering.interpretations.coordinateAccuracy'],
    'locality': ['gathering.locality'],
    'displayDateTime': ['gathering.displayDateTime'],
    'team': ['gathering.team']
  };

  fetch(query: LajiApi.WarehouseQuery, sort: any, direction: any, pageIndex: number, pageSize: number): Observable<Result> {
    const cacheKey = this.getCacheKey(query, sort, direction, pageIndex, pageSize);
    if (cacheKey === this.cacheKey) {
      return of(this.cache);
    }
    return this.lajiApiService.getList(
      LajiApi.Endpoint.warehouseList,
      {
        ...query,
        selected: [
          'document.documentId',
          'unit.unitId',
          'gathering.interpretations.countryDisplayname',
          'gathering.interpretations.biogeographicalProvinceDisplayname',
          'gathering.interpretations.municipalityDisplayname',
          'gathering.interpretations.coordinateAccuracy',
          'gathering.displayDateTime',
          'unit.taxonVerbatim',
          'unit.abundanceString',
          'gathering.locality',
          'gathering.team',
        ].join(','),
        includeNonValidTaxa: false,
        orderBy: this.getOrderBy(sort, direction),
        pageSize: pageSize,
        page: pageIndex + 1
      }
    ).pipe(
      map<PagedResult<any>, Result>(response => ({
          total: response.total,
          results: response.results.map(item => {
            const data: ResultItem = {};
            if (item.document) {
              data.documentId = item.document.documentId;
            }
            if (item.gathering) {
              data.displayDateTime = item.gathering.displayDateTime;
              data.locality = item.gathering.locality;
              data.team = item.gathering.team ? item.gathering.team.join('; ') : undefined;
              if (item.gathering.interpretations) {
                const places = [
                  item.gathering.interpretations.countryDisplayname,
                  item.gathering.interpretations.biogeographicalProvinceDisplayname,
                  item.gathering.interpretations.municipalityDisplayname,
                ];
                data.location = places.filter(val => val).join(', ');
                data.coordinateAccuracy = item.gathering.interpretations.coordinateAccuracy;
              }
            }
            if (item.unit) {
              data.unitId = item.unit.unitId;
              data.species = item.unit.taxonVerbatim;
              data.count = item.unit.abundanceString;
            }
            return data;
          })
        })),
      tap(data => {
        this.cache = data;
        this.cacheKey = cacheKey;
      })
    );
  }

  private getCacheKey(query: LajiApi.WarehouseQuery, sort: any, direction: any, pageIndex: number, pageSize: number) {
    return JSON.stringify(query) + ':' + [sort, direction, pageIndex, pageSize].join(':');
  }

  private getOrderBy(sort: string, direction: string): string {
    if (!this.sorts[sort] || !direction) {
      return '';
    }
    return this.sorts[sort].reduce((cumulative, current) => {
      if (cumulative) {
        cumulative += ',';
      }
      cumulative += current + ' ' + direction.toUpperCase();
      return cumulative;
    }, '');
  }
}
