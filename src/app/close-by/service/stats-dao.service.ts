import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { Result, TableDaoInterface } from './table-dao.interface';
import { LajiApi, LajiApiService } from '../../shared/service/laji-api.service';
import { map, tap } from 'rxjs/operators';
import { WarehouseAggregateResult } from '../../models/warehouse-aggregate-result';
import { PagedResult } from '../../models/paged-result';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatsDaoService implements TableDaoInterface {

  cache: any;
  cacheKey: string;

  taxonSpeciesName = {
    'fi': 'unit.linkings.taxon.speciesNameFinnish',
    'sv': 'unit.linkings.taxon.speciesNameSwedish',
    'en': 'unit.linkings.taxon.speciesNameEnglish'
  };

  constructor(
    private lajiApiService: LajiApiService,
    @Inject(LOCALE_ID) private locale: string
  ) { }

  sorts = {
    'species': ['unit.linkings.taxon.speciesScientificName', '%vernacular%'],
    'count': ['count'],
    'individuals': ['individualCountSum'],
    'period': ['newestRecord', 'oldestRecord']
  };

  columns: string[] = ['species', 'count', 'individuals', 'period'];

  fetch(query: LajiApi.WarehouseQuery, sort: any, direction: any, pageIndex: number, pageSize: number): Observable<Result> {
    const cacheKey = this.getCacheKey(query, sort, direction, pageIndex, pageSize);
    if (cacheKey === this.cacheKey) {
      return of(this.cache);
    }
    const vernacular = this.getVernacularField();
    return this.lajiApiService.getList(
      LajiApi.Endpoint.warehouseAggregate,
      {
        ...query,
        aggregateBy: 'unit.linkings.taxon.speciesScientificName,' + vernacular,
        orderBy: this.getOrderBy(sort, direction),
        onlyCount: false,
        includeNonValidTaxa: false,
        pageSize: pageSize,
        page: pageIndex + 1
      }
    ).pipe(
      map<PagedResult<WarehouseAggregateResult>, Result>(response => ({
        total: response.total,
        results: response.results.map(item => ({
          oldestRecord: item.oldestRecord,
          newestRecord: item.newestRecord,
          individuals: item.individualCountSum,
          count: item.count,
          species: item.aggregateBy[vernacular] || item.aggregateBy['unit.linkings.taxon.speciesScientificName']
        }))
      })),
      tap(data => {
        this.cacheKey = cacheKey;
        this.cache = data;
      })
    );
  }

  private getCacheKey(query: LajiApi.WarehouseQuery, sort: any, direction: any, pageIndex: number, pageSize: number) {
    return JSON.stringify(query) + ':' + [sort, direction, pageIndex, pageSize].join(':');
  }

  private getOrderBy(sort: string, direction: string): string {
    if (!this.sorts[sort] || !direction) {
      return '';
    }
    return this.sorts[sort].reduce((cumulative, current) => {
      if (cumulative) {
        cumulative += ',';
      }
      if (current === '%vernacular%') {
        current = this.getVernacularField();
      }
      cumulative += current + ' ' + direction.toUpperCase();
      return cumulative;
    }, '');
  }

  private getVernacularField() {
    return this.taxonSpeciesName[this.locale] || this.taxonSpeciesName['fi'];
  }
}
