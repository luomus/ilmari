import { Observable } from 'rxjs';
import { LajiApi } from '../../shared/service/laji-api.service';

export interface ResultItem {
  documentId?: string;
  unitId?: string;
  species?: string;
  count?: number;
  individuals?: number;
  oldestRecord?: string;
  newestRecord?: string;
  team?: string;
  displayDateTime?: string;
  location?: string;
  locality?: string;
  coordinateAccuracy?: number;
}

export interface Result {
  total: number;
  results: ResultItem[];
}

export interface TableDaoInterface {
  columns: string[];
  mobileColumns?: string[];
  fetch(query: LajiApi.WarehouseQuery, sort: any, direction: any, pageIndex: number, pageSize: number): Observable<Result>;
}
