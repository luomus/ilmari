import { Injectable } from '@angular/core';
import { DBService } from '../../shared/service/db.service';
import { Observable } from 'rxjs';
import { TableInterface } from '../../shared/service/db/table.interface';
import { map } from 'rxjs/operators';

@Injectable()
export class SelectedService {

  ownLists: TableInterface<string[]>;

  constructor(
    private db: DBService
  ) {
    this.ownLists = this.db.table('ownLists');
  }

  getList(listName): Observable<string[]> {
    return this.ownLists.get(listName);
  }

  getLists(): Observable<string[]> {
    return this.ownLists.keys().pipe(
      map(lists => lists.includes('default') ? lists : ['default', ...lists])
    );
  }

  addOrUpdate(listName: string, data: string[]) {
    return this.ownLists.put(data, listName);
  }

  delete(listName: string) {
    return this.ownLists.remove(listName);
  }

}
