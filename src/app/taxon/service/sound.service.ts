import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ExtendedTaxon, ExtendedTaxonRecording } from '../../models/taxon';
import { of } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { TaxonService } from '../../shared/service/taxon.service';

@Injectable({
  providedIn: 'root'
})
export class SoundService {

  private cache = {};

  constructor(
    private http: HttpClient,
    private taxonService: TaxonService
  ) { }

  getRecordings(taxon: ExtendedTaxon): Observable<ExtendedTaxonRecording[]> {
    if (typeof this.cache[taxon.id] !== 'undefined') {
      return of(this.cache[taxon.id]);
    }
    return this.http.get<any>('https://proxy.riihikoski.net/recordings', {params: {query: taxon.scientificName + ' q:A area:europe since:2012-01-01'}}).pipe(
      map(result => result && result.recordings && result.recordings || null),
      /*tap(recording => {
        taxon._recording = recording;
        // this.taxonService.put(taxon, false).subscribe();
      }),*/
      catchError(() => of(null)),
      tap(recordings => this.cache[taxon.id] = recordings)
    );
  }

}
