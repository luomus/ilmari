import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecognizeService {

  constructor(private http: HttpClient) { }

  tryToRecognizeImage(img: string) {
    return this.http
      .post(this.getBase('recognize'), {image: img});
  }

  markSelectedImage(id: string, selected: string) {
    return this.http
      .patch(this.getBase('recognize/' + id), {selected: selected});
  }

  private getBase(endpoint) {
    return environment.recognizeApi + endpoint;
  }
}
