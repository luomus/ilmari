export interface FilterTaxa {
  name?: string;
  minOccurrences?: number;
  belongsTo?: string;
  informalTaxonGroups?: string;
  invasive?: boolean;
  finnish?: boolean;
  taxonRank?: string;
  hasMedia?: boolean;
  hasDescription?: boolean;
}
