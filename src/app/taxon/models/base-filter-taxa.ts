export interface BaseFilterTaxa {
  belongsTo?: string[];
  name?: string;
  pageSize: number;
  page: number;
}
