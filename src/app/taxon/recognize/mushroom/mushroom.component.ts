import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GetImageComponent } from '../../../shared/component/get-image/get-image.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'ilm-mushroom',
  templateUrl: './mushroom.component.html',
  styleUrls: ['./mushroom.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MushroomComponent implements OnInit {

  @Input() matches: string[] = [];
  @Input() loading = false;
  @Input() notMushroom = true;
  @Input() image: string;

  @Output() clear = new EventEmitter<void>();
  @Output() imageChange = new EventEmitter<string>();
  @Output() addAs = new EventEmitter<string>();

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  empty(event: MouseEvent) {
    event.stopPropagation();
    this.clear.emit();
  }

  add(event: MouseEvent, match) {
    event.stopPropagation();
    this.addAs.emit(match);
  }

  getPic() {
    const addRef = this.dialog.open(GetImageComponent, {
      height: '95vh',
      width: '95vw',
      maxWidth: '95vh'
    });
    addRef.beforeClose().subscribe((img) => {
        if (img) {
          this.imageChange.emit(img);
        }
      });
  }

}
