import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromTaxon from '../../store';
import { Observable } from 'rxjs/index';
import * as fromRecognizeActions from '../../store/actions/recognize-actions';

@Component({
  selector: 'ilm-recognize-container',
  templateUrl: './recognize-container.component.html',
  styleUrls: ['./recognize-container.component.scss']
})
export class RecognizeContainerComponent implements OnInit {

  image$: Observable<string>;
  matches$: Observable<string[]>;
  isLoading$: Observable<boolean>;
  notMushroom$: Observable<boolean>;

  constructor(
    private store: Store<fromTaxon.State>,
  ) { }

  ngOnInit() {
    this.image$ = this.store.select(fromTaxon.getRecognizeImage);
    this.matches$ = this.store.select(fromTaxon.getRecognizeMatches);
    this.isLoading$ = this.store.select(fromTaxon.isRecognizeLoading);
    this.notMushroom$ = this.store.select(fromTaxon.isRecognizeNotMushroom);
    this.store.dispatch(new fromRecognizeActions.RecognizeListActiveActions(true));
  }

  recognize(image) {
    this.store.dispatch(new fromRecognizeActions.RecognizeActions(image));
  }

  add(id) {
    this.store.dispatch(new fromRecognizeActions.AddActions(id));
  }

  clear() {
    this.store.dispatch(new fromRecognizeActions.ClearActions());
  }

}
