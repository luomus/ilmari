import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaxonContainerComponent } from './view/taxon-container/taxon-container.component';
import { ListContainerComponent } from './list/list-container/list-container.component';
import { ListHeaderContainerComponent } from './list/list-header-container/list-header-container.component';
import { RecognizeContainerComponent } from './recognize/recognize-container/recognize-container.component';
import { TaxonViewHeaderComponent } from './view/taxon-view-header/taxon-view-header.component';

const routes: Routes = [
  {path: 'recognize', component: RecognizeContainerComponent, pathMatch: 'full'},
  {path: '', pathMatch: 'full', children: [
      {path: '', component: ListContainerComponent, pathMatch: 'full'},
      {path: '', component: ListHeaderContainerComponent, pathMatch: 'full', outlet: 'header'},
  ]},
  {path: ':id', children: [
    {path: '', component: TaxonContainerComponent, pathMatch: 'full'},
    {path: '', component: TaxonViewHeaderComponent, pathMatch: 'full', outlet: 'header'},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxonRoutingModule { }
