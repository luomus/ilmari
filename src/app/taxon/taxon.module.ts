import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaxonRoutingModule } from './taxon-routing.module';
import { TaxonContainerComponent } from './view/taxon-container/taxon-container.component';
import { SharedModule } from '../shared/shared.module';
import { metaReducers, reducers } from './store';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TaxonEffects } from './store/effects/taxon-effects';
import { TaxonViewComponent } from './view/taxon-view/taxon-view.component';
import { ListContainerComponent } from './list/list-container/list-container.component';
import { ListViewComponent } from './list/list-view/list-view.component';
import { ListItemComponent } from './list/list-item/list-item.component';
import { MatPaginatorModule } from '@angular/material';
import { TaxonViewImageComponent } from './view/taxon-view-image/taxon-view-image.component';
import { TaxonViewDetailsComponent } from './view/taxon-view-details/taxon-view-details.component';
import { TaxonMenuComponent } from './view/taxon-menu/taxon-menu.component';
import { SelectedService } from './service/selected.service';
import { NguCarouselModule } from '@ngu/carousel';
import { TaxonViewDescriptionsComponent } from './view/taxon-view-descriptions/taxon-view-descriptions.component';
import { TaxonViewChartComponent } from './view/taxon-view-chart/taxon-view-chart.component';
import { TaxonViewParentsComponent } from './view/taxon-view-parents/taxon-view-parents.component';
import { TaxonViewMapComponent } from './view/taxon-view-map/taxon-view-map.component';
import { TaxonViewMapContainerComponent } from './view/taxon-view-map-container/taxon-view-map-container.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { ListEffects } from './store/effects/list-effects';
import { ListMenuComponent } from './list/list-menu/list-menu.component';
import { NewOwnListComponent } from './new-own-list/new-own-list.component';
import { ListDownloadComponent } from './list/list-download/list-download.component';
import { ListSearchComponent } from './list/list-search/list-search.component';
import { ListHeaderContainerComponent } from './list/list-header-container/list-header-container.component';
import { RecognizeContainerComponent } from './recognize/recognize-container/recognize-container.component';
import { RecognizeEffects } from './store/effects/recognize-effects';
import { MushroomComponent } from './recognize/mushroom/mushroom.component';
import { TaxonViewHeaderComponent } from './view/taxon-view-header/taxon-view-header.component';
import { TaxonViewSoundComponent } from './view/taxon-view-sound/taxon-view-sound.component';
import { PlayerComponent } from './view/taxon-view-sound/player/player.component';
import { RecordingTypeComponent } from './view/taxon-view-sound/recording-type/recording-type.component';
import { ScrollingModule } from '@angular/cdk-experimental/scrolling';

@NgModule({
  imports: [
    CommonModule,
    ScrollingModule,
    MatPaginatorModule,
    OverlayModule,
    TaxonRoutingModule,
    NgxChartsModule,
    SharedModule,
    NguCarouselModule,
    StoreModule.forFeature('taxon', reducers as any, {metaReducers}),
    EffectsModule.forFeature([TaxonEffects, ListEffects, RecognizeEffects])
  ],
  declarations: [
    TaxonContainerComponent, TaxonViewComponent, ListContainerComponent, ListViewComponent,
    ListItemComponent,
    TaxonViewImageComponent, TaxonViewDetailsComponent,
    TaxonMenuComponent,
    TaxonViewDescriptionsComponent,
    TaxonViewChartComponent,
    TaxonViewParentsComponent,
    TaxonViewMapComponent,
    TaxonViewMapContainerComponent,
    ListMenuComponent,
    NewOwnListComponent,
    ListDownloadComponent,
    ListSearchComponent,
    ListHeaderContainerComponent,
    RecognizeContainerComponent,
    MushroomComponent,
    TaxonViewHeaderComponent,
    TaxonViewSoundComponent,
    PlayerComponent,
    RecordingTypeComponent
   ],
  bootstrap: [
    NewOwnListComponent
  ],
  providers: [SelectedService]
})
export class TaxonModule { }
