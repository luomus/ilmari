import { ExtendedTaxon } from '../../../models/taxon';
import * as taxonAction from '../actions/taxon-actions';

export enum ListType {
  list = 'list',
  species = 'species',
  bookmarked = 'bookmarked'
}

export interface State {
  taxon: ExtendedTaxon;
  parentMenuVisible: boolean;
}

export const initialState: State = {
  taxon: null,
  parentMenuVisible: false
};

export function reducer(
  state = initialState,
  action: taxonAction.Actions
): State {
  switch (action.type) {
    case taxonAction.TOGGLE_PARENT_MENU:
      return {
        ...state,
        parentMenuVisible: !state.parentMenuVisible
      };
    case taxonAction.UPDATE_SELECTED_IMAGE:
      return {
        ...state,
        taxon: {
          ...state.taxon,
          _selectedMedia: action.payload
        }
      };
    case taxonAction.LOADED_ONE: {
      const taxon = <ExtendedTaxon> action.payload;

      if (state.taxon && taxon.id === state.taxon.id) {
        return state;
      }

      return {
        ...state,
        taxon: taxon
      };
    }
    default: {
      return state;
    }
  }
}

export const getTaxon = (state: State) => state.taxon;
export const isParentMenuVisible = (state: State) => state.parentMenuVisible;
