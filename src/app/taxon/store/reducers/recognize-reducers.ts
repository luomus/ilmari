import * as recognizeAction from '../actions/recognize-actions';

export interface State {
  image: string;
  imageID: string;
  matches: string[];
  notMushroom: boolean;
  loading: boolean;
  listActive: boolean;
}

export const initialState: State = {
  image: '',
  imageID: '',
  matches: [],
  notMushroom: false,
  loading: false,
  listActive: false
};

export function reducer(
  state = initialState,
  action: recognizeAction.Actions
): State {
  switch (action.type) {
    case recognizeAction.RECOGNIZE_LIST_ACTIVE:
      return {
        ...state,
        listActive: action.payload
      };
    case recognizeAction.IS_MUSHROOM:
      return {
        ...state,
        notMushroom: !action.payload,
        loading: false
      };
    case recognizeAction.RECOGNIZE_SUCCESFULLY:
      return {
        ...state,
        matches: action.payload.matches.slice(0, 5),
        imageID: action.payload.id,
        loading: false,
        notMushroom: false
      };
    case recognizeAction.RECOGNIZE:
      return {
        ...state,
        image: action.payload,
        loading: true
      };
    case recognizeAction.CLEAR:
      return {
        ...state,
        image: '',
        matches: []
      };
    default: {
      return state;
    }
  }
}

export const getImage = (state: State) => state.image;
export const getImageID = (state: State) => state.imageID;
export const isLoading = (state: State) => state.loading;
export const isListActive = (state: State) => state.listActive;
export const notMushroom = (state: State) => state.notMushroom;
export const getMatches = (state: State) => state.matches;
