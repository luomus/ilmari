import { TaxaResult } from '../../../models/taxon';
import * as listAction from '../actions/list-actions';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { DownloadTaxa } from '../../models/download-taxa';
import { environment } from '../../../../environments/environment';

export enum ListType {
  list = 'list',
  species = 'species',
  bookmarked = 'bookmarked'
}

export interface State {
  active: ListType;
  activeOwnList: string;
  ownLists: string[];
  results: {
    [list: string]: TaxaResult
  };
  filter: {
    [list: string]: BaseFilterTaxa
  };
  download: DownloadTaxa;
}

export const initialState: State = {
  download: {},
  active: ListType.list,
  activeOwnList: 'default',
  ownLists: ['default'],
  filter: {
    list: {
      page: 1,
      pageSize: 24,
      belongsTo: [environment.defaults.informalTaxonGroup]
    },
    species: {
      page: 1,
      pageSize: 24,
      belongsTo: []
    },
    bookmarked: {
      page: 1,
      pageSize: 24,
      belongsTo: []
    }
  },
  results: {
    list: {
      count: 0,
      taxa: []
    },
    species: {
      count: 0,
      taxa: []
    },
    bookmarked: {
      count: 0,
      taxa: []
    }
  }
};

export function reducer(
  state = initialState,
  action: listAction.Actions
): State {
  switch (action.type) {
    case listAction.ACTIVATE_OWN_LIST:
      return {
        ...state,
        activeOwnList: action.payload
      };
    case listAction.ADD_TO_ACTIVE_OWN_LIST:
      if (state.filter.bookmarked.belongsTo.indexOf(action.payload) !== -1) {
        return state;
      }
      return {
        ...state,
        filter: {
          ...state.filter,
          bookmarked: {
            ...state.filter.bookmarked,
            belongsTo: [...state.filter.bookmarked.belongsTo, action.payload]
          }
        }
      };
    case listAction.DEL_FROM_ACTIVE_OWN_LIST:
      if (state.filter.bookmarked.belongsTo.indexOf(action.payload) === -1) {
        return state;
      }
      return {
        ...state,
        filter: {
          ...state.filter,
          bookmarked: {
            ...state.filter.bookmarked,
            belongsTo: state.filter.bookmarked.belongsTo.filter(val => val !== action.payload)
          }
        }
      };
    case listAction.ADD_NEW_OWN_LIST:
      if (state.ownLists.indexOf(action.payload) !== -1) {
        return state;
      }
      return {
        ...state,
        ownLists: [...state.ownLists, action.payload]
      };
    case listAction.DELETE_OWN_LIST:
      if (state.ownLists.indexOf(action.payload) === -1) {
        return state;
      }
      return {
        ...state,
        activeOwnList: 'default',
        ownLists: state.ownLists.filter((list) => list !== action.payload)
      };
    case listAction.LOAD:
      return {
        ...state,
        filter: {
          ...state.filter,
          [action.payload.list]: {
            ...action.payload.filter
          }
        }
      };
    case listAction.LOADED:
      return {
        ...state,
        results: {
          ...state.results,
          [action.payload.list]: action.payload.result
        }
      };
    case listAction.SET_ACTIVE_LIST:
      return {
        ...state,
        active: action.payload
      };
    case listAction.SET_BELONG_TO_FILTER:
      return {
        ...state,
        filter: {
          ...state.filter,
          [action.payload.list]: {
            ...state.filter[action.payload.list],
            page: 1,
            belongsTo: [...action.payload.belongTo]
          }
        }
      };
    case listAction.DOWNLOAD_START:
      return {
        ...state,
        download: {
          ...state.download,
          [action.payload]: 0
        }
      };
    case listAction.DOWNLOAD_PROCESS:
      return {
        ...state,
        download: {
          ...state.download,
          ...action.payload
        }
      };
    case listAction.DOWNLOAD_END:
      return {
        ...state,
        download: {
          ...state.download,
          [action.payload]: 100
        }
      };
    default: {
      return state;
    }
  }
}

export const getFilters = (state: State) => state.filter;
export const getResults = (state: State) => state.results;
export const getDownload = (state: State) => state.download;
export const getActive = (state: State) => state.active;
export const getOwnLists = (state: State) => state.ownLists;
export const getActiveOwnList = (state: State) => state.activeOwnList;
