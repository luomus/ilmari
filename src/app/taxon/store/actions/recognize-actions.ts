import { Action } from '@ngrx/store';

export const RECOGNIZE = '[Recognize] recognize';
export const RECOGNIZE_SUCCESFULLY = '[Recognize] recognize successfully';
export const IS_MUSHROOM = '[Recognize] not mushroom';
export const CLEAR = '[Recognize] clear';
export const ADD = '[Recognize] add';
export const RECOGNIZE_LIST_ACTIVE = '[Recognize] recognize list active';

export class RecognizeActions implements Action {
  readonly type = RECOGNIZE;

  constructor(public payload: string) {}
}

export class RecognizeSuccesfullyActions implements Action {
  readonly type = RECOGNIZE_SUCCESFULLY;

  constructor(public payload: any) {}
}

export class IsMushroomActions implements Action {
  readonly type = IS_MUSHROOM;

  constructor(public payload: boolean) {}
}

export class RecognizeListActiveActions implements Action {
  readonly type = RECOGNIZE_LIST_ACTIVE;

  constructor(public payload: boolean) {}
}

export class ClearActions implements Action {
  readonly type = CLEAR;
}

export class AddActions implements Action {
  readonly type = ADD;

  constructor(public payload: string) {}
}

export type Actions
  = RecognizeActions
  | RecognizeSuccesfullyActions
  | IsMushroomActions
  | ClearActions
  | AddActions
  | RecognizeListActiveActions
;
