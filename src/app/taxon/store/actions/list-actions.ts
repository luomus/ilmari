import { Action } from '@ngrx/store';
import { TaxaResult } from '../../../models/taxon';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { DownloadTaxa } from '../../models/download-taxa';
import { ListType } from '../reducers/taxon-reducers';

export const LOAD = '[List] Load';
export const LOADED = '[List] Loaded';
export const REFRESH = '[List] Refresh';
export const SET_BELONG_TO_FILTER = '[List] Set belong to filter';
export const SET_ACTIVE_LIST = '[List] Set active list';
export const DOWNLOAD_START = '[List] Download start';
export const DOWNLOAD_END = '[List] Download end';
export const DOWNLOAD_PROCESS = '[List] Download process';
export const ACTIVATE_OWN_LIST = '[List] Activate own list';
export const ADD_NEW_OWN_LIST = '[List] Add new own list';
export const DELETE_OWN_LIST = '[List] Delete own list';
export const ADD_TO_ACTIVE_OWN_LIST = '[List] Add to active own list';
export const DEL_FROM_ACTIVE_OWN_LIST = '[List] Del from active own list';


export class ActiveOwnListAction implements Action {
  readonly type = ACTIVATE_OWN_LIST;

  constructor(public payload: string) {}
}

export class RefreshListAction implements Action {
  readonly type = REFRESH;
}

export class AddNewOwnListAction implements Action {
  readonly type = ADD_NEW_OWN_LIST;

  constructor(public payload: string) {}
}

export class DeleteOwnListAction implements Action {
  readonly type = DELETE_OWN_LIST;

  constructor(public payload: string) {}
}

export class AddToOwnListAction implements Action {
  readonly type = ADD_TO_ACTIVE_OWN_LIST;

  constructor(public payload: string) {}
}

export class DelFromOwnListAction implements Action {
  readonly type = DEL_FROM_ACTIVE_OWN_LIST;

  constructor(public payload: string) {}
}

export class SetActiveListAction implements Action {
  readonly type = SET_ACTIVE_LIST;

  constructor(public payload: ListType) {}
}

export class SetBelongToFilterAction implements Action {
  readonly type = SET_BELONG_TO_FILTER;

  constructor(public payload: {list: ListType, belongTo: string[]}) {}
}

export class DownloadStartAction implements Action {
  readonly type = DOWNLOAD_START;

  constructor(public payload: string) {}
}

export class DownloadEndAction implements Action {
  readonly type = DOWNLOAD_END;

  constructor(public payload: string) {}
}

export class DownloadProcessAction implements Action {
  readonly type = DOWNLOAD_PROCESS;

  constructor(public payload: DownloadTaxa) {}
}

export class LoadAction implements Action {
  readonly type = LOAD;

  constructor(public payload: {list: ListType, filter: BaseFilterTaxa}) {}
}
export class LoadedAction implements Action {
  readonly type = LOADED;

  constructor(public payload: {list: ListType, result: TaxaResult}) {}
}

export type Actions
  = SetActiveListAction
  | RefreshListAction
  | SetBelongToFilterAction
  | ActiveOwnListAction
  | AddNewOwnListAction
  | DeleteOwnListAction
  | AddToOwnListAction
  | DelFromOwnListAction
  | DownloadStartAction
  | DownloadProcessAction
  | DownloadEndAction
  | LoadAction
  | LoadedAction
;
