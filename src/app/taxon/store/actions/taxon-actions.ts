import { Action } from '@ngrx/store';
import { ExtendedTaxon, TaxaResult } from '../../../models/taxon';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { DownloadTaxa } from '../../models/download-taxa';
import { ListType } from '../reducers/taxon-reducers';

export const SEARCH = '[Taxon] Search';
export const SEARCH_COMPLETE = '[Taxon] Search Complete';
export const LOAD_ONE = '[Taxon] Load one';
export const LOADED_ONE = '[Taxon] Loaded one';
export const LOAD_LIST = '[Taxon] Load list';
export const LOADED_LIST = '[Taxon] Loaded list';
export const DOWNLOAD_START = '[Taxon] Download start';
export const DOWNLOAD_END = '[Taxon] Download end';
export const DOWNLOAD_PROCESS = '[Taxon] Download process';
export const UPDATE_SELECTED_IMAGE = '[Taxon] Update selected image';
export const UPDATE_TAXON = '[Taxon] Update taxon';
export const SET_BELONG_TO_FILTER = '[Taxon] Set belong to filter';
export const MOVE_TO_NEXT = '[Taxon] Move to next';
export const MOVE_TO_BEFORE = '[Taxon] Move to before';
export const TOGGLE_PARENT_MENU = '[Taxon] Toggle parent menu';

export class ToggleParentMenuAction implements Action {
  readonly type = TOGGLE_PARENT_MENU;
}

export class MoveToNextAction implements Action {
  readonly type = MOVE_TO_NEXT;

  constructor(public payload: string) {}
}

export class MoveToBeforeAction implements Action {
  readonly type = MOVE_TO_BEFORE;

  constructor(public payload: string) {}
}

export class SetBelongToFilterAction implements Action {
  readonly type = SET_BELONG_TO_FILTER;

  constructor(public payload: {list: ListType, belongTo: string[]}) {}
}

export class SearchAction implements Action {
  readonly type = SEARCH;

  constructor(public payload: string) {}
}

export class SearchCompleteAction implements Action {
  readonly type = SEARCH_COMPLETE;

  constructor(public payload: any[]) {}
}

export class UpdateSelectedImageAction implements Action {
  readonly type = UPDATE_SELECTED_IMAGE;

  constructor(public payload: string) {}
}

export class UpdateTaxonAction implements Action {
  readonly type = UPDATE_TAXON;

  constructor(public payload: ExtendedTaxon) {}
}

export class DownloadStartAction implements Action {
  readonly type = DOWNLOAD_START;

  constructor(public payload: string) {}
}

export class DownloadEndAction implements Action {
  readonly type = DOWNLOAD_END;

  constructor(public payload: string) {}
}

export class DownloadProcessAction implements Action {
  readonly type = DOWNLOAD_PROCESS;

  constructor(public payload: DownloadTaxa) {}
}

export class LoadListAction implements Action {
  readonly type = LOAD_LIST;

  constructor(public payload: BaseFilterTaxa) {}
}
export class LoadedListAction implements Action {
  readonly type = LOADED_LIST;

  constructor(public payload: TaxaResult) {}
}

export class LoadOneAction implements Action {
  readonly type = LOAD_ONE;

  constructor(public payload: string) {}
}

export class LoadedOneAction implements Action {
  readonly type = LOADED_ONE;

  constructor(public payload: ExtendedTaxon) {}
}

export type Actions
  = ToggleParentMenuAction
  | MoveToBeforeAction
  | MoveToNextAction
  | SetBelongToFilterAction
  | SearchAction
  | SearchCompleteAction
  | UpdateTaxonAction
  | UpdateSelectedImageAction
  | DownloadStartAction
  | DownloadProcessAction
  | DownloadEndAction
  | LoadOneAction
  | LoadedOneAction
  | LoadListAction
  | LoadedListAction;
