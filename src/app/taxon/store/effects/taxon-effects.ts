import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { TaxonService } from '../../../shared/service/taxon.service';
import * as taxon from '../actions/taxon-actions';
import { LoadOneAction, MoveToNextAction, UpdateSelectedImageAction, UpdateTaxonAction } from '../actions/taxon-actions';
import * as list from '../actions/list-actions';
import * as layout from '../../../core/store/actions/layout-actions';
import { ExtendedTaxon, TaxaResult } from '../../../models/taxon';
import { InformalTaxonService } from '../../../shared/service/informal-taxon.service';
import * as fromTaxon from '../';
import { SettingService } from '../../../shared/service/setting.service';
import { Router } from '@angular/router';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { EMPTY, Observable, of } from 'rxjs';
import { concatMap, distinctUntilChanged, filter, map, take, tap } from 'rxjs/operators';

@Injectable()
export class TaxonEffects {
  @Effect()
  taxon$: Observable<Action> = this.actions$
    .ofType<LoadOneAction>(taxon.LOAD_ONE).pipe(
      map(action => action.payload),
      distinctUntilChanged(),
      tap(() => this.store.dispatch(new layout.IsLoadingAction(true))),
      concatMap(taxonID => this.taxonService.get(taxonID).pipe(
        tap(() => this.store.dispatch(new layout.IsLoadingAction(false))),
        map((data: ExtendedTaxon) => new taxon.LoadedOneAction(data)))
      )
    );

  @Effect()
  updateSelectedImage$: Observable<Action> = this.actions$
    .ofType<UpdateSelectedImageAction>(taxon.UPDATE_SELECTED_IMAGE).pipe(
      concatMap((action) => this.store.select(fromTaxon.getTaxon).pipe(
        take(1),
        map(value => ({...value, _selectedMedia: action.payload})))
      ),
      concatMap((value: ExtendedTaxon) => of(new taxon.UpdateTaxonAction(value)))
    );

  @Effect()
  updateTaxon$: Observable<Action> = this.actions$
    .ofType<UpdateTaxonAction>(taxon.UPDATE_TAXON).pipe(
      map(action => action.payload),
      tap((value: ExtendedTaxon) => this.taxonService.put(value).subscribe()),
      concatMap(() => EMPTY)
    );

  @Effect({dispatch: false})
  moveToNext$: Observable<Action> = this.actions$
    .ofType<MoveToNextAction>(taxon.MOVE_TO_NEXT, taxon.MOVE_TO_BEFORE).pipe(
      concatMap(action => this.store.select(fromTaxon.isRecognizeListActive).pipe(take(1), map(isRecList => isRecList ? null : action))),
      filter(result => !!result),
      concatMap((action) => this.store.select(fromTaxon.getListActive).pipe(
        take(1),
        map(activeList => ({
          id: action.payload,
          list: activeList,
          delta: action.type === taxon.MOVE_TO_NEXT ? 1 : -1
        }))
      )),
      concatMap(data => {
        return this.store.select(fromTaxon.getListResults).pipe(
          take(1),
          map(results => results[data.list]),
          concatMap((results: TaxaResult) => {
            const taxaLen = results.taxa.length;
            const idx = results.taxa.findIndex(item => item.id === data.id);
            let target = idx + data.delta;
            let switchPage = false;

            if (target >= taxaLen) {
              target = 0;
              switchPage = true;
            } else if (target < 0) {
              target = - 1;
              switchPage = true;
            }
            if (!switchPage) {
              this.router.navigate(['/species', results.taxa[target].id]);
              return EMPTY;
            }
            return this.store.select(fromTaxon.getListFilters).pipe(
              take(1),
              map(filters => filters[data.list]),
              concatMap((filter: BaseFilterTaxa) => {
                let moveToPage = filter.page + data.delta;
                const pageIdx = (moveToPage - 1) * filter.pageSize;
                if (target === -1) {
                  target = filter.pageSize - 1;
                }
                if (pageIdx > results.count) {
                  moveToPage = 1;
                } else if (pageIdx < 0) {
                  moveToPage = Math.ceil(results.count / filter.pageSize);
                  target = results.count % filter.pageSize - 1;
                }
                return of({...filter, page: moveToPage}).pipe(
                  tap((filterTaxa: BaseFilterTaxa) => this.store.dispatch(new list.LoadAction({list: data.list, filter: filterTaxa}))),
                  concatMap(filters => this.taxonService.getAll(filters)),
                  tap((taxonResult: TaxaResult) => this.store.dispatch(new list.LoadedAction({list: data.list, result: taxonResult}))),
                  tap((taxaResults: TaxaResult) => {
                    if (taxaResults.taxa[target]) {
                      this.router.navigate(['/species', taxaResults.taxa[target].id]);
                    }
                  })
                );
              })
            );
          })
        );
      }),
      concatMap(() => EMPTY)
    );

  constructor(
    private actions$: Actions,
    private taxonService: TaxonService,
    private settingService: SettingService,
    private informTaxonService: InformalTaxonService,
    private store: Store<fromTaxon.State>,
    private router: Router
  ) {}
}
