import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import * as list from '../actions/list-actions';
import * as recognize from '../actions/recognize-actions';
import * as trip from '../../../document/trip/store/actions/trip-actions';
import { EMPTY, forkJoin, Observable, of } from 'rxjs';
import { catchError, concatMap, map, take, tap } from 'rxjs/operators';
import { RecognizeService } from '../../service/recognize.service';
import * as fromTaxon from '../index';

@Injectable()
export class RecognizeEffects {

  @Effect()
  recognizeImage$: Observable<Action> = this.actions$
    .ofType<list.AddNewOwnListAction>(recognize.RECOGNIZE).pipe(
      map((action) => action.payload),
      concatMap(image => this.recognizeService.tryToRecognizeImage(image).pipe(
        catchError(() => of(false))
      )),
      concatMap(results => of(results ? new recognize.RecognizeSuccesfullyActions(results) : new recognize.IsMushroomActions(false)))
    );

  @Effect()
  addSelection: Observable<Action> = this.actions$
    .ofType<list.AddNewOwnListAction>(recognize.ADD).pipe(
      map((action) => action.payload),
      concatMap(selected => forkJoin(
        this.store.select(fromTaxon.getRecognizeImageID).pipe(take(1)),
        this.store.select(fromTaxon.getRecognizeImage).pipe(take(1))
      ).pipe(
        map(data => ({imageID: data[0], image: data[1], selected: selected}))
      )),
      tap(data => {
        this.recognizeService.markSelectedImage(data.imageID, data.selected).pipe(
          catchError(() => EMPTY)
        ).subscribe();
      }),
      concatMap(results => of(new trip.AddObservationAction({image: results.image, taxonID: results.selected})))
    );

  constructor(
    private actions$: Actions,
    private recognizeService: RecognizeService,
    private store: Store<fromTaxon.State>
  ) {}
}
