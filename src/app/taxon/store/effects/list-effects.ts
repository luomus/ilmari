import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { TaxonService } from '../../../shared/service/taxon.service';
import * as list from '../actions/list-actions';
import { SetActiveListAction } from '../actions/list-actions';
import * as layout from '../../../core/store/actions/layout-actions';
import * as taxon from '../actions/taxon-actions';
import * as fromTaxon from '../';
import { InformalTaxonService } from '../../../shared/service/informal-taxon.service';
import { Settings, SettingService } from '../../../shared/service/setting.service';
import { TaxaResult } from '../../../models/taxon';
import { ListType } from '../reducers/list-reducers';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { SelectedService } from '../../service/selected.service';
import { defer, EMPTY, forkJoin, Observable, of } from 'rxjs';
import { concatMap, map, mergeMap, take, tap } from 'rxjs/operators';

@Injectable()
export class ListEffects {

  @Effect()
  onAddNewOwnList$: Observable<Action> = this.actions$
    .ofType<list.AddNewOwnListAction>(list.ADD_NEW_OWN_LIST).pipe(
      map((action) => action.payload),
      tap(listName => this.selectedService.addOrUpdate(listName, []).subscribe()),
      concatMap(() => EMPTY)
    );

  @Effect()
  onAddToActiveOwnList$: Observable<Action> = this.actions$
    .ofType<list.AddToOwnListAction>(list.ADD_TO_ACTIVE_OWN_LIST).pipe(
      concatMap(() => forkJoin(
        this.store.select(fromTaxon.getListFilters).pipe(take(1), map(filters => filters[ListType.bookmarked])),
        this.store.select(fromTaxon.getListActiveOwnList).pipe(take(1))
      ).pipe(map(data => ({filter: data[0], listName: data[1]})))),
      tap((result) => this.selectedService.addOrUpdate(result.listName, result.filter.belongsTo).subscribe()),
      concatMap(() => EMPTY)
    );

  @Effect()
  onDeleteOwnList$: Observable<Action> = this.actions$
    .ofType<list.DeleteOwnListAction>(list.DELETE_OWN_LIST).pipe(
      map((action) => action.payload),
      tap(listName => this.selectedService.delete(listName).subscribe()),
      concatMap(() => EMPTY)
    );

  @Effect()
  onTaxonUpdate$: Observable<Action> = this.actions$
    .ofType(taxon.UPDATE_TAXON, list.REFRESH, list.DEL_FROM_ACTIVE_OWN_LIST).pipe(
      concatMap(() => this.store.select(fromTaxon.getListActive).pipe(take(1))),
      tap(() => this.taxonService.invalidateCache()),
      map((listName) => new list.SetActiveListAction(listName))
    );

  @Effect()
  setActiveList$: Observable<Action> = this.actions$
    .ofType<SetActiveListAction>(list.SET_ACTIVE_LIST).pipe(
      map(action => action.payload),
      concatMap((listName) => this.store.select(fromTaxon.getListFilters).pipe(
          take(1),
          map(filters => filters[listName]),
          concatMap((filters) => this.taxonService.getAll(filters).pipe(
            map((taxaResult: TaxaResult) => new list.LoadedAction({list: listName, result: taxaResult}))
          ))
        )
      )
    );

  @Effect()
  activate$: Observable<Action> = this.actions$
    .ofType<list.ActiveOwnListAction>(list.ACTIVATE_OWN_LIST).pipe(
      map((action) => action.payload),
      concatMap((listName) => this.selectedService.getList(listName)),
      concatMap((belongsTo) => this.store.select(fromTaxon.getListFilters).pipe(
          take(1),
          map(filters => filters[ListType.bookmarked]),
          map(filters => ({...filters, belongsTo: [...(belongsTo || [])]}))
        )
      ),
      tap((filters) => this.store.dispatch(new list.LoadAction({list: ListType.bookmarked, filter: filters}))),
      map(() => new list.SetActiveListAction(ListType.bookmarked))
    );

  @Effect()
  load$: Observable<Action> = this.actions$
    .ofType<list.LoadAction>(list.LOAD).pipe(
      tap(() => this.store.dispatch(new layout.IsLoadingAction(true))),
      concatMap((action) => this.taxonService.getAll(action.payload.filter).pipe(
        map((taxaResult: TaxaResult) => new list.LoadedAction({list: action.payload.list, result: taxaResult})))
      ),
      tap(() => this.store.dispatch(new layout.IsLoadingAction(false)))
    );

  @Effect()
  updateActiveList$: Observable<Action> = this.actions$
    .ofType<list.SetBelongToFilterAction>(list.SET_BELONG_TO_FILTER).pipe(
      concatMap((action) => this.store.select(fromTaxon.getListFilters).pipe(
        take(1),
        map(filters => filters[action.payload.list]),
        map((filter: BaseFilterTaxa) => new list.LoadAction({list: action.payload.list, filter: filter}))
      ))
    );

  @Effect()
  downloadStart$: Observable<Action> = this.actions$
    .ofType<list.DownloadStartAction>(list.DOWNLOAD_START).pipe(
      map(action => action.payload),
      concatMap(id => this.settingService.get(Settings.downloaded).pipe(
        map(downloaded => ({id, downloaded}))
      )),
      mergeMap(data => {
        if (data.downloaded && data.downloaded[data.id]) {
          return of(new list.DownloadEndAction(data.id));
        }
        let fetch$ = (page) => of(null);
        if (data.id.indexOf('MVL.') === 0) {
          fetch$ = (page) => this.taxonService.downloadByInformalGroup(data.id, page).pipe(
            tap(lastPage => page < lastPage && lastPage > 0 ? this.store.dispatch(new list.DownloadProcessAction({[data.id]: Math.floor((page / lastPage) * 100)})) : undefined),
            tap(() => this.store.dispatch(new list.RefreshListAction())),
            concatMap(lastPage => page < lastPage ? fetch$(++page) : of(null))
          );
        }
        return fetch$(1).pipe(
          concatMap(() => this.settingService.get(Settings.downloaded)),
          tap(downloaded => downloaded && downloaded[data.id] ? undefined : this.settingService.set(Settings.downloaded, {...downloaded, [data.id]: true}).subscribe()),
          concatMap(() => of(new list.DownloadEndAction(data.id)))
        );
      })
    );

  @Effect()
  init$: Observable<Action> = defer(() => {
    return of(null).pipe(
      concatMap(() => this.store.select(fromTaxon.getListFilters).pipe(take(1))),
      tap((filters) => Object.keys(filters).forEach(key => this.store.dispatch(new list.LoadAction({list: key as ListType, filter: filters[key]})))),
      concatMap(() => this.settingService.get(Settings.downloaded)),
      tap((downloaded) => {
        const result = downloaded ? Object.keys(downloaded)
          .reduce((cumulative, current) => ({...cumulative, [current]: 100}), {}) : {};
        this.store.dispatch(new list.DownloadProcessAction(result));
      }),
      concatMap(() => EMPTY)
    );
  });

  constructor(
    private actions$: Actions,
    private taxonService: TaxonService,
    private settingService: SettingService,
    private selectedService: SelectedService,
    private informTaxonService: InformalTaxonService,
    private store: Store<fromTaxon.State>
  ) {}
}
