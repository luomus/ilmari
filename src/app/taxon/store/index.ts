import { ActionReducer, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import * as fromTaxon from './reducers/taxon-reducers';
import * as fromList from './reducers/list-reducers';
import * as fromRecognize from './reducers/recognize-reducers';
import * as fromRoot from '../../core/store';
import { localStorageSync } from 'ngrx-store-localstorage';

export interface TaxonState {
  taxon: fromTaxon.State;
  list: fromList.State;
  recognize: fromRecognize.State;
}

export interface State extends fromRoot.State {
  'taxon': TaxonState;
}

export const reducers = {
  taxon: fromTaxon.reducer,
  list: fromList.reducer,
  recognize: fromRecognize.reducer
};

export function localStorageSyncReducer(reducer: ActionReducer<State>): ActionReducer<any, any> {
  return localStorageSync({
    keys: [
      {
        list: [
          'active',
          'activeOwnList',
          'ownLists',
          'filter',
          'download'
        ]
      },
      {
        taxon: [
          'parentMenuVisible'
        ]
      },
      {
        recognize: [
          'image',
          'imageID',
          'matches'
        ]
      }],
    rehydrate: true,
    removeOnUndefined: true,
    restoreDates: false
  })(reducer);
}

export const metaReducers: MetaReducer<State>[] = [localStorageSyncReducer];

export const getTaxaState = createFeatureSelector<TaxonState>('taxon');

export const getTaxonState = createSelector(getTaxaState, (state: TaxonState) => state.taxon);
export const getListState = createSelector(getTaxaState, (state: TaxonState) => state.list);
export const getRecognizeState = createSelector(getTaxaState, (state: TaxonState) => state.recognize);

// taxon view
export const getTaxon = createSelector(getTaxonState, fromTaxon.getTaxon);
export const isParentMenuVisible = createSelector(getTaxonState, fromTaxon.isParentMenuVisible);

// List
export const getListActive = createSelector(getListState, fromList.getActive);
export const getListFilters = createSelector(getListState, fromList.getFilters);
export const getListResults = createSelector(getListState, fromList.getResults);
export const getListOwnLists = createSelector(getListState, fromList.getOwnLists);
export const getListActiveOwnList = createSelector(getListState, fromList.getActiveOwnList);
export const getListDownload = createSelector(getListState, fromList.getDownload);

// Recognize
export const getRecognizeImage = createSelector(getRecognizeState, fromRecognize.getImage);
export const getRecognizeImageID = createSelector(getRecognizeState, fromRecognize.getImageID);
export const getRecognizeMatches = createSelector(getRecognizeState, fromRecognize.getMatches);
export const isRecognizeLoading = createSelector(getRecognizeState, fromRecognize.isLoading);
export const isRecognizeNotMushroom = createSelector(getRecognizeState, fromRecognize.notMushroom);
export const isRecognizeListActive = createSelector(getRecognizeState, fromRecognize.isListActive);
