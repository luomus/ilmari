import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromUser from '../../../user/store';
import * as fromTaxon from '../../store';
import * as listActions from '../../store/actions/list-actions';
import { Observable } from 'rxjs';
import { ExtendedTaxon, TaxaResult } from '../../../models/taxon';
import { MatDialog } from '@angular/material';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { ListType } from '../../store/reducers/taxon-reducers';
import { DownloadTaxa } from '../../models/download-taxa';
import { map } from 'rxjs/operators';
import * as fromRecognizeActions from '../../store/actions/recognize-actions';

@Component({
  selector: 'ilm-list-container',
  templateUrl: './list-container.component.html',
  styleUrls: ['./list-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListContainerComponent implements OnInit {
  private static downloading: {[list: string]: boolean} = {};
  activeTaxon$: Observable<ExtendedTaxon>;
  results$: Observable<{[list: string]: TaxaResult}>;
  filter$: Observable<{[list: string]: BaseFilterTaxa}>;
  active$: Observable<ListType>;
  activeOwnList$: Observable<string>;
  downloaded$: Observable<DownloadTaxa>;
  ownLists$: Observable<string[]>;
  useHighQualityImages$: Observable<boolean>;
  listTypes = ListType;
  downloading: {[list: string]: boolean} = {};

  constructor(
    private store: Store<fromTaxon.State>,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.activeTaxon$ = this.store.select(fromTaxon.getTaxon);
    this.active$ = this.store.select(fromTaxon.getListActive);
    this.filter$ = this.store.select(fromTaxon.getListFilters);
    this.results$ = this.store.select(fromTaxon.getListResults);
    this.ownLists$ = this.store.select(fromTaxon.getListOwnLists);
    this.downloaded$ = this.store.select(fromTaxon.getListDownload);
    this.activeOwnList$ = this.store.select(fromTaxon.getListActiveOwnList);
    this.useHighQualityImages$ = this.store.select(fromUser.getUserSettings).pipe(
      map(settings => settings.useHighQualityImageInList)
    );
    this.downloading = ListContainerComponent.downloading;
    this.store.dispatch(new fromRecognizeActions.RecognizeListActiveActions(false));
  }

  onDeleteFromOwn(value: string) {
    this.store.dispatch(new listActions.DelFromOwnListAction(value));
  }

  onDownload(id: string) {
    if (ListContainerComponent.downloading[id]) {
      return;
    }
    ListContainerComponent.downloading[id] = true;
    this.store.dispatch(new listActions.DownloadStartAction(id));
  }

  onInformalGroupSelect(id: string) {
    this.store.dispatch(new listActions.SetBelongToFilterAction({list: ListType.list, belongTo: [id]}));
  }

}
