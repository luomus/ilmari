import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ExtendedTaxon } from '../../../models/taxon';
import { Observable } from 'rxjs';

@Component({
  selector: 'ilm-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListItemComponent implements OnInit, OnChanges {

  @Input() taxon: ExtendedTaxon;
  @Input() scrollObservable: Observable<any>;
  @Input() useHighQualityImages = false;
  thumbnail: string;

  constructor() { }

  ngOnInit() {
    this.thumbnail = '';
    this.updateThumbnail();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.thumbnail = '';
    this.updateThumbnail();
  }

  private updateThumbnail() {
    const multimedia = this.taxon.multimedia || this.taxon._media;
    if (!(this.taxon._selectedMedia && multimedia)) {
      return;
    }
    const img = multimedia.filter(media => media.id === this.taxon._selectedMedia)[0];
    if (!img) {
      return;
    }
    this.thumbnail = this.useHighQualityImages ?
      img.largeURL || img.fullURL || img.thumbnailURL :
      img.thumbnailURL || img.largeURL || img.fullURL;
  }

}
