import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NewOwnListComponent } from '../../new-own-list/new-own-list.component';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { ExtendedInformalTaxonGroup } from '../../../models/informal-taxon-group';

@Component({
  selector: 'ilm-list-menu',
  templateUrl: './list-menu.component.html',
  styleUrls: ['./list-menu.component.scss']
})
export class ListMenuComponent implements OnInit {

  @Input() ownLists: string[];
  @Input() activeOwnList: string;
  @Input() filters: {[list: string]: BaseFilterTaxa};
  @Input() informalGroups: ExtendedInformalTaxonGroup[];

  @Output() newOwnList = new EventEmitter<string>();
  @Output() selectOwnList = new EventEmitter<string>();
  @Output() deleteOwnList = new EventEmitter<string>();
  @Output() activateList = new EventEmitter<string>();
  @Output() groupSelect = new EventEmitter<string>();

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openNewListDialog(): void {
    const dialogRef = this.dialog.open(NewOwnListComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.newOwnList.next(result);
      }
    });
  }

}
