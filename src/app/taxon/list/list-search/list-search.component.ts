import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ListType } from '../../store/reducers/taxon-reducers';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'ilm-list-search',
  templateUrl: './list-search.component.html',
  styleUrls: ['./list-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('search', [
      state('expanded', style({
        width: '*'
      })),
      state('collapsed', style({
        width: '0'
      })),
      transition('expanded => collapsed', animate('200ms ease-in-out')),
      transition('collapsed => expanded', animate('200ms ease-out'))
    ]),
    trigger('clear', [
      state('expanded', style({
        opacity: '1'
      })),
      state('collapsed', style({
        opacity: '0'
      })),
      transition('expanded => collapsed', animate('200ms ease-in-out')),
      transition('collapsed => expanded', animate('200ms ease-out'))
    ])
  ]
})
export class ListSearchComponent implements OnInit, OnChanges, OnDestroy {

  @Input() filters: {[list: string]: BaseFilterTaxa};
  @Input() active: ListType;
  @Output() update = new EventEmitter<{list: ListType, filter: BaseFilterTaxa}>();

  focused = false;
  query: string;
  visible = false;
  querySubject = new Subject();
  queryObs: Subscription;

  constructor() { }

  ngOnInit() {
    this.queryObs = this.querySubject.pipe(
      debounceTime(500),
      distinctUntilChanged()
    )
      .subscribe(value => {
        this.update.emit({
          list: this.active,
          filter: {
            ...this.filters[this.active],
            page: 1,
            name: value.toString().toLocaleLowerCase()
          }
        });
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initQuery(changes['filters'] && changes['filters'].isFirstChange() || !!changes['active']);
  }

  ngOnDestroy() {
    this.queryObs.unsubscribe();
  }

  showSearch() {
    this.visible = true;
  }

  hideSearch() {
    this.query = '';
    this.visible = false;
    this.querySubject.next(this.query);
  }

  initQuery(changeQuery: boolean) {
    if (!this.filters || !this.active) {
      this.visible = false;
      return;
    }
    if (this.filters[this.active].name) {
      if (changeQuery) {
        this.query = this.filters[this.active].name;
      }
      this.visible = true;
    } else {
      if (!this.focused) {
        this.visible = false;
      }
    }
  }

  queryUpdate(event) {
    this.querySubject.next(event);
  }

  onFocus() {
    this.focused = true;
  }

  onBlur() {
    this.focused = false;
    this.initQuery(false);
  }
}
