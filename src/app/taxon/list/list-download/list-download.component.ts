import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DownloadTaxa } from '../../models/download-taxa';

@Component({
  selector: 'ilm-list-download',
  templateUrl: './list-download.component.html',
  styleUrls: ['./list-download.component.scss']
})
export class ListDownloadComponent implements OnInit {

  @Input() downloaded: DownloadTaxa;
  @Input() taxonID: string;
  @Input() disabled = false;

  @Output() download = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

}
