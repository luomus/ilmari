import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ExtendedTaxon, TaxaResult } from '../../../models/taxon';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { animate, query, style, transition, trigger } from '@angular/animations';
import { CdkVirtualScrollViewport } from '@angular/cdk-experimental/scrolling';

@Component({
  selector: 'ilm-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', [
          style({ opacity: 0, transform: 'translateY(100px)' }),
          animate('200ms ease-out', style({ opacity: 1, transform: 'none' }))
        ], { optional: true })
      ])
    ])
  ]
})
export class ListViewComponent implements AfterViewInit {

  @ViewChild(CdkVirtualScrollViewport) public virtualViewport: CdkVirtualScrollViewport;

  @Input() activeTaxon: ExtendedTaxon;
  @Input() taxaResult: TaxaResult;
  @Input() filters: {[list: string]: BaseFilterTaxa};
  @Input() activeList = 'list';
  @Input() ownLists: string[];
  @Input() activeOwnList: string;
  @Input() useHighQualityImages: boolean;

  @Output() page = new EventEmitter<{pageSize: number, pageIndex: number}>();
  @Output() newOwnList = new EventEmitter<string>();
  @Output() selectOwnList = new EventEmitter<string>();
  @Output() deleteOwnList = new EventEmitter<string>();
  @Output() activateList = new EventEmitter<string>();

  constructor() { }

  ngAfterViewInit() {
    if (this.taxaResult && this.activeTaxon) {
      const idx = this.taxaResult.taxa.findIndex(taxon => taxon.id === this.activeTaxon.id);
      if (idx > -1) {
        setTimeout(() => {
          console.log((this.virtualViewport as any)._scrollStrategy);
          const viewportElement = this.virtualViewport.elementRef.nativeElement;
          const currentOffset = this.virtualViewport.measureScrollOffset();
          const currentOffsetEnd = currentOffset + this.virtualViewport.getViewportSize();
          const offset = 72 * idx;
          const shouldScroll = offset < currentOffset || offset > currentOffsetEnd;
          // viewportElement.scrollTo({ left: 0, top: offset, behavior: 'smooth' });
          try {
            if (shouldScroll) {
              viewportElement.scrollTop = offset;
            }
          } catch (e) {}
        });
        // This is not yet implemented in the cdk!
        // waits for this to be merged https://github.com/angular/material2/pull/11498/files
        // this.virtualViewport.scrollToIndex(idx);
      }
    }
  }

}
