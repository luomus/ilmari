import { Component, OnInit } from '@angular/core';
import * as fromTaxon from '../../store';
import { ListType } from '../../store/reducers/taxon-reducers';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as listActions from '../../store/actions/list-actions';
import * as fromCore from '../../../core/store';
import { ExtendedInformalTaxonGroup } from '../../../models/informal-taxon-group';

@Component({
  selector: 'ilm-list-search-container',
  templateUrl: './list-header-container.component.html',
  styleUrls: ['./list-header-container.component.scss']
})
export class ListHeaderContainerComponent implements OnInit {

  filter$: Observable<{[list: string]: BaseFilterTaxa}>;
  active$: Observable<ListType>;
  activeOwnList$: Observable<string>;
  ownLists$: Observable<string[]>;
  informalGroups$: Observable<ExtendedInformalTaxonGroup[]>;

  constructor(private store: Store<fromTaxon.State>) { }

  ngOnInit() {
    this.active$ = this.store.select(fromTaxon.getListActive);
    this.filter$ = this.store.select(fromTaxon.getListFilters);
    this.ownLists$ = this.store.select(fromTaxon.getListOwnLists);
    this.activeOwnList$ = this.store.select(fromTaxon.getListActiveOwnList);
    this.informalGroups$ = this.store.select(fromCore.getInformalTaxonGroupRoots);
  }

  onUpdate(event: { list: ListType; filter: BaseFilterTaxa }) {
    this.store.dispatch(new listActions.LoadAction(event));
  }

  activeList(list: ListType) {
    this.store.dispatch(new listActions.SetActiveListAction(list));
  }

  newOwnList(value: string) {
    this.store.dispatch(new listActions.AddNewOwnListAction(value));
  }

  selectOwnList(value: string) {
    this.store.dispatch(new listActions.ActiveOwnListAction(value));
  }

  deleteOwnList(value: string) {
    this.store.dispatch(new listActions.DeleteOwnListAction(value));
  }

  groupSelect(group: string) {
    this.store.dispatch(new listActions.SetActiveListAction(ListType.list));
    this.store.dispatch(new listActions.SetBelongToFilterAction({list: ListType.list, belongTo: [group]}));
  }
}
