import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ExtendedTaxon, TaxaResult } from '../../../models/taxon';
import { Store } from '@ngrx/store';
import * as fromTaxon from '../../store';
import * as taxonActions from '../../store/actions/taxon-actions';
import * as listActions from '../../store/actions/list-actions';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';
import * as fromRoot from '../../../core/store';
import { ListType } from '../../store/reducers/list-reducers';
import { concatMap, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  moduleId: module.id,
  selector: 'ilm-taxon-container',
  template: `
    <ilm-taxon-view [taxon]="taxon$ | async"
                    [taxaResult]="taxonResult$ | async"
                    [isMobile]="isMobile$ | async"
                    [filters]="filters$ | async"
                    [parentMenuVisible]="parentMenuVisible$ | async"
                    [activeOwnList]="activeOwnList$ | async"
                    [listPath]="listPath$ | async"
                    (toggleParentMenu)="toggleParentMenu()"
                    (before)="onBefore($event)"
                    (next)="onNext($event)"
                    (addToList)="addToList($event)"
                    (showLevel)="showLevel($event)"
                    (updateSelectedMedia)="updateSelectedMedia($event)"></ilm-taxon-view>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaxonContainerComponent implements OnInit, OnDestroy {

  isMobile$: Observable<boolean>;
  parentMenuVisible$: Observable<boolean>;
  taxon$: Observable<ExtendedTaxon>;
  taxonResult$: Observable<TaxaResult>;
  filters$: Observable<BaseFilterTaxa>;
  routeSubscription: Subscription;
  activeOwnList$: Observable<string>;
  listPath$: Observable<string>;

  constructor(
    private store: Store<fromTaxon.State>,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.isMobile$ = this.store.select(fromRoot.getIsMobile);
    this.parentMenuVisible$ = this.store.select(fromTaxon.isParentMenuVisible);
    this.taxon$ = store.select(fromTaxon.getTaxon);
    this.filters$ = store.select(fromTaxon.getListActive).pipe(
      concatMap(active => store.select(fromTaxon.getListFilters).pipe(map((filters) => filters ? filters[active] : null)))
    );
    this.taxonResult$ = store.select(fromTaxon.getListActive).pipe(
      concatMap(active => store.select(fromTaxon.getListResults).pipe(map((filters) => filters ? filters[active] : null)))
    );
    this.activeOwnList$ = this.store.select(fromTaxon.getListActiveOwnList);
    this.listPath$ = store.select(fromTaxon.isRecognizeListActive).pipe(
      map(isRecognize => isRecognize ? '/species/recognize' : '/species')
    );
  }

  ngOnInit() {
    this.routeSubscription = this.route.params.pipe(
      map(params => params.id),
      distinctUntilChanged(),
      map(taxonId => new taxonActions.LoadOneAction(taxonId))
    )
    .subscribe(this.store);
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  toggleParentMenu() {
    this.store.dispatch(new taxonActions.ToggleParentMenuAction());
  }

  updateSelectedMedia(imageID: string) {
    this.store.dispatch(new taxonActions.UpdateSelectedImageAction(imageID));
  }

  onBefore(id: string) {
    this.store.dispatch(new taxonActions.MoveToBeforeAction(id));
  }

  onNext(id: string) {
    this.store.dispatch(new taxonActions.MoveToNextAction(id));
  }

  addToList(id: string) {
    this.store.dispatch(new listActions.AddToOwnListAction(id));
  }

  showLevel(id: string) {
    this.store.dispatch(new listActions.SetBelongToFilterAction({list: ListType.species, belongTo: [id]}));
    this.store.dispatch(new listActions.SetActiveListAction(ListType.species));
    this.router.navigate(['/species']);
  }
}
