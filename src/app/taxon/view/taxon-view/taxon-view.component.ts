import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ExtendedTaxon, ExtendedTaxonImage, TaxaResult } from '../../../models/taxon';
import { BaseFilterTaxa } from '../../models/base-filter-taxa';

@Component({
  selector: 'ilm-taxon-view',
  templateUrl: './taxon-view.component.html',
  styleUrls: ['./taxon-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaxonViewComponent implements OnInit, OnChanges {

  @Input() taxon: ExtendedTaxon;
  @Input() taxaResult: TaxaResult;
  @Input() filters: BaseFilterTaxa;
  @Input() isMobile: boolean;
  @Input() parentMenuVisible: boolean;
  @Input() activeOwnList: string;
  @Input() listPath: string;
  @Output() updateSelectedMedia = new EventEmitter<string>();
  @Output() next = new EventEmitter<string>();
  @Output() before = new EventEmitter<string>();
  @Output() showLevel = new EventEmitter<string>();
  @Output() addToList = new EventEmitter<string>();
  @Output() toggleParentMenu = new EventEmitter<void>();
  selectedMedia: ExtendedTaxonImage;
  position = 0;
  current: string;

  constructor() { }

  ngOnInit() {
    this.initData();
  }

  ngOnChanges() {
    this.initData();
  }

  private initData() {
    if (!this.taxon) {
      return;
    }
    if (this.taxon.id === this.current) {
      return;
    }
    this.current = this.taxon.id;
    if (this.filters && this.taxaResult && this.taxaResult.taxa && this.taxaResult.taxa.length > 1) {
      this.position = this.taxaResult
        .taxa
        .findIndex(taxon => taxon.id === this.taxon.id)
        + 1
        + ((this.filters.page - 1) * this.filters.pageSize);
    }
    const multimedia = this.taxon.multimedia || this.taxon._media;
    this.selectedMedia = this.taxon._selectedMedia && multimedia ?
      multimedia.filter(media => media.id === this.taxon._selectedMedia)[0] :
      null;
  }

}
