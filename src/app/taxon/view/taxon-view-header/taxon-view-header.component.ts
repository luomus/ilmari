import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/index';
import { ExtendedTaxon } from '../../../models/taxon';
import * as listActions from '../../store/actions/list-actions';
import { map } from 'rxjs/operators';
import * as fromTaxon from '../../store';
import { Store } from '@ngrx/store';

@Component({
  selector: 'ilm-taxon-view-header',
  templateUrl: './taxon-view-header.component.html',
  styleUrls: ['./taxon-view-header.component.scss']
})
export class TaxonViewHeaderComponent implements OnInit {
  taxon$: Observable<ExtendedTaxon>;
  activeOwnList$: Observable<string>;
  listPath$: Observable<string>;

  constructor(
    private store: Store<fromTaxon.State>
  ) { }

  ngOnInit() {
    this.taxon$ = this.store.select(fromTaxon.getTaxon);
    this.activeOwnList$ = this.store.select(fromTaxon.getListActiveOwnList);
    this.listPath$ = this.store.select(fromTaxon.isRecognizeListActive).pipe(
      map(isRecognize => isRecognize ? '/species/recognize' : '/species')
    );
  }

  addToList(id: string) {
    this.store.dispatch(new listActions.AddToOwnListAction(id));
  }

}
