import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'ilm-recording-type',
  templateUrl: './recording-type.component.html',
  styleUrls: ['./recording-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecordingTypeComponent {

  _types: string[];

  constructor() { }

  @Input()
  set type(type: string) {
    this._types = type.split(',').map(val => val.trim().toLowerCase());
  }

}
