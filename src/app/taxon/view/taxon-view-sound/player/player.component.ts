import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ilm-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerComponent implements OnInit, OnDestroy {
  @ViewChild('audioPlayer') audioPlayerRef: ElementRef<HTMLAudioElement>;
  @Input() recorder: string;
  @Input() location: string;
  @Input() recorderLink: string;

  _audioFile: string;
  _license: string;

  licenseIcon = '';
  playing = false;
  duration = 0;
  currentTime = 0;
  timeLeft = 0;
  sliderUpdateInterval: any;
  progress = 0;

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.clearProgressTime();
  }

  @Input()
  set audioFile(file: string) {
    this._audioFile = file;
    this.audioPlayerRef.nativeElement.ondurationchange = (e) => {
      this.duration = (e.target as any).duration;
      this.updateNumbers();
      this.cdr.markForCheck();
    };
  }

  @Input()
  set license(license: string) {
    this._license = license;
    if (license.indexOf('by-nc-nd') !== -1) {
      this.licenseIcon = 'by-nc-nd';
    } else if (license.indexOf('by-nc-sa') !== -1) {
      this.licenseIcon = 'by-nc-sa';
    } else if (license.indexOf('by-sa') !== -1) {
      this.licenseIcon = 'by-sa';
    }
  }

  play() {
    this.audioPlayerRef.nativeElement.play();
    this.playing = true;
    this.startProgressTimer();
  }

  pause() {
    this.audioPlayerRef.nativeElement.pause();
    this.playing = false;
  }

  onEnd() {
    this.playing = false;
  }

  goTo(event: MouseEvent) {
    const relPos = event.layerX || event.offsetX;
    const desiredPos = (relPos / (event.target as any).offsetWidth);
    this.audioPlayerRef.nativeElement.currentTime = this.duration * desiredPos;
    this.updateNumbers();
    this.cdr.markForCheck();
  }

  private updateNumbers() {
    this.currentTime = this.audioPlayerRef.nativeElement.currentTime;
    this.timeLeft = Math.floor(this.duration - this.currentTime);
    this.progress = (this.currentTime / this.duration) * 100;
  }

  private startProgressTimer() {
    this.clearProgressTime();
    this.sliderUpdateInterval = setInterval(() => {
      if (this.playing) {
        this.updateNumbers();
        this.cdr.markForCheck();
      } else {
        this.clearProgressTime();
      }
    }, 60);
  }

  private clearProgressTime() {
    if (this.sliderUpdateInterval) {
      clearInterval(this.sliderUpdateInterval);
    }
  }

}
