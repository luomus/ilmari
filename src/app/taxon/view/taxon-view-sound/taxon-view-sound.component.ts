import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { ExtendedTaxon, ExtendedTaxonRecording } from '../../../models/taxon';
import { SoundService } from '../../service/sound.service';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'ilm-taxon-view-sound',
  templateUrl: './taxon-view-sound.component.html',
  styleUrls: ['./taxon-view-sound.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaxonViewSoundComponent {

  recordings$: Observable<ExtendedTaxonRecording[]>;
  active: ExtendedTaxonRecording;
  showPlayer = true;

  constructor(private soundService: SoundService, private cdr: ChangeDetectorRef) { }

  @Input()
  set taxon(taxon: ExtendedTaxon) {
    this.recordings$ = this.soundService.getRecordings(taxon).pipe(
      tap(reordings => {
        if (reordings && reordings[0]) {
          this.active = reordings[0];
          this.cdr.markForCheck();
        }
      })
    );
  }

  changeSelection(sel: any) {
    this.active = sel.value;
    this.showPlayer = false;
    this.cdr.markForCheck();
    setTimeout(() => {
      this.showPlayer = true;
      this.cdr.markForCheck();
    }, 200);
  }

}
