import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

interface TaxonMenuOptions {
  hideLevel?: boolean;
  hideParentToggle?: boolean;
  showSpeciesLink?: boolean;
}

@Component({
  selector: 'ilm-taxon-menu',
  templateUrl: './taxon-menu.component.html',
  styleUrls: ['./taxon-menu.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaxonMenuComponent implements OnInit {

  @Input() options: TaxonMenuOptions = {};
  @Input() taxonID: string;
  @Input() parentMenuVisible: boolean;
  @Input() activeOwnMenu: string;
  @Input() listPath = '/species';
  @Output() addToList = new EventEmitter<string>();
  @Output() showLevel = new EventEmitter<string>();
  @Output() toggleParentMenu = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

}
