import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ExtendedTaxon } from '../../../models/taxon';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'ilm-taxon-view-parents',
  templateUrl: './taxon-view-parents.component.html',
  styleUrls: ['./taxon-view-parents.component.scss'],
  animations: [
    trigger('expand', [
      state('expanded', style({
        overflow: 'hidden',
        height: '25px'
      })),
      state('collapsed', style({
        overflow: 'hidden',
        height: '42px'
      })),
      transition('expanded => collapsed', animate('200ms ease-in-out')),
      transition('collapsed => expanded', animate('200ms ease-out'))
    ])
  ]
})
export class TaxonViewParentsComponent implements OnInit {

  showButton: boolean;
  @Input() taxon: ExtendedTaxon;
  @Input() showParentMenu;
  @Input() activeOwnMenu: string;
  @Output() parentSelect = new EventEmitter<string>();
  @Output() listSelect = new EventEmitter<string>();
  @Output() toggleParentMenu = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.showButton = this.showParentMenu;
  }

  expandStart() {
    if (!this.showParentMenu && this.showButton) {
      this.showButton = false;
    }
  }

  expandDone() {
    if (this.showParentMenu && !this.showButton) {
      this.showButton = true;
    }
  }

}
