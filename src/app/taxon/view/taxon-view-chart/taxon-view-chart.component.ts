import { Component, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import * as shape from 'd3-shape';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'ilm-taxon-view-chart',
  templateUrl: './taxon-view-chart.component.html',
  styleUrls: ['./taxon-view-chart.component.scss']
})
export class TaxonViewChartComponent implements OnInit {

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  tooltipDisabled = false;
  showGridLines = true;
  roundDomains = false;
  xScaleMin: any;
  xScaleMax: any;
  yScaleMin: number;
  yScaleMax: number;


  showLegend = false;
  showXAxisLabel = false;
  showYAxisLabel = false;
  legendTitle = 'Legend';
  xAxisLabel = 'Month';
  yAxisLabel = 'Observations';

  curve = shape.curveBasis;

  colorScheme: any = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  schemeType = 'ordinal';
  rangeFillOpacity = 0.15;

  // line, area
  autoScale = true;
  timeline = false;
  _results: any = [];

  names = {
    1: 'Jan 01',
    14: 'Apr 14',
    27: 'Jul 27',
    40: 'Oct 40'
  };

  @Input() set results(value) {
    const series = [];
    for (let i = 1; i <= 52; i++) {
      series.push({name: '' + (this.names[i] || (i < 10 ? '0' + i : i)), value: value[i] || 0});
    }
    this._results = [
      {
        name: 'Observations during this week',
        series: series
      }
    ];
  }

  isBrowser: boolean;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit() {
  }

}
