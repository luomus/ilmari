import { Component, Input, OnInit } from '@angular/core';
import { TaxaGrid } from '../../../models/taxon';

@Component({
  selector: 'ilm-taxon-view-map-container',
  templateUrl: './taxon-view-map-container.component.html',
  styleUrls: ['./taxon-view-map-container.component.scss']
})
export class TaxonViewMapContainerComponent implements OnInit {

  @Input() ykjGrid: TaxaGrid[];

  constructor() { }

  ngOnInit() {
  }

}
