import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ExtendedTaxon } from '../../../models/taxon';

@Component({
  selector: 'ilm-taxon-view-details',
  templateUrl: './taxon-view-details.component.html',
  styleUrls: ['./taxon-view-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaxonViewDetailsComponent implements OnInit {

  _taxon: ExtendedTaxon;
  iucnStatus;

  constructor() { }

  ngOnInit() {
  }

  @Input()
  set taxon(taxon: ExtendedTaxon) {
    this._taxon = taxon;
    this.iucnStatus = taxon.latestRedListStatusFinland && taxon.latestRedListStatusFinland.status || '';
  }
  get taxon() {
    return this._taxon;
  }

}
