import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ExtendedTaxonImage } from '../../../models/taxon';
import { NguCarouselConfig} from '@ngu/carousel';
import { ImagePreviewOverlayService } from '../../../shared/component/image-preview-overlay/image-preview-overlay.service';
import { ImagePreviewOverlayRef } from '../../../shared/component/image-preview-overlay/image-preview-overlay-ref';
import { ToHttpsPipe } from '../../../shared/pipe/to-https.pipe';

@Component({
  selector: 'ilm-taxon-view-image',
  templateUrl: './taxon-view-image.component.html',
  styleUrls: ['./taxon-view-image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaxonViewImageComponent implements OnInit, OnChanges {

  @Input() images: ExtendedTaxonImage[];
  @Input() selected: string;
  @Output() selectImage = new EventEmitter<string>();

  selectedMedia: ExtendedTaxonImage;
  carouselOne: NguCarouselConfig = {
    grid: {xs: 0, sm: 0, md: 0, lg: 0, all: 150},
    slide: 3,
    point: {
      visible: true,
      pointStyles: `
          .ngucarouselPoint {
            list-style-type: none;
            text-align: center;
            padding: 3px;
            margin: 0;
            white-space: nowrap;
            overflow: auto;
            position: absolute;
            width: 100%;
            bottom: 0;
            left: 0;
            box-sizing: border-box;
          }
          .ngucarouselPoint li {
            display: inline-block;
            border-radius: 999px;
            background: rgba(255, 255, 255, 0.55);
            padding: 5px;
            margin: 0 3px;
            transition: .4s ease all;
          }
          .ngucarouselPoint li.active {
              background: white;
              width: 10px;
          }
        `
    } as any,
    load: 2,
    loop: true,
    touch: true
  };

  constructor(
    private previewDialog: ImagePreviewOverlayService,
    private cdr: ChangeDetectorRef,
    private toHttpsPipe: ToHttpsPipe,
  ) { }

  ngOnInit() {
    this.updateSelectedMedia();
  }

  ngOnChanges() {
    this.updateSelectedMedia();
  }

  updateSelectedMedia() {
    if (!(this.selected && this.images) || (this.selectedMedia && this.selected === this.selectedMedia.id)) {
      return;
    }
    this.selectedMedia = this.images.filter(media => media.id === this.selected)[0];
  }

  fullScreen(taxonImage: ExtendedTaxonImage) {
    const dialogRef: ImagePreviewOverlayRef = this.previewDialog.open({
      image: {
        url: this.toHttpsPipe.transform(taxonImage.largeURL || taxonImage.fullURL),
        description: `${taxonImage.author} - ${taxonImage.licenseDescription || taxonImage.licenseAbbreviation}`
      }
    });
  }

  public onCarouselMove(event: Event) {
    this.cdr.markForCheck();
  }

}
