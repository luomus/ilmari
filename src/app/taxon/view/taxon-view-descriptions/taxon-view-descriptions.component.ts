import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ilm-taxon-view-descriptions',
  templateUrl: './taxon-view-descriptions.component.html',
  styleUrls: ['./taxon-view-descriptions.component.scss']
})
export class TaxonViewDescriptionsComponent implements OnInit {

  @Input() descriptions;

  constructor() { }

  ngOnInit() {
  }

}
