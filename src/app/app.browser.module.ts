import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Injectable, NgModule } from '@angular/core';
import { HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { AppModule } from './app.module';
import { AppContainerComponent } from './core/app-container/app-container.component';

@Injectable()
export class HammerConfig extends HammerGestureConfig  {
  buildHammer(element: HTMLElement) {
    return new Hammer(element, {
      touchAction: 'pan-y'
    });
  }
}

@NgModule({
  declarations: [],
  imports: [
    BrowserAnimationsModule,
    AppModule
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig ,
    }
  ],
  bootstrap: [AppContainerComponent]
})
export class AppBrowserModule {}
