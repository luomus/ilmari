import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Profile } from '../../models/profile';
import { PersonModel } from '../../models/person';
import { UserSettings } from '../../models/user-settings';

@Component({
  selector: 'ilm-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @Input() profile: Profile;
  @Input() user: PersonModel;
  @Input() userSettings: UserSettings;
  @Output() updateUserSettings = new EventEmitter<UserSettings>();

  constructor() { }

  ngOnInit() {
  }

}
