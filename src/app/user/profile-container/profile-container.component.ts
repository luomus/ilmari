import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Profile } from '../../models/profile';
import { Store } from '@ngrx/store';
import * as fromUser from '../store';
import * as userActions from '../store/actions/user-actions';
import { PersonModel } from '../../models/person';
import { UserSettings } from '../../models/user-settings';

@Component({
  selector: 'ilm-profile-container',
  templateUrl: './profile-container.component.html',
  styleUrls: ['./profile-container.component.scss']
})
export class ProfileContainerComponent implements OnInit {

  user$: Observable<PersonModel>;
  profile$: Observable<Profile>;
  userSettings$: Observable<UserSettings>;

  constructor(private store: Store<fromUser.State>) {
    this.user$ = this.store.select(fromUser.getUser);
    this.profile$ = this.store.select(fromUser.getProfile);
    this.userSettings$ = this.store.select(fromUser.getUserSettings);
  }

  ngOnInit() {
  }

  updateUserSettings(settings: UserSettings) {
    this.store.dispatch(new userActions.UpdateUserSettings(settings));
  }

}
