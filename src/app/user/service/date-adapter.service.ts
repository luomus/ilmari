import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { NativeDateAdapter } from '@angular/material';
import { TimeService } from '../../shared/service/time.service';
import { Platform } from '@angular/cdk/platform';

export const APP_DATE_FORMATS =
  {
    parse: {
      dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    },
    display: {
      dateInput: 'input',
      monthYearLabel: { year: 'numeric', month: 'short' },
      dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
      monthYearA11yLabel: { year: 'numeric', month: 'long' },
    }
  };

@Injectable()
export class DateAdapterService extends NativeDateAdapter {

  constructor(
    @Inject(LOCALE_ID) matDateLocale: string,
    private timeService: TimeService,
    private platform: Platform
  ) {
    super('fi', platform);
  }

  getFirstDayOfWeek(): number {
    return 1;
  }

  format(date: Date, displayFormat: Object): string {
    if (displayFormat === 'input') {
      return this.timeService.dateToDateString(date);
    } else {
      return super.format(date, displayFormat);
    }
  }

}
