import { Injectable } from '@angular/core';
import { UserData } from '../../models/user-data';
import { LajiApi, LajiApiService } from '../../shared/service/laji-api.service';
import { PersonModel } from '../../models/person';
import { Settings, SettingService } from '../../shared/service/setting.service';
import { Profile } from '../../models/profile';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class PersonService {

  constructor(
    private apiService: LajiApiService,
    private settingService: SettingService
  ) { }

  logoutPerson(personToken: string) {
    return this.apiService
      .deleteEntry(LajiApi.Endpoint.personToken, personToken).pipe(
        catchError((err) => err.status === 404 ? of({}) : throwError(err))
      );
  }

  getPerson(personToken: string): Observable<PersonModel> {
    return this.apiService.getEntry(LajiApi.Endpoint.person, personToken);
  }

  getStoredUser() {
    return this.settingService.get(Settings.userData);
  }

  storeUser(data: UserData) {
    this.settingService.set(Settings.userData, data).subscribe();
  }

  getProfile(personToken: string) {
    return this.apiService.getEntry(LajiApi.Endpoint.profile, personToken);
  }

  getStoredProfile() {
    return this.settingService.get(Settings.profile);
  }

  storeProfile(data: Profile) {
    this.settingService.set(Settings.profile, data).subscribe();
  }

  deleteStoredUser() {
    this.settingService.remove(Settings.userData).subscribe();
  }

  deleteStoredProfile() {
    this.settingService.remove(Settings.profile).subscribe();
  }

}
