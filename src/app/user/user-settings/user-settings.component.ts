import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserSettings } from '../../models/user-settings';

@Component({
  selector: 'ilm-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {

  @Input() userSettings: UserSettings;
  @Output() update = new EventEmitter<UserSettings>();

  ignoreUnits = UserSettings.IgnoreUnitByType;

  constructor() { }

  ngOnInit() {
  }

  updateValue(value: string, place: string) {
    if (this.userSettings.imageMetadata[place]) {
      return this.update.emit({...this.userSettings, imageMetadata: {...this.userSettings.imageMetadata, [place]: value}});
    }
    this.update.emit({...this.userSettings, [place]: value});
  }
}
