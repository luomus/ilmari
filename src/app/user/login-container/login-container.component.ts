import { AfterViewInit, ChangeDetectionStrategy, Component, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { Store } from '@ngrx/store';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { Subscription } from 'rxjs';

import * as fromUser from '../../user/store';
import { CheckLoginAction, CloseLoginAction } from '../store/actions/user-actions';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'ilm-login-container',
  templateUrl: './login-container.component.html',
  styleUrls: ['./login-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginContainerComponent implements AfterViewInit {

  showLogin$: Subscription;

  private isOpen = false;
  private modalRef: MatDialogRef<LoginModalComponent>;
  fRef = (event) => this.analyseMessage(event);

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private store: Store<fromUser.State>,
    private dialog: MatDialog
  ) {
  }

  ngAfterViewInit() {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    this.showLogin$ = this.store
      .select(fromUser.showLogin)
      .subscribe((show) => this.openLoginDialog(show));
  }

  openLoginDialog(show) {
    if (show === false || this.isOpen) {
      return;
    }
    this.addListener();
    this.isOpen = true;
    this.modalRef = this.dialog.open(LoginModalComponent, {
      width: '80vw',
    });
    this.modalRef.afterClosed()
      .subscribe((data) => {
        if (data) {
          this.store.dispatch(new CheckLoginAction(data));
        }
        this.isOpen = false;
        this.store.dispatch(new CloseLoginAction());
        this.removeListener();
      });
  }

  private closeModal(token?: string) {
    if (this.modalRef) {
      this.modalRef.close(token);
    }
  }

  private analyseMessage(event) {
    if (!event.data || event.data.action !== 'LOGIN') {
      return;
    }
    if (event.origin !== environment.base) {
      this.closeModal();
      return;
    }
    const query = new URLSearchParams(event.data.query);
    this.closeModal(query.get('token'));
  }

  private addListener() {
    window.addEventListener('message', this.fRef, false);
  }

  private removeListener() {
    window.removeEventListener('message', this.fRef, false);
  }
}
