import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { LoginContainerComponent } from './login-container/login-container.component';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from './store';
import { LoginModalComponent } from './login-container/login-modal/login-modal.component';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './store/effects/user-effects';
import { PersonService } from './service/person.service';
import { ProfileContainerComponent } from './profile-container/profile-container.component';
import { ProfileComponent } from './profile/profile.component';
import { SharedModule } from '../shared/shared.module';
import { UserSettingsComponent } from './user-settings/user-settings.component';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ],
  declarations: [LoginContainerComponent, LoginModalComponent, ProfileContainerComponent, ProfileComponent, UserSettingsComponent],
  exports: [LoginContainerComponent],
  entryComponents: [LoginModalComponent]
})
export class UserModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RootAuthModule,
      providers: [PersonService]
    };
  }
}

@NgModule({
  imports: [
    UserModule,
    StoreModule.forFeature('user', reducers as any, {metaReducers}),
    EffectsModule.forFeature([UserEffects])
  ],
})
export class RootAuthModule {}
