import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginContainerComponent } from './login-container/login-container.component';
import { ProfileContainerComponent } from './profile-container/profile-container.component';

const routes: Routes = [
  {path: 'login', component: LoginContainerComponent, pathMatch: 'full'},
  {path: 'profile', component: ProfileContainerComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
