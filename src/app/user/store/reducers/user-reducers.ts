import * as userAction from '../actions/user-actions';
import { PersonModel } from '../../../models/person';
import { Profile } from '../../../models/profile';
import { UserSettings } from '../../../models/user-settings';
import { Document } from '../../../models/document/document';

export interface State {
  isLoggedIn: boolean;
  showLogin: boolean;
  personToken: string;
  user: PersonModel;
  profile: Profile;
  userSettings: UserSettings;
}

export const initialState: State = {
  isLoggedIn: false,
  showLogin: false,
  personToken: '',
  user: {},
  profile: {},
  userSettings: {
    useHighQualityImageInList: false,
    ignoreUnits: UserSettings.IgnoreUnitByType.withNoTaxon,
    publicityRestrictions: Document.PublicityRestrictionsEnum.PublicityRestrictionsPublic,
    imageMetadata: {
      intellectualRights: 'MZ.intellectualRightsARR'
    }
  }
};

export function reducer(
  state = initialState,
  action: userAction.Actions
): State {
  switch (action.type) {
    case userAction.UPDATE_USER_SETTINGS:
      return {
        ...state,
        userSettings: action.payload
      };
    case userAction.LOADED_PROFILE:
      return {
        ...state,
        profile: action.payload
      };
    case userAction.OPEN_LOGIN:
      return {
        ...state,
        showLogin: true
      };
    case userAction.CLOSE_LOGIN:
      return {
        ...state,
        showLogin: false
      };
    case userAction.LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        personToken: '',
        user: {}
      };
    case userAction.LOGIN:
      return {
        ...state,
        isLoggedIn: true,
        user: action.payload.person,
        personToken: action.payload.personToken
      };
    default: {
      return state;
    }
  }
}

export const isLoggedIn = (state: State) => state.isLoggedIn;
export const showLogin = (state: State) => state.showLogin;
export const getProfile = (state: State) => state.profile;
export const getUser = (state: State) => state.user;
export const getPersonToken = (state: State) => state.personToken;
export const getUserSettings = (state: State) => state.userSettings;
