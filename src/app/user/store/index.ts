import { ActionReducer, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import * as fromUser from './reducers/user-reducers';
import * as fromRoot from '../../core/store';
import { localStorageSync } from 'ngrx-store-localstorage';

export interface UserState {
  user: fromUser.State;
}

export interface State extends fromRoot.State {
  'user': UserState;
}

export const reducers = {
  user: fromUser.reducer
};

export function localStorageSyncReducer(reducer: ActionReducer<State>): ActionReducer<any, any> {
  return localStorageSync({
    keys: [
      {
        user: [
          'showLogin',
          'userSettings'
        ]
      }
    ],
    rehydrate: true,
    removeOnUndefined: true,
    restoreDates: false
  })(reducer);
}

export const metaReducers: MetaReducer<State>[] = [localStorageSyncReducer];

export const getUserFeatureState = createFeatureSelector<UserState>('user');

export const getUserState = createSelector(getUserFeatureState, (state: UserState) => state.user);

export const isLoggedIn = createSelector(getUserState, fromUser.isLoggedIn);
export const showLogin = createSelector(getUserState, fromUser.showLogin);
export const getPersonToken = createSelector(getUserState, fromUser.getPersonToken);
export const getProfile = createSelector(getUserState, fromUser.getProfile);
export const getUser = createSelector(getUserState, fromUser.getUser);
export const getUserSettings = createSelector(getUserState, fromUser.getUserSettings);
