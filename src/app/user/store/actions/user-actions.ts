import { Action } from '@ngrx/store';
import { UserData } from '../../../models/user-data';
import { Profile } from '../../../models/profile';
import { UserSettings } from '../../../models/user-settings';

export const LOGIN_REQUEST = '[User] login request';
export const LOGOUT_REQUEST = '[User] logout request';
export const CHECK_LOGIN = '[User] check login';
export const LOGIN = '[User] login';
export const LOGOUT = '[User] logout';
export const OPEN_LOGIN = '[User] open login';
export const CLOSE_LOGIN = '[User] close login';
export const LOADED_PROFILE = '[User] loaded profile';
export const UPDATE_USER_SETTINGS = '[User] update user settings';

export class UpdateUserSettings implements Action {
  readonly type = UPDATE_USER_SETTINGS;

  constructor(public payload: UserSettings) {}
}

export class OpenLoginAction implements Action {
  readonly type = OPEN_LOGIN;
}

export class CloseLoginAction implements Action {
  readonly type = CLOSE_LOGIN;
}

export class LoginRequestAction implements Action {
  readonly type = LOGIN_REQUEST;
}

export class LogoutRequestAction implements Action {
  readonly type = LOGOUT_REQUEST;
}

export class LogoutAction implements Action {
  readonly type = LOGOUT;
}

export class CheckLoginAction implements Action {
  readonly type = CHECK_LOGIN;

  constructor(public payload: string) {}
}

export class LoginAction implements Action {
  readonly type = LOGIN;

  constructor(public payload: UserData) {}
}

export class LoadedProfileAction implements Action {
  readonly type = LOADED_PROFILE;

  constructor(public payload: Profile) {}
}

export type Actions
  = LoginRequestAction
  | LoadedProfileAction
  | CheckLoginAction
  | LoginAction
  | LogoutRequestAction
  | LogoutAction
  | OpenLoginAction
  | CloseLoginAction
  | UpdateUserSettings;
