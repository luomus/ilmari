import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import * as user from '../actions/user-actions';
import { CheckLoginAction, LoadedProfileAction, LoginAction, LogoutRequestAction } from '../actions/user-actions';
import { PersonService } from '../../service/person.service';
import { PersonModel } from '../../../models/person';
import * as fromUser from '../';
import { defer, EMPTY, Observable, of } from 'rxjs';
import { catchError, concatMap, map, switchMap, take, tap } from 'rxjs/operators';

@Injectable()
export class UserEffects {
  @Effect()
  init$: Observable<Action> = defer(() => {
    return of(null).pipe(
      concatMap(() => this.personService.getStoredUser()),
      tap(() => this.personService.getStoredProfile().pipe(tap(profile => this.store.dispatch(new user.LoadedProfileAction(profile)))).subscribe()),
      concatMap((data) => data ? of(new user.LoginAction(data)) : EMPTY)
    );
  });

  @Effect()
  checkLogin$: Observable<Action> = this.actions$
    .ofType<CheckLoginAction>(user.CHECK_LOGIN).pipe(
      map(action => action.payload),
      tap(personToken => this.personService.getProfile(personToken).pipe(tap(profile => this.store.dispatch(new user.LoadedProfileAction(profile)))).subscribe()),
      concatMap((personToken) => this.personService.getPerson(personToken).pipe(
        switchMap((person: PersonModel) => of(new user.LoginAction({personToken: personToken, person: person})))
      )),
      catchError(() => of(new user.LogoutAction()))
    );

  @Effect()
  login$: Observable<Action> = this.actions$
    .ofType<LoginAction>(user.LOGIN).pipe(
      map(action => action.payload),
      tap(data => this.personService.storeUser(data)),
      concatMap(() => EMPTY)
    );

  @Effect()
  storeProfile$: Observable<Action> = this.actions$
    .ofType<LoadedProfileAction>(user.LOADED_PROFILE).pipe(
      map(action => action.payload),
      tap(data => this.personService.storeProfile(data)),
      concatMap(() => EMPTY)
    );

  @Effect()
  logout$: Observable<Action> = this.actions$
    .ofType<LogoutRequestAction>(user.LOGOUT_REQUEST).pipe(
      concatMap(() => this.store.select(fromUser.getPersonToken).pipe(take(1))),
      concatMap(token => this.personService.logoutPerson(token)),
      catchError(() => EMPTY),
      tap(() => this.personService.deleteStoredUser()),
      tap(() => this.personService.deleteStoredProfile()),
      map(() => new user.LogoutAction())
    );

  constructor(
    private actions$: Actions,
    private personService: PersonService,
    private store: Store<fromUser.State>
  ) {}
}
