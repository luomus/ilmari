import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { ExtendedTaxon } from '../../models/taxon';
import { TaxonService } from '../service/taxon.service';
import { map, startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'ilm-species-autocomplete',
  templateUrl: './species-autocomplete.component.html',
  styleUrls: ['./species-autocomplete.component.scss']
})
export class SpeciesAutocompleteComponent implements OnInit {

  userInput: FormControl;

  filteredOptions: Observable<ExtendedTaxon[]>;

  constructor(private taxonService: TaxonService) { }

  ngOnInit() {
    this.userInput = new FormControl();
    this.filteredOptions = this.userInput.valueChanges.pipe(
      startWith(null),
      map(taxon => taxon && typeof taxon === 'object' ? taxon.vernacularName : taxon),
      switchMap(name => this.taxonService.findName(name, ['MVL.1']))
    );
  }

  displayFn(value: ExtendedTaxon) {
    return value ? value.vernacularName : value;
  }


}
