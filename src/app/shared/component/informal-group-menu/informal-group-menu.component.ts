import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ExtendedInformalTaxonGroup } from '../../../models/informal-taxon-group';
import * as fromCore from '../../../core/store';
import { Store } from '@ngrx/store';
import * as fromTrip from '../../../document/trip/store';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ilm-informal-group-menu',
  templateUrl: './informal-group-menu.component.html',
  styleUrls: ['./informal-group-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InformalGroupMenuComponent implements OnInit {

  informalGroups$: Observable<ExtendedInformalTaxonGroup[]>;
  @Input() includeAny = false;
  @Input() color = 'default';
  @Input() selected = environment.defaults.informalTaxonGroup;
  @Output() groupSelect = new EventEmitter<string>();

  constructor(
    private store: Store<fromTrip.State>) {
    this.informalGroups$ = this.store.select(fromCore.getInformalTaxonGroupRoots);
  }

  ngOnInit() {
  }

}
