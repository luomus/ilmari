import { Component, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { isPlatformServer } from '@angular/common';

@Component({
  selector: 'ilm-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit {

  _icon = '';

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, @Inject(PLATFORM_ID) private platformId: string) {
    const svgUrl = 'assets/icons/icons.svg';
    const domain = (isPlatformServer(platformId)) ? 'http://localhost:4000/' : '';
    iconRegistry.addSvgIconSetInNamespace(
      'ilm',
      sanitizer.bypassSecurityTrustResourceUrl(domain + svgUrl));
  }

  @Input()
  set icon(icon: string) {
    if (typeof icon !== 'string') {
      return;
    }
    this._icon = icon.replace(/\./g, '_');
  }

  ngOnInit() {
  }

}
