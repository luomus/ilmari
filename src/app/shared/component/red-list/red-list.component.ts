import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';

@Component({
  selector: 'ilm-red-list',
  templateUrl: './red-list.component.html',
  styleUrls: ['./red-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RedListComponent {

  @Input() status;

  statuses = [
    {
      label: 'EX',
      txtColor: '#c33',
      bgColor: '#000',
      value: 'MX.iucnEX'
    },
    {
      label: 'EW',
      txtColor: '#fff',
      bgColor: '#000',
      value: 'MX.iucnEW'
    },
    {
      label: 'CR',
      txtColor: '#fff',
      bgColor: '#c33',
      value: 'MX.iucnCR'
    },
    {
      label: 'EN',
      txtColor: '#fff',
      bgColor: '#c53',
      value: 'MX.iucnEN'
    },
    {
      label: 'VU',
      txtColor: '#fff',
      bgColor: '#c90',
      value: 'MX.iucnVU'
    },
    {
      label: 'NT',
      txtColor: '#9c9',
      bgColor: '#196566',
      value: 'MX.iucnNT'
    },
    {
      label: 'LC',
      txtColor: '#fff',
      bgColor: '#196566',
      value: 'MX.iucnLC'
    }
  ];

  constructor(private cdr: ChangeDetectorRef) { }

}
