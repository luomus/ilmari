import { ComponentRef, Injectable, Injector } from '@angular/core';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';

import { ImagePreviewOverlayComponent } from './image-preview-overlay.component';

import { ImagePreviewOverlayRef } from './image-preview-overlay-ref';
import { IMAGE_PREVIEW_DIALOG_DATA } from './image-preview-overlay.tokens';

export interface ImageData {
  url: string;
  description?: string;
  showDelete?: boolean;
}

interface ImagePreviewDialogConfig {
  panelClass?: string;
  hasBackdrop?: boolean;
  backdropClass?: string;
  image?: ImageData;
}

const DEFAULT_CONFIG: ImagePreviewDialogConfig = {
  hasBackdrop: true,
  backdropClass: 'cdk-overlay-dark-backdrop',
  panelClass: 'image-preview-dialog-panel',
  image: null
};

@Injectable({
  providedIn: 'root'
})
export class ImagePreviewOverlayService {

  constructor(
    private injector: Injector,
    private overlay: Overlay) { }

  open(config: ImagePreviewDialogConfig = {}) {
    const dialogConfig = { ...DEFAULT_CONFIG, ...config };
    const overlayRef = this.createOverlay(dialogConfig);
    const dialogRef = new ImagePreviewOverlayRef(overlayRef);

    dialogRef.componentInstance = this.attachDialogContainer(overlayRef, dialogConfig, dialogRef);
    overlayRef.backdropClick().subscribe(() => dialogRef.close());

    return dialogRef;
  }

  private createOverlay(config: ImagePreviewDialogConfig) {
    const overlayConfig = this.getOverlayConfig(config);
    return this.overlay.create(overlayConfig);
  }

  private attachDialogContainer(overlayRef: OverlayRef, config: ImagePreviewDialogConfig, dialogRef: ImagePreviewOverlayRef) {
    const injector = this.createInjector(config, dialogRef);

    const containerPortal = new ComponentPortal(ImagePreviewOverlayComponent, null, injector);
    const containerRef: ComponentRef<ImagePreviewOverlayComponent> = overlayRef.attach(containerPortal);

    return containerRef.instance;
  }

  private createInjector(config: ImagePreviewDialogConfig, dialogRef: ImagePreviewOverlayRef): PortalInjector {
    const injectionTokens = new WeakMap();

    injectionTokens.set(ImagePreviewOverlayRef, dialogRef);
    injectionTokens.set(IMAGE_PREVIEW_DIALOG_DATA, config.image);

    return new PortalInjector(this.injector, injectionTokens);
  }

  private getOverlayConfig(config: ImagePreviewDialogConfig): OverlayConfig {
    const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();

    return new OverlayConfig({
      hasBackdrop: config.hasBackdrop,
      backdropClass: config.backdropClass,
      panelClass: config.panelClass,
      scrollStrategy: this.overlay.scrollStrategies.block(),
      positionStrategy
    });
  }
}
