import { InjectionToken } from '@angular/core';
import { ExtendedTaxonImage } from '../../../models/taxon';

export const IMAGE_PREVIEW_DIALOG_DATA = new InjectionToken<ExtendedTaxonImage>('IMAGE_PREVIEW_DIALOG_DATA');
