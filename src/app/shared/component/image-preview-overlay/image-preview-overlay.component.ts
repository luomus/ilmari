import { Component, EventEmitter, HostListener, Inject } from '@angular/core';
import { animate, AnimationEvent, state, style, transition, trigger } from '@angular/animations';

import { ImagePreviewOverlayRef } from './image-preview-overlay-ref';
import { IMAGE_PREVIEW_DIALOG_DATA } from './image-preview-overlay.tokens';

const ESCAPE = 27;
const ANIMATION_TIMINGS = '400ms cubic-bezier(0.25, 0.8, 0.25, 1)';

@Component({
  selector: 'ilm-image-preview-overlay',
  templateUrl: './image-preview-overlay.component.html',
  styleUrls: ['./image-preview-overlay.component.scss'],
  animations: [
    trigger('slideContent', [
      state('void', style({ transform: 'translate3d(0, 25%, 0) scale(0.9)', opacity: 0 })),
      state('enter', style({ transform: 'none', opacity: 1 })),
      state('leave', style({ transform: 'translate3d(0, 25%, 0)', opacity: 0 })),
      transition('* => *', animate(ANIMATION_TIMINGS)),
    ])
  ]
})
export class ImagePreviewOverlayComponent {

  animationState: 'void' | 'enter' | 'leave' = 'enter';
  animationStateChanged = new EventEmitter<AnimationEvent>();

  @HostListener('document:keydown', ['$event']) private handleKeydown(event: KeyboardEvent) {
    if (event.keyCode === ESCAPE) {
      this.dialogRef.close();
    }
  }

  constructor(
    public dialogRef: ImagePreviewOverlayRef,
    @Inject(IMAGE_PREVIEW_DIALOG_DATA) public image: any
  ) { }

  onAnimationStart(event: AnimationEvent) {
    this.animationStateChanged.emit(event);
  }

  onAnimationDone(event: AnimationEvent) {
    this.animationStateChanged.emit(event);
  }

  startExitAnimation() {
    this.animationState = 'leave';
  }
}
