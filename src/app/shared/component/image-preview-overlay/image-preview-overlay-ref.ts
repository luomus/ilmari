import { OverlayRef } from '@angular/cdk/overlay';
import { Observable, Subject } from 'rxjs';

import { filter, take } from 'rxjs/operators';

import { ImagePreviewOverlayComponent } from './image-preview-overlay.component';

export class ImagePreviewOverlayRef {

  private _beforeClose = new Subject<void>();
  private _afterClosed = new Subject<void>();

  componentInstance: ImagePreviewOverlayComponent;

  constructor(private overlayRef: OverlayRef) { }

  close(data?: any): void {
    this.componentInstance.animationStateChanged.pipe(
      filter(event => event.phaseName === 'start'),
      take(1)
    ).subscribe(() => {
      this._beforeClose.next(data);
      this._beforeClose.complete();
      this.overlayRef.detachBackdrop();
    });

    this.componentInstance.animationStateChanged.pipe(
      filter(event => event.phaseName === 'done' && event.toState === 'leave'),
      take(1)
    ).subscribe(() => {
      this.overlayRef.dispose();
      this._afterClosed.next(data);
      this._afterClosed.complete();

      this.componentInstance = undefined;
    });

    this.componentInstance.startExitAnimation();
  }

  afterClosed(): Observable<any> {
    return this._afterClosed.asObservable();
  }

  beforeClose(): Observable<any> {
    return this._beforeClose.asObservable();
  }

}
