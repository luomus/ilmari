import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, Inject, OnDestroy, ViewChild } from '@angular/core';
import { ImageService } from '../../service/image.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ConfirmationService } from '../../service/confirmation.service';

export interface GetImageData {
  showUpload?: boolean;
  maxSize?: number;
}

@Component({
  selector: 'ilm-get-image',
  templateUrl: './get-image.component.html',
  styleUrls: ['./get-image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GetImageComponent implements AfterViewInit, OnDestroy {
  @ViewChild('video') video: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  mediaStream: MediaStream;
  width = 0;
  height = 0;

  constructor(
    public dialogRef: MatDialogRef<GetImageComponent>,
    private imageService: ImageService,
    private confirmationService: ConfirmationService,
    @Inject(MAT_DIALOG_DATA) public data: GetImageData
  ) { }

  ngAfterViewInit() {
    const video: HTMLVideoElement = this.video.nativeElement;
    this.imageService.getStream()
      .subscribe((stream) => {
        this.mediaStream = stream;
        video.srcObject = stream;
        video.addEventListener('canplay', (ev) => {
          this.width = video.videoWidth;
          this.height = video.videoHeight;
          const canvas: HTMLCanvasElement = this.canvas.nativeElement;
          if (this.data && this.data.maxSize) {
            const ratio = this.getRatio();
            canvas.width = this.width * ratio;
            canvas.height = this.height * ratio;
          } else {
            canvas.width = this.width;
            canvas.height = this.height;
          }
        }, {once: true});
        video.play();
      });
  }

  ngOnDestroy() {
    if (this.mediaStream) {
      const track = this.mediaStream.getVideoTracks()[0];
      track.stop();
    }
  }

  takePic() {
    const video: HTMLVideoElement = this.video.nativeElement;
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    if (this.data && this.data.maxSize) {
      const ratio = this.getRatio();
      canvas.getContext('2d').drawImage(video, 0, 0, this.width * ratio, this.height * ratio);
    } else {
      canvas.getContext('2d').drawImage(video, 0, 0, this.width, this.height);
    }
    this.dialogRef.close(canvas.toDataURL('image/jpeg', 0.8));
  }

  upload() {
    const element: HTMLElement = document.getElementById('upload-file') as HTMLElement;
    element.click();
  }

  handleUpload(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

    const pattern = /image-*/;
    const reader = new FileReader();

    if (!file || !file.type) {
      return;
    }
    if (!file.type.match(pattern)) {
      this.confirmationService.confirm('invalidFormat').subscribe();
      return;
    }

    reader.onload = (event) => {
      this.dialogRef.close((event.target as any).result);
    };
    reader.readAsDataURL(file);
  }

  private getRatio() {
    if (this.width >= this.height) {
      return 1 / (this.width / this.data.maxSize);
    }
    return 1 / (this.height / this.data.maxSize);
  }
}
