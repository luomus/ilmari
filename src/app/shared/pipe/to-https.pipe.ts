import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toHttps'
})
export class ToHttpsPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value || typeof value !== 'string' || value.indexOf('https') === 0) {
      return value;
    }
    return value.replace('http:', 'https:');
  }

}
