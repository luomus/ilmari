import { ChangeDetectorRef, Pipe, PipeTransform } from '@angular/core';
import { TaxonService } from '../service/taxon.service';
import { Nameable } from '../service/nameable';
import { InformalTaxonService } from '../service/informal-taxon.service';
import { LabelService } from '../service/label.service';

@Pipe({
  name: 'label',
  pure: false
})
export class LabelPipe implements PipeTransform {

  lastKey: string;
  currentValue: string;

  constructor(
    private cd: ChangeDetectorRef,
    private taxonService: TaxonService,
    private informalGroup: InformalTaxonService,
    private labelService: LabelService
  ) {}

  transform(value: string, args?: any): any {
    if (typeof value !== 'string' || value.length === 0) {
      return value;
    }
    if (value === this.lastKey) {
      return this.currentValue;
    }
    this.lastKey = value;
    this.getName(value, this.labelService);
    return this.currentValue;
  }

  private getName(key: string, service: Nameable) {
    service
      .getName(key)
      .subscribe(name => {
        this.currentValue = name;
        this.cd.markForCheck();
      });
  }
}
