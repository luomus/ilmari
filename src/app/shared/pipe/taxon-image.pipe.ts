import { Pipe, PipeTransform } from '@angular/core';
import { TaxonService } from '../service/taxon.service';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'taxonImage',
  pure: false
})
export class TaxonImagePipe implements PipeTransform {
  private cachedUrl: any = null;
  private cachedId = '';

  constructor(private taxonService: TaxonService) {}

  transform(value: any, args?: any): any {
    if (this.cachedId !== value) {
      this.cachedUrl = null;
      this.cachedId = value;
      if (!value) {
        return value;
      }
      this.taxonService.get(value).pipe(
        map(taxon => {
          const multimedia = taxon.multimedia || taxon._media;
          if (!(taxon._selectedMedia && multimedia)) {
            return '';
          }
          const img = multimedia.filter(media => media.id === taxon._selectedMedia)[0];
          if (!img) {
            return '';
          }
          return img.thumbnailURL || img.largeURL || img.fullURL;
        })
      ).subscribe(url => {
        this.cachedUrl = url;
      });
    }
    return this.cachedUrl;
  }

}
