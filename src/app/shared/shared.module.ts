import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatCommonModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpeciesAutocompleteComponent } from './species-autocomplete/species-autocomplete.component';
import { LabelPipe } from './pipe/label.pipe';
import { SafePipe } from './pipe/safe.pipe';
import { IconComponent } from './component/laji-icon/laji-icon.component';
import { InformalGroupMenuComponent } from './component/informal-group-menu/informal-group-menu.component';
import { ToHttpsPipe } from './pipe/to-https.pipe';
import { ToLowerCasePipe } from './pipe/to-lower-case.pipe';
import { ImagePreviewOverlayComponent } from './component/image-preview-overlay/image-preview-overlay.component';
import { ImagePreviewOverlayToolbarComponent } from './component/image-preview-overlay/image-preview-overlay-toolbar/image-preview-overlay-toolbar.component';
import { GetImageComponent } from './component/get-image/get-image.component';
import { TaxonImagePipe } from './pipe/taxon-image.pipe';
import { ConfirmationComponent } from './component/confirmation/confirmation.component';
import { RedListComponent } from './component/red-list/red-list.component';

export const EXTERNAL_COMPONENTS = [
  FormsModule,
  ReactiveFormsModule,
  CommonModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatTabsModule,
  MatCardModule,
  MatSortModule,
  MatBadgeModule,
  MatRadioModule,
  MatChipsModule,
  MatInputModule,
  MatTableModule,
  MatButtonModule,
  MatCommonModule,
  MatDialogModule,
  MatSelectModule,
  MatStepperModule,
  MatToolbarModule,
  MatTooltipModule,
  MatSidenavModule,
  FlexLayoutModule,
  HttpClientModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatGridListModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatProgressBarModule,
  MatButtonToggleModule,
  MatAutocompleteModule,
  MatProgressSpinnerModule
];

export const INTERNAL_COMPONENTS = [
  LabelPipe,
  SafePipe,
  ToHttpsPipe,
  IconComponent,
  TaxonImagePipe,
  ToLowerCasePipe,
  RedListComponent,
  GetImageComponent,
  ConfirmationComponent,
  InformalGroupMenuComponent,
  ImagePreviewOverlayComponent,
  SpeciesAutocompleteComponent,
  ImagePreviewOverlayToolbarComponent
];

@NgModule({
  entryComponents: [ImagePreviewOverlayComponent, GetImageComponent, ConfirmationComponent],
  imports: EXTERNAL_COMPONENTS,
  exports: [...EXTERNAL_COMPONENTS, ...INTERNAL_COMPONENTS],
  declarations: INTERNAL_COMPONENTS
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        ToHttpsPipe
      ]
    };
  }
}
