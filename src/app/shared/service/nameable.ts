import { Observable } from 'rxjs';

export interface Nameable {
  getName(key: string): Observable<string>;
}
