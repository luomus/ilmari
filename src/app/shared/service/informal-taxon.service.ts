import { Injectable } from '@angular/core';
import { DBService } from './db.service';
import { LajiApi, LajiApiService } from './laji-api.service';
import { ExtendedInformalTaxonGroup } from '../../models/informal-taxon-group';
import { TableInterface } from './db/table.interface';
import { environment } from '../../../environments/environment';
import { Nameable } from './nameable';
import { forkJoin, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InformalTaxonService implements Nameable {

  table: TableInterface<ExtendedInformalTaxonGroup>;

  constructor(
    private db: DBService,
    private lajiApi: LajiApiService
  ) {
    this.table = this.db.table('informalGroup');
  }

  getName(key: string): Observable<string> {
    return this.table.get(key).pipe(map(group => group && group.name || ''));
  }

  getRoots(): Observable<ExtendedInformalTaxonGroup[]> {
    const query$ = this.table.search('_master',  1);

    return query$.pipe(
      switchMap(roots => roots && roots.length > 0 ?
        of(roots) :
        this.updateData().pipe(map(groups => groups.filter(group => group._master === 1)))
      ),
      map(roots => roots.sort((a, b) =>
        environment.masterGroups.indexOf(a.id) -
        environment.masterGroups.indexOf(b.id)
      ))
    );
  }

  updateData(): Observable<ExtendedInformalTaxonGroup[]> {
    return forkJoin(
        this.lajiApi.getList(LajiApi.Endpoint.InformalTaxonGroup, {pageSize: 1000}).pipe(map(result => result.results)),
        this.lajiApi.getList(LajiApi.Endpoint.InformalTaxonGroupRoots).pipe(map(groups => groups.results.map(group => group.id)))
      ).pipe(
        map(data => data[0].map(group => ({
          ...group,
          _root: data[1].indexOf(group.id) > -1 ? 1 : 0,
          _master: environment.masterGroups.indexOf(group.id) > -1 ? 1 : 0
        }))),
        switchMap(groups => this.table.bulkPut(groups).pipe(switchMap(success => of(success ? groups : []))))
      );
  }

}
