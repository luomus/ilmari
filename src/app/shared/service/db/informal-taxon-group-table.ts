import { Table } from './table';
import { ExtendedInformalTaxonGroup } from '../../../models/informal-taxon-group';
import { from, Observable } from 'rxjs';

export class InformalTaxonGroupTable extends Table<ExtendedInformalTaxonGroup> {

  search(index?: string, value?: any): Observable<any> {
    if (index) {
      return from(this.db.where(index).equals(value).toArray());
    }
    return from(this.db.toArray());
  }
}
