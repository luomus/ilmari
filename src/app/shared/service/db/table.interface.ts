import { Observable } from 'rxjs';

export interface TableInterface<T> {
  search(index?: string, value?: any): Observable<any>;
  exists(key): Observable<boolean>;
  keys(): Observable<string[]>;
  get(key: string): Observable<T>;
  put(model: T, key?: string): Observable<T>;
  remove(key: string): Observable<boolean>;
  bulkPut(models: T[]): Observable<boolean>;
}
