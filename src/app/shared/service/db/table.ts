import Dexie from 'dexie';
import { from, Observable } from 'rxjs';
import { TableInterface } from './table.interface';
import { map } from 'rxjs/operators';

export class Table<T> implements TableInterface<T> {

  constructor(protected db: Dexie.Table<T, string>) {}

  search(index?: string, value?: any): Observable<any> {
    return from(this.db.toCollection().toArray());
  }

  keys(): Observable<string[]> {
    return from(this.db.toCollection().keys() as any);
  }

  exists(key): Observable<boolean> {
    return from(this.db.get(key)).pipe(map(value => !!value));
  }

  get(key: string): Observable<T> {
    return from(this.db.get(key));
  }

  remove(key: string): Observable<boolean> {
    return from(this.db.delete(key)).pipe(
      map(() => true)
    );
  }

  put(model: any, id: string): Observable<T> {
    id = id || model.id || '';
    return from(this.db.put(model, id)).pipe(
      map(() => model)
    );
  }

  bulkPut(models: T[]): Observable<boolean> {
    return from(this.db.bulkPut(models)).pipe(
      map((data) => !!data)
    );
  }
}
