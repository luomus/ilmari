import Dexie from 'dexie';
import { Table } from './table';
import { ExtendedTaxon, TaxaGrid, TaxaResult, TaxonModel, TaxonParent } from '../../../models/taxon';
import { TableInterface } from './table.interface';
import { BaseFilterTaxa } from '../../../taxon/models/base-filter-taxa';
import { environment } from '../../../../environments/environment';
import { forkJoin, from, Observable, of } from 'rxjs';
import { map, publishReplay, refCount, switchMap } from 'rxjs/operators';

export class TaxonTable extends Table<ExtendedTaxon> {

  private taxa;
  private cacheKey;

  constructor(protected db: Dexie.Table<ExtendedTaxon, string>,
              protected taxonParent: TableInterface<TaxonParent>,
              protected taxonGrid: TableInterface<TaxaGrid[]>) {
    super(db);
  }

  get(key: string): Observable<ExtendedTaxon>;
  get(key: string, resolveRelations?, includeParents?): Observable<ExtendedTaxon|TaxonParent|TaxonModel>;
  get(
    key: string,
    resolveRelations = true,
    includeParents = false
  ): Observable<ExtendedTaxon|TaxonParent|TaxonModel> {
    const addRelations = (taxon: ExtendedTaxon) => forkJoin(
        of(taxon),
        this.taxonParent.search('id', taxon._belongsTo.filter(value => value && value.indexOf('MX.') === 0)),
        this.taxonGrid.get(taxon.id)
      ).pipe(
        map(data => ({
          ...data[0],
          _grid: data[2],
          _parents: data[0]._belongsTo
            .reduce((cumulative, current) => current && current.indexOf('MX.') === 0 ?
              cumulative.concat(data[1].filter(parent => parent.id === current)) : cumulative, [])
        }))
      );

    const species = from(this.db.get(key)).pipe(
      switchMap(taxon =>
        resolveRelations ? addRelations(taxon) : of(taxon)
      ));

    const speciesNParent = forkJoin(
      from(this.db.get(key)),
      this.taxonParent.get(key)
    ).pipe(
      map(data => data[0] || data[1])
    );

    return includeParents ? speciesNParent : species;
  }


  put(model: ExtendedTaxon, id: string, updateRelations = false): Observable<ExtendedTaxon> {
    const relations$ = updateRelations ?
      forkJoin(
        this.taxonGrid.put(model._grid, model.id),
        this.taxonParent.bulkPut(model._parents)
      ) :
      of(null);

    return relations$.pipe(
      switchMap(() => from(this.db.put({...model, _grid: [], _parents: []}))),
      map(() => model)
    );
  }

  search(index?: string, value?: any, filter?: BaseFilterTaxa): Observable<TaxaResult> {
    const offset = filter.pageSize * ((filter.page || 1) - 1);
    const cacheKey = this.getCacheKey(filter);
    if (this.cacheKey !== cacheKey) {
      this.taxa = false;
      this.cacheKey = cacheKey;
    }
    if (!this.taxa) {
      const belong$ = (filter.name ?
        from(this.db.where('_belongsTo')
          .anyOf(filter.belongsTo)
          .distinct()
          .and((row) => row.finnish === 1)
          .and((row) => this.matchName(row, filter))
          .toArray()) :
        from(this.db.where('_belongsTo')
          .anyOf(filter.belongsTo)
          .distinct()
          .and((row) => row.finnish === 1)
          .toArray())
      ).pipe(
        map(data => ({
          count: data.length,
          taxa: data.map(taxon => {
            taxon._grid = [];
            taxon._thumbnail = this.getThumbnail(taxon);
            return taxon;
          })
        }))
      );

      const master$ = (filter.name ?
        from(this.db.where('[_masterGroup+finnish]')
          .equals([...filter.belongsTo, 1])
          .and((row) => this.matchName(row, filter))
          .toArray()) :
        from(this.db.where('[_masterGroup+finnish]')
          .equals([...filter.belongsTo, 1])
          .toArray())
      ).pipe(
        map(data => ({
          count: data.length,
          taxa: data.map(taxon => {
            taxon._grid = [];
            taxon._thumbnail = this.getThumbnail(taxon);
            return taxon;
          })
        }))
      );

      this.taxa =
        this.canUserMasterGroup(filter) ?
          master$ :
          belong$.pipe(
            publishReplay(1),
            refCount()
          );
    }
    return this.taxa;
  }

  localAutocomplete(index: string, value: string, groups?: string[], limit = 10) {
    return from(this.db.where(index)
      .startsWithIgnoreCase(value)
      .and((row) => row._used && (groups ? row.informalTaxonGroups.some((val) => groups.includes(val)) : true))
      .limit(limit)
      .reverse()
      .sortBy('_count')
    );
  }

  invalidateCache() {
    this.cacheKey = '';
  }

  private getThumbnail(taxon: ExtendedTaxon) {
    const multimedia = taxon.multimedia || taxon._media;
    if (!(taxon._selectedMedia && multimedia)) {
      return;
    }
    const img = multimedia.filter(media => media.id === taxon._selectedMedia)[0];
    if (!img) {
      return;
    }
    return img.thumbnailURL || img.largeURL || img.fullURL;
  }

  private matchName(taxon: ExtendedTaxon, filter: BaseFilterTaxa): boolean {
    if (!filter.name) {
      return true;
    }
    if (taxon.vernacularName && taxon.vernacularName.indexOf(filter.name) > -1) {
      return true;
    }
    return (taxon.scientificName && taxon.scientificName.toLocaleLowerCase().indexOf(filter.name) > -1) ||
      (taxon.vernacularName && taxon.vernacularName.indexOf(filter.name) > -1);
  }

  private canUserMasterGroup(filter: BaseFilterTaxa) {
    if (!filter || filter.belongsTo.length !== 1) {
      return false;
    }
    return environment.masterGroups.indexOf(filter.belongsTo[0]) > -1;
  }

  private getCacheKey(filter: BaseFilterTaxa) {
    return [filter.pageSize, filter.page, filter.name, filter.belongsTo.join(',')].join(':');
  }
}
