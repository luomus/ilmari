import { TableInterface } from './table.interface';
import { Observable, of } from 'rxjs';

export class NoopTable<T> implements TableInterface<T> {

  constructor() {}

  exists(key): Observable<boolean> {
    return of(false);
  }

  search(index?: string, value?: any): Observable<any> {
    return of([]);
  }

  keys(): Observable<string[]> {
    return of([]);
  }

  get(key: string): Observable<T> {
    return of(undefined);
  }

  remove(key: string): Observable<boolean> {
    return of(true);
  }

  put(model: any, id: string): Observable<T> {
    return of(model);
  }

  bulkPut(models: T[]): Observable<boolean> {
    return of(true);
  }
}
