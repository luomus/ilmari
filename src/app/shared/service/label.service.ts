import { Injectable } from '@angular/core';
import { DBService } from './db.service';
import { LajiApi, LajiApiService } from './laji-api.service';
import { Nameable } from './nameable';
import { TableInterface } from './db/table.interface';
import { InformalTaxonService } from './informal-taxon.service';
import { TaxonService } from './taxon.service';
import { Publication } from '../../models/publication';
import { Checklist } from '../../models/checklist';
import { PersonModel } from '../../models/person';
import { from, Observable, of } from 'rxjs';
import { catchError, concatMap, map, mergeMap, publishReplay, refCount, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LabelService implements Nameable {

  private static metadataUpdated = false;
  private static cache = {};
  table: TableInterface<string>;

  metadataClasses = [
    'MX.taxon',
    'MY.document',
    'MY.gathering',
    'MY.gatheringFactClass',
    'MY.unit',
    'MY.unitFactClass',
    'MY.identification',
    'MY.typeSpecimen',
  ];

  private pendingMetadata;

  constructor(
    private db: DBService,
    private lajiApi: LajiApiService,
    private taxonService: TaxonService,
    private informalGroup: InformalTaxonService,
  ) {
    this.table = this.db.table('label');
    this.updateMetadata().subscribe();
  }

  getName(key: string): Observable<string> {
    if (key.length === 0) {
      return of(key);
    }
    if (LabelService.cache[key]) {
      return of(LabelService.cache[key]);
    }
    return this.getFromSource(key).pipe(map(label => label || key));
  }

  private getFromSource(key: string): Observable<string> {
    const parts = key.split('.', 2);

    if (!parts[1]) {
      return of(key);
    }

    if (!parts[1].match(/^[0-9]+$/)) {
      return this.getLabel(key, this.updateMetadata());
    }

    switch (parts[0]) {
      case 'MVL':
        return this.informalGroup.getName(key);
      case 'MX':
        return this.taxonService.getName(key);
      case 'MA':
        return this.getLabel(key, this.updateFromApi<PersonModel>(LajiApi.Endpoint.personById, key, (data) => data.fullName));
      case 'MR':
        return this.getLabel(key, this.updateFromApi<Checklist>(LajiApi.Endpoint.checklist, key, (data) => data['dc:bibliographicCitation']));
      case 'MP':
        return this.getLabel(key, this.updateFromApi<Publication>(LajiApi.Endpoint.publication, key, (data) => data['dc:bibliographicCitation']));
      default:
        return this.getLabel(key, this.updateMetadata());
    }
  }

  private getLabel(key: string, remote: Observable<void>): Observable<string> {
    return this.table.get(key).pipe(
      switchMap(label => label ? of(label) : remote.pipe(concatMap(() => this.table.get(key)))),
      tap((label) => LabelService.cache[key] = label),
      catchError(() => of(key))
    );
  }

  private updateFromApi<T>(endpoint: LajiApi.Endpoint, id: string, label: (data: T) => string): Observable<void> {
    return this.lajiApi.getEntry<T>(endpoint, id).pipe(
      switchMap(data => this.table.put(label(data), id)),
      map(() => void 0)
    );
  }

  private updateMetadata(): Observable<void> {
    return new Observable(observer => {
      if (LabelService.metadataUpdated) {
        observer.next();
        observer.complete();
        return;
      }

      if (!this.pendingMetadata) {
        this.pendingMetadata = from(this.metadataClasses).pipe(
          mergeMap(className => this.lajiApi
            .getEntry(LajiApi.Endpoint.classRanges, className, {asLookupObject: true})),
          publishReplay(),
          refCount()
        );
      }

      const hasUpdated = {};

      function updateDone() {
        observer.next();
        observer.complete();
        LabelService.metadataUpdated = true;
      }

      this.pendingMetadata
        .subscribe(
          (lookup) => {
            Object.keys(lookup).map((key) => {
              if (!hasUpdated[key] && lookup[key]) {
                this.table.put(lookup[key], key).subscribe();
              }
            });
          },
          () => updateDone(),
          () => updateDone()
        );
    });
  }
}
