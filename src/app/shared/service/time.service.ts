import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  public readonly hours = [
    '00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11',
    '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'
  ];

  public readonly minutes = [
    '00', '01', '02', '03', '04', '05', '06', '07', '08', '09',
    '10', '11', '12', '13', '14', '15', '16', '17', '18', '19',
    '20', '21', '22', '23', '24', '25', '26', '27', '28', '29',
    '30', '31', '32', '33', '34', '35', '36', '37', '38', '39',
    '40', '41', '42', '43', '44', '45', '46', '47', '48', '49',
    '50', '51', '52', '53', '54', '55', '56', '57', '58', '59'
  ];

  /**
   * Return current datetime in ISO time format
   *
   * @returns {string}
   */
  getCurrentTime(): string {
    return this.dateToDateTimeString(new Date());
  }

  dateToDateTimeString(date: Date, full = true) {
    return this.dateToDateString(date) +
      'T' + this.getHours(date) +
      ':' + this.getMinutes(date) +
      (full ? ':' + this.pad(date.getSeconds()) : '');
  }

  dateToDateString(date?: Date) {
    if (!date) {
      date = new Date();
    }
    return date.getFullYear() +
      '-' + this.pad(date.getMonth() + 1) +
      '-' + this.pad(date.getDate());
  }

  getHours(date?: Date) {
    if (!date) {
      date = new Date();
    }
    return '' + this.pad(date.getHours());
  }

  getMinutes(date?: Date) {
    if (!date) {
      date = new Date();
    }
    return '' + this.pad(date.getMinutes());
  }

  parseDate(date: string): {date?: string, hour?: string, minute?: string} {
    const parts = date.split(/[\sT]/, 2);
    if (parts.length === 0) {
      return {};
    } else if (parts.length === 1) {
      return {
        date: parts[0]
      };
    }
    return {
      ...this.getTime(parts[1]),
      date: parts[0]
    };
  }

  cmpTime(a: string, b: string): -1|0|1 {
    const aD = new Date(a);
    const bD = new Date(b);
    return aD > bD ? -1 : (aD < bD ? 1 : 0);
  }

  private getTime(time: string): {hour?: string, minute?: string} {
    const timeReg = new RegExp('([0-9]{1,2}):([0-9]{1,2})');
    const matches = time.match(timeReg);
    if (!matches) {
      return {};
    }
    return {
      hour: matches[1],
      minute: matches[2]
    };
  }

  private pad(number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }
}
