import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserData } from '../../models/user-data';
import { DBService } from './db.service';
import { TableInterface } from './db/table.interface';
import { Profile } from '../../models/profile';
import { Form } from '../../models/form';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  table: TableInterface<SettingsInterface>;

  constructor(private db: DBService) {
    this.table = this.db.table('settings');
  }

  get(id: Settings.tripForm): Observable<Form>;
  get(id: Settings.profile): Observable<Profile>;
  get(id: Settings.activeList): Observable<string>;
  get(id: Settings.userData): Observable<UserData>;
  get(id: Settings.downloaded): Observable<{[key: string]: boolean}>;
  get(id: Settings): Observable<any> {
    return this.table.get(id);
  }

  set(id: Settings.tripForm, data: Form);
  set(id: Settings.profile, data: Profile);
  set(id: Settings.activeList, data: string);
  set(id: Settings.userData, data: UserData);
  set(id: Settings.downloaded, data: {[key: string]: boolean});
  set(id: Settings, data: any) {
    return this.table.put(data, id);
  }

  remove(id: Settings) {
    return this.table.remove(id);
  }
}

export interface SettingsInterface {
  id: string;
  value: any;
}

export enum Settings {
  downloaded = 'downloaded',
  activeList = 'activeList',
  userData = 'userData',
  profile = 'profile',
  tripForm = 'tripForm'
}
