import { Injectable } from '@angular/core';
import { ExtendedTaxon, TaxaGrid, TaxaResult, TaxonAutocomplete, TaxonModel } from '../../models/taxon';
import { DBService } from './db.service';
import { LajiApi, LajiApiService } from './laji-api.service';
import { environment } from '../../../environments/environment';
import { PagedResult } from '../../models/paged-result';
import { BaseFilterTaxa } from '../../taxon/models/base-filter-taxa';
import { WarehouseAggregateResult } from '../../models/warehouse-aggregate-result';
import { TaxonTable } from './db/taxon-table';
import { Nameable } from './nameable';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Proxy, ProxyService } from './proxy.service';

const MAX_DAY_DIFF = 101;

@Injectable({
  providedIn: 'root'
})
export class TaxonService implements Nameable {
  taxonTable: TaxonTable;

  private taxa;

  constructor(
    private db: DBService,
    private lajiApi: LajiApiService,
    private proxyService: ProxyService
  ) {
    this.taxonTable = this.db.table('taxa');
  }

  static getName(taxon: TaxonModel) {
    if (!taxon) {
      return '';
    }
    if (taxon.vernacularName && taxon.scientificName) {
      return taxon.vernacularName + ' – ' + taxon.scientificName;
    }
    return taxon.vernacularName || taxon.scientificName;
  }

  autocompleteTaxon(value, group?: string[]): Observable<TaxonAutocomplete[]> {
    if (value.length <= 3) {
      return this.findName(value, group).pipe(
        map((taxa: ExtendedTaxon[]): TaxonAutocomplete[] => taxa.map(taxon => ({key: taxon.id, value: taxon.vernacularName })))
      );
    }
    return this.lajiApi.getList(
      LajiApi.Endpoint.autocompleteTaxon,
      {
        q: value,
        onlySpecies: true,
        onlyFinnish: true,
        matchType: 'exact,partial',
        informalTaxonGroup: group ? group.join(',') : undefined
      }
    );
  }

  findName(value: string, group?: string[]): Observable<ExtendedTaxon[]> {
    return this.taxonTable.localAutocomplete('vernacularName', value, group);
  }

  getName(key: string): Observable<string> {
    return this.get(key).pipe(map(TaxonService.getName));
  }

  get(id: string): Observable<ExtendedTaxon> {
    // return this.getTaxonFromApi(id);
    return this.taxonTable.exists(id).pipe(
      switchMap(exists => exists ?
        this.taxonTable.get(id) :
        this.getTaxonFromApi(id).pipe(
          tap(data => this.taxonTable.put(data, data.id, true).subscribe())
        )
      )
    );
  }

  getAll(filter: BaseFilterTaxa): Observable<TaxaResult> {
    if (!filter || !filter.belongsTo) {
      return this.empty();
    }
    return this.taxonTable.search('_belongsTo', filter.belongsTo, filter);
  }

  put(data: ExtendedTaxon, updateRelations = false) {
    return this.taxonTable.put(data, data.id, updateRelations);
  }

  downloadByInformalGroup(groupID: string, page = 1): Observable<number> {
    return this.lajiApi.getEntry(
      LajiApi.Endpoint.taxonSpecies,
      environment.rootTaxon,
      {pageSize: 10, page: page, onlyFinnish: true, sortOrder: 'taxonomic', informalGroupFilters: groupID}
    ).pipe(
      switchMap((result: PagedResult<TaxonModel>) => forkJoin(result.results.map(taxon => this.get(taxon.id))).pipe(switchMap(() => of(result.lastPage)))
    ));
  }

  invalidateCache() {
    this.taxonTable.invalidateCache();
  }

  private empty(): Observable<TaxaResult> {
    return of({count: 0, taxa: []});
  }

  private getTaxonFromApi(id): Observable<ExtendedTaxon> {
    const timeLimit = this.getTimeLimit();

    const lajiApi$ =  forkJoin(
        this.lajiApi.getEntry(LajiApi.Endpoint.taxon, id).pipe(
          map(taxon => ({
              ...taxon,
              species: taxon.species as any === true ? 1 : 0,
              finnish: taxon.finnish as any === true ? 1 : 0,
              invasiveSpecies: taxon.invasiveSpecies as any === true ? 1 : 0,
              stableInFinland: taxon.stableInFinland as any === true ? 1 : 0
            }))
        ),
        this.lajiApi.getEntry(LajiApi.Endpoint.taxonMedia, id, {externalSources: 'eol:api,naturforskaren:species'}).pipe(
          map(images => images.map(image => ({...image, id: image.id || image.fullURL || image.largeURL})))
        ),
        this.lajiApi.getEntry(LajiApi.Endpoint.taxonDescriptions, id, {externalSources: 'eol:api,naturforskaren:species'}),
        this.lajiApi.getEntry(LajiApi.Endpoint.taxonParents, id).pipe(
          map(parents => parents.map((parent: TaxonModel) => ({
            id: parent.id,
            taxonRank: parent.taxonRank,
            vernacularName: parent.vernacularName,
            scientificName: parent.scientificName
          })))
        ),
        this.lajiApi.getList(LajiApi.Endpoint.warehouseAggregate, {
          taxonId: id,
          time: timeLimit,
          pageSize: 10000,
          aggregateBy: 'gathering.conversions.dayOfYearBegin,gathering.conversions.dayOfYearEnd'
        }).pipe(
          map((result) => this.getAggregatedDateCounts(result))
        ),
        this.lajiApi.getList(LajiApi.Endpoint.warehouseAggregate, {
          taxonId: id,
          time: timeLimit,
          pageSize: 10000,
          onlyCount: false,
          aggregateBy: 'gathering.conversions.ykj10kmCenter.lat,gathering.conversions.ykj10kmCenter.lon'
        }).pipe(
            map((result) => this.getAggregatedMap(result))
          )
      ).pipe(
        map<any, ExtendedTaxon>(data => ({
          ...data[0],
          _media: data[1],
          _parents: data[3],
          _belongsTo: [data[0].id].concat(data[3].map(parent => parent.id)).concat(data[0].informalTaxonGroups),
          _description: data[2].filter(description => description.id === 'default')[0] || data[2][0] || undefined,
          _masterGroup: this.getMasterGroup(data[0].informalTaxonGroups),
          _descriptionCount: data[0].descriptions ? data[0].descriptions.length : 0,
          _selectedMedia: data[1][0] ? data[1][0].id : undefined,
          _updated: Math.floor(Date.now() / 1000),
          _count: data[4].total,
          _weeks: data[4].weeks,
          _grid: data[5]
        }))
    );

    // return lajiApi$;
    return this.proxyService.getEntry(Proxy.Endpoint.taxon, id).pipe(
      catchError((err) => lajiApi$)
    );
  }

  private getMasterGroup(groups: string[] = []) {
    for (const group of environment.masterGroups) {
      if (groups.indexOf(group) > -1) {
        return group;
      }
    }
    return '';
  }

  private getTimeLimit() {
    const now = new Date();
    if (now.getMonth() < 1) {
      return (now.getFullYear() - 12) + '/' + (now.getFullYear() - 2);
    }
    return (now.getFullYear() - 11) + '/' + (now.getFullYear() - 1);
  }

  private getAggregatedMap(aggregateResult: PagedResult<WarehouseAggregateResult>): TaxaGrid[] {
    return aggregateResult.results.map((result) => ({
      lat: +result.aggregateBy['gathering.conversions.ykj10kmCenter.lat'],
      lon: +result.aggregateBy['gathering.conversions.ykj10kmCenter.lon'],
      count: result.count,
      individualCountMax: result.individualCountMax,
      individualCountSum: result.individualCountSum,
      newestRecord: result.newestRecord,
      oldestRecord: result.oldestRecord
    }));
  }

  private getAggregatedDateCounts(aggregateResult: PagedResult<WarehouseAggregateResult>): {total: number, weeks: {[w: number]: number}} {
    const weeks = {};
    let total = 0;
    aggregateResult.results.map((result) => {
      const begin = +result.aggregateBy['gathering.conversions.dayOfYearBegin'];
      const end = +result.aggregateBy['gathering.conversions.dayOfYearEnd'];
      total += result.count;
      if ((end - begin) > MAX_DAY_DIFF) {
        return;
      }
      const beginWeek = Math.ceil(begin / 7);
      const endWeek = Math.ceil(end / 7);
      for (let w = beginWeek; w <= endWeek; w++) {
        if (!weeks[w]) {
          weeks[w] = 0;
        }
        weeks[w] += result.count;
      }
    });
    if (typeof weeks['53'] !== 'undefined') {
      weeks['53'] = (weeks['52'] + weeks['53']) / 2;
      delete weeks['53'];
    }
    return {
      total: total,
      weeks: weeks
    };
  }
}
