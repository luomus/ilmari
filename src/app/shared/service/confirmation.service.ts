import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmationComponent } from '../component/confirmation/confirmation.component';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfirmationService {

  constructor(public dialog: MatDialog) { }

  confirm(type) {
    return new Observable((observer) => {
      const dialogRef = this.dialog.open(ConfirmationComponent, {
        width: '250px',
        data: type
      });
      dialogRef.afterClosed().subscribe(result => {
        observer.next( result || false );
        observer.complete();
      });
    });

  }
}
