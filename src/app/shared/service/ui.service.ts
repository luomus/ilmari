import { Injectable } from '@angular/core';
import { fromEvent, of } from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class UiService {

  scrollElement$;

  constructor() {
    this.scrollElement$ = typeof document === 'object' ?
      fromEvent(document.querySelector('.mat-sidenav-content'), 'scroll') :
      of(null);
  }

}
