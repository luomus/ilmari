import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ExtendedTaxon } from '../../models/taxon';

@Injectable({
  providedIn: 'root'
})
export class ProxyService {

  constructor(private http: HttpClient, @Inject(LOCALE_ID) private locale: string) {
    if (this.locale.length > 2) {
      this.locale = 'fi';
    }
  }

  getEntry(endpoint: Proxy.Endpoint.taxon, id: string): Observable<ExtendedTaxon>;
  getEntry<T>(endpoint: EndPoints, id: string): Observable<T> {
    return this.http.get<T>(`${this.getBase(endpoint, id)}`);
  }


  private getBase(endpoint: EndPoints, id?: string): string {
    if (id) {
      return `${environment.proxy}/${this.locale}/${endpoint}`.replace('%id%', id);
    }
    return `${environment.proxy}/${this.locale}/${endpoint}`;
  }
}

export type EndPoints = Proxy.Endpoint;

export namespace Proxy {

  export enum Endpoint {
    taxon = 'taxon/%id%.json'
  }
}
