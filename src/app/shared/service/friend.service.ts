import { Injectable } from '@angular/core';
import { TaxonAutocomplete } from '../../models/taxon';
import { Observable } from 'rxjs';
import { LajiApi, LajiApiService } from './laji-api.service';
import * as fromUser from '../../user/store';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class FriendService {

  private personToken: string;

  constructor(
    private lajiApi: LajiApiService,
    private store: Store<fromUser.State>
  ) {
    this.store.select(fromUser.getPersonToken)
      .subscribe(token => this.personToken = token);
  }

  autocompleteTaxon(value): Observable<TaxonAutocomplete[]> {
    return this.lajiApi.getList(
      LajiApi.Endpoint.autocompleteFriends,
      {
        q: value,
        personToken: this.personToken,
        includeSelf: true
      } as LajiApi.AutocompleteFriendQuery
    );
  }
}
