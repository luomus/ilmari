import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { PagedResult } from '../../models/paged-result';
import { InformalTaxonGroup } from '../../models/informal-taxon-group';
import { FormSchemaModel } from '../../models/form-schema';
import { WarehouseAggregateResult } from '../../models/warehouse-aggregate-result';
import { PersonModel } from '../../models/person';
import { TaxonAutocomplete, TaxonDescription, TaxonImage, TaxonModel } from '../../models/taxon';
import { Publication } from '../../models/publication';
import { Checklist } from '../../models/checklist';
import { Profile } from '../../models/profile';
import { Document } from '../../models/document/document';
import { Image } from '../../models/image';
import { Feedback } from '../../models/feedback';
import { Autocomplete } from '../../models/autocomplete';

@Injectable({
  providedIn: 'root'
})
export class LajiApiService {

  constructor(private http: HttpClient, @Inject(LOCALE_ID) private locale: string) {
    if (this.locale.length > 2) {
      this.locale = 'fi';
    }
  }

  addEntry(endpoint: LajiApi.Endpoint.feedback, query: LajiApi.FeedbackQuery, data: Feedback): Observable<void>;
  addEntry(endpoint: LajiApi.Endpoint.image, query: LajiApi.TmpImageQuery, data: Image, id: string): Observable<Image>;
  addEntry(endpoint: LajiApi.Endpoint.images, query: LajiApi.ImageQuery, data: any): Observable<{'name': string, 'filename': string, 'id': string, 'expires': number}[]>;
  addEntry(endpoint: LajiApi.Endpoint.documents, query: LajiApi.DocumentQuery, data: Document): Observable<Document>;
  addEntry(endpoint: EndPoints, query: any, data: any, id?: string): Observable<any> {
    return this.http
      .post(this.getBase(endpoint, id), data, {params: this.getParams(query, endpoint)});
  }

  deleteEntry(endpoint: LajiApi.Endpoint.personToken, id: string): Observable<void>;
  deleteEntry(endpoint: LajiApi.Endpoint.image, id: string, query: LajiApi.ImageQuery): Observable<boolean>;
  deleteEntry(endpoint: EndPoints, id: string, query: {[param: string]: any} = {}): Observable<any> {
    return this.http
      .delete(`${this.getBase(endpoint, id)}`, {params: this.getParams(query, endpoint)});
  }

  getEntry(endpoint: LajiApi.Endpoint.checklist, id: string, query?: LajiApi.LangQuery): Observable<Checklist>;
  getEntry(endpoint: LajiApi.Endpoint.publication, id: string, query?: LajiApi.LangQuery): Observable<Publication>;
  getEntry(endpoint: LajiApi.Endpoint.form, id: string, query?: LajiApi.FormQuery): Observable<FormSchemaModel>;
  getEntry(endpoint: LajiApi.Endpoint.person, id: string): Observable<PersonModel>;
  getEntry(endpoint: LajiApi.Endpoint.profile, id: string): Observable<Profile>;
  getEntry(endpoint: LajiApi.Endpoint.taxon, id: string, query?: LajiApi.TaxonQuery): Observable<TaxonModel>;
  getEntry(endpoint: LajiApi.Endpoint.taxonMedia, id: string, query?: LajiApi.TaxonQuery): Observable<TaxonImage[]>;
  getEntry(endpoint: LajiApi.Endpoint.taxonParents, id: string, query?: LajiApi.TaxonQuery): Observable<TaxonModel[]>;
  getEntry(endpoint: LajiApi.Endpoint.taxonSpecies, id: string, query?: LajiApi.TaxonQuery): Observable<PagedResult<TaxonModel>>;
  getEntry(endpoint: LajiApi.Endpoint.taxonDescriptions, id: string, query?: LajiApi.TaxonQuery): Observable<TaxonDescription[]>;
  getEntry(endpoint: LajiApi.Endpoint.classes | LajiApi.Endpoint.classRanges, id: string, query: LajiApi.MetadataQuery): Observable<{[k: string]: string}>;
  getEntry<T>(endpoint: LajiApi.Endpoint, id: string, query?: {[param: string]: string | string[]}): Observable<T>;
  getEntry<T>(endpoint: EndPoints, id: string, query: {[param: string]: any} = {}): Observable<T> {
    return this.http
      .get<T>(
        `${this.getBase(endpoint, id)}`,
        {params: this.getParams(query, endpoint)}
      );
  }

  getList(endpoint: LajiApi.Endpoint.autocompleteTaxon, query: LajiApi.AutocompleteTaxonQuery): Observable<TaxonAutocomplete[]>;
  getList(endpoint: LajiApi.Endpoint.autocompleteFriends, query: LajiApi.AutocompleteFriendQuery): Observable<Autocomplete[]>;
  getList(endpoint: LajiApi.Endpoint.documents, query: LajiApi.DocumentQuery): Observable<PagedResult<WarehouseAggregateResult>>;
  getList(endpoint: LajiApi.Endpoint.warehouseAggregate, query: LajiApi.WarehouseAggregatedQuery): Observable<PagedResult<WarehouseAggregateResult>>;
  getList(endpoint: LajiApi.Endpoint.warehouseList, query: LajiApi.WarehouseListQuery): Observable<PagedResult<any>>;
  getList(endpoint: LajiApi.Endpoint.InformalTaxonGroup | LajiApi.Endpoint.InformalTaxonGroupRoots, query?: LajiApi.InformalTaxonQuery): Observable<PagedResult<InformalTaxonGroup>>;
  getList<T>(endpoint: LajiApi.Endpoint, query: any = {}): Observable<T[]|PagedResult<T>> {
    return this.http
      .get<T[]|PagedResult<T>>(
        `${this.getBase(endpoint)}`,
        {params: this.getParams(query, endpoint)}
        );
  }

  private getParams(query: {[param: string]: queryParam}, endpoint: LajiApi.Endpoint): {[param: string]: string | string[]} {
    if (!query.lang &&
      ![LajiApi.Endpoint.warehouseAggregate, LajiApi.Endpoint.warehouseList].includes(endpoint)) {
      query.lang = this.locale;
      query.langFallback = false;
    }
    query.access_token = environment.lajiApi.token;
    const result: {[param: string]: string | string[]} = {};
    Object.keys(query).map(key => {
      const type = typeof query[key];
      if (type === 'boolean') {
        result[key] = query[key] === true ? 'true' : 'false';
      } else if (type === 'number') {
        result[key] = '' + query[key];
      } else {
        result[key] = <any>query[key];
      }
    });
    return result;
  }

  private getBase(endpoint: EndPoints, id?: string): string {
    if (id) {
      return `${environment.lajiApi.base}${endpoint}`.replace('%id%', id);
    }
    return `${environment.lajiApi.base}${endpoint}`;
  }
}

export type queryParam = boolean | number | string | string[];

export type langs = 'en' | 'sv' | 'fi';

export type EndPoints = LajiApi.Endpoint;

export namespace LajiApi {

  export enum Endpoint {
    autocompleteTaxon = 'autocomplete/taxon',
    autocompleteFriends = 'autocomplete/friends',
    checklist = 'checklists/%id%',
    documents = 'documents',
    document = 'documents/%id%',
    publication = 'publications/%id%',
    personToken = 'person-token/%id%',
    person = 'person/%id%',
    profile = 'person/%id%/profile',
    personById = 'person/by-id/%id%',
    form = 'forms/%id%',
    taxa = 'taxa',
    taxon = 'taxa/%id%',
    taxonMedia = 'taxa/%id%/media',
    taxonDescriptions = 'taxa/%id%/descriptions',
    taxonParents = 'taxa/%id%/parents',
    taxonSpecies = 'taxa/%id%/species',
    warehouseAggregate = 'warehouse/query/aggregate',
    warehouseList = 'warehouse/query/list',
    images = 'images',
    image = 'images/%id%',
    InformalTaxonGroup = 'informal-taxon-groups',
    InformalTaxonGroupRoots = 'informal-taxon-groups/roots',
    classes = 'metadata/classes',
    classRanges = 'metadata/classes/%id%/ranges',
    feedback = 'feedback'
  }

  export interface PagedLangQuery extends PagedQuery {
    lang?: langs;
    langFallback?: boolean;
  }

  export interface LangQuery {
    lang?: langs;
    langFallback?: boolean;
  }

  export interface FormQuery {
    lang?: langs;
    format?: 'json'|'schema';
  }

  export interface AutocompleteQuery {
    q?: string;
    lang?: langs;
    limit?: number;
    includePayload?: boolean;
  }

  export interface AutocompleteTaxonQuery extends AutocompleteQuery {
    checklist?: string;
    informalTaxonGroup?: string;
    matchType?: string;
    excludeNameTypes?: string;
    onlySpecies?: boolean;
    onlyFinnish?: boolean;
    onlyInvasive?: boolean;
  }

  export interface AutocompleteFriendQuery extends AutocompleteQuery {
    personToken: string;
    includeSelf?: boolean;
  }

  export interface MetadataQuery {
    lang?: langs;
    asLookupObject?: boolean;
  }

  export interface WarehouseQuery extends PagedQuery {
    orderBy?: string;
    onlyCount?: boolean;
    taxonId?: string;
    taxonRankId?: string;
    target?: string;
    time?: string;
    informalTaxonGroupId?: string;
    coordinates?: string;
    wgs84CenterPoint?: string;
    includeNonValidTaxa?: boolean;
    coordinateAccuracyMax?: number;
    editorOrObserverPersonToken?: string;
    qualityIssues?: '' | 'BOTH' | 'NO_ISSUES' | 'ONLY_ISSUES';
  }

  export interface WarehouseListQuery extends WarehouseQuery {
    selected?: string;
  }

  export interface WarehouseAggregatedQuery extends WarehouseQuery {
    aggregateBy?: string;
  }

  export interface DocumentQuery extends PagedQuery {
    personToken: string;
    observationYear?: number;
    templates?: boolean;
    namedPlace?: string;
    validationErrorFormat?: 'object'|'jsonPath';
    lang?: langs;
  }

  export interface FeedbackQuery {
    personToken?: string;
  }

  export interface PagedQuery {
    page?: number;
    pageSize?: number;
  }

  export interface TaxonQuery extends PagedLangQuery {
    externalSources?: string;
    informalGroupFilters?: string;
    includeMedia?: boolean;
    includeDescriptions?: boolean;
    onlyFinnish?: boolean;
    sortOrder?: 'taxonomic'|'scientific_name'|'finnish_name';
  }

  export interface InformalTaxonQuery extends PagedLangQuery {
    idIn?: string[];
  }

  export interface ImageQuery {
    personToken?: string;
  }
  export interface TmpImageQuery extends ImageQuery {
    tempId?: string;
  }
}
