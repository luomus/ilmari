import { ApplicationRef, Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { isPlatformBrowser } from '@angular/common';
import { MatSnackBar } from '@angular/material';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  checkInterval = 1000 * 60 * 10 ; // min

  hasUpdate: Observable<boolean>;
  lastCheck = 0;

  constructor(
    private updates: SwUpdate,
    private applicationRef: ApplicationRef,
    private snackbar: MatSnackBar,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.hasUpdate = updates.available.pipe(
      tap(() => {
        const snack = this.snackbar.open('Update Available', 'Reload', {duration: 6000});
        snack.onAction().subscribe(() => this.activateUpdate());
      }),
      map(() => true),
      catchError(() => of(false))
    );
  }

  activateUpdate(): void {
    this.updates.activateUpdate()
      .then(() => {
        if (isPlatformBrowser(this.platformId)) {
          window.location.reload();
        }
      });
  }

  checkForUpdate() {
    const now = new Date().getTime();
    if (this.lastCheck + this.checkInterval < now) {
      this.lastCheck = now;
      this.updates.checkForUpdate()
        .then(() => {})
        .catch(() => {});
    }
  }

}
