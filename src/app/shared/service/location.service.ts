import { Inject, Injectable, NgZone, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { EMPTY, Observable, Observer, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import proj4 from 'proj4';
import LatLngBounds = google.maps.LatLngBounds;

const POSITION_OPTIONS = {
  enableHighAccuracy: true,
  timeout: 10000,
  maximumAge: 10000
};

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private lastLat;
  private lastLng;
  private distanceFromLastSpot = 0.05;
  private minAccuracy = 30;
  private geoLocationEnabled = false;
  private watchId;
  private lock;
  private subject = new Subject<{lat: number, lng: number}>();
  location$ = this.subject.asObservable();

  constructor(
    @Inject(PLATFORM_ID) private platformId: string,
    private ngZone: NgZone
  ) {
    if (isPlatformBrowser(platformId)) {
      this.geoLocationEnabled = !!window.navigator.geolocation && !!window.navigator.geolocation.watchPosition;
      proj4.defs('WGS84', '+title=*GPS (WGS84) (deg) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees');
      proj4.defs('EPSG:2393', '+title=KKJ Zone 3 +proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=3500000 +y_0=0 +ellps=intl ' +
        '+towgs84=-96.0617,-82.4278,-121.7435,4.80107,0.34543,-1.37646,1.4964 +units=m +no_defs');
    }
  }

  /**
   * Is recording active
   *
   * @returns {boolean}
   */
  isRecording(): boolean {
    return !!this.watchId;
  }

  /**
   * Start recording
   *
   * Param values are in meters
   *
   * @param {number} distanceFromLast
   */
  startRecording(distanceFromLast = 50) {
    this.distanceFromLastSpot = distanceFromLast / 1000;
    this.minAccuracy = Math.max(Math.ceil(distanceFromLast / 10), 10);

    if (this.geoLocationEnabled) {
      const navigator = <any>window.navigator;
      if (navigator.requestWakeLock) {
        this.lock = navigator.requestWakeLock('gps');
      }
      this.ngZone.runOutsideAngular(() => {
        this.watchId = navigator.geolocation
          .watchPosition(this.addLocation.bind(this), undefined, POSITION_OPTIONS);
      });
    }
  }

  /**
   * Stop recording location
   */
  stopRecording() {
    if (this.watchId) {
      window.navigator.geolocation.clearWatch(this.watchId);
      this.watchId = undefined;
      this.lastLat = undefined;
      this.lastLng = undefined;
    }
    if (this.lock) {
      this.lock.unlock();
    }
  }

  /**
   * Get current location observable
   *
   * @returns {Observable<Position>}
   */
  getCurrentLocation(format: 'wgs'|'ykj' = 'wgs'): Observable<Position> {
    if (!this.geoLocationEnabled) {
      return EMPTY;
    }
    return new Observable((observer: Observer<Position>) => {
      window.navigator.geolocation.getCurrentPosition(
        (position: Position) => {
          observer.next(position);
          observer.complete();
        },
        (error: PositionError) => {
          observer.error(error);
          observer.complete();
        },
        POSITION_OPTIONS
      );
    }).pipe(
      map(value => format === 'wgs' ? value : {
        ...value,
        coords: this.convertToWgsToYkj(value.coords)
      })
    );
  }

  /**
   * Distance between wgs84 coordinates in km
   *
   * @param lat1
   * @param lon1
   * @param lat2
   * @param lon2
   * @returns {number}
   */
  distance(lat1, lon1, lat2, lon2): number {
    const p = 0.017453292519943295;    // Math.PI / 180
    const c = Math.cos;
    const a = 0.5 - c((lat2 - lat1) * p) / 2 +
      c(lat1 * p) * c(lat2 * p) *
      (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  }

  convertToWgsToYkj(coord: Coordinates): Coordinates {
    const spots = this.toInteger(proj4('WGS84', 'EPSG:2393', [coord.longitude, coord.latitude]));
    return {
      ...coord,
      longitude: spots[0],
      latitude: spots[1]
    };
  }

  coordinatesFromBounds(bounds: LatLngBounds): string {
    if (!bounds) {
      return '';
    }
    const swLat = bounds.getSouthWest().lat();
    const swLng = bounds.getSouthWest().lng();
    const neLat = bounds.getNorthEast().lat();
    const neLng = bounds.getNorthEast().lng();
    return Math.min(swLat, neLat).toFixed(6) + ':' +
      Math.max(swLat, neLat).toFixed(6) + ':' +
      Math.min(swLng, neLng).toFixed(6) + ':' +
      Math.max(swLng, neLng).toFixed(6);
  }

  coordinatesWithSystem(coordinates: string) {
    if (coordinates.match(/^[0-9]{3,7}:[0-9]{3,7}$/)) {
      return coordinates + ':YKJ';
    }
    return coordinates + ':WGS84';
  }

  private toInteger(val: any) {
    return val.map(Math.floor);
  }

  private addLocation(pos) {
    const crd = pos.coords;
    if (crd.accuracy > 30) {
    }
    const latitude = crd.latitude;
    const longitude = crd.longitude;
    if (this.isNewLocation(latitude, longitude)) {
      this.subject.next({lat: latitude, lng: longitude});
    }
  }

  private isNewLocation(lat, lng) {
    if (!this.lastLat || !this.lastLng || this.distance(lat, lng, this.lastLat, this.lastLng) >= this.distanceFromLastSpot) {
      this.lastLat = lat;
      this.lastLng = lng;
      return true;
    }
    return false;
  }

}
