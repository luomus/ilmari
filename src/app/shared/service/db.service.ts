import Dexie from 'dexie';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { TableInterface } from './db/table.interface';
import { Table } from './db/table';
import { InformalTaxonGroupTable } from './db/informal-taxon-group-table';
import { isPlatformBrowser } from '@angular/common';
import { NoopTable } from './db/noop-table';
import { TaxonTable } from './db/taxon-table';

@Injectable({
  providedIn: 'root'
})
export class DBService {

  private db: Dexie;
  private _tables: {[name: string]: TableInterface<any>} = {};

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    if (isPlatformBrowser(platformId)) {
      this.db = new Dexie('IDB');
      this.db.version(1)
        .stores({
          taxa: 'id,taxonRank,scientificName,vernacularName,finnish,species,[_masterGroup+finnish],*_belongsTo',
          taxonParent: 'id',
          taxonGrid: '',
          informalGroup: 'id,*hasSubGroup,_root,_master',
          settings: '',
          ownLists: '',
          documents: 'id',
          gatherings: 'id,rootID',
          units: 'id,rootID',
          identifications: 'id,rootID',
          label: '',
          publication: '',
          files: ''
        });
    }
  }

  table(name: string): any {
    if (!this.db) {
      return new NoopTable();
    }
    if (!this._tables[name]) {
      switch (name) {
        case 'taxa':
          this._tables[name] = new TaxonTable(
            this.db.table('taxa'),
            new Table<any>(this.db.table('taxonParent')),
            new Table<any>(this.db.table('taxonGrid'))
          );
          break;
        case 'informalGroup':
          this._tables[name] = new InformalTaxonGroupTable(this.db.table(name));
          break;
        case 'settings':
        case 'label':
        case 'files':
        case 'ownLists':
          this._tables[name] = new Table<any>(this.db.table(name));
          break;
        default:
          return this.db.table(name);
      }
    }
    return this._tables[name];
  }
}
