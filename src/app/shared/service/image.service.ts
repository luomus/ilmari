import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { environment } from '../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { DBService } from './db.service';
import { TableInterface } from './db/table.interface';
import { LajiApi, LajiApiService } from './laji-api.service';
import { Store } from '@ngrx/store';
import * as fromUser from '../../user/store/index';
import { catchError, concatMap, map, share, switchMap, take, tap } from 'rxjs/operators';
import { forkJoin, from, Observable, of, throwError } from 'rxjs';

const uuid = require('uuid/v4');

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  table: TableInterface<string>;
  private readonly userMediaEnabled;
  private sending: {[id: string]: Observable<string>} = {};

  static isRemoteImage(id: string) {
    if (typeof id === 'string') {
      return id.startsWith('MM.');
    }
    return false;
  }

  static isDataUrl(value) {
    if (typeof value === 'string') {
      return value.startsWith('data:');
    }
    return false;
  }

  constructor(
    private db: DBService,
    private lajiApiService: LajiApiService,
    private store: Store<fromUser.State>,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.table = this.db.table('files');
    this.userMediaEnabled = isPlatformBrowser(platformId) &&
      window.navigator && window.navigator.mediaDevices &&
      !!window.navigator.mediaDevices.getUserMedia;
  }

  save(image: string): Observable<string> {
    const id = uuid();
    return this.table.put(image, id).pipe(
      map(() => id),
      tap(() => this.store.select(fromUser.getPersonToken).pipe(
        take(1),
        concatMap(personToken => personToken ? this.sendImage(id) : of(null))
        ).subscribe()
      ));
  }

  remove(id: string): Observable<boolean> {
    if (ImageService.isRemoteImage(id)) {
      return this.store.select(fromUser.getPersonToken).pipe(
        take(1),
        concatMap(personToken => this.lajiApiService.deleteEntry(LajiApi.Endpoint.image, id, {personToken})),
        catchError(err => {
          return err.status === 404 ? of(true) : throwError(err);
        })
      );
    }
    return this.table.get(id).pipe(
      concatMap(data => ImageService.isRemoteImage(data) ? this.remove(data) : of(null)),
      concatMap(() => this.table.remove(id))
    );
  }

  getStream(): Observable<MediaStream> {
    if (!this.userMediaEnabled) {
      return of(null);
    }
    return from(window.navigator.mediaDevices.getUserMedia({
      video: {
        width: { ideal: 1920 },
        height: { ideal: 1080 },
        facingMode: {ideal: 'environment'}
      },
      audio: false
    }));
  }

  isUserMediaEnabled() {
    return this.userMediaEnabled;
  }

  getUrl(value: string, size: 'thumbnail'|'large' = 'thumbnail'): Observable<string> {
    if (!value) {
      return of(value);
    }
    if (ImageService.isRemoteImage(value)) {
      return of(`${environment.lajiApi.base}images/${value}/${size}.jpg?access_token=${environment.lajiApi.token}`);
    }
    return this.table.get(value).pipe(
      switchMap(data => ImageService.isRemoteImage(data) ? this.getUrl(data, size) : of(data))
    );
  }

  sendImage(id: string): Observable<string> {
    if (ImageService.isRemoteImage(id)) {
      return of(id);
    }
    if (!this.sending[id]) {
      this.sending[id] = this._sendImage(id).pipe(share());
    }
    return this.sending[id];
  }

  private _sendImage(id: string): Observable<string> {
    if (ImageService.isRemoteImage(id)) {
      return of(id);
    }
    return this.table.get(id).pipe(
      concatMap(imgData => ImageService.isRemoteImage(imgData) ?
        of(imgData) :
        forkJoin(
          this.store.select(fromUser.getPersonToken).pipe(take(1)),
          this.store.select(fromUser.getUserSettings).pipe(take(1)),
          this.store.select(fromUser.getUser).pipe(take(1))
        ).pipe(
          map(data => ({personToken: data[0], settings: data[1], person: data[2]})),
          concatMap(data => {
            const formData = new FormData();
            formData.append('data', this.dataUrlToBlob(imgData));
            return this.lajiApiService.addEntry(LajiApi.Endpoint.images, {personToken: data.personToken}, formData).pipe(
              map(res => res[0].id),
              concatMap(tmpID => this.lajiApiService.addEntry(LajiApi.Endpoint.image, {personToken: data.personToken}, {
                ...data.settings.imageMetadata,
                intellectualOwner: data.person.fullName
              }, tmpID))
            );
          }),
          map(img => img.id),
          concatMap(imageID => this.updateLocal(id, imageID).pipe(
            map(() => imageID)
          ))
        )
      ),
      concatMap(img => ImageService.isRemoteImage(img) ? of(img) : throwError('Adding image failed'))
    );
  }

  private updateLocal(id: string, remoteID: string): Observable<string> {
    return this.table.put(remoteID, id).pipe(map(() => id));
  }

  private dataUrlToBlob(dataURL: string): Blob {
    const BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) === -1) {
      const items = dataURL.split(',');
      const type = items[0].split(':')[1];

      return new Blob([decodeURIComponent(items[1])], {type: type});
    }

    const parts = dataURL.split(BASE64_MARKER);
    const contentType = parts[0].split(':')[1];
    const raw = window.atob(parts[1]);
    const rawLength = raw.length;

    const uInt8Array = new Uint8Array(rawLength);

    for (let i = 0; i < rawLength; ++i) {
      uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], {type: contentType});
  }

}
