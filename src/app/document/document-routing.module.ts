import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentContainerComponent } from './document-container/document-container.component';

const routes: Routes = [
  {path: '', component: DocumentContainerComponent, pathMatch: 'full'},
  {path: 'trip', loadChildren: './trip/trip.module#TripModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRoutingModule { }
