import { Injectable } from '@angular/core';
import { Document } from '../../models/document/document';
import { Travel, Travels } from '../trip/models/travels';
import { Gatherings } from '../../models/document/gatherings';
import { LineString } from 'geojson';
import { TimeService } from '../../shared/service/time.service';
import { LajiApi, LajiApiService } from '../../shared/service/laji-api.service';
import { GatheringEvent } from '../../models/document/gathering-event';
import { FlatField } from '../../models/form';
import { ImageService } from '../../shared/service/image.service';
import { forkJoin, from, Observable, of } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';
import { UserSettings } from '../../models/user-settings';
import { Units } from '../../models/document/units';
import { TaxonService } from '../../shared/service/taxon.service';

@Injectable()
export class DocumentService {

  static errorMessages = {
    'noLogin': 'You need to login. If you\'re already logged in please logout and then back again.',
    'generic': 'Server failed to receive the document!'
  };

  static getErrorMessages(body: any): {[field: string]: string[]} {
    const messages = DocumentService.errorMessages;
    if (body.error) {
      if (body.error && body.error.error && body.error.error.details) {
        if (body.error.error.details === 'Incorrect personToken given') {
          return {'_generic': [messages.noLogin]};
        }
        return body.error.error.details;
      } else if (body.message) {
        if (Array.isArray(body.message)) {
          return {'_generic': body.message};
        }
        return {'_generic': [body.message]};
      }
    }
    return {'_generic': [body.status === 404 ?  messages.noLogin : messages.generic]};
  }

  constructor(
    private timeService: TimeService,
    private lajiApiService: LajiApiService,
    private imageService: ImageService,
    private taxonService: TaxonService
  ) { }

  taxonCountInDocument(document: Document, taxa: {[key: string]: number} = {}): {[key: string]: number} {
    if (Array.isArray(document.gatherings)) {
      document.gatherings.forEach(gathering => {
        if (Array.isArray(gathering.units)) {
          gathering.units.forEach(unit => {
            if (Array.isArray(unit.identifications)) {
              unit.identifications.forEach(identification => {
                this.addObsTaxon(taxa, identification, ['taxonID']);
              });
            }
            if (unit.unitFact) {
              this.addObsTaxon(taxa, unit.unitFact, ['autocompleteSelectedTaxonID']);
            }
          });
        }
      });
    }
    return taxa;
  }

  markTaxonAsUser(taxa: {[key: string]: number}): void {
    from(Object.keys(taxa)).pipe(
      concatMap(taxon => this.taxonService.get(taxon).pipe(
        map(value => ({...value, _used: value._used ? value._used + taxa[taxon] : taxa[taxon]}))
      )),
      concatMap(taxon => this.taxonService.put(taxon))
    ).subscribe(() => {});
  }

  private addObsTaxon(taxa: {[key: string]: number}, obj: Object, keys: string[]) {
    keys.forEach(key => {
      if (obj[key]) {
        taxa[obj[key]] = taxa[obj[key]] ? taxa[obj[key]] + 1 : 1;
      }
    });
  }

  flatDataToDocument(data: {[key: string]: any}, fields: FlatField[]): Observable<Document> {
    return this.prepareData(data, fields).pipe(
      map(result => this.flatToObject(result))
    );
  }

  saveDocument(document: Document, personToken: string): Observable<Document> {
    return this.lajiApiService.addEntry(
      LajiApi.Endpoint.documents,
      {
        personToken: personToken,
        validationErrorFormat: 'jsonPath',
      },
      document
    );
  }

  travelsToDocument(travels: Travels, document: Document) {
    Object.keys(travels).forEach(idx => {
      if (!document.gatherings[idx]) {
        return;
      }
      const travel: Travel = travels[idx];
      const gathering: Gatherings = document.gatherings[idx];
      if (!gathering.dateBegin && travel.start) {
        gathering.dateBegin = travel.start;
      }
      if (!gathering.dateEnd) {
        gathering.dateEnd = travel.end ? travel.end : this.timeService.getCurrentTime();
      }
      if (travel.coordinates && travel.coordinates.length > 0) {
        const coordinates = travel.coordinates.map(coord => [coord.lng, coord.lat]);
        const geometries: any = [{
          type: coordinates.length > 1 ? 'LineString' : 'Point',
          coordinates: coordinates.length > 1 ? coordinates : coordinates[0]
        }];
        if (gathering.geometry) {
          if (gathering.geometry.type === 'GeometryCollection') {
            geometries.push(...gathering.geometry.geometries);
          } else {
            geometries.push(gathering.geometry);
          }
        }
        if (geometries.length > 1) {
          gathering.geometry = {
            type: 'GeometryCollection',
            geometries: geometries
          };
        } else {
          gathering.geometry = geometries[0];
          gathering.coordinateSource = Gatherings.CoordinateSourceEnum.CoordinateSourceGps;
        }
      }
    });
    return document;
  }

  addInterpretations(document: Document, settings: UserSettings): Document {
    if (Array.isArray(document.gatherings) && document.gatherings.length > 0) {
      this.checkGatheringEventTimes(document.gatheringEvent, document.gatherings);
      document.gatherings.forEach(gathering => {
        this.addMissingGeometry(gathering);
        if (Array.isArray(gathering.units)) {
          gathering.units = gathering.units.filter(unit => !this.isEmptyUnit(unit, settings));
        }
      });
    }
    document.publicityRestrictions = settings.publicityRestrictions;
    return document;
  }

  private isEmptyUnit(unit: Units, settings: UserSettings): boolean {
    if (Array.isArray(unit.images) && unit.images.length > 0) {
      return false;
    }
    if (settings.ignoreUnits === UserSettings.IgnoreUnitByType.withNoCount) {
      return this.isEmpty(unit.count) &&
        this.isEmpty(unit.abundanceString) &&
        this.isEmpty(unit.adultIndividualCount) &&
        this.isEmpty(unit.individualCount) &&
        this.isEmpty(unit.pairCount) &&
        this.isEmpty(unit.populationAbundance) &&
        this.isEmpty(unit.femaleIndividualCount) &&
        this.isEmpty(unit.maleIndividualCount);
    } else {
      return this.isEmpty(unit.informalNameString) &&
        (!unit.identifications || !unit.identifications[0] || this.isEmpty(unit.identifications[0].taxon));
    }
  }

  private isEmpty(value) {
    return typeof value === 'undefined' || value === '';
  }

  private prepareData(data: {[key: string]: any}, fields: FlatField[]): Observable<{[key: string]: any}> {
    const lookup = {};
    const work: {key: string, obs: Observable<any>}[] = [];
    fields.forEach(field => lookup[field.path] = field);

    Object.keys(data).forEach(field => {
      const fieldPath = field.replace(/\[\d+]/g, '[*]');
      const value = data[field];
      if (!lookup[fieldPath]) {
        return;
      }
      switch (lookup[fieldPath].type) {
        case 'images':
          if (ImageService.isRemoteImage(value)) {
            return;
          }
          if (Array.isArray(value) && value.length > 0) {
            work.push({
              key: field,
              obs: forkJoin(value.map(img => this.imageService.sendImage(img)))
            });
          }
      }
    });

    if (work.length === 0) {
      return of(data);
    }
    return forkJoin(work.map(item => item.obs)).pipe(
        map(results => {
          results.forEach((result, idx) => {
            data[work[idx].key] = result;
          });
          return data;
        })
      );
  }

  private addMissingGeometry(gathering: Gatherings) {
    if (gathering.geometry) {
      return;
    }
    const lng = [];
    const lat = [];
    if (Array.isArray(gathering.units) && gathering.units.length > 0) {
      gathering.units.forEach(unit => {
        if (unit.unitGathering && unit.unitGathering.geometry) {
          this.findBBox(unit.unitGathering.geometry, lng, lat);
        }
      });
    }
    const lastLngIdx = lng.length - 1;
    const lastLatIdx = lat.length - 1;
    if (lastLngIdx === 0 && lastLatIdx === 0) {
      gathering.geometry = {
        type: 'Point',
        coordinates: [lng[0], lat[0]]
      };
    } else if (lastLngIdx > 0 && lastLatIdx > 0) {
      gathering.geometry = {
        type: 'Polygon',
        coordinates: [[
          [lng[0], lat[0]],
          [lng[lastLngIdx], lat[0]],
          [lng[lastLngIdx], lat[lastLatIdx]],
          [lng[0], lat[lastLatIdx]],
          [lng[0], lat[0]]
        ]]
      };
    }
  }

  private findBBox(geometry: any, lng: number[], lat: number[]) {
    if (Array.isArray(geometry)) {
      if (typeof geometry[0] === 'number') {
        if (!lng[0]) {
          lng.push(geometry[0]);
          lat.push(geometry[1]);
          return;
        }
        const lastLngIdx = lng.length - 1;
        const lastLatIdx = lat.length - 1;
        if (lng[0] > geometry[0]) {
          lng.unshift(geometry[0]);
        }
        if (lat[0] > geometry[1]) {
          lat.unshift(geometry[1]);
        }
        if (lng[lastLngIdx] < geometry[0]) {
          lng.push(geometry[0]);
        }
        if (lat[lastLatIdx] < geometry[1]) {
          lat.push(geometry[1]);
        }
      } else {
        geometry.forEach(geom => this.findBBox(geom, lng, lat));
      }
    } else if (geometry.type === 'GeometryCollection') {
      geometry.geometries.forEach(geom => this.findBBox(geom, lng, lat));
    } else if (geometry.coordinates) {
      this.findBBox(geometry.coordinates, lng, lat);
    }
  }

  private checkGatheringEventTimes(gatheringEvent: GatheringEvent, gatherings: Gatherings[]) {
    const dates = [];
    const cmp = this.timeService.cmpTime;
    gatherings.forEach(gathering => {
      if (gathering.dateEnd) {
        if (!dates[0] || cmp(gathering.dateEnd, dates[dates.length - 1]) < 0) {
          dates.push(gathering.dateEnd);
        }
      }
      if (gathering.dateBegin) {
        if (!dates[0] || cmp(gathering.dateBegin, dates[0]) > 0) {
          dates.unshift(gathering.dateBegin);
        }
        if (cmp(gathering.dateBegin, dates[dates.length - 1]) < 0) {
          dates.push(gathering.dateBegin);
        }
      }
      if (gathering.units) {
        gathering.units.forEach(unit => {
          if (unit.unitGathering) {
            if (unit.unitGathering.dateEnd) {
              if (!dates[0] || cmp(unit.unitGathering.dateEnd, dates[dates.length - 1]) < 0) {
                dates.push(unit.unitGathering.dateEnd);
              }
            }
            if (unit.unitGathering.dateBegin) {
              if (!dates[0] || cmp(unit.unitGathering.dateBegin, dates[0]) > 0) {
                dates.unshift(unit.unitGathering.dateBegin);
              }
              if (cmp(unit.unitGathering.dateBegin, dates[dates.length - 1]) < 0) {
                dates.push(unit.unitGathering.dateBegin);
              }
            }
          }
        });
      }
    });
    if (dates.length > 0) {
      const lastIdx = dates.length - 1;
      gatheringEvent.dateBegin = !gatheringEvent.dateBegin || cmp(gatheringEvent.dateBegin, dates[0]) > 0 ? dates[0] : gatheringEvent.dateBegin;
      if (dates[0] !== dates[lastIdx]) {
        gatheringEvent.dateEnd = !gatheringEvent.dateEnd || cmp(gatheringEvent.dateEnd, dates[0]) > 0 ? dates[lastIdx] : gatheringEvent.dateEnd;
      }
    } else if (!gatheringEvent.dateBegin) {
      gatheringEvent.dateBegin = this.timeService.dateToDateString();
    }
  }

  private flatToObject(values: {[key: string]: any}): Document {
    const document: Document = {};

    Object.keys(values).map(path => {
      const re = /[.(\[\])]/;
      const parts = path.split(re).filter(value => value !== '');
      let pointer = document;
      let now: string|number = parts.shift();
      while (parts.length > 0) {
        if (!pointer[now]) {
          pointer[now] = !isNaN(Number(parts[1] || '')) ? {} : [];
        }
        pointer = pointer[now];
        now = parts.shift();
      }
      pointer[now] = values[path];
    });
    return document;
  }

}
