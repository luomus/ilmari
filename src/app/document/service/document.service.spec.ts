import { inject, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DocumentService } from './document.service';
import { TimeService } from '../../shared/service/time.service';
import { LajiApiService } from '../../shared/service/laji-api.service';

describe('DocumentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        DocumentService,
        TimeService,
        LajiApiService
      ]
    });
  });
  afterEach(inject([HttpTestingController], (backend: HttpTestingController) => {
    backend.verify();
  }));

  it(`should convert flat to document`, () => {
    const service = TestBed.get(DocumentService);
    service.flatDataToDocument({
      'gatherings[0].location': ['Kuru'],
      'gatherings[0].units[0].name': 'Diptera',
      'gatherings[1].units[0].name': 'Parus major',
      'gatherings[1].units[1].name': 'Bufo bufo',
      'gatherings[1].location': 'Helsinki'
    }).subscribe(document => {
      expect(document).toEqual({
        gatherings: [
          {
            location: ['Kuru'],
            units: [
              {
                name: 'Diptera'
              }
            ]
          },
          {
            location: 'Helsinki',
            units: [
              {name: 'Parus major'},
              {name: 'Bufo bufo'}
            ]
          }
        ]
      });
    });
  });

  it(`should send document`, () => {
    const service = TestBed.get(DocumentService);
    const http = TestBed.get(HttpTestingController);
    let doc;
    service.flatDataToDocument({
      'gatherings[0].location': ['Kuru'],
      'gatherings[0].units[0].name': 'Diptera',
      'gatherings[1].units[0].name': 'Parus major',
      'gatherings[1].units[1].name': 'Bufo bufo'
    })
      .do(document => doc = document)
      .switchMap(document => service.saveDocument(document, 'foobar'))
      .subscribe();
    const req = http.expectOne(request => request.url.includes('documents'));
    expect(req.request.params.has('personToken')).toBeTruthy();
    expect(req.request.body).toEqual(doc);

    req.flush('');
  });

});
