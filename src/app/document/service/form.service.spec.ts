import { inject, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TimeService } from '../../shared/service/time.service';
import { LajiApiService } from '../../shared/service/laji-api.service';
import { SettingService } from '../../shared/service/setting.service';
import { PersonService } from '../../user/service/person.service';
import { FormService } from './form.service';
import { DBService } from '../../shared/service/db.service';
import { Observable, of } from 'rxjs';
import { FlatField } from '../../models/form';
import { Store } from '@ngrx/store';

const formSchema = {
  fields: [
    {
      'name': 'editors',
      'label': 'Edit rights',
      'type': 'collection',
      'options': {
        'target_element': {
          'type': 'text'
        }
      }
    },
    {
      'name': 'gatheringEvent',
      'label': '',
      'type': 'fieldset',
      'validators': {
        'presence': {
          'allowEmpty': false
        }
      },
      'fields': [
        {
          'name': 'leg',
          'label': 'Observers',
          'validators': {
            'presence': {
              'message': 'At least one observer must be reported.'
            }
          },
          'type': 'collection',
          'options': {
            'target_element': {
              'type': 'text'
            }
          }
        }
      ]
    },
    {
      'name': 'gatherings',
      'type': 'collection',
      'label': 'Location',
      'fields': [
        {
          'name': 'dateBegin',
          'type': 'text',
          'label': 'Start date'
        },
        {
          'name': 'units',
          'label': 'Observations',
          'type': 'collection',
          'fields': [
            {
              'name': 'recordBasis',
              'options': {
                'default': 'MY.recordBasisHumanObservation'
              },
              'type': 'select',
              'label': 'Record type'
            }
          ]
        }
      ]
    },
    {
      'name': 'keywords',
      'type': 'collection',
      'label': 'Keywords',
      'options': {
        'target_element': {
          'type': 'text'
        }
      }
    }
  ]
};

const flatFields = [
  { name: 'editors', parent: 'root', path: 'editors', label: 'Edit rights', type: 'multiText' },
  { name: 'leg', parent: 'gatheringEvent', path: 'gatheringEvent.leg', label: 'Observers', type: 'multiFriend', defaultOverride: [ '%userID%' ] },
  { name: 'dateBegin', parent: 'gatherings', path: 'gatherings[*].dateBegin', label: 'Start date', type: 'datetime' },
  { name: 'recordBasis', parent: 'units', path: 'gatherings[*].units[*].recordBasis', label: 'Record type', type: 'select', default: 'MY.recordBasisHumanObservation'},
  { name: 'keywords', parent: 'root', path: 'keywords', label: 'Keywords', type: 'multiText' }
];

const Person = {
  personToken: 'TOKEN123',
  person: {
    id: 'MA.007',
    fullName: 'Test User'
  }
};

const PersonServiceMock = {
  getPerson: (personToken: string): Observable<any> => of(Person),
  getStoredUser: (): Observable<any> => of(Person)
};

const SettingsServiceMock = {
  get: (key) => of(false),
  set: (key, value) => {}
};

const StoreMock = {
  select: () => {
    return of('MVL.1');
  }
};

describe('FormService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        TimeService,
        LajiApiService,
        SettingService,
        {provide: PersonService, useValue: PersonServiceMock },
        {provide: SettingService, useValue: SettingsServiceMock },
        {provide: Store, useValue: StoreMock },
        FormService,
        DBService
      ]
    });
  });
  afterEach(inject([HttpTestingController], (backend: HttpTestingController) => {
    backend.verify();
  }));

  it(`should fetch form`, () => {
    const service: FormService = TestBed.get(FormService);
    const http = TestBed.get(HttpTestingController);
    let fields;

    service.getForm('JX.519')
      .subscribe(form => fields = form.fields);
    http.expectOne(request => request.url.includes('forms')).flush(formSchema);

    expect(fields).toEqual(flatFields as any);
  });

  it('should set default values', () => {
    FormService.updateDefaults({
      userID: 'MA.000',
      informalTaxonID: 'MVL.1',
      notSet: undefined
    });
    const fields: FlatField[] = [
      {name: '1', exactPath: '1', path: '1', defaultOverride: '%informalTaxonID%', default: '%userID%', parent: 'document', label: '', type: 'text'},
      {name: '2', exactPath: '2', path: '2', default: '%userID%', parent: 'document', label: '', type: 'text'},
      {name: '3', exactPath: '3', path: '3', default: '%notSet%', parent: 'document', label: '', type: 'text'},
      {name: '4', exactPath: '4', path: '4', default: ['%userID%'], parent: 'document', label: '', type: 'text'},
      {name: '5', exactPath: '5', path: '5', defaultOverride: ['%notSet%'], parent: 'document', label: '', type: 'text'},
      {name: '6', exactPath: '6', path: '6', defaultOverride: ['Test - %notSet%'], parent: 'document', label: '', type: 'text'},
      {name: '7', exactPath: '7', path: '7', defaultOverride: ['User - %userID%'], parent: 'document', label: '', type: 'text'},
      {name: '8', exactPath: '8', path: '8', defaultOverride: 'group - %informalTaxonID%', parent: 'document', label: '', type: 'text'},
    ];

    const date = {};
    const result = FormService.addDefaults(fields, date);

    expect(result).toEqual({
      formID: 'JX.519',
      1: 'MVL.1',
      2: 'MA.000',
      3: undefined,
      4: ['MA.000'],
      5: undefined,
      6: ['Test - '],
      7: ['User - MA.000'],
      8: 'group - MVL.1'
    });
  });

});
