import { Injectable } from '@angular/core';
import { LajiApi, LajiApiService } from '../../shared/service/laji-api.service';
import { FlatField, Form } from '../../models/form';
import { FormSchemaModel } from '../../models/form-schema';
import { Section, SectionHistory, Sections, SingleSection, SubSections } from '../trip/models/sections';
import { Travels } from '../trip/models/travels';
import { environment } from '../../../environments/environment';
import { PersonService } from '../../user/service/person.service';
import { UserData } from '../../models/user-data';
import { Settings, SettingService } from '../../shared/service/setting.service';
import { Observable, of } from 'rxjs';
import { concatMap, map, switchMap, tap } from 'rxjs/operators';

@Injectable()
export class FormService {

  static readonly specialFields = {
    'gatherings[*].units[*].identifications[*].taxon': 'taxon',
    'gatherings[*].notes': 'textarea',
    'gatherings[*].units[*].notes': 'textarea',
    'gatheringEvent.leg': 'multiFriend',
    'gatheringEvent.dateBegin': 'datetime',
    'gatheringEvent.dateEnd': 'datetime',
    'gatherings[*].dateBegin': 'datetime',
    'gatherings[*].dateEnd': 'datetime',
    'gatherings[*].units[*].unitGathering.dateBegin': 'datetime',
    'gatherings[*].units[*].unitGathering.dateEnd': 'datetime',
    'gatherings[*].units[*].informalTaxonGroups': 'hidden',
    'gatherings[*].units[*].images': 'images',
    'gatherings[*].images': 'images',
  };

  static readonly defaultFieldValue = {
    'gatheringEvent.leg': ['%userID%'],
    'gatherings[*].units[*].informalTaxonGroups': ['%informalTaxonID%']
  };

  static refreshed = {};

  private static defaults: any = {
    'userID': undefined,
    'informalTaxonID': environment.defaults.informalTaxonGroup
  };

  static updateDefaults(value: any) {
    FormService.defaults = {...FormService.defaults, ...value};
  }

  static makeSubSectionStatus(current: Sections, target: SingleSection, targetSection: Section) {
    const subStatus = current._subSectionStatus;
    if (subStatus[targetSection] && current._subSections[targetSection]) {
      if (!subStatus[targetSection]) {
        subStatus[targetSection] = {};
      }
      if (!subStatus[targetSection][current[targetSection]]) {
        subStatus[targetSection][current[targetSection].active] = {};
      }
      current._subSections[targetSection].map(key => {
        subStatus[targetSection][current[targetSection].active][key] = current[key];
      });
    }
    return subStatus;
  }

  static fetchFromSubSectionStatus(targetSection: Section, targetIdx: number, history: SectionHistory, subSections: SubSections) {
    if (history[targetSection] && history[targetSection][targetIdx]) {
      return history[targetSection] && history[targetSection][targetIdx];
    } else if (subSections[targetSection]) {
      return subSections[targetSection].reduce((prev, current) => {
        prev[current] = {max: 0, active: 0};
        return prev;
      }, {});
    }
    return {};
  }

  static moveSection(section: Section, fromIdx: number, toIdx: number, sections: Sections): Sections {
    if (sections._subSectionStatus[section] && fromIdx !== toIdx) {
      const subStatus = sections._subSectionStatus[section];
      const newSubStatus = {};
      Object.keys(subStatus).map((idx) => {
        const idxNum = +idx;
        if (idxNum === toIdx) {
          newSubStatus[toIdx] = subStatus[fromIdx];
        } else if (fromIdx < toIdx) {
          newSubStatus[(fromIdx <= idxNum && idxNum <= toIdx) ? idxNum - 1 : idxNum] = subStatus[idx];
        } else {
          newSubStatus[(toIdx <= idxNum && idxNum <= fromIdx) ? idxNum + 1 : idxNum] = subStatus[idx];
        }
      });
      sections._subSectionStatus[section] = newSubStatus;
    }
    return sections;
  }

  static moveData(section: Section, fromIdx: number, toIdx: number, sections: Sections, data: any): any {
    const result = {};
    const parentRegEx = this.generateParentRegExp(sections, section);
    Object.keys(data).map(key => {
      const sectionRegEx = new RegExp(`${parentRegEx}\\b${section}\\[([0-9]+)\\]`, 'g');
      const match = sectionRegEx.exec(key);
      let to = key;
      if (match) {
        const idxNum = +match[1];
        if (idxNum === fromIdx) {
          to = key.replace(`${section}[${fromIdx}]`, `${section}[${toIdx}]`);
        } else if (fromIdx < toIdx) {
          to = key.replace(`${section}[${idxNum}]`, `${section}[${(fromIdx <= idxNum && idxNum <= toIdx) ? idxNum - 1 : idxNum}]`);
        } else {
          to = key.replace(`${section}[${idxNum}]`, `${section}[${(toIdx <= idxNum && idxNum <= fromIdx) ? idxNum + 1 : idxNum}]`);
        }
      }
      result[to] = data[key];
    });
    return result;
  }

  static delFromSection(del: {idx: number, section: Section}, sections: Sections): Sections {
    const maxAfterDelete = sections[del.section].max - 1;
    const activeAfterDelete = del.idx <= maxAfterDelete ? del.idx : maxAfterDelete;
    const subStatus = sections._subSectionStatus[del.section] ? {
      ...sections._subSectionStatus,
      [del.section]: Object.keys(sections._subSectionStatus[del.section]).reduce((prev, current) => {
        const currentNum = +current;
        if (currentNum < del.idx) {
          prev[current] = sections._subSectionStatus[del.section][current];
        } else if (currentNum > del.idx) {
          prev[currentNum - 1] = sections._subSectionStatus[del.section][current];
        }
        return prev;
      }, {})
    } : sections._subSectionStatus;
    return {
      ...sections,
      ...(sections._subSectionStatus[del.section] ? subStatus[del.section][activeAfterDelete] : {}),
      [del.section]: {
        max: maxAfterDelete,
        active: activeAfterDelete
      },
      _subSectionStatus: subStatus
    };
  }

  static delFromTravel(del: {idx: number, section: Section}, sections: Sections, travel: Travels) {
    if (del.section !== 'gatherings') {
      return travel;
    }
    const result = {};
    const removeFrom = del.idx;
    Object.keys(travel).forEach(key => {
      const keyNum = +key;
      let delta = 0;
      if (keyNum === removeFrom) {
        return;
      } else if (keyNum > removeFrom) {
        delta = -1;
      }
      result[keyNum + delta] = travel[key];
    });
    return result;
  }

  static delFromData(del: {idx: number, section: Section}, sections: Sections, data: any): any {
    const result = {};
    const removeFrom = del.idx;
    const parentRegEx = this.generateParentRegExp(sections, del.section);
    Object.keys(data).map(key => {
      const sectionRegEx = new RegExp(`${parentRegEx}\\b${del.section}\\[([0-9]+)\\]`, 'g');
      const match = sectionRegEx.exec(key);
      let to = key;
      if (match) {
        const curIdx = +match[1];
        if (curIdx === removeFrom) {
          return;
        } else if (curIdx > removeFrom) {
          to = key.replace(`${del.section}[${curIdx}]`, `${del.section}[${curIdx - 1}]`);
        }
      }
      result[to] = data[key];
    });
    return result;
  }

  private static generateParentRegExp(sections: Sections, section: string) {
    return sections._parentSections[section] ? sections._parentSections[section].reduce((prev, current) => {
      prev += `\\b${current}\\[${sections[current].active}\\].*`;
      return prev;
    }, '') : '';
  }

  static addDefaults(fields: FlatField[], data: any): any {
    const result = {...data};
    result['formID'] = environment.forms.trip;
    fields.map(field => {
      if (typeof result[field.exactPath] === 'undefined' && (typeof field.default !== 'undefined' || typeof field.defaultOverride !== 'undefined')) {
        result[field.exactPath] = field.defaultOverride ? FormService.defaultValue(field.defaultOverride) : FormService.defaultValue(field.default);
      }
    });
    return result;
  }

  static setExactPath(fields: FlatField[], sections: Sections): FlatField[] {
    const replaces = sections._allSections.map(section => ({
      from: `${section}[*]`,
      to: `${section}[${sections[section].active}]`
    }));
    return fields.map(field => ({
      ...field,
      exactPath: replaces
        .reduce((prev, current) => prev.replace(current.from, current.to), field.path)
        .replace(/\[\*]/, '[0]')
      }));
  }

  static isEmptyUnit(data: any, fields: FlatField[]) {
    let empty = true;
    fields.forEach(field => {
      if (!field.path.match(/\.units\[/)) {
        return;
      }
      if (typeof data[field.exactPath] !== 'undefined' && data[field.exactPath] !== '') {
        const defaultValue = FormService.defaultFieldValue[field.path] ?
          (field.defaultOverride ? FormService.defaultValue(field.defaultOverride) : FormService.defaultValue(field.default)) :
          (field.default || '');
        if (['multiFriend', 'multiText', 'images'].indexOf(field.type) !== -1) {
          if (Array.isArray(data[field.exactPath]) && data[field.exactPath].length === 0) {
            return;
          }
        }
        if (defaultValue && JSON.stringify(defaultValue) === JSON.stringify(data[field.exactPath])) {
          return;
        } else {
          empty = false;
        }
      }
    });
    return empty;
  }


  private static defaultValue(value: any) {
    if (Array.isArray(value)) {
      const candi = value.map(val => FormService.defaultValue(val)).filter((val) => val !== undefined);
      if (candi.length > 0) {
        return candi;
      }
    } else if (typeof value === 'string') {
      let fullKey;
      Object.keys(FormService.defaults).forEach((key) => {
        const repKey = '%' + key + '%';
        if (value === repKey) {
          fullKey = key;
        }
        value = value.replace(repKey, typeof FormService.defaults[key] !== 'undefined' ? FormService.defaults[key] : '');
      });
      if (fullKey) {
        return typeof FormService.defaults[fullKey] !== 'undefined' ? FormService.defaults[fullKey] : undefined;
      }
      return value;
    }
  }

  constructor(
    private apiService: LajiApiService,
    private personService: PersonService,
    private settingsService: SettingService
  ) {}

  getForm(id: string): Observable<Form> {
    return this.settingsService.get(Settings.tripForm).pipe(
      switchMap(form => {
        if (form && !FormService.refreshed[id]) {
          this.fetchForm(id).subscribe();
        }
        return form ? of(form) : this.fetchForm(id);
      })
    );
  }

  analyzeFields(schemaFields, fields = [], base = '', parent = 'root'): FlatField[] {
    if (!schemaFields) {
      return fields;
    }
    schemaFields.map((field) => {
      const res: FlatField = {
        name: field.name,
        parent: parent,
        path: base ? base + '.' + field.name : field.name,
        label: field.label || field.name,
        type: field.type,
      };

      if (field.options && field.options.default) {
        res.default = field.options.default;
      }
      if (FormService.defaultFieldValue[res.path]) {
        res.defaultOverride = FormService.defaultFieldValue[res.path];
      }
      if (field.validators) {
        res.validators = field.validators;
      }
      if (field.warnings) {
        res.warningValidators = field.warnings;
      }

      if (field.type === 'fieldset') {
        return this.analyzeFields(field.fields, fields, res.path, field.name);
      } else if (field.type === 'collection') {
        if (field.fields) {
          return this.analyzeFields(field.fields, fields, res.path + '[*]', field.name);
        }
        res.type = 'multiText';
      } else if (field.type === 'select' && field.options && field.options.value_options) {
        res.values = [];
        res.valueLabels = {};
        Object.keys(field.options.value_options).map(key => {
          const newKey = (key === '' ? undefined : key);
          res.values.push(newKey);
          res.valueLabels[newKey] = field.options.value_options[key];
        });
      }
      if (FormService.specialFields[res.path]) {
        res.type = FormService.specialFields[res.path];
      }
      fields.push(res);
    });
    return fields;
  }

  private fetchForm(id: string): Observable<Form> {
    return this.apiService.getEntry(LajiApi.Endpoint.form, id, {format: 'json'}).pipe(
      concatMap(form => this.personService.getStoredUser().pipe(
        map(person => ({form, person}))
      )),
      map<({form: FormSchemaModel, person: UserData}), Form>((data) => {
        if (data.person && data.person.person) {
          FormService.defaults.userID = data.person.person.id;
        }
        return {
          id: data.form.id,
          title: data.form.title,
          description: data.form.description,
          fields: this.analyzeFields(data.form.fields)
        };
      }),
      tap((form) => this.settingsService.set(Settings.tripForm, form as Form))
    );
  }

}
