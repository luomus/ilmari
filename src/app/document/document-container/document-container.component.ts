import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'ilm-document-container',
  templateUrl: './document-container.component.html',
  styleUrls: ['./document-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocumentContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
