import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocumentRoutingModule } from './document-routing.module';
import { DocumentContainerComponent } from './document-container/document-container.component';
import { SharedModule } from '../shared/shared.module';
import { FormService } from './service/form.service';
import { DocumentService } from './service/document.service';
import { LajiValidatorDirective } from './directive/laji-validator.directive';

@NgModule({
  imports: [
    CommonModule,
    DocumentRoutingModule,
    SharedModule
  ],
  declarations: [DocumentContainerComponent, LajiValidatorDirective],
  providers: [FormService, DocumentService],
})
export class DocumentModule { }
