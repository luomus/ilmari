import { Directive, forwardRef, Output } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[ilmLajiValidator][ngModel],[ilmLajiValidator][formControl]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => LajiValidatorDirective), multi: true }
  ]
})
export class LajiValidatorDirective implements Validator {
  @Output() ilmLajiValidator;

  constructor() { }

  validate(c: AbstractControl): ValidationErrors | null {
    return null;
  }
}
