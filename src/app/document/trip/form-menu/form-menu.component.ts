import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Sections } from '../models/sections';

@Component({
  selector: 'ilm-form-menu',
  templateUrl: './form-menu.component.html',
  styleUrls: ['./form-menu.component.scss']
})
export class FormMenuComponent {

  @Input() saving = false;
  @Input() recording: boolean;
  @Input() sections: Sections;
  @Input() extended: boolean;

  @Output() add = new EventEmitter<{section: string}>();
  @Output() activate = new EventEmitter<{idx: number, section: string}>();
  @Output() delete = new EventEmitter<{idx: number, section: string}>();
  @Output() toggleRecording = new EventEmitter<void>();
  @Output() toggleExtended = new EventEmitter<void>();
  @Output() save = new EventEmitter<void>();
  @Output() clearAll = new EventEmitter<void>();

  constructor() { }

  _del(idx, section) {
    if ((section === 'gatherings' && this.recording) || this.sections[section].max === 0) {
      return;
    }
    this.delete.emit({idx: idx, section: section});
  }

  _add(section) {
    if (section === 'gatherings' && this.recording) {
      return;
    }
    this.add.emit({section: section});
  }

  _activate(idx, section) {
    if ((section === 'gatherings' && this.recording) || idx < 0) {
      return;
    }
    this.activate.emit({idx: idx, section: section});
  }
}
