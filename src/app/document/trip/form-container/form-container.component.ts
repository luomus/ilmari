import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Sections } from '../models/sections';
import { FormService } from '../../service/form.service';
import { Form } from '../../../models/form';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Travels } from '../models/travels';
import * as fromTrip from '../../trip/store';
import * as tripActions from '../store/actions/trip-actions';
import { map, take } from 'rxjs/operators';
import { ConfirmationService } from '../../../shared/service/confirmation.service';

@Component({
  selector: 'ilm-form-container',
  templateUrl: './form-container.component.html',
  styleUrls: ['./form-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormContainerComponent implements OnInit {

  travel$: Observable<Travels>;
  sections$: Observable<Sections>;
  formData$: Observable<Form>;
  recording$: Observable<boolean>;
  saving$: Observable<boolean>;
  mapFullscreen$: Observable<boolean>;
  activeGroup$: Observable<string>;
  isExtended$: Observable<boolean>;
  isGeneralMap$: Observable<boolean>;
  selectedFields: Observable<string[]>;
  errors$: Observable<{[field: string]: string[]}>;

  data$: Observable<any>;

  constructor(
    private formService: FormService,
    private cd: ChangeDetectorRef,
    private store: Store<fromTrip.State>,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.data$ = this.store.select(fromTrip.getTripData);
    this.travel$ = this.store.select(fromTrip.getTripTravel);
    this.formData$ = this.store.select(fromTrip.getTripForm);
    this.sections$ = this.store.select(fromTrip.getTripSections);
    this.recording$ = this.store.select(fromTrip.isTripRecording);
    this.saving$ = this.store.select(fromTrip.isTripSaving);
    this.mapFullscreen$ = this.store.select(fromTrip.isTripMapFullscreen);
    this.activeGroup$ = this.store.select(fromTrip.getTripActiveGroup);
    this.isExtended$ = this.store.select(fromTrip.isTripExtended);
    this.isGeneralMap$ = this.store.select(fromTrip.isGeneralMap);
    this.selectedFields = this.store.select(fromTrip.getTripSelectedFields);
    this.errors$ = this.store.select(fromTrip.getTripErrors).pipe(map(errors => errors || {}));
  }

  toggleFullscreenMap() {
    this.doAction(new tripActions.ToggleFullscreenMapAction());
  }

  toggleMap() {
    this.doAction(new tripActions.ToggleGeneralMapAction());
  }

  setData(data) {
    this.doAction(new tripActions.SetDataAction(data));
  }

  updateData(data) {
    this.doAction(new tripActions.UpdateDataAction(data));
  }

  onAdd(event) {
    this.doAction(new tripActions.AddSectionAction(event.section));
  }

  onDelete(event) {
    this.confirmationService.confirm(event.section === 'units' ? 'deleteObservation' : 'deleteGathering')
      .subscribe(confirm => {
        if (confirm) {
          this.doAction(new tripActions.DelSectionAction(event));
        }
      });
  }

  onActivate(event) {
    this.doAction(new tripActions.ActivateSectionAction(event));
  }

  onMove(event) {
    this.doAction(new tripActions.MoveSectionAction(event));
  }

  toggleRecording() {
    this.doAction(new tripActions.ToggleRecordingAction());
  }

  onSave() {
    this.doAction(new tripActions.StartSavingAction());
  }

  onClearAll() {
    this.confirmationService.confirm('deleteDocument')
      .subscribe(confirm => {
        if (confirm) {
          this.doAction(new tripActions.ClearFormAction());
        }
      });
  }

  doAction(action: Action) {
    this.store.select(fromTrip.isTripSaving).pipe(take(1))
      .subscribe((saving) => {
        if (!saving) {
          this.store.dispatch(action);
        }
      });
  }

}
