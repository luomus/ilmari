import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { InformalTaxonService } from '../../../shared/service/informal-taxon.service';
import { Action, Store } from '@ngrx/store';
import * as fromTrip from '../store';
import * as tripActions from '../store/actions/trip-actions';
import { Sections } from '../models/sections';
import { take } from 'rxjs/operators';
import { ConfirmationService } from '../../../shared/service/confirmation.service';

@Component({
  selector: 'ilm-header-container',
  templateUrl: './header-container.component.html',
  styleUrls: ['./header-container.component.scss']
})
export class HeaderContainerComponent implements OnInit {

  sections$: Observable<Sections>;
  recording$: Observable<boolean>;
  saving$: Observable<boolean>;
  isExtended$: Observable<boolean>;

  constructor(
    private informalTaxonService: InformalTaxonService,
    private store: Store<fromTrip.State>,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.sections$ = this.store.select(fromTrip.getTripSections);
    this.recording$ = this.store.select(fromTrip.isTripRecording);
    this.saving$ = this.store.select(fromTrip.isTripSaving);
    this.isExtended$ = this.store.select(fromTrip.isTripExtended);
  }

  toggleSelected() {
    this.store.dispatch(new tripActions.ToggleExtendedAction());
  }

  onAdd(event) {
    this.doAction(new tripActions.AddSectionAction(event.section));
  }

  onDelete(event) {
    this.confirmationService.confirm(event.section === 'units' ? 'deleteObservation' : 'deleteGathering')
      .subscribe(confirm => {
        if (confirm) {
          this.doAction(new tripActions.DelSectionAction(event));
        }
      });
  }

  onActivate(event) {
    this.doAction(new tripActions.ActivateSectionAction(event));
  }

  toggleRecording() {
    this.doAction(new tripActions.ToggleRecordingAction());
  }

  onSave() {
    this.doAction(new tripActions.StartSavingAction());
  }

  onClearAll() {
    this.confirmationService.confirm('deleteDocument')
      .subscribe(confirm => {
        if (confirm) {
          this.doAction(new tripActions.ClearFormAction());
        }
      });
  }

  doAction(action: Action) {
    this.store.select(fromTrip.isTripSaving).pipe(take(1))
      .subscribe((saving) => {
        if (!saving) {
          this.store.dispatch(action);
        }
      });
  }

}
