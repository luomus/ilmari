import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Sections } from '../models/sections';

@Component({
  selector: 'ilm-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent implements OnInit {

  @Input() saving = false;
  @Input() recording: boolean;
  @Input() sections: Sections;

  @Output() add = new EventEmitter<{section: string}>();
  @Output() activate = new EventEmitter<{idx: number, section: string}>();
  @Output() delete = new EventEmitter<{idx: number, section: string}>();
  @Output() toggleRecording = new EventEmitter<void>();
  @Output() save = new EventEmitter<void>();
  @Output() clearAll = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  _add(section) {
    if (section === 'gatherings' && this.recording) {
      return;
    }
    this.add.emit({section: section});
  }

  _activate(idx, section) {
    if ((section === 'gatherings' && this.recording) || idx < 0) {
      return;
    }
    this.activate.emit({idx: idx, section: section});
  }

}
