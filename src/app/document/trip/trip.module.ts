import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { FormComponentsModule } from '../form-components/form-components.module';

import { TripRoutingModule } from './trip-routing.module';
import { FormComponent } from './form/form.component';
import { FormContainerComponent } from './form-container/form-container.component';
import { FooterComponent } from './footer/footer.component';
import { metaReducers, reducers } from './store';
import { TripEffects } from './store/effects/trip-effects';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { UnitListComponent } from './unit-list/unit-list.component';
import { HeaderContainerComponent } from './header-container/header-container.component';
import { FormMenuComponent } from './form-menu/form-menu.component';

@NgModule({
  imports: [
    CommonModule,
    TripRoutingModule,
    SharedModule,
    FormComponentsModule,
    StoreModule.forFeature('form-trip', reducers as any, {metaReducers}),
    EffectsModule.forFeature([TripEffects]),
  ],
  declarations: [FormComponent, FormContainerComponent, FooterComponent, UnitListComponent, HeaderContainerComponent, FormMenuComponent]
})
export class TripModule { }
