import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FlatField, Form } from '../../../models/form';
import { Section, Sections } from '../models/sections';
import { Travels } from '../models/travels';
import { Document } from '../../../models/document/document';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'ilm-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('visible', [
      state('in', style({
        opacity: 1,
        height: '*'
      })),
      transition(':enter', [
        style({opacity: 0, height: 0}),
        animate('200ms ease-in-out')
      ]),
      transition(':leave', [
        animate('200ms ease-out', style({opacity: 0, height: 0}))
      ])
    ])
  ]
})
export class FormComponent implements OnInit, OnChanges {

  @Input() travel: Travels;
  @Input() formData: Form;
  @Input() data = {};
  @Input() sections: Sections;
  @Input() selectedFields: string[];
  @Input() extended: boolean;
  @Input() mapFullscreen: boolean;
  @Input() errors: {[field: string]: string[]} = {};
  @Output() dataChange = new EventEmitter<any>();
  @Output() activate = new EventEmitter<{idx: number, section: string}>();
  @Output() delete = new EventEmitter<{idx: number, section: string}>();
  @Output() move = new EventEmitter<{fromIdx: number, toIdx: number, section: Section}>();
  @Output() updateData = new EventEmitter<{[value: string]: any}>();
  @Output() mapFullScreenToggle = new EventEmitter();
  @Output() mapSwitch = new EventEmitter();

  unitGatheringGeometry = Document.Path.unitGeometry;
  gatheringGeometry = Document.Path.gatheringGeometry;

  geometrySource = Document.Path.unitGeometry;

  fieldLookup: {[path: string]: FlatField};
  visibleUnitFields: {[field: string]: boolean} = {};
  visibleGatheringFields: {[field: string]: boolean} = {};

  informalTaxonGroup: string;
  currentInformalTaxonGroup: string;

  private _isGeneralMap: boolean;
  private baseGatheringFields: string[] = [
    'gatherings[*].locality',
    'gatherings[*].localityDescription',
    'gatherings[*].images',
    'secureLevel'
  ];
  private baseUnitFields: string[] = [
    'gatherings[*].units[*].unitGathering.geometry',
    'gatherings[*].units[*].identifications[*].taxon',
    'gatherings[*].units[*].recordBasis',
    'gatherings[*].units[*].unitGathering.dateBegin',
    'gatherings[*].units[*].taxonConfidence',
    'gatherings[*].units[*].notes',
    'gatherings[*].units[*].unitGathering.geometry',
    'gatherings[*].units[*].count',
    'gatherings[*].units[*].images'
  ];

  private groupFields: {[group: string]: string[]} = {
    'MVL.1': [ // Linnut
      'gatherings[*].units[*].sex',
      'gatherings[*].units[*].lifeStage'
    ],
    'MVL.2': [ // Nisäkkäät
      'gatherings[*].units[*].sex'
    ],
    'MVL.21': [ // Kasvit
      'gatherings[*].units[*].plantLifeStage',
      'gatherings[*].units[*].plantStatusCode'
    ],
    'MVL.26': [ // Matelijat ja sammakko eläimet
      'gatherings[*].units[*].lifeStage'
    ],
    'MVL.27': [ // Kalat
    ],
    'MVL.31': [ // Perhoset
      'gatherings[*].units[*].lifeStage'
    ],
    'MVL.37': [ // Tuhatjalkaiset
      'gatherings[*].units[*].lifeStage'
    ],
    'MVL.39': [ // Äyriäiset
      'gatherings[*].units[*].lifeStage'
    ],
    'MVL.40': [ // Nilvijäiset
      'gatherings[*].units[*].lifeStage'
    ],
    'MVL.61': [ // Helttasienet
      'gatherings[*].units[*].plantLifeStage',
      'gatherings[*].units[*].smell',
      'gatherings[*].units[*].smellNotes',
      'gatherings[*].units[*].taste',
      'gatherings[*].units[*].tasteNotes',
    ],
    'MVL.232': [ // Hyönteiset ja hämähäkkielaimet
      'lifeStage',
      'hostInformalNameString'
    ],
    'MVL.233': [ // Sienet ja jäkälät
      'gatherings[*].units[*].plantLifeStage',
      'gatherings[*].units[*].smell',
      'gatherings[*].units[*].smellNotes',
      'gatherings[*].units[*].taste',
      'gatherings[*].units[*].tasteNotes',
    ]
  };

  gatheringFields = [
    'gatherings[*].country',
    'gatherings[*].administrativeProvince',
    'gatherings[*].municipality',
    'gatherings[*].biologicalProvince',
    'gatherings[*].locality',
    'gatherings[*].localityDescription',
    'gatherings[*].images',
    'secureLevel'
  ];

  otherGatheringFields = [
    'gatheringEvent.leg',
    'gatherings[*].weather',
    'gatherings[*].notes'
  ];

  unitFields = [
    'gatherings[*].units[*].identifications[*].taxon',
    'gatherings[*].units[*].recordBasis',
    'gatherings[*].units[*].unitGathering.dateBegin',
    'gatherings[*].units[*].images',
    'gatherings[*].units[*].taxonConfidence',
    'gatherings[*].units[*].count',
    'gatherings[*].units[*].sex',
    'gatherings[*].units[*].lifeStage',
    'gatherings[*].units[*].plantLifeStage',
    'gatherings[*].units[*].plantStatusCode',
    'gatherings[*].units[*].hostInformalNameString',
    'gatherings[*].units[*].smell',
    'gatherings[*].units[*].smellNotes',
    'gatherings[*].units[*].taste',
    'gatherings[*].units[*].tasteNotes',
    'gatherings[*].units[*].notes'
  ];

  unitListFields = [
    'gatherings[*].units[*].identifications[*].taxon',
    'gatherings[*].units[*].count',
    'gatherings[*].units[*].unitGathering.geometry'
  ];

  constructor() { }

  ngOnInit() {
    this.initLookup();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['formData'] ||
      changes['sections'] ||
      changes['extended'] ||
      changes['selectedFields']
    ) {
      this.initLookup();
      this.analyzeInformalTaxonGroup();
    }
  }

  @Input()
  set activeGroup(group) {
    this.informalTaxonGroup = group;
    this.analyzeInformalTaxonGroup();
  }


  @Input()
  set isGeneralMap(general: boolean) {
    this._isGeneralMap = general;
    const target = general ? this.gatheringGeometry : this.unitGatheringGeometry;
    if (this.geometrySource !== target) {
      this.geometrySource = target;
    }
  }

  get isGeneralMap() {
    return this._isGeneralMap;
  }

  onChange() {
    this.dataChange.emit(this.data);
  }

  onRowClick(row) {
    this.activate.emit({idx: row['_idx'] || 0, section: 'units'});
  }

  trackByFn(index, item) {
    return item;
  }

  private analyzeInformalTaxonGroup() {
    if (!this.fieldLookup) {
      return;
    }
    const taxonPath = this.fieldLookup[Document.Path.taxon].exactPath;
    const groupPath = this.fieldLookup[Document.Path.informalTaxonGroups].exactPath;
    const visibleUnit = {};
    const visibleGathering = {};
    let group = this.informalTaxonGroup;
    if (this.data && this.data[taxonPath] && this.data[groupPath]) {
      group = Array.isArray(this.data[groupPath]) ? this.data[groupPath][0] : '';
    }

    [...this.baseUnitFields, ...this.groupFields[group]].forEach(field => {
      visibleUnit[field] = this.extended ? true : (this.selectedFields || []).indexOf(field) !== -1;
    });
    this.gatheringFields.forEach(field => {
      visibleGathering[field] = this.extended ? true : this.baseGatheringFields.indexOf(field) !== -1;
    });
    this.visibleUnitFields = visibleUnit;
    this.visibleGatheringFields = visibleGathering;
    this.currentInformalTaxonGroup = group;
  }

  private initLookup() {
    if (!this.formData) {
      return;
    }
    const lookup = {};
    this.formData.fields.map(field => lookup[field.path] = field);
    this.fieldLookup = lookup;
  }

}
