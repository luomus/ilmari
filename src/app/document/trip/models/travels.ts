export interface Travel {
  start: string;
  end: string;
  coordinates: {lat: number, lng: number}[];
}
export interface Travels {
  [gathering: number]: Travel;
}
