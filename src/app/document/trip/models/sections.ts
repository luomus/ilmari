export type Section = 'gatherings'|'units'|'identifications'|'unitGathering'|'gatheringEvent';

export interface SectionHistory {
  gatherings: {
    [idx: number]: {units: SingleSection}
  };
}

export interface SubSections {
  [section: string]: Section[];
}

export interface SingleSection {
  active: number;
  max: number;
}

export interface ActiveSection {
  gatherings: SingleSection;
  units: SingleSection;
}

export interface Sections extends ActiveSection {
  _parentSections: SubSections;
  _subSectionStatus: SectionHistory;
  _subSections: SubSections;
  _allSections: Section[];
}
