import { ActionReducer, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import * as fromTrip from './reducers/trip-reducers';
import * as fromRoot from '../../../core/store';
import { localStorageSync } from 'ngrx-store-localstorage';

export interface TripState {
  trip: fromTrip.State;
}

export interface State extends fromRoot.State {
  'form-trip': TripState;
}

export const reducers = {
  trip: fromTrip.reducer
};

export function localStorageSyncReducer(reducer: ActionReducer<State>): ActionReducer<any, any> {
  return localStorageSync({
    keys: [
      {
        trip: [
          'mapFullscreen',
          'travel',
          'data',
          'sections',
          'activeGroup',
          'extended',
          'selectedFields',
          'generalMap'
        ]
      }
    ],
    rehydrate: true,
    removeOnUndefined: true,
    restoreDates: false
  })(reducer);
}

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: MetaReducer<State>[] = [localStorageSyncReducer];

export const getFormTripState = createFeatureSelector<TripState>('form-trip');

export const getTripState = createSelector(getFormTripState, (state: TripState) => state.trip);

export const getTripData = createSelector(getTripState, fromTrip.getData);
export const getTripForm = createSelector(getTripState, fromTrip.getForm);
export const getTripSections = createSelector(getTripState, fromTrip.getSections);
export const getTripTravel = createSelector(getTripState, fromTrip.getTravel);
export const getTripActiveGroup = createSelector(getTripState, fromTrip.getActiveGroup);
export const isTripRecording = createSelector(getTripState, fromTrip.isRecording);
export const isTripSaving = createSelector(getTripState, fromTrip.isSaving);
export const isTripMapFullscreen = createSelector(getTripState, fromTrip.isMapFullscreen);
export const getTripSelectedFields = createSelector(getTripState, fromTrip.getSelectedFields);
export const isTripExtended = createSelector(getTripState, fromTrip.isExtended);
export const isGeneralMap = createSelector(getTripState, fromTrip.isGeneralMap);
export const getTripErrors = createSelector(getTripState, fromTrip.getErrors);
