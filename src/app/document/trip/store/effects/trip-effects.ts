import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { FormService } from '../../../service/form.service';
import { environment } from '../../../../../environments/environment';
import * as trip from '../actions/trip-actions';
import * as tripActions from '../actions/trip-actions';
import { MoveSectionAction, SetActiveGroupAction } from '../actions/trip-actions';
import { TimeService } from '../../../../shared/service/time.service';
import { LocationService } from '../../../../shared/service/location.service';
import * as fromTrip from '../';
import * as fromUser from '../../../../user/store';
import * as layout from '../../../../core/store/actions/layout-actions';
import { DocumentService } from '../../../service/document.service';
import { MatSnackBar } from '@angular/material';
import { defer, EMPTY, forkJoin, Observable, of, Subscription } from 'rxjs';
import { catchError, concatMap, filter, map, take, tap } from 'rxjs/operators';
import { ImageService } from '../../../../shared/service/image.service';
import { TaxonService } from '../../../../shared/service/taxon.service';
import { FlatField } from '../../../../models/form';
import { Router } from '@angular/router';
import { LoginAction } from '../../../../user/store/actions/user-actions';
import * as user from '../../../../user/store/actions/user-actions';
import { UpdateDataAction } from '../actions/trip-actions';

@Injectable()
export class TripEffects {
  location$: Subscription;

  @Effect()
  init$: Observable<Action> = defer(() => {
    return of(null).pipe(
      tap(() => this.store.dispatch(new layout.IsLoadingAction(true))),
      concatMap(() => this.formService.getForm(environment.forms.trip)),
      map((form) => new trip.LoadFormAction(form)),
      tap(() => this.store.dispatch(new layout.IsLoadingAction(false)))
    );
  });

  @Effect()
  onSavingStart: Observable<Action> = this.actions$
    .ofType(trip.START_SAVING).pipe(
      concatMap(() => this.store.select(fromTrip.isTripRecording).pipe(take(1))),
      tap((recording) => {if (recording) { this.store.dispatch(new trip.ToggleRecordingAction()); }}),
      concatMap(() => forkJoin(
        this.store.select(fromTrip.getTripData).pipe(take(1)),
        this.store.select(fromTrip.getTripTravel).pipe(take(1)),
        this.store.select(fromTrip.getTripForm).pipe(take(1)),
        this.store.select(fromUser.getUserSettings).pipe(take(1))
      ).pipe(map(data => ({data: data[0], travel: data[1], form: data[2], settings: data[3]})))),
      concatMap(stored => this.documentService.flatDataToDocument(stored.data, stored.form.fields).pipe(
        map(document => stored.travel ? this.documentService.travelsToDocument(stored.travel, document) : document),
        map(document => this.documentService.addInterpretations(document, stored.settings)),
        tap(document => this.documentService.markTaxonAsUser(this.documentService.taxonCountInDocument(document)))
      )),
      concatMap(document => this.store.select(fromUser.getPersonToken).pipe(
        take(1),
        map(token => ({document, token}))
      )),
      concatMap(data => this.documentService.saveDocument(data.document, data.token).pipe(
        catchError(body => of({_error: DocumentService.getErrorMessages(body)} as any))
      )),
      catchError(body => of({_error: DocumentService.getErrorMessages(body)} as any)),
      tap(result => this.snackBar.open(result && !result._error ? 'Successfully saved document' : 'Failed to save', 'Dismiss', {duration: 3000})),
      map(result => result && !result._error ? new trip.SavingOkAction(result) : new trip.SavingFailedAction(result._error))
    );


  @Effect()
  onMove$: Observable<Action> = this.actions$
    .ofType(trip.MOVE_SECTION).pipe(
      map((action: MoveSectionAction) => new trip.ActivateSectionAction({idx: action.payload.toIdx, section: action.payload.section}))
    );

  @Effect()
  onGroupChange$: Observable<Action> = this.actions$
    .ofType(trip.SET_ACTIVE_GROUP).pipe(
      map((action: SetActiveGroupAction) => action.payload),
      tap(group => FormService.updateDefaults({informalTaxonID: group})),
      concatMap(() => EMPTY)
    );

  @Effect()
  toggleRecording: Observable<Action> = this.actions$
    .ofType(trip.TOGGLE_RECORDING).pipe(
      concatMap(() => this.store.select(fromTrip.isTripRecording).pipe(take(1))),
      map(recording => {
        if (recording) {
          this.locationService.stopRecording();
          this.location$.unsubscribe();
          return new trip.StopRecordingAction(this.timeService.getCurrentTime());
        }
        this.locationService.startRecording();
        this.location$ = this.locationService.location$.pipe(filter(coordinates => coordinates !== null))
          .subscribe(coordinates => this.store.dispatch(new tripActions.AddLocationAction(coordinates)));
        return new trip.StartRecordingAction(this.timeService.getCurrentTime());
      })
    );

  @Effect()
  addObservation$: Observable<Action> = this.actions$
    .ofType<trip.AddObservationAction>(trip.ADD_OBSERVATION).pipe(
      map(action => action.payload),
      concatMap(data => this.store.select(fromTrip.getTripForm).pipe(take(1), map(form => ({...data, form: form})))),
      concatMap(data => this.locationService.getCurrentLocation().pipe(map(position => ({...data, position: position})))),
      concatMap(data => data.taxonID ? this.taxonService.get(data.taxonID).pipe(map(taxon => ({...data, taxon: taxon}))) : of(data)),
      concatMap(data => data.image ? this.imageService.save(data.image).pipe(map(id => ({...data, image: id}))) : of(data)),
      concatMap(data => {
        const document = {};
        data.form.fields.forEach((field: FlatField) => {
          if (field.path === 'gatherings[*].units[*].identifications[*].taxon' && data.taxon) {
            document[field.exactPath] = data.taxon.vernacularName || data.taxon.scientificName;
          } else if (field.path === 'gatherings[*].units[*].autocompleteSelectedTaxonID' && data.taxon) {
            document[field.exactPath] = data.taxon.id;
          } else if (field.path === 'gatherings[*].units[*].images' && data.image) {
            document[field.exactPath] = [data.image];
          } else if (field.path === 'gatherings[*].units[*].informalTaxonGroups') {
            document[field.exactPath] = ['MVL.61'];
          } else if (field.path === 'gatherings[*].units[*].unitGathering.dateBegin') {
            document[field.exactPath] = this.timeService.dateToDateTimeString(new Date, false);
          } else if (field.path === 'gatherings[*].units[*].unitGathering.geometry' && data.position) {
            document[field.exactPath] = {type: 'Point', coordinates: [data.position.coords.longitude,  data.position.coords.latitude]};
          }
        });
        return of(new trip.UpdateDataAction(document));
      }),
      tap(() => this.router.navigate(['/observations/trip']))
    );

  @Effect()
  login$: Observable<Action> = this.actions$
    .ofType<LoginAction>(user.LOGIN).pipe(
      map(action => action.payload),
      map((data) => new UpdateDataAction({'gatheringEvent.leg': [data.person.id]}))
    );

  constructor(
    private actions$: Actions,
    private formService: FormService,
    private timeService: TimeService,
    private locationService: LocationService,
    private store: Store<fromTrip.State>,
    private documentService: DocumentService,
    private snackBar: MatSnackBar,
    private imageService: ImageService,
    private taxonService: TaxonService,
    private router: Router
  ) {}
}
