import { Action } from '@ngrx/store';
import { Form } from '../../../../models/form';
import { Section } from '../../models/sections';
import { Document } from '../../../../models/document/document';
import { ExtendedTaxon } from '../../../../models/taxon';

export const LOAD_FORM = '[Trip] load form';
export const ADD_SECTION = '[Trip] add section';
export const DEL_SECTION = '[Trip] del section';
export const MOVE_SECTION = '[Trip] move section';
export const ACTIVATE_SECTION = '[Trip] activate section';
export const SET_DATA = '[Trip] set data';
export const UPDATE_DATA = '[Trip] update data';
export const TOGGLE_RECORDING = '[Trip] toggle recording';
export const START_RECORDING = '[Trip] start recording';
export const STOP_RECORDING = '[Trip] stop recording';
export const ADD_LOCATION = '[Trip] add location';
export const START_SAVING = '[Trip] start saving';
export const SAVING_OK = '[Trip] saving ok';
export const CLEAR_FORM = '[Trip] clear form';
export const SAVING_FAILED = '[Trip] start failed';
export const TOGGLE_FULLSCREEN_MAP = '[Trip] toggle fullscreen map';
export const TOGGLE_EXTENDED = '[Trip] toggle extended';
export const SET_ACTIVE_GROUP = '[Trip] set active group';
export const TOGGLE_GENERAL_MAP = '[Trip] toggle general map';
export const ADD_OBSERVATION = '[Trip] add observation';


export class SavingFailedAction implements Action {
  readonly type = SAVING_FAILED;

  constructor(public payload: {[field: string]: string[]}) {}
}

export class SetActiveGroupAction implements Action {
  readonly type = SET_ACTIVE_GROUP;

  constructor(public payload: string) {}
}

export class ToggleFullscreenMapAction implements Action {
  readonly type = TOGGLE_FULLSCREEN_MAP;
}

export class ToggleExtendedAction implements Action {
  readonly type = TOGGLE_EXTENDED;
}

export class ClearFormAction implements Action {
  readonly type = CLEAR_FORM;
}

export class SavingOkAction implements Action {
  readonly type = SAVING_OK;

  constructor(public payload: Document) {}
}

export class StartSavingAction implements Action {
  readonly type = START_SAVING;
}

export class LoadFormAction implements Action {
  readonly type = LOAD_FORM;

  constructor(public payload: Form) {}
}

export class ToggleRecordingAction implements Action {
  readonly type = TOGGLE_RECORDING;

  constructor() {}
}

export class StartRecordingAction implements Action {
  readonly type = START_RECORDING;

  constructor(public payload: string) {}
}

export class StopRecordingAction implements Action {
  readonly type = STOP_RECORDING;

  constructor(public payload: string) {}
}

export class AddLocationAction implements Action {
  readonly type = ADD_LOCATION;

  constructor(public payload: {lat: number, lng: number}) {}
}

export class AddSectionAction implements Action {
  readonly type = ADD_SECTION;

  constructor(public payload: Section) {}
}

export class MoveSectionAction implements Action {
  readonly type = MOVE_SECTION;

  constructor(public payload: {fromIdx: number, toIdx: number, section: Section}) {}
}

export class ActivateSectionAction implements Action {
  readonly type = ACTIVATE_SECTION;

  constructor(public payload: {idx: number, section: Section}) {}
}

export class DelSectionAction implements Action {
  readonly type = DEL_SECTION;

  constructor(public payload: {idx: number, section: Section}) {}
}

export class SetDataAction implements Action {
  readonly type = SET_DATA;

  constructor(public payload: any) {}
}

export class UpdateDataAction implements Action {
  readonly type = UPDATE_DATA;

  constructor(public payload: any) {}
}

export class AddObservationAction implements Action {
  readonly type = ADD_OBSERVATION;

  constructor(public payload: {
    image?: string,
    taxonID?: string,
    taxon?: ExtendedTaxon
  }) {}
}

export class ToggleGeneralMapAction implements Action {
  readonly type = TOGGLE_GENERAL_MAP;
}

export type Actions
  = LoadFormAction
  | ToggleExtendedAction
  | ToggleFullscreenMapAction
  | ClearFormAction
  | SavingFailedAction
  | SavingOkAction
  | StartSavingAction
  | ToggleRecordingAction
  | StartRecordingAction
  | StopRecordingAction
  | AddLocationAction
  | MoveSectionAction
  | ActivateSectionAction
  | AddSectionAction
  | DelSectionAction
  | SetDataAction
  | UpdateDataAction
  | AddObservationAction
  | SetActiveGroupAction
  | ToggleGeneralMapAction
;
