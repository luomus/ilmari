import { Form } from '../../../../models/form';
import { Sections } from '../../models/sections';
import { FormService } from '../../../service/form.service';
import { Travels } from '../../models/travels';
import * as tripAction from '../actions/trip-actions';
import { environment } from '../../../../../environments/environment';

export interface State {
  generalMap: boolean;
  recording: boolean;
  saving: boolean;
  mapFullscreen: boolean;
  travel: Travels;
  form?: Form;
  data: any;
  sections: Sections;
  activeGroup: string;
  selectedFields: string[];
  extended: boolean;
  errors: {[field: string]: string[]};
  warnings: {[field: string]: string[]};
}

export const initialState: State = {
  generalMap: false,
  extended: false,
  recording: false,
  saving: false,
  mapFullscreen: false,
  activeGroup: environment.defaults.informalTaxonGroup,
  travel: {},
  data: {},
  selectedFields: [
    'gatherings[*].units[*].unitGathering.geometry',
    'gatherings[*].units[*].identifications[*].taxon',
    'gatherings[*].units[*].unitGathering.dateBegin',
    'gatherings[*].units[*].notes',
    'gatherings[*].units[*].unitGathering.geometry',
    'gatherings[*].units[*].count',
    'gatherings[*].units[*].images',
    'gatherings[*].country',
    'gatherings[*].administrativeProvince',
    'gatherings[*].municipality',
    'gatherings[*].biologicalProvince',
    'gatherings[*].locality',
    'gatherings[*].localityDescription',
    'secureLevel',
    'gatheringEvent.leg',
    'gatherings[*].weather',
    'gatherings[*].notes'
  ],
  sections: {
    units: {
      active: 0,
      max: 0
    },
    gatherings: {
      active: 0,
      max: 0
    },
    _allSections: ['gatherings', 'units'],
    _subSectionStatus: {gatherings: {}},
    _subSections: {gatherings: ['units']},
    _parentSections: {units: ['gatherings']}
  },
  errors: {},
  warnings: {}
};

export function reducer(
  state = initialState,
  action: tripAction.Actions
): State {
  switch (action.type) {
    case tripAction.SET_ACTIVE_GROUP:
      return {
        ...state,
        activeGroup: action.payload,
        selectedFields: initialState.selectedFields
      };
    case tripAction.TOGGLE_FULLSCREEN_MAP:
      return {
        ...state,
        mapFullscreen: !state.mapFullscreen
      };
    case tripAction.TOGGLE_GENERAL_MAP:
      return {
        ...state,
        generalMap: !state.generalMap
      };
    case tripAction.TOGGLE_EXTENDED:
      return {
        ...state,
        extended: !state.extended
      };
    case tripAction.SAVING_FAILED:
      return {
        ...state,
        saving: false,
        errors: action.payload
      };
    case tripAction.SAVING_OK:
    case tripAction.CLEAR_FORM:
      const emptySections = {
        ...state.sections,
        _subSectionStatus: {gatherings: {}},
        units: {
          active: 0,
          max: 0
        },
        gatherings: {
          active: 0,
          max: 0
        }
      };
      const emptyFields = FormService.setExactPath(state.form.fields, emptySections);
      return {
        ...state,
        errors: {},
        warnings: {},
        form: {...state.form, fields: emptyFields},
        saving: false,
        travel: {},
        data: FormService.addDefaults(emptyFields, {}),
        sections: emptySections
      };
    case tripAction.START_SAVING:
      return {
        ...state,
        saving: true
      };
    case tripAction.LOAD_FORM:
      const fields = FormService.setExactPath(action.payload.fields, state.sections);
      return {
        ...state,
        data: FormService.addDefaults(fields, state.data),
        form: {
          ...action.payload,
          fields: fields
        }
      };
    case tripAction.ADD_OBSERVATION:
    case tripAction.ADD_SECTION:
      if (!state.form) {
        return state;
      }
      const section = typeof action.payload === 'string' ? action.payload : 'units';

      if (section === 'units' && FormService.isEmptyUnit(state.data, state.form.fields)) {
        return state;
      }

      const newSectionIdx = state.sections[section].max + 1;
      const newSection: Sections = {
        ...state.sections,
        ...FormService.fetchFromSubSectionStatus(section, newSectionIdx, state.sections._subSectionStatus, state.sections._subSections),
        [section]: {
          max: newSectionIdx,
          active: newSectionIdx
        }
      };
      newSection._subSectionStatus = FormService.makeSubSectionStatus(state.sections, newSection[section], section);
      const newFields = FormService.setExactPath(state.form.fields, newSection);
      return {
        ...state,
        data: FormService.addDefaults(newFields, state.data),
        form: {
          ...state.form,
          fields: newFields
        },
        sections: newSection
      };
    case tripAction.MOVE_SECTION:
      return {
        ...state,
        data: FormService.moveData(action.payload.section, action.payload.fromIdx, action.payload.toIdx, state.sections, state.data),
        sections: FormService.moveSection(action.payload.section, action.payload.fromIdx, action.payload.toIdx, state.sections)
      };
    case tripAction.DEL_SECTION:
      if (state.sections[action.payload.section].max - 1 < 0 || !state.form) {
        return state;
      }
      const delSection = FormService.delFromSection(action.payload, state.sections);
      return {
        ...state,
        data: FormService.delFromData(action.payload, state.sections, state.data),
        travel: FormService.delFromTravel(action.payload, state.sections, state.travel),
        form: {
          ...state.form,
          fields: FormService.setExactPath(state.form.fields, delSection)
        },
        sections: delSection
      };
    case tripAction.ACTIVATE_SECTION:
      if (!state.form) {
        return state;
      }
      const activeSection = {
        ...state.sections,
        ...FormService.fetchFromSubSectionStatus(action.payload.section, action.payload.idx, state.sections._subSectionStatus, state.sections._subSections),
        [action.payload.section]: {
          active: action.payload.idx,
          max: state.sections[action.payload.section].max
        }
      };
      activeSection._subSectionStatus = FormService.makeSubSectionStatus(
        state.sections,
        activeSection[action.payload.section],
        action.payload.section
      );
      return {
        ...state,
        form: {
          ...state.form,
          fields: FormService.setExactPath(state.form.fields, activeSection)
        },
        sections: activeSection
      };
    case tripAction.SET_DATA:
      return {
        ...state,
        data: {...action.payload}
      };
    case tripAction.UPDATE_DATA:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.payload
        }
      };
    case tripAction.START_RECORDING:
      return {
        ...state,
        recording: true,
        travel: {
          ...state.travel,
          [state.sections.gatherings.active]: {
            start: state.travel[state.sections.gatherings.active] && state.travel[state.sections.gatherings.active].start || action.payload,
            end: '',
            coordinates: state.travel[state.sections.gatherings.active] && state.travel[state.sections.gatherings.active].coordinates || []
          }
        }
      };
    case tripAction.STOP_RECORDING:
      return {
        ...state,
        recording: false,
        travel: {
          ...state.travel,
          [state.sections.gatherings.active]: {
            ...state.travel[state.sections.gatherings.active],
            end: action.payload
          }
        }
      };
    case tripAction.ADD_LOCATION:
      return {
        ...state,
        travel: {
          ...state.travel,
          [state.sections.gatherings.active]: {
            ...state.travel[state.sections.gatherings.active],
            coordinates: [
              ...state.travel[state.sections.gatherings.active].coordinates,
              action.payload
            ]
          }
        }
      };
    default: {
      return state;
    }
  }
}

export const getForm = (state: State): Form => state.form;
export const getData = (state: State) => state.data;
export const getTravel = (state: State) => state.travel;
export const getSections = (state: State) => state.sections;
export const getActiveGroup = (state: State) => state.activeGroup;
export const isRecording = (state: State) => state.recording;
export const isSaving = (state: State) => state.saving;
export const isMapFullscreen = (state: State) => state.mapFullscreen;
export const getSelectedFields = (state: State) => state.selectedFields;
export const isExtended = (state: State) => state.extended;
export const isGeneralMap = (state: State) => state.generalMap;
export const getErrors = (state: State): {[field: string]: string[]} => state.errors;
