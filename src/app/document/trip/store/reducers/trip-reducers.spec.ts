import * as fromTrip from './trip-reducers';
import * as fromActions from '../actions/trip-actions';
import { Form } from '../../../../models/form';
import { Sections } from '../../models/sections';

const MockForm: Form = {
  id: 'JX.519',
  fields: [
    {
      parent: 'gatheringUnit',
      name: 'dateBegin',
      path: 'gatherings[*].units[*].gatheringUnit[*]',
      type: 'text',
      label: 'Date begin'
    },
    {
      parent: 'units',
      name: 'name',
      default: 'Parus major',
      path: 'gatherings[*].units[*].name',
      type: 'text',
      label: 'Name'
    },
    {
      parent: 'gatherings',
      name: 'location',
      path: 'gatherings[*].location',
      type: 'text',
      label: 'Location'
    },
    {
      parent: 'document',
      name: 'additionalID',
      path: 'additionalID',
      type: 'text',
      label: 'Additional ID'
    }
  ],
  title: 'Test form',
  description: 'Unit testing form',
  language: 'en'
};

describe('TripReducer', () => {
  const sectionsBase: Sections = {
    gatherings: {active: 0, max: 0},
    units: {active: 0, max: 0},
    _allSections: ['gatherings', 'units'],
    _subSectionStatus: {gatherings: {}},
    _subSections: {gatherings: ['units']},
    _parentSections: {units: ['gatherings']}
  };
  describe('undefined action', () => {
    it(`should return the default state`, () => {
      const {initialState} = fromTrip;
      const action = {} as any;
      const state = fromTrip.reducer(undefined, action);

      expect(state).toBe(initialState);
    });
  });
  describe('SET_ACTIVE_GROUP action', () => {
    it(`should set active group`, () => {
      const newValue = 'MVL.2';
      const action = new fromActions.SetActiveGroupAction(newValue);
      const state = fromTrip.reducer(undefined, action);

      expect(state.activeGroup).toEqual(newValue);
    });
  });
  describe('TOGGLE_FULLSCREEN_MAP action', () => {
    it(`should toggle fullscreen map true when no state`, () => {
      const action = new fromActions.ToggleFullscreenMapAction();
      const state = fromTrip.reducer(undefined, action);

      expect(state.mapFullscreen).toEqual(true);
    });
    it(`should toggle fullscreen map to false`, () => {
      const {initialState} = fromTrip;
      const previousState = {...initialState, mapFullscreen: true};
      const action = new fromActions.ToggleFullscreenMapAction();
      const state = fromTrip.reducer(previousState, action);

      expect(state.mapFullscreen).toEqual(false);
    });
  });
  describe('UPDATE_DATA action', () => {
    it(`should toggle fullscreen map to false`, () => {
      const {initialState} = fromTrip;
      const previousState = {...initialState, data: {'test': 1, 'foo': 'bar', 'over': 'to'}};
      const action = new fromActions.UpdateDataAction({'test': 33, 'over': ''});
      const state = fromTrip.reducer(previousState, action);

      expect(state.data).toEqual({'test': 33, 'foo': 'bar', 'over': ''});
    });
  });
  describe('LOAD_FORM action', () => {
    it(`should set default values and exact paths`, () => {
      const {initialState} = fromTrip;
      const previousState = {...initialState, data: {'test': 1, 'foo': 'bar'}};
      const action = new fromActions.LoadFormAction(MockForm);
      const state = fromTrip.reducer(previousState, action);
      const fields = MockForm.fields.map(field => ({
        ...field,
        exactPath: field.path.replace(/\[\*]/g, '[0]')
      }));

      expect(state.data).toEqual({
        'formID': 'JX.519',
        'test': 1,
        'foo': 'bar',
        'gatherings[0].units[0].name': 'Parus major'
      });
      expect(state.form.fields).toEqual(fields);
    });
    it(`should not override existing data with default values`, () => {
      const {initialState} = fromTrip;
      const previousState = {...initialState, data: {'gatherings[0].units[0].name': 'FooBar'}};
      const action = new fromActions.LoadFormAction(MockForm);
      const state = fromTrip.reducer(previousState, action);
      const fields = MockForm.fields.map(field => ({
        ...field,
        exactPath: field.path.replace(/\[\*]/g, '[0]')
      }));

      expect(state.data).toEqual({
        'formID': 'JX.519',
        'gatherings[0].units[0].name': 'FooBar'
      });
      expect(state.form.fields).toEqual(fields);
    });
  });
  describe('ADD_SECTION action', () => {
    it(`should add gathering`, () => {
      const loadFormAction = new fromActions.LoadFormAction(MockForm);
      const action = new fromActions.AddSectionAction('gatherings');
      const state = fromTrip.reducer(fromTrip.reducer(undefined, loadFormAction), action);

      expect(state.data).toEqual({
        'formID': 'JX.519',
        'gatherings[0].units[0].name': 'Parus major',
        'gatherings[1].units[0].name': 'Parus major'
      });
    });
    it(`should add unit`, () => {
      const loadFormAction = new fromActions.LoadFormAction(MockForm);
      const action = new fromActions.AddSectionAction('units');
      const state = fromTrip.reducer(fromTrip.reducer(undefined, loadFormAction), action);

      expect(state.data).toEqual({
        'formID': 'JX.519',
        'gatherings[0].units[0].name': 'Parus major',
        'gatherings[0].units[1].name': 'Parus major'
      });
    });
  });
  describe('MOVE_SECTION action', () => {
    const moveTestData = {
      'gatherings[0].units[0].name': 'Diptera',
      'gatherings[1].units[0].name': 'Parus major',
      'gatherings[1].units[1].name': 'Bufo bufo'
    };
    it(`should move gathering`, () => {
      const {initialState} = fromTrip;
      const previousState = {...initialState, data: moveTestData};
      const action = new fromActions.MoveSectionAction({section: 'gatherings', toIdx: 0, fromIdx: 1});
      const state = fromTrip.reducer(previousState, action);

      expect(state.data).toEqual({
        'gatherings[1].units[0].name': 'Diptera',
        'gatherings[0].units[0].name': 'Parus major',
        'gatherings[0].units[1].name': 'Bufo bufo'
      });
    });
    it(`should move unit`, () => {
      const {initialState} = fromTrip;
      const previousState = {...initialState, data: moveTestData};
      const action = new fromActions.MoveSectionAction({section: 'units', toIdx: 1, fromIdx: 0});
      const state = fromTrip.reducer(previousState, action);

      expect(state.data).toEqual({
        'gatherings[0].units[1].name': 'Diptera',
        'gatherings[1].units[0].name': 'Parus major',
        'gatherings[1].units[1].name': 'Bufo bufo'
      });
    });
    it(`should move unit within active gathering`, () => {
      const {initialState} = fromTrip;
      const moveTestSections: Sections = {...sectionsBase, gatherings: {active: 1, max: 1}};
      const previousState = {...initialState, data: moveTestData, sections: moveTestSections};
      const action = new fromActions.MoveSectionAction({section: 'units', toIdx: 1, fromIdx: 0});
      const state = fromTrip.reducer(previousState, action);

      expect(state.data).toEqual({
        'gatherings[0].units[0].name': 'Diptera',
        'gatherings[1].units[1].name': 'Parus major',
        'gatherings[1].units[0].name': 'Bufo bufo'
      });
    });
  });
  describe('DEL_SECTION action', () => {
    const delTestData = {
      'gatherings[0].location': 'Kuru',
      'gatherings[0].units[0].name': 'Diptera',
      'gatherings[1].units[0].name': 'Parus major',
      'gatherings[1].units[0].gatheringUnit[0].location': 'Parkkuu',
      'gatherings[1].units[1].name': 'Bufo bufo'
    };
    const delSections: Sections = {...sectionsBase, gatherings: {active: 0, max: 1}, units: {active: 0, max: 0}};
    it(`should delete gathering`, () => {
      const {initialState} = fromTrip;
      const previousState: fromTrip.State = {...initialState, data: delTestData, sections: delSections, form: MockForm};
      const action = new fromActions.DelSectionAction({section: 'gatherings', idx: 0});
      const state = fromTrip.reducer(previousState, action);

      expect(state.data).toEqual({
        'gatherings[0].units[0].name': 'Parus major',
        'gatherings[0].units[0].gatheringUnit[0].location': 'Parkkuu',
        'gatherings[0].units[1].name': 'Bufo bufo'
      });
    });
    it(`should not delete only unit`, () => {
      const {initialState} = fromTrip;
      const previousState: fromTrip.State = {...initialState, data: delTestData, sections: delSections, form: MockForm};
      const action = new fromActions.DelSectionAction({section: 'units', idx: 0});
      const state = fromTrip.reducer(previousState, action);

      expect(state.data).toEqual({
        'gatherings[0].location': 'Kuru',
        'gatherings[0].units[0].name': 'Diptera',
        'gatherings[1].units[0].name': 'Parus major',
        'gatherings[1].units[0].gatheringUnit[0].location': 'Parkkuu',
        'gatherings[1].units[1].name': 'Bufo bufo'
      });
    });
    it(`should delete last unit within active gathering`, () => {
      const {initialState} = fromTrip;
      const delTestSections: Sections = {...delSections, gatherings: {active: 1, max: 1}, units: {active: 1, max: 1}};
      const previousState: fromTrip.State = {...initialState, data: delTestData, sections: delTestSections, form: MockForm};
      const action = new fromActions.DelSectionAction({section: 'units', idx: 1});
      const state = fromTrip.reducer(previousState, action);

      expect(state.data).toEqual({
        'gatherings[0].location': 'Kuru',
        'gatherings[0].units[0].name': 'Diptera',
        'gatherings[1].units[0].name': 'Parus major',
        'gatherings[1].units[0].gatheringUnit[0].location': 'Parkkuu'
      });
    });
    it(`should delete first unit within active gathering`, () => {
      const {initialState} = fromTrip;
      const delTestSections: Sections = {...delSections, gatherings: {active: 1, max: 1}, units: {active: 0, max: 1}};
      const previousState: fromTrip.State = {...initialState, data: delTestData, sections: delTestSections, form: MockForm};
      const action = new fromActions.DelSectionAction({section: 'units', idx: 0});
      const state = fromTrip.reducer(previousState, action);

      expect(state.data).toEqual({
        'gatherings[0].location': 'Kuru',
        'gatherings[0].units[0].name': 'Diptera',
        'gatherings[1].units[0].name': 'Bufo bufo'
      });
    });
    it(`should delete element that is not active`, () => {
      const {initialState} = fromTrip;
      const delTestSections: Sections = {...delSections, gatherings: {active: 1, max: 1}, units: {active: 0, max: 1}};
      const previousState: fromTrip.State = {...initialState, data: delTestData, sections: delTestSections, form: MockForm};
      const action = new fromActions.DelSectionAction({section: 'gatherings', idx: 0});
      const state = fromTrip.reducer(previousState, action);

      expect(state.data).toEqual({
        'gatherings[0].units[0].name': 'Parus major',
        'gatherings[0].units[0].gatheringUnit[0].location': 'Parkkuu',
        'gatherings[0].units[1].name': 'Bufo bufo'
      });
    });
  });
});
