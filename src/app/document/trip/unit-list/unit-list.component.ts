import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Section, Sections } from '../models/sections';
import { UnitDataSource } from './unit-datasource';
import { FlatField } from '../../../models/form';
import { MatSort } from '@angular/material';
import { Document } from '../../../models/document/document';

export interface Row {
  [path: string]: Cell;
}

interface Cell {
  value: any;
  exact: string;
}

@Component({
  selector: 'ilm-unit-list',
  templateUrl: './unit-list.component.html',
  styleUrls: ['./unit-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UnitListComponent implements OnChanges, AfterViewInit {

  @ViewChild(MatSort) sort: MatSort;
  @Input() sections: Sections;
  @Input() fieldLookup: {[path: string]: FlatField};
  @Input() selectedFields: string[] = [];
  @Input() data: any;
  @Output() rowClick = new EventEmitter<number>();
  @Output() delete = new EventEmitter<{idx: number, section: string}>();
  @Output() move = new EventEmitter<{fromIdx: number, toIdx: number, section: Section}>();
  @Output() updateData = new EventEmitter<{[value: string]: any}>();

  tableData:  UnitDataSource = new UnitDataSource([]);
  tableCols: string[] = [];
  idxKey = '_idx';
  groupCol = Document.Path.informalTaxonGroups;
  unitCount = 0;

  constructor() { }

  ngOnChanges(change: SimpleChanges) {
    this.initTableData();
  }

  ngAfterViewInit() {
    this.tableData.sort = this.sort;
  }

  initTableData() {
    if (!this.data || !this.sections) {
      this.unitCount = 0;
      return;
    }
    const currentGathering = `gatherings[${this.sections.gatherings.active}]`;
    const allKeys = [];
    let data = [];
    const cols = [...this.selectedFields, this.groupCol];
    cols.forEach(pathKey => {
      for (let idx = 0; idx <= this.sections.units.max; idx++) {
        const exactKey = pathKey
          .replace('gatherings[*]', currentGathering)
          .replace('units[*]', `units[${idx}]`)
          .replace(/\[\*]/, '[0]');
        if (!data[idx]) {
          data[idx] = {[this.idxKey]: idx};
        }
        allKeys[pathKey] = true;
        data[idx][pathKey] = {
          value: this.data[exactKey],
          exact: exactKey
        };
      }
    });
    data = data.filter(val => !!val);
    data.reverse();
    this.tableCols = [this.idxKey, this.groupCol, ...this.selectedFields, '_actions'];
    this.tableData = new UnitDataSource(data);
    this.unitCount = data.length;
    if (this.sort) {
      this.tableData.sort = this.sort;
    }
  }

  addObservation(cell: Cell, addition = 1) {
    const newValue = cell.value ? addition + (+cell.value) : addition;
    this.updateData.emit({[cell.exact]: '' + newValue});
  }

  addingPossible(cell: Cell) {
    return !isNaN(cell.value) || cell.value === undefined;
  }

  getFieldValue(field: FlatField, cell: any) {
    if (field.valueLabels) {
      return typeof field.valueLabels[cell.value] !== 'undefined' ? field.valueLabels[cell.value] : '';
    }
    if (field.name === 'geometry') {
      return cell.value ? '<i class="material-icons">&#xE5CA;</i>' : '';
    }
    return cell.value;
  }

  trackByIdx(idx, item) {
    return item ? item[this.idxKey] : idx;
  }

}
