import { MatTableDataSource } from '@angular/material';
import { Row } from './unit-list.component';
import { _isNumberValue } from '@angular/cdk/coercion';

export class UnitDataSource extends MatTableDataSource<Row> {

  /**
   * Data accessor function that is used for accessing data properties for sorting through
   * the default sortData function.
   * This default function assumes that the sort header IDs (which defaults to the column name)
   * matches the data's properties (e.g. column Xyz represents data['Xyz']).
   * May be set to a custom function for different behavior.
   * @param data Data object that is being accessed.
   * @param sortHeaderId The name of the column that represents the data.
   */
  sortingDataAccessor: ((data: Row, sortHeaderId: string) => string|number) =
    (data: Row, sortHeaderId: string): string|number => {
      const value: any = typeof data[sortHeaderId] === 'object' ? data[sortHeaderId].value : data[sortHeaderId];
      return _isNumberValue(value) ? Number(value) : value;
    }
}
