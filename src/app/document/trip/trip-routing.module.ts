import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormContainerComponent } from './form-container/form-container.component';
import { HeaderContainerComponent } from './header-container/header-container.component';

const routes: Routes = [
  {path: '', component: FormContainerComponent, pathMatch: 'full'},
  {path: '', component: HeaderContainerComponent, outlet: 'header'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TripRoutingModule { }
