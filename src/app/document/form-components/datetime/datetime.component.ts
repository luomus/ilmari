import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostBinding, Inject, Input, NgZone, OnDestroy, OnInit, Optional, Self, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { TimeService } from '../../../shared/service/time.service';
import { MAT_INPUT_VALUE_ACCESSOR, MAT_OPTION_PARENT_COMPONENT, MatFormFieldControl, MatInput } from '@angular/material';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { ErrorStateMatcher } from '@angular/material/core';
import { Platform } from '@angular/cdk/platform';
import { AutofillMonitor } from '@angular/cdk/text-field';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'ilm-datetime',
  templateUrl: './datetime.component.html',
  styleUrls: ['./datetime.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {provide: MatFormFieldControl, useExisting: DatetimeComponent},
    {provide: MAT_OPTION_PARENT_COMPONENT, useExisting: DatetimeComponent}
  ]
})
export class DatetimeComponent extends MatInput implements OnInit, OnDestroy, ControlValueAccessor {

  date = '';
  hour = '';
  minute  = '';
  hours: string[] = [];
  minutes: string[] = [];

  private dateChangeObs = new Subject();
  private dateChangeSub: Subscription;

  onChange: any = () => {};
  onTouched: any = () => {};

  constructor(
    private cdr: ChangeDetectorRef,
    private timeService: TimeService,
    private fm: FocusMonitor,
    protected _elementRef: ElementRef,
    protected _platform: Platform,
    autofillMonitor: AutofillMonitor,
    ngZone: NgZone,
    @Optional() @Self() public ngControl: NgControl,
    @Optional() _parentForm: NgForm,
    @Optional() _parentFormGroup: FormGroupDirective,
    _defaultErrorStateMatcher: ErrorStateMatcher,
    @Optional() @Self() @Inject(MAT_INPUT_VALUE_ACCESSOR) inputValueAccessor: any
  ) {
    super(_elementRef, _platform, ngControl, _parentForm, _parentFormGroup, _defaultErrorStateMatcher, inputValueAccessor, autofillMonitor, ngZone);
    this.hours = timeService.hours;
    this.minutes = timeService.minutes;
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
    fm.monitor(_elementRef.nativeElement, true).subscribe(origin => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  ngOnInit() {
    this.dateChangeSub = this.dateChangeObs.pipe(
      debounceTime(2000)
    ).subscribe((date: Date) => {
      if (date) {
        this.date = date.getFullYear() +
          '-' + ('' + (date.getMonth() + 1)).padStart(2, '0') +
          '-' + ('' + (date.getDate())).padStart(2, '0');
      } else {
        this.date = '';
      }
      this.valueChange();
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.stateChanges.complete();
    this.fm.stopMonitoring(this._elementRef.nativeElement);
    this.dateChangeSub.unsubscribe();
  }

  valueChange() {
    const date = this.date + (this.hour ? `T${this.hour}:${this.minute}:00` : '');
    this.onChange(date);
    this.onTouched();
    this.stateChanges.next();
    this.cdr.markForCheck();
  }

  dateChange(date: Date) {
    this.dateChangeObs.next(date);
  }

  dateSelected(event) {
    this.date = this.timeService.dateToDateString(event);
    this.valueChange();
  }

  timeChange(spot: 'hour'|'minute', value) {
    const other = spot === 'hour' ? 'minute' : 'hour';
    const now = new Date();
    if (value === 'now') {
      this.date = this.timeService.dateToDateString(now);
      this.hour = this.timeService.getHours(now);
      this.minute = this.timeService.getMinutes(now);
    } else {
      this[spot] = value;
      if (value) {
        this[other] = this[other] || (other === 'hour' ? this.timeService.getHours(now) : this.timeService.getMinutes(now));
        if (!this.date) {
          this.date = this.timeService.dateToDateString(now);
        }
      } else {
        this[other] = '';
      }
    }
    this.valueChange();
  }

  writeValue(obj: string): void {
    if (typeof obj !== 'string' || obj.length === 0) {
      this.date = '';
      this.hour = '';
      this.minute = '';
      this.valueChange();
      return;
    }
    const parsed = this.timeService.parseDate(obj);
    this.date = parsed.date;
    this.hour = parsed.hour;
    this.minute = parsed.minute;
    this.valueChange();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  @HostBinding('class.floating')
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  get empty(): boolean {
    return !this.date && !this.hour && !this.minute;
  }

  @Input()
  get required(): boolean { return this._required; }
  set required(value) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get disabled() { return this._disabled; }
  set disabled(dis) {
    this._disabled = coerceBooleanProperty(dis);
    this.stateChanges.next();
  }

}
