import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostBinding, Inject, Input, OnDestroy, Optional, Self, ViewEncapsulation } from '@angular/core';
import { CanUpdateErrorState, MAT_INPUT_VALUE_ACCESSOR, MatDialog, MatFormFieldControl } from '@angular/material';
import { ControlValueAccessor, FormControl, FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { GetImageComponent } from '../../../shared/component/get-image/get-image.component';
import { ImageService } from '../../../shared/service/image.service';
import { of, Subject } from 'rxjs';
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'ilm-images-field',
  templateUrl: './images-field.component.html',
  styleUrls: ['./images-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {provide: MatFormFieldControl, useExisting: ImagesFieldComponent}
  ]
})
export class ImagesFieldComponent implements MatFormFieldControl<string[]>, CanUpdateErrorState, OnDestroy, ControlValueAccessor {
  static nextId = 0;

  controlType = 'ilm-images-field';
  errorState: boolean;
  shouldPlaceholderFloat: boolean;
  value: string[] | null;
  _images: string[] = [];
  focused = false;
  _required = false;
  _disabled = false;
  _ariaDescribedby: string;
  readonly stateChanges = new Subject<void>();
  private _placeholder: string;
  @Input() errorStateMatcher: ErrorStateMatcher;
  @HostBinding() id = `images-field-${ImagesFieldComponent.nextId++}`;

  onChange: any = () => {};
  onTouched: any = () => {};

  constructor(
    public dialog: MatDialog,
    private imageService: ImageService,
    private cdr: ChangeDetectorRef,
    private fm: FocusMonitor,
    protected _elementRef: ElementRef,
    @Optional() @Self() public ngControl: NgControl,
    @Optional() private _parentForm: NgForm,
    @Optional() private _parentFormGroup: FormGroupDirective,
    private _defaultErrorStateMatcher: ErrorStateMatcher,
    @Optional() @Self() @Inject(MAT_INPUT_VALUE_ACCESSOR) private inputValueAccessor: any
  ) {
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
    this.fm.monitor(this._elementRef.nativeElement, true).subscribe(origin => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this.fm.stopMonitoring(this._elementRef.nativeElement);
  }

  valueChange() {
    this.onChange(this._images || []);
    this.onTouched();
    this.stateChanges.next();
    this.cdr.markForCheck();
  }

  writeValue(obj: string[]): void {
    if (!Array.isArray(obj) || obj.length === 0) {
      this._images = [];
      this.valueChange();
      return;
    }
    this._images = obj;
    this.valueChange();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  @HostBinding('class.floating')
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  get empty(): boolean {
    return !this._images || this._images.length === 0;
  }

  @Input()
  get placeholder() {
    return this._placeholder;
  }
  set placeholder(plh) {
    this._placeholder = plh;
    this.stateChanges.next();
  }

  @Input()
  get required(): boolean { return this._required; }
  set required(value) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get disabled(): boolean {
    if (this.ngControl && this.ngControl.disabled !== null) {
      return this.ngControl.disabled;
    }
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);

    if (this.focused) {
      this.focused = false;
      this.stateChanges.next();
    }
  }

  addImageDialog() {
    const addRef = this.dialog.open(GetImageComponent, {
      height: '95vh',
      width: '95vw',
      maxWidth: '95vh',
      data: {
        showUpload: true
      }
    });
    addRef.beforeClose().pipe(
        concatMap(image => image ? this.imageService.save(image) : of(null))
      )
      .subscribe((id) => {
        if (id) {
          this._images = [...this._images, id];
          this.valueChange();
        }
      }, () => {});
  }

  removeImage(id: string) {
    this.imageService.remove(id).subscribe((success) => {
      if (success) {
        this._images = this._images.filter(imgId => id !== imgId);
        this.valueChange();
      }
    });
  }

  onContainerClick(event: MouseEvent): void {
  }

  setDescribedByIds(ids: string[]): void {
    this._ariaDescribedby = ids.join(' ');
  }

  updateErrorState() {
    const oldState = this.errorState;
    const parent = this._parentFormGroup || this._parentForm;
    const matcher = this.errorStateMatcher || this._defaultErrorStateMatcher;
    const control = this.ngControl ? this.ngControl.control as FormControl : null;
    const newState = matcher.isErrorState(control, parent);

    if (newState !== oldState) {
      this.errorState = newState;
      this.stateChanges.next();
    }
  }
}
