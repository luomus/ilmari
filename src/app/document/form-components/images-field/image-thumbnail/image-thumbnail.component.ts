import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ImageService } from '../../../../shared/service/image.service';
import { ImagePreviewOverlayService } from '../../../../shared/component/image-preview-overlay/image-preview-overlay.service';
import { ImagePreviewOverlayRef } from '../../../../shared/component/image-preview-overlay/image-preview-overlay-ref';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'ilm-image-thumbnail',
  templateUrl: './image-thumbnail.component.html',
  styleUrls: ['./image-thumbnail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageThumbnailComponent implements OnInit {

  _url$;
  _value;

  @Output() remove = new EventEmitter<string>();

  constructor(
    private imageService: ImageService,
    private previewDialog: ImagePreviewOverlayService,
  ) { }

  ngOnInit() {
  }

  @Input()
  set image(value: string) {
    this._value = value;
    this._url$ = this.imageService.getUrl(value).pipe(
      tap(val => {
        if (!val) {
          this.remove.emit(value);
        }
      })
    );
  }

  fullscreen() {
    this.imageService.getUrl(this._value, 'large')
      .subscribe((imageUrl) => {
        const dialogRef: ImagePreviewOverlayRef = this.previewDialog.open({
          image: {
            url: imageUrl,
            description: '',
            showDelete: true
          }
        });
        dialogRef.afterClosed().subscribe((result) => {
          if (result && result.action && result.action === 'delete') {
            this.remove.emit(this._value);
          }
        });
      });
  }

}
