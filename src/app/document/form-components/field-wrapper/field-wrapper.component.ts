import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, forwardRef, Inject, Input, OnDestroy, OnInit, Output, PLATFORM_ID, ViewChild, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FlatField } from '../../../models/form';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Store } from '@ngrx/store';
import * as fromTrip from '../../trip/store';
import { AbstractExtension } from './extension/abstract-extension';
import { TaxonExtension } from './extension/taxon-extension';
import { MultiExtension } from './extension/multi-extension';
import { isPlatformBrowser } from '@angular/common';
import { TaxonService } from '../../../shared/service/taxon.service';
import { ErrorStateMatcher, MatInput } from '@angular/material';
import { FriendExtension } from './extension/friend-extension';
import { FriendService } from '../../../shared/service/friend.service';

@Component({
  selector: 'ilm-field-wrapper',
  templateUrl: './field-wrapper.component.html',
  styleUrls: ['./field-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FieldWrapperComponent),
      multi: true
    }
  ]
})
export class FieldWrapperComponent implements OnInit, OnDestroy, ControlValueAccessor {

  @ViewChild('chipInput') chipInput: MatInput;
  @Output() updateData = new EventEmitter<any>();

  _data = {};
  _errors: string[] = [];
  _allErrors: {[field: string]: string[]};
  _field: FlatField;
  _fields: FlatField[];
  value: any;
  disabled: boolean;
  separatorKeysCodes = [ENTER, COMMA];
  extension: AbstractExtension;
  matcher: ErrorStateMatcher;

  onChange: any = () => {};
  onTouched: any = () => {};

  constructor(
    public cd: ChangeDetectorRef,
    public store: Store<fromTrip.State>,
    private taxonService: TaxonService,
    private friendService: FriendService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.matcher = {
      isErrorState: () => {
        return this._errors && this._errors.length > 0;
      }
    };
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.cd.detach();
    if (this.extension) {
      this.extension.onDestroy();
      delete this.extension;
    }
  }

  valueChangeAndTouched(value) {
    this.valueChange(value);

    this.onTouched();
  }

  valueChange(value) {
    this.value = value;

    this.onChange(this.value);
    if (this.extension) {
      this.extension.onValueUpdate(value);
    }
  }

  writeValue(obj: any): void {
    this.value = obj;
    this.cd.detectChanges();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  hasValue() {
    return typeof this.value !== 'undefined';
  }

  @Input()
  set errors(errors) {
    this._allErrors = errors;
    this.initErrors();
  }

  @Input()
  set fields(fields: FlatField[]) {
    this._fields = fields;
  }
  get fields() {
    return this._fields;
  }

  @Input()
  set field(field: FlatField) {
    this._field = field;
    if (field.type === 'taxon') {
      this.extension = new TaxonExtension(this, this.taxonService);
    } else if (
      field.type === 'multiText'
    ) {
      this.extension = new MultiExtension(this);
    } else if (field.type === 'multiFriend') {
      this.extension = new FriendExtension(this, this.friendService);
    }
    if (this.extension) {
      this.extension.onInit();
      this.extension.onFieldUpdate();
    }
  }
  get field() {
    return this._field;
  }

  @Input()
  set data(data) {
    this._data = data;
    if (this.extension) {
      this.extension.onDataUpdate();
    }
  }
  get data() {
    return this._data;
  }

  onFocus(event: FocusEvent) {
    if (this.extension) {
      this.extension.onFocus();
    }
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    try {
      setTimeout(() => {
        const rect = (event.target as any).getBoundingClientRect();
        const inView = (
          rect.top >= 40 &&
          rect.left >= 0 &&
          (rect.bottom + 40) <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
          rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );
        if (!inView) {
          (event.target as any).scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'});
        }
      }, 200);
    } catch (e) {
    }
  }

  private initErrors() {
    this._errors = [];
    if (!this._allErrors || !this._field) {
      return;
    }
    if (this._allErrors[this._field.exactPath]) {
      this._errors = this._allErrors[this._field.exactPath];
    }
  }

}
