import { AbstractExtension } from './abstract-extension';
import { Document } from '../../../../models/document/document';
import { environment } from '../../../../../environments/environment';
import * as trip from '../../../trip/store/actions/trip-actions';
import { FieldWrapperComponent } from '../field-wrapper.component';
import { TaxonService } from '../../../../shared/service/taxon.service';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Autocomplete } from '../../../../models/autocomplete';

export class TaxonExtension extends AbstractExtension {

  private static defaultGroup;

  private taxonGroupIdx;
  private taxonGroupExactPath;
  private taxonIdIdx;
  private taxonIdExactPath;
  private taxonSub = new Subject<string>();
  private lastSelect: Autocomplete;
  obs: Observable<Autocomplete[]>;
  current;

  constructor(
    fieldWrapper: FieldWrapperComponent,
    protected taxonService: TaxonService
  ) {
    super(fieldWrapper);

  }

  onInit() {
    this.obs = this.taxonSub.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      switchMap(value => this.taxonService.autocompleteTaxon(value, [this.value]))
    );
  }

  onDestroy() {
    this.taxonSub.complete();
  }

  onFocus() {
    if (!this.fieldWrapper.value) {
      this.taxonSub.next('');
    }
  }

  onValueUpdate(value) {
    if (this.lastSelect && this.lastSelect.value === value) {
      return;
    }
    this.taxonSub.next(value);
  }

  onDataUpdate() {
    if (!this.taxonGroupExactPath || !this.fieldWrapper._data || !this.fieldWrapper._data[this.taxonGroupExactPath]) {
      return;
    }
    if (this.fieldWrapper._data[this.taxonGroupExactPath] !== this.value) {
      this.value = this.fieldWrapper._data[this.taxonGroupExactPath][0];
    }
  }

  onFieldUpdate() {
    const defaultGroup = TaxonExtension.defaultGroup || environment.defaults.informalTaxonGroup;
    const fieldWrapper = this.fieldWrapper;
    this.taxonGroupExactPath = '';

    if (!fieldWrapper._fields) {
      this.value = defaultGroup;
      return;
    }
    let taxonIdIdx = this.taxonGroupIdx;
    if (!this.taxonIdIdx || fieldWrapper._fields[this.taxonIdIdx].path !== Document.Path.autocompleteSelectedTaxonID) {
      taxonIdIdx = fieldWrapper._fields.findIndex(obj => obj.path === Document.Path.autocompleteSelectedTaxonID);
      this.taxonIdIdx = taxonIdIdx;
    }
    this.taxonIdExactPath = fieldWrapper._fields[taxonIdIdx].exactPath;

    let taxonGroupIdx = this.taxonGroupIdx;
    if (!this.taxonGroupIdx || fieldWrapper._fields[this.taxonGroupIdx].path !== Document.Path.informalTaxonGroups) {
      taxonGroupIdx = fieldWrapper._fields.findIndex(obj => obj.path === Document.Path.informalTaxonGroups);
      this.taxonGroupIdx = taxonGroupIdx;
    }
    this.taxonGroupExactPath = fieldWrapper._fields[taxonGroupIdx].exactPath;

    if (taxonGroupIdx < 0 || !fieldWrapper._data[this.taxonGroupExactPath]) {
      this.value =  defaultGroup;
    } else {
      this.value = fieldWrapper._data[this.taxonGroupExactPath][0];
    }
  }

  event(event, type) {
    if (this[type]) {
      this[type](event);
    } else {
      console.error('Taxon extension doesn\'t have method ' + type);
    }
  }

  groupSelect(event) {
    this.fieldWrapper.store.dispatch(new trip.SetActiveGroupAction(event));
    this.fieldWrapper.updateData.emit({
      [this.taxonGroupExactPath]: [event]
    });
    TaxonExtension.defaultGroup = event;
    this.value = event;
  }

  autocompleteSelect(selection: Autocomplete) {
    this.lastSelect = selection;
    this.fieldWrapper.valueChange(selection.value);
    if (this.taxonIdExactPath) {
      this.fieldWrapper.updateData.emit({[this.taxonIdExactPath]: this.lastSelect.key});
    }
  }

  displayFn(state) {
    if (!state || typeof state === 'string') {
      return state;
    }
    return state.value;
  }

}
