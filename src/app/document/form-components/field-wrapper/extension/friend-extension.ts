import { FieldWrapperComponent } from '../field-wrapper.component';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { FriendService } from '../../../../shared/service/friend.service';
import { Autocomplete } from '../../../../models/autocomplete';
import { MultiExtension } from './multi-extension';
import { MatChipInputEvent } from '@angular/material';

export class FriendExtension extends MultiExtension {

  private friendSub = new Subject<string>();
  private addSub = new Subject<MatChipInputEvent>();
  obs: Observable<Autocomplete[]>;
  current;

  constructor(
    fieldWrapper: FieldWrapperComponent,
    protected friendService: FriendService
  ) {
    super(fieldWrapper);
  }

  addByOption(value) {
    this.fieldWrapper.chipInput['nativeElement'].value = '';
    this.addSub.next({input: null, value: value} as MatChipInputEvent);
  }

  addByInput(value: MatChipInputEvent) {
    this.addSub.next(value);
  }

  onInit() {
    this.obs = this.friendSub.pipe(
      distinctUntilChanged(),
      debounceTime(1000),
      switchMap(value => this.friendService.autocompleteTaxon(value))
    );
    this.addSub.pipe(
      debounceTime(100)
    ).subscribe(val => this.add(val));
  }

  onDestroy() {
    this.friendSub.complete();
    this.addSub.complete();
  }

  onFocus() {
    if (!this.fieldWrapper.value) {
      this.friendSub.next('');
    }
  }

  onValueUpdate(value) {
    this.friendSub.next(value);
  }

}
