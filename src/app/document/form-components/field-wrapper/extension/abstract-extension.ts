import { FieldWrapperComponent } from '../field-wrapper.component';
import { Observable } from 'rxjs';

export abstract class AbstractExtension {

  value;
  obs: Observable<any>;

  protected constructor(protected fieldWrapper: FieldWrapperComponent) { }

  onFieldUpdate() { }

  onDataUpdate() { }

  onFocus() { }

  onValueUpdate(value) { }

  event(event, meta?) { }

  onInit() { }

  onDestroy() { }

  displayFn(state) {}
}
