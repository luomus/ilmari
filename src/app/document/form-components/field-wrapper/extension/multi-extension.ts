import { AbstractExtension } from './abstract-extension';
import { MatChipInputEvent } from '@angular/material';
import { FieldWrapperComponent } from '../field-wrapper.component';

export class MultiExtension extends AbstractExtension {

  constructor(protected fieldWrapper: FieldWrapperComponent) {
    super(fieldWrapper);
  }

  event(event, type) {
    this[type](event);
  }

  add(event: MatChipInputEvent) {
    const input = event.input;
    const value = (event.value || '').trim();

    if (value) {
      if (Array.isArray(this.fieldWrapper.value)) {
        this.fieldWrapper.value = [...this.fieldWrapper.value, value];
      } else {
        this.fieldWrapper.value = [value];
      }
      this.fieldWrapper.valueChangeAndTouched(this.fieldWrapper.value);
    }

    if (input) {
      input.value = '';
    }
  }

  remove(event) {
    const index = this.fieldWrapper.value.indexOf(event);

    if (index >= 0) {
      this.fieldWrapper.value.splice(index, 1);
      this.fieldWrapper.valueChangeAndTouched(this.fieldWrapper.value);
    }
  }

}
