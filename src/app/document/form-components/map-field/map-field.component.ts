import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { Sections } from '../../trip/models/sections';
import { Travels } from '../../trip/models/travels';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Circle, Geometry } from '../../../models/document/geometry';
import { DrawingManager, NguiMapComponent } from '@ngui/map';
import { LineString, Point, Polygon, Polygon as GeoPolygon } from 'geojson';

@Component({
  selector: 'ilm-map-field',
  templateUrl: './map-field.component.html',
  styleUrls: ['./map-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MapFieldComponent),
      multi: true
    }
  ]
})
export class MapFieldComponent implements ControlValueAccessor {

  path: {lat: number, lng: number}[] = [];
  value: Geometry;
  disabled: boolean;
  drawingMode: 'none'|'marker'|'polyLine'|'rectangle'|'circle'|'polygon'|'delete' = 'none';
  errors = [];
  @ViewChild(NguiMapComponent) map: NguiMapComponent;
  @ViewChild(DrawingManager) drawingManager: DrawingManager;
  @Output() fullscreenToggle = new EventEmitter();
  @Input() fullscreen = false;
  @Input() gathering: Geometry;
  @Input() gatheringActive = false;

  private _sections: Sections;
  private _travel: Travels;
  private overlays = {};
  private gatheringOverlays = {};
  private _gatheringErrors = [];
  private _unitGatheringErrors = [];

  bgOptions = {
    fillColor: '#2d6dbd',
    fillOpacity: 0.3,
    strokeColor: '#2d6dbd',
    strokeOpacity: 0.3,
    opacity: 0.3,
    draggable: false
  };

  fgOptions = {
    draggable: true,
  };

  onChange: any = () => {};

  onTouched: any = () => {};

  constructor(
    private cdr: ChangeDetectorRef
  ) { }

  @Input()
  set travel(travel: Travels) {
    this._travel = travel;
    this.initPath();
  }

  @Input()
  set sections(sections: Sections) {
    this._sections = sections;
    this.initPath();
  }


  @Input()
  set gatheringErrors(errors: string[]) {
    this._gatheringErrors = errors;
    this.errors = [...errors, ...this._unitGatheringErrors];
  }

  @Input()
  unitGatheringErrors(errors: string[]) {
    this._unitGatheringErrors = errors;
    this.errors = [...this._gatheringErrors, ...errors];
  }

  initMap() {
    if (this.value) {
      this.geometryToOverlay(this.value, true);
    }
    if (this.gathering && !this.gatheringActive) {
      this.geometryToOverlay(this.gathering, false);
    }
  }

  initPath() {
    if (!this._sections || !this._travel || !this._travel[this._sections.gatherings.active]) {
      this.path = [];
      return;
    }
    this.path = this._travel[this._sections.gatherings.active].coordinates;
    if (this.drawingMode === 'none') {
      this.map.setCenter();
    }
  }

  writeValue(obj: Geometry): void {
    this.value = obj;
    this.clearMap();
    if (typeof google !== 'undefined') {
      this.initMap();
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  toMyLocation() {
    this.map.setCenter();
  }

  addOverlay(event, refresh = true) {
    if (!this.overlays[event.type]) {
      this.overlays[event.type] = [];
    }
    this.overlays[event.type] = [...this.overlays[event.type], event.overlay];
    this.addEventListeners(event.type, event.overlay);
    if (refresh) {
      this.refreshValue();
      this.cancelDrawingMode();
    }
  }

  addGatheringOverlay(event) {
    if (!this.gatheringOverlays[event.type]) {
      this.gatheringOverlays[event.type] = [];
    }
    this.gatheringOverlays[event.type] = [...this.gatheringOverlays[event.type], event.overlay];
  }

  drawPolyLine() {
    this.drawingMode = 'polyLine';
    this.setDrawingMode(google.maps.drawing.OverlayType.POLYLINE);
  }

  drawMarker() {
    this.drawingMode = 'marker';
    this.setDrawingMode(google.maps.drawing.OverlayType.MARKER);
  }

  drawCircle() {
    this.drawingMode = 'circle';
    this.setDrawingMode(google.maps.drawing.OverlayType.CIRCLE);
  }

  drawRectangle() {
    this.drawingMode = 'rectangle';
    this.setDrawingMode(google.maps.drawing.OverlayType.RECTANGLE);
  }

  drawPolygon() {
    this.drawingMode = 'polygon';
    this.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
  }

  cancelDrawingMode() {
    this.drawingMode = 'none';
    this.setDrawingMode(null);
  }

  deleteMode() {
    this.drawingMode = 'delete';
    this.setDrawingMode(null);
  }

  deleteOverlay(target) {
    if (this.drawingMode !== 'delete') {
      return;
    }
    Object.keys(this.overlays).forEach(type => {
      this.overlays[type] = this.overlays[type].filter((overlay) => {
        if (overlay === target) {
          overlay.setMap(null);
          return false;
        }
        return true;
      });
    });
    this.cancelDrawingMode();
    this.refreshValue();
  }

  private setDrawingMode(mode) {
    this.drawingManager.mapObject.setDrawingMode(mode);
    this.cdr.detectChanges();
  }

  private clearMap() {
    for (const type in this.overlays) {
      if (this.overlays[type]) {
        this.overlays[type].forEach(overlay => {
          overlay.setMap(null);
        });
        this.overlays[type] = [];
      }
    }
    for (const type in this.gatheringOverlays) {
      if (this.gatheringOverlays[type]) {
        this.gatheringOverlays[type].forEach(overlay => {
          overlay.setMap(null);
        });
        this.gatheringOverlays[type] = [];
      }
    }
  }

  private geometryToOverlay(geometry: Geometry, editable) {
    if (!geometry) {
      return;
    }
    const events: { type: google.maps.drawing.OverlayType, overlay: any }[] = [];
    switch (geometry.type) {
      case 'GeometryCollection':
        geometry.geometries.forEach(geom => this.geometryToOverlay(<any>geom, editable));
        break;
      case 'Point':
        events.push(this.pointToOverlay(geometry, !editable));
        break;
      case 'Polygon':
        events.push(this.polygonToOverlay(geometry, !editable));
        break;
      case 'LineString':
        events.push(this.lineStringToOverlay(geometry, !editable));
        break;
      case 'MultiPolygon':
        geometry.coordinates.forEach(coord => {
          events.push(this.polygonToOverlay({
            type: 'Polygon',
            coordinates: coord
          }, !editable));
        });
        break;
      case 'MultiLineString':
        geometry.coordinates.forEach(coord => {
          events.push(this.lineStringToOverlay({
            type: 'LineString',
            coordinates: coord
          }, !editable));
        });
        break;
      case 'MultiPoint':
        geometry.coordinates.forEach(point => {
          events.push(this.pointToOverlay({
            type: 'Point',
            coordinates: point
          }, !editable));
        });
        break;
    }
    if (events.length > 0) {
      events.forEach(event => {
        event.overlay.setMap(this.map.map);
        if (event.overlay.setEditable) {
          event.overlay.setEditable(editable);
        }
        if (editable) {
          this.addOverlay(event, false);
        } else {
          this.addGatheringOverlay(event);
        }
      });
    }
  }

  private lineStringToOverlay(geometry: LineString, bg = false) {
    return {
      type: google.maps.drawing.OverlayType.POLYLINE,
      overlay: new google.maps.Polyline({
        ...(bg ? this.bgOptions : this.fgOptions),
        path: geometry.coordinates.map(coord => new google.maps.LatLng(coord[1], coord[0]))
      })
    };
  }

  private   polygonToOverlay(geometry: Polygon, bg = false) {
    const lat = {};
    const lng = {};
    geometry.coordinates[0].map(coord => {
      lat[coord[1]] = true;
      lng[coord[0]] = true;
    });
    let uniqueLat: any = Object.keys(lat);
    let uniqueLng: any = Object.keys(lng);
    if (uniqueLat.length === 2 && uniqueLng.length === 2) {
      uniqueLat = uniqueLat.map(val => +val).sort();
      uniqueLng = uniqueLng.map(val => +val).sort();
      return {
        type: google.maps.drawing.OverlayType.RECTANGLE,
        overlay: new google.maps.Rectangle({
          ...(bg ? this.bgOptions : this.fgOptions),
          bounds: {
            north: uniqueLat[0],
            south: uniqueLat[1],
            east: uniqueLng[1],
            west: uniqueLng[0]
          }
        })
      };
    }
    return {
      type: google.maps.drawing.OverlayType.POLYGON,
      overlay: new google.maps.Polygon({
        ...(bg ? this.bgOptions : this.fgOptions),
        paths: geometry.coordinates[0].map(coord => new google.maps.LatLng(coord[1], coord[0]))
      })
    };
  }

  private pointToOverlay(geometry: Point, bg = false) {
    if ((geometry as any).radius) {
      return {
        type: google.maps.drawing.OverlayType.CIRCLE,
        overlay: new google.maps.Circle({
          ...(bg ? this.bgOptions : this.fgOptions ),
          center: new google.maps.LatLng(geometry.coordinates[1], geometry.coordinates[0]),
          radius: (geometry as any).radius
        })
      };
    }
    return {
      type: google.maps.drawing.OverlayType.MARKER,
      overlay: new google.maps.Marker({
        ...(bg ? this.bgOptions : this.fgOptions),
        position: new google.maps.LatLng(geometry.coordinates[1], geometry.coordinates[0])
      })
    };
  }

  private addEventListeners(type, overlay) {
    const self = this;
    overlay.addListener('click', function() {
      self.deleteOverlay(this);
    });
    overlay.addListener('dragend', () => {
      this.refreshValue();
    });
    switch (type) {
      case google.maps.drawing.OverlayType.RECTANGLE:
        overlay.addListener('bounds_changed', () => {
          this.refreshValue();
        });
        break;
      case google.maps.drawing.OverlayType.POLYGON:
        overlay.getPaths().forEach((path) => {
          path.addListener('insert_at', () => {
            this.refreshValue();
          });
          path.addListener('remove_at', () => {
            this.refreshValue();
          });
          path.addListener('set_at', () => {
            this.refreshValue();
          });
        });
        break;
      case google.maps.drawing.OverlayType.POLYLINE:
        overlay.getPath().addListener('insert_at', () => {
          this.refreshValue();
        });
        overlay.getPath().addListener('remove_at', () => {
          this.refreshValue();
        });
        overlay.getPath().addListener('set_at', () => {
          this.refreshValue();
        });
        break;
      case google.maps.drawing.OverlayType.CIRCLE:
        overlay.addListener('radius_changed', () => {
          this.refreshValue();
        });
        overlay.addListener('center_changed', () => {
          this.refreshValue();
        });
        break;
    }
  }

  private overlayToGeometry(type, overlay) {
    let geometry: any;
    switch (type) {
      case google.maps.drawing.OverlayType.MARKER:
        geometry = <Point> {
          type: 'Point',
          coordinates: [
            overlay.getPosition().lng(),
            overlay.getPosition().lat()
          ]
        };
        break;
      case google.maps.drawing.OverlayType.RECTANGLE:
        const b = overlay.getBounds();
        geometry = <GeoPolygon> {
          type: 'Polygon',
          coordinates: [[
            [b.getSouthWest().lng(), b.getSouthWest().lat()],
            [b.getNorthEast().lng(), b.getSouthWest().lat()],
            [b.getNorthEast().lng(), b.getNorthEast().lat()],
            [b.getSouthWest().lng(), b.getNorthEast().lat()],
            [b.getSouthWest().lng(), b.getSouthWest().lat()]
          ]]
        };
        break;
      case google.maps.drawing.OverlayType.POLYGON:
        geometry = <GeoPolygon> {
          type: 'Polygon',
          coordinates: [overlay.getPath().getArray().map(latLng => [latLng.lng(), latLng.lat()])]
        };
        break;
      case google.maps.drawing.OverlayType.POLYLINE:
        geometry = <LineString> {
          type: 'LineString',
          coordinates: overlay.getPath().getArray().map(latLng => [latLng.lng(), latLng.lat()])
        };
        break;
      case google.maps.drawing.OverlayType.CIRCLE:
        geometry = <Circle> {
          type: 'Point',
          coordinates: [
            overlay.getCenter().lng(),
            overlay.getCenter().lat()
          ],
          radius: overlay.getRadius()
        };
        break;
    }

    return geometry;
  }

  private refreshValue() {
    const geometries = [];
    for (const type in this.overlays) {
      if (Array.isArray(this.overlays[type])) {
        this.overlays[type].forEach(overlay => {
          geometries.push(this.overlayToGeometry(type, overlay));
        });
      }
    }
    if (geometries.length === 0) {
      this.value = undefined;
    } else if (geometries.length === 1) {
      this.value = geometries[0];
    } else {
      this.value = {
        type: 'GeometryCollection',
        geometries: geometries
      };
    }
    this.onChange(this.value);
    this.onTouched();
  }

}
