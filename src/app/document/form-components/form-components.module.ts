import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { FieldWrapperComponent } from './field-wrapper/field-wrapper.component';
import { MapFieldComponent } from './map-field/map-field.component';
import { environment } from '../../../environments/environment';
import { NguiMapModule } from '@ngui/map';
import { DatetimeComponent } from './datetime/datetime.component';
import { ImagesFieldComponent } from './images-field/images-field.component';
import { ImageThumbnailComponent } from './images-field/image-thumbnail/image-thumbnail.component';
import { AggregatedErrorsComponent } from './aggregated-errors/aggregated-errors.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NguiMapModule.forRoot({
      apiUrl: 'https://maps.google.com/maps/api/js?v=3.33&libraries=visualization,places,drawing&key=' + environment.mapsToken
    })
  ],
  declarations: [FieldWrapperComponent, MapFieldComponent, DatetimeComponent, ImagesFieldComponent, ImageThumbnailComponent, AggregatedErrorsComponent],
  exports: [FieldWrapperComponent, MapFieldComponent, AggregatedErrorsComponent]
})
export class FormComponentsModule { }
