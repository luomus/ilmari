import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ilm-aggregated-errors',
  templateUrl: './aggregated-errors.component.html',
  styleUrls: ['./aggregated-errors.component.scss']
})
export class AggregatedErrorsComponent implements OnInit {

  _errors: string[];
  hasErrors = false;

  constructor() { }

  ngOnInit() {
  }

  @Input()
  set errors(errors: {[key: string]: string[]}) {
    const result = [];
    if (typeof errors === 'object') {
      Object.keys(errors).forEach(key => {
        if (Array.isArray(errors[key])) {
          result.push(...errors[key]);
        } else {
          result.push(errors[key]);
        }
      });
    } else {
      result.push(errors);
    }
    this._errors = result;
    this.hasErrors = result.length > 0;
  }

}
