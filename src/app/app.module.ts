import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { PreloadAllModules, RouterModule } from '@angular/router';
import { routes } from './routes';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from './core/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';
import { CoreModule } from './core/core.module';
import { AppContainerComponent } from './core/app-container/app-container.component';
import { SharedModule } from './shared/shared.module';
import { LayoutEffects } from './core/store/effects/layout-effects';
import { UserModule } from './user/user.module';
import { DateAdapter, MAT_DATE_FORMATS, MatNativeDateModule } from '@angular/material';
import { APP_DATE_FORMATS, DateAdapterService } from './user/service/date-adapter.service';
import { CoreEffects } from './core/store/effects/core-effects';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { environment } from '../environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';


@NgModule({
  declarations: [],
  imports: [
    BrowserModule.withServerTransition({
      appId: 'ilmari'
    }),
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument({maxAge: 30}) : [],
    EffectsModule.forRoot([LayoutEffects, CoreEffects]),
    CoreModule.forRoot(),
    SharedModule.forRoot(),
    UserModule.forRoot(),
    MatNativeDateModule,
    TransferHttpCacheModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  providers: [
    {provide: LocationStrategy, useClass: PathLocationStrategy},
    {provide: DateAdapter, useClass: DateAdapterService},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ],
  bootstrap: [AppContainerComponent]
})
export class AppModule { }
