import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ilm-toolbar',
  template: `
    <mat-toolbar color="primary" fxLayoutGap="10px">
      <div class="hamburger" (click)="toggleMenu.emit()" [class.open]="open" fxShow.lt-md="true" fxHide.gt-sm="true">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <ng-content></ng-content>
    </mat-toolbar>
  `,
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent {
  @Output() toggleMenu = new EventEmitter();
  @Input() open = false;

  constructor() {}

}
