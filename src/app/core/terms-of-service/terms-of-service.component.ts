import { ChangeDetectionStrategy, Component } from '@angular/core';

export const TERMS_VERSION = 2;

@Component({
  selector: 'ilm-terms-of-service',
  templateUrl: './terms-of-service.component.html',
  styleUrls: ['./terms-of-service.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TermsOfServiceComponent {

  constructor() { }
}
