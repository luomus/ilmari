import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { AppContainerComponent } from './app-container/app-container.component';
import { NavItemComponent } from './nav-item/nav-item.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { UserModule } from '../user/user.module';
import { FeedbackComponent } from './feedback/feedback.component';
import { TermsOfServiceComponent } from './terms-of-service/terms-of-service.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

export const COMPONENTS = [
  AppContainerComponent,
  NavItemComponent,
  ToolbarComponent,
  FeedbackComponent,
  TermsOfServiceComponent,
  PrivacyPolicyComponent
];

@NgModule({
  entryComponents: [
    FeedbackComponent,
    TermsOfServiceComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    UserModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: []
    };
  }
}
