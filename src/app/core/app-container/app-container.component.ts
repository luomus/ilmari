import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromRoot from '../store';
import * as fromUser from '../../user/store';
import * as coreActions from '../store/actions/core-actions';
import * as layoutActions from '../store/actions/layout-actions';
import * as userActions from '../../user/store/actions/user-actions';
import { UpdateService } from '../../shared/service/update.service';
import { NavigationEnd, Router } from '@angular/router';
import { Feedback } from '../../models/feedback';
import { MatDialog } from '@angular/material';
import { FeedbackComponent } from '../feedback/feedback.component';
import { concatMap, distinctUntilChanged, filter, switchMap, take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { TERMS_VERSION, TermsOfServiceComponent } from '../terms-of-service/terms-of-service.component';

@Component({
  selector: 'ilm-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './app-container.component.html',
  styleUrls: ['./app-container.component.scss']
})
export class AppContainerComponent {
  showSidenav$: Observable<boolean>;
  isMobile$: Observable<boolean>;
  isLoading$: Observable<boolean>;
  loggedIn$: Observable<boolean>;
  hasUpdate$: Observable<boolean>;
  updating = false;

  constructor(
    private store: Store<fromRoot.State>,
    private updateService: UpdateService,
    private router: Router,
    private dialog: MatDialog
  ) {
    this.isLoading$ = this.store.select(fromRoot.isLoading);
    this.isMobile$ = this.store.select(fromRoot.getIsMobile);
    this.showSidenav$ = this.isMobile$.pipe(
      switchMap(mobile => mobile ? this.store.select(fromRoot.getShowSidenav) : of(true))
    );
    this.loggedIn$ = this.store.select(fromUser.isLoggedIn);
    this.hasUpdate$ = this.updateService.hasUpdate;
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.updateService.checkForUpdate();
      }
    });
    this.store.select(fromRoot.isFeedbackVisible).pipe(
      distinctUntilChanged(),
      filter(visible => visible),
      concatMap(() => this.store.select(fromRoot.getFeedback).pipe(take(1)))
    ).subscribe(feedback => this.feedbackModal(feedback));

    this.store.select(fromRoot.getTermsAccepted).pipe(
      take(1)
    ).subscribe(accepted => {
      if (accepted === TERMS_VERSION) {
        return;
      }
      const dialogRef = this.dialog.open(TermsOfServiceComponent, {
        closeOnNavigation: false,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(() => {
        this.store.dispatch(new coreActions.AcceptTermsAction(TERMS_VERSION));
      });
    });
  }

  logout() {
    this.store.dispatch(new userActions.LogoutRequestAction());
  }

  closeSidenav() {
    this.isMobile$.pipe(take(1))
      .subscribe(isMobile => isMobile ? this.store.dispatch(new layoutActions.CloseSidenavAction()) : undefined);
  }

  sendFeedback() {
    this.store.dispatch(new coreActions.FeedbackDialogAction(true));
    this.closeSidenav();
  }

  showTerms() {
    this.dialog.open(TermsOfServiceComponent);
  }

  showLogin() {
    this.store.dispatch(new userActions.OpenLoginAction());
  }

  openSidenav() {
    this.store.dispatch(new layoutActions.OpenSidenavAction());
  }

  toggleSidenav() {
    this.isMobile$.pipe(take(1))
      .subscribe(isMobile => isMobile ? this.store.dispatch(new layoutActions.ToggleSidenavAction()) : undefined);
  }

  activateUpdate() {
    this.updating = true;
    this.updateService.activateUpdate();
  }

  private feedbackModal(feedback: Feedback) {
    const modalRef = this.dialog.open(FeedbackComponent, {
      data: feedback
    });
    modalRef.afterClosed()
      .subscribe((data) => {
        if (data) {
          this.store.dispatch(new coreActions.FeedbackSendAction(data));
        } else if (data === false) {
          this.store.dispatch(new coreActions.FeedbackUpdateAction({subject: '', message: ''}));
        }
        this.store.dispatch(new coreActions.FeedbackDialogAction(false));
      });
  }
}
