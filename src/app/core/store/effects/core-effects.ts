import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import * as fromCore from '../reducers/core-reducers';
import * as core from '../actions/core-actions';
import { FeedbackSendAction } from '../actions/core-actions';
import { InformalTaxonService } from '../../../shared/service/informal-taxon.service';
import { LajiApi, LajiApiService } from '../../../shared/service/laji-api.service';
import { MatSnackBar } from '@angular/material';
import { concatMap, map, tap } from 'rxjs/operators';
import { Feedback } from '../../../models/feedback';
import { defer, Observable, of } from 'rxjs';

@Injectable()
export class CoreEffects {

  @Effect()
  sendFeedback$: Observable<Action> = this.actions$
    .ofType<FeedbackSendAction>(core.FEEDBACK_SEND).pipe(
      map<FeedbackSendAction, Feedback>(action => action.payload),
      map(feedback => ({subject: feedback.subject || 'Ilmari feedback', ...feedback})),
      concatMap((feedback) => this.lajiApiService.addEntry(LajiApi.Endpoint.feedback, {}, feedback)),
      map(() => new core.FeedbackSuccessAction()),
      tap(() => this.snackBar.open('Thank you for your feedback', 'Dismiss', {duration: 3000}))
    );

  @Effect()
  init$: Observable<Action> = defer(() => {
    return of(null).pipe(
      concatMap(() => this.informTaxonService.getRoots()),
      map(roots => new core.LoadedInformalTaxonGroupRoots(roots))
    );
  });

  constructor(
    private actions$: Actions,
    private store: Store<fromCore.State>,
    private informTaxonService: InformalTaxonService,
    private lajiApiService: LajiApiService,
    private snackBar: MatSnackBar
  ) { }
}
