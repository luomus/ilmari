import { Injectable } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { MobileAction } from '../actions/layout-actions';
import { concatMap, distinctUntilChanged, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable()
export class LayoutEffects {
  @Effect() mobile$: Observable<Action> = this.media
    .asObservable().pipe(
      map((change: MediaChange) => change.mqAlias),
      distinctUntilChanged(),
      concatMap((mediaQuery) => of(new MobileAction(['xs', 'sm'].indexOf(mediaQuery) > -1)))
    );

  constructor(
    private media: ObservableMedia
  ) { }
}
