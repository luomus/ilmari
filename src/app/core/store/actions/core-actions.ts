import { Action } from '@ngrx/store';
import { ExtendedInformalTaxonGroup } from '../../../models/informal-taxon-group';
import { Feedback } from '../../../models/feedback';

export const LOADED_INFORMAL_TAXON_GROUP_ROOTS = '[Core] Loaded informal taxon group';
export const ACCEPT_TERMS = '[Core] Accept terms';
export const FEEDBACK_DIALOG = '[Core] Feedback dialog';
export const FEEDBACK_UPDATE = '[Core] Feedback update';
export const FEEDBACK_SEND = '[Core] Feedback send';
export const FEEDBACK_SUCCESS = '[Core] Feedback success';

export class AcceptTermsAction implements Action {
  readonly type = ACCEPT_TERMS;

  constructor(public payload: number) {}
}

export class LoadedInformalTaxonGroupRoots implements Action {
  readonly type = LOADED_INFORMAL_TAXON_GROUP_ROOTS;

  constructor(public payload: ExtendedInformalTaxonGroup[]) {}
}

export class FeedbackDialogAction implements Action {
  readonly type = FEEDBACK_DIALOG;

  constructor(public payload: boolean) {}
}

export class FeedbackUpdateAction implements Action {
  readonly type = FEEDBACK_UPDATE;

  constructor(public payload: Feedback) {}
}

export class FeedbackSendAction implements Action {
  readonly type = FEEDBACK_SEND;

  constructor(public payload: Feedback) {}
}

export class FeedbackSuccessAction implements Action {
  readonly type = FEEDBACK_SUCCESS;
}

export type Actions
  = LoadedInformalTaxonGroupRoots
  | AcceptTermsAction
  | FeedbackSendAction
  | FeedbackSuccessAction
  | FeedbackDialogAction
  | FeedbackUpdateAction
;
