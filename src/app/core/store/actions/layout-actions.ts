import { Action } from '@ngrx/store';

export const OPEN_SIDENAV = '[Layout] Open Sidenav';
export const CLOSE_SIDENAV = '[Layout] Close Sidenav';
export const TOGGLE_SIDENAV = '[Layout] Toggle Sidenav';
export const MOBILE = '[Layout] Mobile';
export const IS_LOADING = '[Layout] Is loading';

export class OpenSidenavAction implements Action {
  readonly type = OPEN_SIDENAV;
}

export class CloseSidenavAction implements Action {
  readonly type = CLOSE_SIDENAV;
}

export class ToggleSidenavAction implements Action {
  readonly type = TOGGLE_SIDENAV;
}

export class MobileAction implements Action {
  readonly type = MOBILE;

  constructor(public payload: boolean) {}
}

export class IsLoadingAction implements Action {
  readonly type = IS_LOADING;

  constructor(public payload: boolean) {}
}

export type Actions = MobileAction | OpenSidenavAction | CloseSidenavAction | ToggleSidenavAction | IsLoadingAction;
