import { ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import * as fromLayout from './reducers/layout-reducers';
import * as fromCore from './reducers/core-reducers';
import * as fromRouter from '@ngrx/router-store';
import { localStorageSync } from 'ngrx-store-localstorage';

export interface State {
  layout: fromLayout.State;
  routerReducer: fromRouter.RouterReducerState;
  core: fromCore.State;
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<State> = {
  layout: fromLayout.reducer,
  routerReducer: fromRouter.routerReducer,
  core: fromCore.reducer
};

export function localStorageSyncReducer(reducer: ActionReducer<State>): ActionReducer<any, any> {
  return localStorageSync({
    keys: [
      'core'
    ],
    rehydrate: true,
    removeOnUndefined: true,
    restoreDates: false
  })(reducer);
}

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: MetaReducer<State>[] = [localStorageSyncReducer];

/**
 * Core Reducers
 */
export const getCoreState = createFeatureSelector<fromCore.State>('core');
export const getInformalTaxonGroupRoots = createSelector(getCoreState, fromCore.getInformalTaxonGroupRoots);
export const isFeedbackVisible = createSelector(getCoreState, fromCore.isFeedbackVisible);
export const getTermsAccepted = createSelector(getCoreState, fromCore.getTermsAccepted);
export const getFeedback = createSelector(getCoreState, fromCore.getFeedback);

/**
 * Layout Reducers
 */
export const getLayoutState = createFeatureSelector<fromLayout.State>('layout');
export const getShowSidenav = createSelector(getLayoutState, fromLayout.getShowSidenav);
export const getIsMobile =  createSelector(getLayoutState, fromLayout.getIsMobile);
export const isLoading =  createSelector(getLayoutState, fromLayout.isLoading);
