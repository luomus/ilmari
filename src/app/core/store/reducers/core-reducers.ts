import * as core from '../actions/core-actions';
import { ExtendedInformalTaxonGroup } from '../../../models/informal-taxon-group';
import { Feedback } from '../../../models/feedback';

export interface State {
  informalGroupRoots: ExtendedInformalTaxonGroup[];
  termsAccepted: number;
  feedbackVisible: boolean;
  feedback: Feedback;
}

const initialState: State = {
  informalGroupRoots: [],
  termsAccepted: 0,
  feedbackVisible: false,
  feedback: {
    subject: '',
    message: ''
  }
};

export function reducer(state = initialState, action: core.Actions): State {
  switch (action.type) {
    case core.ACCEPT_TERMS:
      return {
        ...state,
        termsAccepted: action.payload
      };
    case core.LOADED_INFORMAL_TAXON_GROUP_ROOTS:
      return {
        ...state,
        informalGroupRoots: action.payload
      };
    case core.FEEDBACK_DIALOG:
      return {
        ...state,
        feedbackVisible: action.payload
      };
    case core.FEEDBACK_UPDATE:
      return {
        ...state,
        feedback: {
          ...action.payload
        }
      };
    case core.FEEDBACK_SUCCESS:
      return {
        ...state,
        feedback: {
          subject: '',
          message: ''
        }
      };
    default:
      return state;
  }
}

export const getInformalTaxonGroupRoots = (state: State) => state.informalGroupRoots;
export const isFeedbackVisible = (state: State) => state.feedbackVisible;
export const getTermsAccepted = (state: State) => state.termsAccepted;
export const getFeedback = (state: State) => state.feedback;
