import * as layout from '../actions/layout-actions';

export interface State {
  showSidenav: boolean;
  isMobile: boolean;
  isLoading: boolean;
}

const initialState: State = {
  showSidenav: false,
  isMobile: true,
  isLoading: false
};

export function reducer(state = initialState, action: layout.Actions): State {
  switch (action.type) {
    case layout.CLOSE_SIDENAV:
      return {
        ...state,
        showSidenav: false,
      };
    case layout.OPEN_SIDENAV:
      return {
        ...state,
        showSidenav: true,
      };
    case layout.TOGGLE_SIDENAV:
      return {
        ...state,
        showSidenav: !state.showSidenav,
      };
    case layout.MOBILE:
      if (state.isMobile === action.payload) {
        return state;
      }
      return {
        ...state,
        isMobile: action.payload
      };
    case layout.IS_LOADING:
      return {
        ...state,
        isLoading: action.payload
      };
    default:
      return state;
  }
}

export const getShowSidenav = (state: State) => state.showSidenav;
export const getIsMobile = (state: State) => state.isMobile;
export const isLoading = (state: State) => state.isLoading;
