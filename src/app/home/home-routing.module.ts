import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeContainerComponent } from './home-container/home-container.component';
import { HeaderContainerComponent } from './header-container/header-container.component';

const routes: Routes = [
  {path: '', component: HomeContainerComponent, pathMatch: 'full'},
  {path: '', component: HeaderContainerComponent, pathMatch: 'full', outlet: 'header'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
