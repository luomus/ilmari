import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeContainerComponent } from './home-container/home-container.component';
import { IntroComponent } from './home-container/intro/intro.component';
import { HeaderContainerComponent } from './header-container/header-container.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ],
  declarations: [HomeContainerComponent, IntroComponent, HeaderContainerComponent]
})
export class HomeModule { }
