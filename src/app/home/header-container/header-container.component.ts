import { ChangeDetectionStrategy, Component, Inject, LOCALE_ID } from '@angular/core';

@Component({
  selector: 'ilm-header-container',
  templateUrl: './header-container.component.html',
  styleUrls: ['./header-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderContainerComponent {

  constructor(@Inject(LOCALE_ID) public locale: string) {}

}
