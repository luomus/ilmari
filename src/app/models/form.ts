export interface Form {
  id: string;
  language?: string;
  title: string;
  description: string;
  fields?: FlatField[];
}

export interface FlatField {
  name: string;
  parent: string;
  path: string;
  exactPath?: string;
  label: string;
  type: 'text'|'multiText'|'select'|'radio'|'hidden'|'taxon'|'multiFriend'|'images';
  values?: string[];
  valueLabels?: {[key: string]: string};
  default?: any;
  defaultOverride?: any;
  validators?: LajiValidator;
  warningValidators?: LajiValidator;
}

export interface LajiValidator {
  [validator: string]:
      LajiValidatorPresence
    | LajiValidatorDatetime
    | LajiValidatorCompareDate
    | LajiValidatorCrossCheck;
}

export interface LajiValidatorBase {
  message: string;
}

export interface LajiValidatorPresence extends LajiValidatorBase {
  allowEmpty?: boolean;
}
export interface LajiValidatorCompareDate extends LajiValidatorBase {
  hasTimeIfOtherHas?: boolean;
  isAfter?: string;
}

export interface LajiValidatorDatetime extends LajiValidatorBase {
  latest?: string;
}

export interface LajiValidatorCrossCheck extends LajiValidatorBase {
  check?: string;
}
