export interface SearchTaxa {
  belongsTo?: string[];
  name?: string;
  size: number;
}
