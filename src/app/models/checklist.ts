export interface Checklist {
  id: string;
  rootTaxon: string;
  'dc:bibliographicCitation'?: string;
}
