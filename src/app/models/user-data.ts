import { PersonModel } from './person';

export interface UserData {
  personToken: string;
  person: PersonModel;
}
