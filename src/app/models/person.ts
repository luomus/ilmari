export interface PersonModel {

  id?: string;

  fullName?: string;

  emailAddress?: string;

  defaultLanguage?: string;

  role?: string[];

}
