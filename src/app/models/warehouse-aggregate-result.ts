export interface WarehouseAggregateResult {
  aggregateBy: {
    [key: string]: string;
  };
  count: number;
  individualCountSum?: number;
  individualCountMax?: number;
  oldestRecord?: string;
  newestRecord?: string;
}
