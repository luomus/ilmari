import { IdAware } from './id-aware';
import { Autocomplete } from './autocomplete';

export interface TaxonModel extends IdAware {

  id: string;

  distributionMapFinland?: string;

  sortOrder?: number;

  redListStatus2000Finland?: string;

  redListStatus2010Finland?: string;

  redListStatus2015Finland?: string;

  winteringSecureLevel?: string;

  typeOfOccurrenceInFinlandNotes?: string;

  typeOfOccurrenceInFinland?: Array<string>;

  tradeName?: Array<string>;

  taxonomyText?: string;

  reproduction?: string;

  originalPublication?: Array<string>;

  occurrenceInFinlandPublication?: Array<string>;

  /**
   *  If lang parameter is 'multi' this will be a lang object instead of a string or an array of strings!
   */
  obsoleteVernacularName?: Array<string>;

  notes?: string;

  nestSiteSecureLevel?: string;

  naturaAreaSecureLevel?: string;

  nameDecidedDate?: Date;

  nameDecidedBy?: string;

  misappliedNameNotes?: string;

  isPartOfInformalTaxonGroup?: Array<string>;

  invasiveSpeciesEstablishment?: string;

  invasiveSpeciesCategory?: string;

  invasivePreventionMethodsText?: string;

  invasiveEffectText?: string;

  invasiveCitizenActionsText?: string;

  ingressText?: string;

  hasAdminStatus?: Array<string>;

  externalLinkURL?: string;

  euringNumber?: number;

  euringCode?: string;

  etymologyText?: string;

  customReportFormLink?: string;

  circumscription?: string;

  breedingSecureLevel?: string;

  birdlifeCode?: string;

  alsoKnownAs?: Array<string>;

  taxonRank?: string;

  isPartOf?: string;

  scientificName?: string;

  scientificNameAuthorship?: string;

  nameAccordingTo?: string;

  /**
   *  If lang parameter is 'multi' this will be a lang object instead of a string or an array of strings!
   */
  vernacularName?: string;

  misappliedName?: Array<string>;

  occurrenceInFinland?: string;

  checklistStatus?: string;

  checklist?: Array<string>;

  higherTaxaStatus?: boolean;

  finnishSpeciesTaggingStatus?: boolean;

  taxonExpert?: Array<string>;

  taxonEditor?: Array<string>;

  secureLevel?: string;

  informalTaxonGroups?: Array<string>;

  alternativeVernacularName?: Array<string>;

  occurrences?: Array<any>;

  synonyms?: Array<TaxonModel>;

  children?: Array<TaxonModel>;

  administrativeStatuses?: Array<string>;

  species?: any;

  invasiveSpecies?: any;

  /**
   * should the name appear cursive
   */
  cursiveName?: boolean;

  countOfSpecies?: number;

  countOfFinnishSpecies?: number;

  /**
   * is taxon species or subspecies or etc and occurs in Finland
   */
  finnishSpecies?: boolean;

  /**
   * taxon occurs in Finland
   */
  finnish?: any;

  /**
   * stable in Finland
   */
  stableInFinland?: any;

  expertChangesFromParent?: boolean;

  /**
   * sort order for taxonomic sorting
   */
  taxonomicSortOrder?: number;

  /**
   * true if has parents
   */
  hasParent?: boolean;

  /**
   * true if has children
   */
  hasChildren?: boolean;

  latestRedListStatusFinland?: LatestRedListStatusFinland;

  redListStatusesInFinland?: LatestRedListStatusFinland[];

  multimedia?: TaxonImage[];

  descriptions?: any;
}

export interface LatestRedListStatusFinland {
  status: string;
  year: number;
}

export interface TaxonDescriptionVariable {

  title?: string;

  content?: string;

}

export interface TaxonDescriptionGroup {

  title?: string;

  variables?: Array<TaxonDescriptionVariable>;

}

export interface TaxonDescription {

  id: string;

  title?: string;

  groups?: Array<TaxonDescriptionGroup>;

  speciesCardAuthors?: TaxonDescriptionVariable;

}

export interface TaxonImage {

  id?: string;

  author?: string;

  copyrightOwner?: string;

  largeURL?: string;

  fullURL?: string;

  licenseId?: string;

  licenseAbbreviation?: string;

  licenseDescription?: string;

  source?: string;

  thumbnailURL?: string;

}

export interface TaxonAutocomplete extends Autocomplete {
  payload?: {
    matchingName: string;
    informalTaxonGroups: {id: string, name: string}[];
    scientificName: string;
    scientificNameAuthorship: string;
    taxonRankId: string;
    matchType: string;
    cursiveName: boolean;
    finnish: boolean;
    species: boolean;
    nameType: string;
    vernacularName: string;
  };
}

export interface TaxonParent {
  id: string;
  taxonRank: string;
  vernacularName: string;
  scientificName: string;
}

export interface ExtendedTaxonImage extends TaxonImage {
  id: string;
}

export interface ExtendedTaxonRecording {
  'id': string;
  'gen': string;
  'sp': string;
  'ssp': string;
  'en': string;
  'rec': string;
  'cnt': string;
  'loc': string;
  'lat': string;
  'lng': string;
  'type': string;
  'file': string;
  'lic': string;
  'url': string;
  'q': string;
  'time': string;
  'date': string;
}

export interface ExtendedTaxon extends TaxonModel {
  species?: number;
  finnish?: number;
  invasiveSpecies?: number;
  stableInFinland?: number;
  multimedia?: ExtendedTaxonImage[];
  _media?: ExtendedTaxonImage[];
  _thumbnail?: string;
  _parents: TaxonParent[];
  _belongsTo: string[];
  _description: TaxonDescription;
  _updated: number;
  _selectedMedia: string;
  _descriptionCount: number;
  _count: number;
  _weeks: {[w: number]: number};
  _grid: TaxaGrid[];
  _masterGroup: string;
  _used?: number;
  _recordings?: ExtendedTaxonRecording[];
}

export interface TaxaGrid {
  lat: number;
  lon: number;
  count: number;
  individualCountMax: number;
  individualCountSum: number;
  newestRecord: string;
  oldestRecord: string;
}

export interface TaxaResult {
  taxa: ExtendedTaxon[];
  count: number;
}
