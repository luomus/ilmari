import { Document } from './document/document';

export interface UserSettings {
  useHighQualityImageInList: boolean;
  ignoreUnits: UserSettings.IgnoreUnitByType;
  publicityRestrictions: Document.PublicityRestrictionsEnum;
  imageMetadata: {
    intellectualRights: string
  };
}

export namespace UserSettings {
  export enum IgnoreUnitByType {
    withNoTaxon = 'withNoTaxon',
    withNoCount = 'withNoCount'
  }
}
