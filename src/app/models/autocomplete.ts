export interface Autocomplete {
  key: string;
  value: string;
}
