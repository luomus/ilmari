export interface Publication {
  id: string;
  'dc:URI'?: string;
  'dc:bibliographicCitation'?: string;
}
