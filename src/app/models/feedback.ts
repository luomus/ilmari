export class Feedback {
  subject: string;
  message: string;
  meta?: string;
}
