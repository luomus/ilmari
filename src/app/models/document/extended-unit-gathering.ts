import { UnitGathering } from './unit-gathering';

export interface ExtendedUnitGathering extends UnitGathering {
  _root: string;
  _parent: string;
}
