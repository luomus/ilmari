export interface TaxonCensus {
    /**
     * QName for MX.taxon
     */
    censusTaxonID: string;

    taxonCensusType: TaxonCensus.TaxonCensusTypeEnum;

}
export namespace TaxonCensus {
    export enum TaxonCensusTypeEnum {
        TaxonCensusTypeCounted = 'MY.taxonCensusTypeCounted',
        TaxonCensusTypeEstimated = 'MY.taxonCensusTypeEstimated',
        TaxonCensusTypeNotCounted = 'MY.taxonCensusTypeNotCounted',
        TaxonCensusTypeUHEXCounted = 'MY.taxonCensusTypeUHEXCounted',
        TaxonCensusTypeUHEXNotCounted = 'MY.taxonCensusTypeUHEXNotCounted'
    }
}
