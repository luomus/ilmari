export interface GatheringFact {
    aCAFLAonAlder?: number;

    aCAFLAonBirch?: number;

    aCAFLAonGround?: number;

    aCAFLAonPine?: number;

    aCAFLAonSpruce?: number;

    cARSPIonAlder?: number;

    cARSPIonBirch?: number;

    cARSPIonGround?: number;

    cARSPIonPine?: number;

    cARSPIonSpruce?: number;

    lOXIAonAlder?: number;

    lOXIAonBirch?: number;

    lOXIAonGround?: number;

    lOXIAonPine?: number;

    lOXIAonSpruce?: number;

    abundanceBOMGAR?: GatheringFact.AbundanceBOMGAREnum;

    abundancePINENU?: GatheringFact.AbundancePINENUEnum;

    abundanceTURPIL?: GatheringFact.AbundanceTURPILEnum;

    birdFeederCount?: number;

    birdFeederCountBiotopeA?: number;

    birdFeederCountBiotopeB?: number;

    birdFeederCountBiotopeC?: number;

    birdFeederCountBiotopeD?: number;

    birdFeederCountBiotopeE?: number;

    birdFeederCountBiotopeF?: number;

    birdFeederCountBiotopeG?: number;

    birdFeederCountBiotopeH?: number;

    cloudAndRain?: GatheringFact.CloudAndRainEnum;

    descriptionBiotopeF?: string;

    feedingStationCount?: number;

    feedingStationCountBiotopeA?: number;

    feedingStationCountBiotopeB?: number;

    feedingStationCountBiotopeC?: number;

    feedingStationCountBiotopeD?: number;

    feedingStationCountBiotopeE?: number;

    feedingStationCountBiotopeF?: number;

    feedingStationCountBiotopeG?: number;

    feedingStationCountBiotopeH?: number;

    iceCover?: GatheringFact.IceCoverEnum;

    meanTemperature?: number;

    observerCount?: number;

    /**
     * Non-negative integer
     */
    observerID?: number;

    pineConesAtCensus?: GatheringFact.PineConesAtCensusEnum;

    /**
     * Non-negative integer
     */
    routeID?: number;

    /**
     * Non-negative integer
     */
    routeLength?: number;

    /**
     * Non-negative integer
     */
    routeLengthBiotopeA?: number;

    /**
     * Non-negative integer
     */
    routeLengthBiotopeB?: number;

    /**
     * Non-negative integer
     */
    routeLengthBiotopeC?: number;

    /**
     * Non-negative integer
     */
    routeLengthBiotopeD?: number;

    /**
     * Non-negative integer
     */
    routeLengthBiotopeE?: number;

    /**
     * Non-negative integer
     */
    routeLengthBiotopeF?: number;

    /**
     * Non-negative integer
     */
    routeLengthBiotopeG?: number;

    /**
     * Non-negative integer
     */
    routeLengthBiotopeH?: number;

    routeLengthChange?: number;

    routeLengthChangeBiotopeA?: number;

    routeLengthChangeBiotopeB?: number;

    routeLengthChangeBiotopeC?: number;

    routeLengthChangeBiotopeD?: number;

    routeLengthChangeBiotopeE?: number;

    routeLengthChangeBiotopeF?: number;

    routeLengthChangeBiotopeG?: number;

    routeLengthChangeBiotopeH?: number;

    snowAndIceOnTrees?: GatheringFact.SnowAndIceOnTreesEnum;

    snowCover?: GatheringFact.SnowCoverEnum;

    sorbusBerriesAtCensus?: GatheringFact.SorbusBerriesAtCensusEnum;

    sorbusBerriesEarlyFall?: GatheringFact.SorbusBerriesEarlyFallEnum;

    spruceConesAtCensus?: GatheringFact.SpruceConesAtCensusEnum;

    typeOfSnowCover?: GatheringFact.TypeOfSnowCoverEnum;

    visibility?: GatheringFact.VisibilityEnum;

    waterbodies?: GatheringFact.WaterbodiesEnum;

    wayOfTravel?: GatheringFact.WayOfTravelEnum;

    wayOfTravelNotes?: string;

    wind?: GatheringFact.WindEnum;

}
export namespace GatheringFact {
    export enum AbundanceBOMGAREnum {
        SpeciesAbundanceEnum0 = 'WBC.speciesAbundanceEnum0',
        SpeciesAbundanceEnum1 = 'WBC.speciesAbundanceEnum1',
        SpeciesAbundanceEnum2 = 'WBC.speciesAbundanceEnum2',
        SpeciesAbundanceEnum3 = 'WBC.speciesAbundanceEnum3'
    }
    export enum AbundancePINENUEnum {
        SpeciesAbundanceEnum0 = 'WBC.speciesAbundanceEnum0',
        SpeciesAbundanceEnum1 = 'WBC.speciesAbundanceEnum1',
        SpeciesAbundanceEnum2 = 'WBC.speciesAbundanceEnum2',
        SpeciesAbundanceEnum3 = 'WBC.speciesAbundanceEnum3'
    }
    export enum AbundanceTURPILEnum {
        SpeciesAbundanceEnum0 = 'WBC.speciesAbundanceEnum0',
        SpeciesAbundanceEnum1 = 'WBC.speciesAbundanceEnum1',
        SpeciesAbundanceEnum2 = 'WBC.speciesAbundanceEnum2',
        SpeciesAbundanceEnum3 = 'WBC.speciesAbundanceEnum3'
    }
    export enum CloudAndRainEnum {
        CloudAndRainEnum0 = 'WBC.cloudAndRainEnum0',
        CloudAndRainEnum1 = 'WBC.cloudAndRainEnum1',
        CloudAndRainEnum2 = 'WBC.cloudAndRainEnum2',
        CloudAndRainEnum3 = 'WBC.cloudAndRainEnum3',
        CloudAndRainEnum4 = 'WBC.cloudAndRainEnum4'
    }
    export enum IceCoverEnum {
        IceCoverEnum0 = 'WBC.iceCoverEnum0',
        IceCoverEnum1 = 'WBC.iceCoverEnum1',
        IceCoverEnum2 = 'WBC.iceCoverEnum2',
        IceCoverEnum3 = 'WBC.iceCoverEnum3',
        IceCoverEnum4 = 'WBC.iceCoverEnum4'
    }
    export enum PineConesAtCensusEnum {
        BerriesAndConesEnum0 = 'WBC.berriesAndConesEnum0',
        BerriesAndConesEnum1 = 'WBC.berriesAndConesEnum1',
        BerriesAndConesEnum2 = 'WBC.berriesAndConesEnum2',
        BerriesAndConesEnum3 = 'WBC.berriesAndConesEnum3',
        BerriesAndConesEnum4 = 'WBC.berriesAndConesEnum4',
        BerriesAndConesEnum5 = 'WBC.berriesAndConesEnum5',
        BerriesAndConesEnum6 = 'WBC.berriesAndConesEnum6'
    }
    export enum SnowAndIceOnTreesEnum {
        SnowAndIceOnTreesEnum0 = 'WBC.snowAndIceOnTreesEnum0',
        SnowAndIceOnTreesEnum1 = 'WBC.snowAndIceOnTreesEnum1',
        SnowAndIceOnTreesEnum2 = 'WBC.snowAndIceOnTreesEnum2',
        SnowAndIceOnTreesEnum3 = 'WBC.snowAndIceOnTreesEnum3'
    }
    export enum SnowCoverEnum {
        SnowCoverEnum0 = 'WBC.snowCoverEnum0',
        SnowCoverEnum1 = 'WBC.snowCoverEnum1',
        SnowCoverEnum2 = 'WBC.snowCoverEnum2',
        SnowCoverEnum3 = 'WBC.snowCoverEnum3',
        SnowCoverEnum4 = 'WBC.snowCoverEnum4',
        SnowCoverEnum5 = 'WBC.snowCoverEnum5',
        SnowCoverEnum6 = 'WBC.snowCoverEnum6',
        SnowCoverEnum7 = 'WBC.snowCoverEnum7',
        SnowCoverEnum8 = 'WBC.snowCoverEnum8'
    }
    export enum SorbusBerriesAtCensusEnum {
        BerriesAndConesEnum0 = 'WBC.berriesAndConesEnum0',
        BerriesAndConesEnum1 = 'WBC.berriesAndConesEnum1',
        BerriesAndConesEnum2 = 'WBC.berriesAndConesEnum2',
        BerriesAndConesEnum3 = 'WBC.berriesAndConesEnum3',
        BerriesAndConesEnum4 = 'WBC.berriesAndConesEnum4',
        BerriesAndConesEnum5 = 'WBC.berriesAndConesEnum5',
        BerriesAndConesEnum6 = 'WBC.berriesAndConesEnum6'
    }
    export enum SorbusBerriesEarlyFallEnum {
        BerriesAndConesEnum0 = 'WBC.berriesAndConesEnum0',
        BerriesAndConesEnum1 = 'WBC.berriesAndConesEnum1',
        BerriesAndConesEnum2 = 'WBC.berriesAndConesEnum2',
        BerriesAndConesEnum3 = 'WBC.berriesAndConesEnum3',
        BerriesAndConesEnum4 = 'WBC.berriesAndConesEnum4',
        BerriesAndConesEnum5 = 'WBC.berriesAndConesEnum5',
        BerriesAndConesEnum6 = 'WBC.berriesAndConesEnum6'
    }
    export enum SpruceConesAtCensusEnum {
        BerriesAndConesEnum0 = 'WBC.berriesAndConesEnum0',
        BerriesAndConesEnum1 = 'WBC.berriesAndConesEnum1',
        BerriesAndConesEnum2 = 'WBC.berriesAndConesEnum2',
        BerriesAndConesEnum3 = 'WBC.berriesAndConesEnum3',
        BerriesAndConesEnum4 = 'WBC.berriesAndConesEnum4',
        BerriesAndConesEnum5 = 'WBC.berriesAndConesEnum5',
        BerriesAndConesEnum6 = 'WBC.berriesAndConesEnum6'
    }
    export enum TypeOfSnowCoverEnum {
        TypeOfSnowCoverEnum0 = 'WBC.typeOfSnowCoverEnum0',
        TypeOfSnowCoverEnum1 = 'WBC.typeOfSnowCoverEnum1',
        TypeOfSnowCoverEnum2 = 'WBC.typeOfSnowCoverEnum2'
    }
    export enum VisibilityEnum {
        VisibilityEnum0 = 'WBC.visibilityEnum0',
        VisibilityEnum1 = 'WBC.visibilityEnum1',
        VisibilityEnum2 = 'WBC.visibilityEnum2',
        VisibilityEnum3 = 'WBC.visibilityEnum3',
        VisibilityEnum4 = 'WBC.visibilityEnum4'
    }
    export enum WaterbodiesEnum {
        WaterbodiesEnum0 = 'WBC.waterbodiesEnum0',
        WaterbodiesEnum1 = 'WBC.waterbodiesEnum1',
        WaterbodiesEnum2 = 'WBC.waterbodiesEnum2',
        WaterbodiesEnum3 = 'WBC.waterbodiesEnum3',
        WaterbodiesEnum4 = 'WBC.waterbodiesEnum4',
        WaterbodiesEnum5 = 'WBC.waterbodiesEnum5'
    }
    export enum WayOfTravelEnum {
        WayOfTravelEnum0 = 'WBC.wayOfTravelEnum0',
        WayOfTravelEnum1 = 'WBC.wayOfTravelEnum1',
        WayOfTravelEnum2 = 'WBC.wayOfTravelEnum2'
    }
    export enum WindEnum {
        WindEnum0 = 'WBC.windEnum0',
        WindEnum1 = 'WBC.windEnum1',
        WindEnum2 = 'WBC.windEnum2',
        WindEnum3 = 'WBC.windEnum3',
        WindEnum4 = 'WBC.windEnum4'
    }
}
