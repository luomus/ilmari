import { UnitFact } from './unit-fact';

export interface ExtendedUnitFact extends UnitFact {
  _root: string;
  _parent: string;
}
