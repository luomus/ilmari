import { GatheringEvent } from './gathering-event';

export interface ExtendedGatheringEvent extends GatheringEvent {
  _root: string;
}
