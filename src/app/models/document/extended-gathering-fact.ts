import { GatheringFact } from './gathering-fact';

export interface ExtendedGatheringFact extends GatheringFact {
  _root: string;
  _parent: string;
}
