import { GeometryCollection, LineString, MultiLineString, MultiPoint, MultiPolygon, Point, Polygon } from 'geojson';

export type Geometry = Circle | Point | Polygon | MultiLineString | MultiPoint | MultiPolygon | LineString | GeometryCollection;

export interface Circle extends Point {
  radius: number;
}
