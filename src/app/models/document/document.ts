import { GatheringEvent } from './gathering-event';
import { Gatherings } from './gatherings';

export interface Document {
  /**
   * Unique ID for the object. This will be automatically generated.
   */
  id?: string;

  /**
   * Leave empty if no sample taken, or if the sample was recorded separately
   */
  dNASampleLocation?: string;

  iPEN?: string;

  /**
   * URL where more information is available about the specimen
   */
  uRL?: string;

  /**
   * From who/where the specimen was acquired (if not recorded as a transaction)
   */
  acquiredFrom?: string;

  /**
   * QName for MOS.organization
   */
  acquiredFromOrganization?: string;

  /**
   * Date or year on which the specimen was acquired to the collection. Empty means and old specimen acquired on an unknown date.
   */
  acquisitionDate?: string;

  /**
   * Other identifiers this specimen has, in format 'type:identifier'. For example: 'mzhtypes:123' (old MAZ-type number)
   */
  additionalIDs?: Array<string>;

  /**
   * You can include additonal comment by separating them with colon, e.g. \\\"AY123456:comments here\\\"
   */
  bold?: Array<string>;

  /**
   * Clad book id number or such
   */
  cladBookID?: string;

  /**
   * Clad specimen id: usually color description and a number
   */
  cladSpecimenID?: string;

  /**
   * Verbatim specimen data from clad book
   */
  cladVerbatim?: string;

  /**
   * The collection which this specimen belongs to. QName for MY.collection
   */
  collectionID?: string;

  /**
   * Notes on the defects of the specimen (missing parts or such). Empty value means same as \\\"good\\\" or \\\"hyvä\\\" - that the specimen is in fine condition.
   */
  condition?: string;

  /**
   * QName for MA.person
   */
  creator?: string;

  /**
   * Where the data about this specimen is from, in addition to labels.
   */
  dataSource?: string;

  /**
   * The dataset(s) this specimen belongs to. QName for GX.dataset
   */
  datasetID?: Array<string>;

  datatype?: string;

  /**
   * dateTime string using ISO8601 format
   */
  dateCreated?: string;

  /**
   * dateTime string using ISO8601 format
   */
  dateEdited?: string;

  deviceID?: string;

  /**
   * Location of the specimen so that museum personnel can find it. E.g. taxon under which it is stored (if not clear from the identification), or shelf number
   */
  documentLocation?: string;

  /**
   * Description where duplicates (specimens of the same individual) have been sent to and by which ID's
   */
  duplicatesIn?: string;

  /**
   * Reason for this edit or notes about it.
   */
  editNotes?: string;

  /**
   * Name of the person(s) (and possibly the organization) who first transcribed the data
   */
  editor?: string;

  /**
   * QName for MA.person
   */
  editors?: Array<string>;

  /**
   * Date the data was first transcribed into electronic format or paper registry
   */
  entered?: string;

  /**
   * Diary-style information about what has been  done to the specimen
   */
  event?: Array<string>;

  /**
   * Name of the exiccatum this specimen belongs to
   */
  exsiccatum?: string;

  /**
   * Id of the form that was used for the document
   */
  formID?: string;

  /**
   * instance of gatheringEvent
   */
  gatheringEvent?: GatheringEvent;

  /**
   * Array of gatherings
   */
  gatherings?: Array<Gatherings>;

  /**
   * You can include additonal comment by separating them with colon, e.g. \\\"AY123456:comments here\\\"
   */
  genbank?: Array<string>;

  /**
   * QName for MM.image
   */
  images?: Array<string>;

  /**
   * Filled in by ICT team
   */
  inMustikka?: boolean;

  isTemplate?: boolean;

  keywords?: Array<string>;

  /**
   * Text from labels word-for-word, including spelling gatheringErrors. Separate each label on its own row, starting from topmost label.
   */
  labelsVerbatim?: string;

  /**
   * Language the specimen data is (mainly) written in, if applicable.
   */
  language?: string;

  /**
   * Collector's identifier (field identifier, keruunumero) for the specimen
   */
  legID?: string;

  /**
   * QName for MNP.namedPlace
   */
  namedPlaceID?: string;

  /**
   * Free-text notes
   */
  notes?: string;

  /**
   * Original catalogue number or other  original identifier of the specimen. E.g. H9000000
   */
  originalSpecimenID?: string;

  /**
   * Team that owns the record and can edit it.. QName for MOS.organization
   */
  owner?: string;

  plannedLocation?: string;

  /**
   * Main method of preservation
   */
  preservation?: Document.PreservationEnum;

  /**
   * Location of the primary data if not Kotka.
   */
  primaryDataLocation?: string;

  publication?: Array<string>;

  /**
   * PUBLIC: all data can be published; PROTECTED: exact locality is hidden; PRIVATE: most of the data is hidden. If blank means same as public
   */
  publicityRestrictions?: Document.PublicityRestrictionsEnum;

  /**
   * Relationship to another taxon OR specimen. Prefix with relationship type, e.g. \\\"parasite: Parasiticus specius\\\" OR \\\"host:http://tun.fi/JAA.123\\\"
   */
  relationship?: Array<string>;

  /**
   * The history of the sample
   */
  sampleHistory?: string;

  scheduledForDeletion?: boolean;

  secureLevel?: Document.SecureLevelEnum;

  /**
   * ID of the specimen from which this has been separated from
   */
  separatedFrom?: string;

  separatedTo?: Array<string>;

  serialNumber?: string;

  /**
   * QName for KE.informationSystem
   */
  sourceID?: string;

  /**
   * Empty value means same as \\\"ok\\\" - that there is not anything special about the status of the specimen.
   */
  status?: Document.StatusEnum;

  temp?: boolean;

  templateDescription?: string;

  templateName?: string;

  /**
   * Common name of agreement concerning the transfer, if any.
   */
  transferAgreement?: string;

  /**
   * List of those fields that contain unreliable data. The list is created automatically.
   */
  unreliableFields?: string;

  /**
   * Information about the quality of the specimen
   */
  verificationStatus?: Document.VerificationStatusEnum;

  voucherSpecimenID?: string;

}

export namespace Document {

  export enum Path {
    autocompleteSelectedTaxonID = 'gatherings[*].units[*].unitFact.autocompleteSelectedTaxonID',
    informalTaxonGroups = 'gatherings[*].units[*].informalTaxonGroups',
    taxon = 'gatherings[*].units[*].identifications[*].taxon',
    unitGeometry = 'gatherings[*].units[*].unitGathering.geometry',
    gatheringGeometry = 'gatherings[*].geometry'
  }

  export enum PreservationEnum {
    PreservationPinned = 'MY.preservationPinned',
    PreservationGlued = 'MY.preservationGlued',
    PreservationEthanol = 'MY.preservationEthanol',
    PreservationEthanolPure = 'MY.preservationEthanolPure',
    PreservationEthanol96 = 'MY.preservationEthanol96',
    PreservationEthanol80 = 'MY.preservationEthanol80',
    PreservationEthanol80Pure = 'MY.preservationEthanol80Pure',
    PreservationEthanol70 = 'MY.preservationEthanol70',
    PreservationEthanolDenatured = 'MY.preservationEthanolDenatured',
    PreservationFormalin = 'MY.preservationFormalin',
    PreservationEthanolFormalin = 'MY.preservationEthanolFormalin',
    PreservationGlycerol = 'MY.preservationGlycerol',
    PreservationLiquid = 'MY.preservationLiquid',
    PreservationEulan = 'MY.preservationEulan',
    PreservationSlide = 'MY.preservationSlide',
    PreservationSlideEuparal = 'MY.preservationSlideEuparal',
    PreservationSlidePolyviol = 'MY.preservationSlidePolyviol',
    PreservationSlideCanadaBalsam = 'MY.preservationSlideCanadaBalsam',
    PreservationCriticalPointDrying = 'MY.preservationCriticalPointDrying',
    PreservationGoldPlated = 'MY.preservationGoldPlated',
    PreservationFreezeDried = 'MY.preservationFreezeDried',
    PreservationFrozen = 'MY.preservationFrozen',
    PreservationDry = 'MY.preservationDry',
    PreservationStuffed = 'MY.preservationStuffed',
    PreservationParaffin = 'MY.preservationParaffin',
    PreservationPressed = 'MY.preservationPressed',
    PreservationLiving = 'MY.preservationLiving',
    PreservationCast = 'MY.preservationCast',
    PreservationEthanolExFormalin = 'MY.preservationEthanolExFormalin',
    PreservationMercuricChloride = 'MY.preservationMercuricChloride',
    PreservationBouinSolution = 'MY.preservationBouinSolution',
    PreservationPampelsFluid = 'MY.preservationPampelsFluid'
  }

  export enum PublicityRestrictionsEnum {
    PublicityRestrictionsPublic = 'MZ.publicityRestrictionsPublic',
    PublicityRestrictionsProtected = 'MZ.publicityRestrictionsProtected',
    PublicityRestrictionsPrivate = 'MZ.publicityRestrictionsPrivate'
  }

  export enum SecureLevelEnum {
    SecureLevelNone = 'MX.secureLevelNone',
    SecureLevelKM1 = 'MX.secureLevelKM1',
    SecureLevelKM5 = 'MX.secureLevelKM5',
    SecureLevelKM10 = 'MX.secureLevelKM10',
    SecureLevelKM25 = 'MX.secureLevelKM25',
    SecureLevelKM50 = 'MX.secureLevelKM50',
    SecureLevelKM100 = 'MX.secureLevelKM100',
    SecureLevelHighest = 'MX.secureLevelHighest',
    SecureLevelNoShow = 'MX.secureLevelNoShow'
  }

  export enum StatusEnum {
    StatusOk = 'MY.statusOk',
    StatusMissing = 'MY.statusMissing',
    StatusUnrecoverable = 'MY.statusUnrecoverable',
    StatusLost = 'MY.statusLost',
    StatusDonated = 'MY.statusDonated',
    StatusDeaccessioned = 'MY.statusDeaccessioned',
    StatusSpent = 'MY.statusSpent',
    StatusDestroyed = 'MY.statusDestroyed',
    StatusUndefined = 'MY.statusUndefined',
    StatusAxenic = 'MY.statusAxenic',
    StatusNonAxenic = 'MY.statusNonAxenic'
  }

  export enum VerificationStatusEnum {
    VerificationStatusOk = 'MY.verificationStatusOk',
    VerificationStatusVerify = 'MY.verificationStatusVerify',
    VerificationStatusComplete = 'MY.verificationStatusComplete',
    VerificationStatusGeoreference = 'MY.verificationStatusGeoreference',
    VerificationStatusDet = 'MY.verificationStatusDet',
    VerificationStatusCheckID = 'MY.verificationStatusCheckID',
    VerificationStatusVerifyCoordinates = 'MY.verificationStatusVerifyCoordinates'
  }
}
