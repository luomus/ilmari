import { Identifications } from './identifications';

export interface ExtendedIdentifications extends Identifications {
  _root: string;
  _parent: string;
}
