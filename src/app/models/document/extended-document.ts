import { ExtendedGatherings } from './extended-gatherings';
import { ExtendedGatheringEvent } from './extended-gathering-event';
import { Document } from './document';

export interface ExtendedDocument extends Document {
  gatheringEvent?: ExtendedGatheringEvent;
  gatherings?: ExtendedGatherings[];
}
