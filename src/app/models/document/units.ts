import { Identifications } from './identifications';
import { TypeSpecimens } from './type-specimens';
import { UnitFact } from './unit-fact';
import { UnitGathering } from './unit-gathering';

export interface Units {
    /**
     * Diameter at breast height, in centimeters
     */
    dBH?: string;

    /**
     * Leave empty if no sample taken, or if the sample was recorded separately
     */
    dNASampleLocation?: string;

    abundanceString?: string;

    adultIndividualCount?: number;

    age?: string;

    ageNotes?: string;

    alive?: boolean;

    ankleInMillimeters?: Array<string>;

    atlasCode?: Units.AtlasCodeEnum;

    beakInMillimeters?: Array<string>;

    breeding?: boolean;

    broodSize?: number;

    causeOfDeath?: string;

    /**
     * QName for MR.checklist
     */
    checklistID?: string;

    chemistry?: string;

    /**
     * Numeric or other description on the amount of individuals (or sprouts,
     * fruiting bodies or such) in the specimen. Sexes and juveniles can be specified like so: 1m2f3j (=1 male, 2 females, 3 juveniles)
     */
    count?: string;

    decayStage?: string;

    /**
     * Non-negative integer
     */
    femaleIndividualCount?: number;

    genotype?: string;

    gonadInMillimeters?: Array<string>;

    growthMediumName?: string;

    growthOtherConditions?: string;

    growthTemperature?: string;

    /**
     * QName for MX.taxon
     */
    hostID?: string;

    hostInformalNameString?: string;

    /**
     * Unique ID for the object. This will be automatically generated.
     */
    id?: string;

    /**
     * Array of identifications
     */
    identifications?: Array<Identifications>;

    /**
     * QName for MM.image
     */
    images?: Array<string>;

    /**
     * Non-negative integer
     */
    individualCount?: number;

    informalNameString?: string;

    /**
     * QName for MVL.informalTaxonGroup
     */
    informalTaxonGroup?: string;

    /**
     * Valitut muotoryhmät. QName for MVL.informalTaxonGroup
     */
    informalTaxonGroups?: Array<string>;

    infrasubspecificSubdivision?: string;

    juvenileIndividualCount?: number;

    lengthInMillimeters?: Array<string>;

    lifeStage?: Units.LifeStageEnum;

    lifeStageDescription?: string;

    lineTransectObsType?: Units.LineTransectObsTypeEnum;

    lineTransectPairCountChanged?: boolean;

    lineTransectRouteFieldType?: Units.LineTransectRouteFieldTypeEnum;

    macroscopy?: string;

    /**
     * Non-negative integer
     */
    maleIndividualCount?: number;

    measurement?: Array<string>;

    microscopy?: string;

    movingStatus?: string;

    mutant?: string;

    nativeStatus?: Units.NativeStatusEnum;

    /**
     * Free-text notes
     */
    notes?: string;

    pairCount?: number;

    plantLifeStage?: Units.PlantLifeStageEnum;

    plantStatusCode?: Units.PlantStatusCodeEnum;

    /**
     * Abundance of the taxon in the field
     */
    populationAbundance?: string;

    preparations?: string;

    /**
     * Main method of preservation
     */
    preservation?: Units.PreservationEnum;

    /**
     * Source of the accession
     */
    provenance?: Units.ProvenanceEnum;

    /**
     * PUBLIC: all data can be published; PROTECTED: exact locality is hidden; PRIVATE: most of the data is hidden. If blank means same as public
     */
    publicityRestrictions?: Units.PublicityRestrictionsEnum;

    recordBasis?: Units.RecordBasisEnum;

    recordParts?: Array<string>;

    ring?: string;

    samplingMethod?: Units.SamplingMethodEnum;

    sex?: Units.SexEnum;

    sexNotes?: string;

    shortHandText?: string;

    smell?: Units.SmellEnum;

    smellNotes?: string;

    substrateClassification?: Units.SubstrateClassificationEnum;

    substrateDecayStage?: Units.SubstrateDecayStageEnum;

    substrateNotes?: string;

    substrateSpecies?: string;

    substrateTreeClassification?: Array<string>;

    tailInMillimeters?: Array<string>;

    taste?: Units.TasteEnum;

    tasteNotes?: string;

    taxonConfidence?: Units.TaxonConfidenceEnum;

    twitched?: boolean;

    /**
     * QName for MY.typeSpecimen
     */
    typeSpecimen?: Array<string>;

    /**
     * Array of typeSpecimens
     */
    typeSpecimens?: Array<TypeSpecimens>;

    /**
     * instance of unitFact
     */
    unitFact?: UnitFact;

    /**
     * instance of unitGathering
     */
    unitGathering?: UnitGathering;

    unitType?: Array<string>;

    weightInGrams?: Array<string>;

    wingInMillimeters?: Array<string>;

    /**
     * Decimal number
     */
    wingLengthAccuracy?: number;

    /**
     * Decimal number
     */
    wingLengthMax?: number;

    /**
     * Decimal number
     */
    wingLengthMin?: number;

}
export namespace Units {
    export enum AtlasCodeEnum {
        AtlasCodeEnum1 = 'MY.atlasCodeEnum1',
        AtlasCodeEnum2 = 'MY.atlasCodeEnum2',
        AtlasCodeEnum3 = 'MY.atlasCodeEnum3',
        AtlasCodeEnum4 = 'MY.atlasCodeEnum4',
        AtlasCodeEnum5 = 'MY.atlasCodeEnum5',
        AtlasCodeEnum6 = 'MY.atlasCodeEnum6',
        AtlasCodeEnum7 = 'MY.atlasCodeEnum7',
        AtlasCodeEnum8 = 'MY.atlasCodeEnum8',
        AtlasCodeEnum61 = 'MY.atlasCodeEnum61',
        AtlasCodeEnum62 = 'MY.atlasCodeEnum62',
        AtlasCodeEnum63 = 'MY.atlasCodeEnum63',
        AtlasCodeEnum64 = 'MY.atlasCodeEnum64',
        AtlasCodeEnum65 = 'MY.atlasCodeEnum65',
        AtlasCodeEnum66 = 'MY.atlasCodeEnum66',
        AtlasCodeEnum71 = 'MY.atlasCodeEnum71',
        AtlasCodeEnum72 = 'MY.atlasCodeEnum72',
        AtlasCodeEnum73 = 'MY.atlasCodeEnum73',
        AtlasCodeEnum74 = 'MY.atlasCodeEnum74',
        AtlasCodeEnum75 = 'MY.atlasCodeEnum75',
        AtlasCodeEnum81 = 'MY.atlasCodeEnum81',
        AtlasCodeEnum82 = 'MY.atlasCodeEnum82'
    }
    export enum LifeStageEnum {
        LifeStageEgg = 'MY.lifeStageEgg',
        LifeStageLarva = 'MY.lifeStageLarva',
        LifeStagePupa = 'MY.lifeStagePupa',
        LifeStageJuvenile = 'MY.lifeStageJuvenile',
        LifeStageNymph = 'MY.lifeStageNymph',
        LifeStageSubimago = 'MY.lifeStageSubimago',
        LifeStageImmature = 'MY.lifeStageImmature',
        LifeStageAdult = 'MY.lifeStageAdult',
        LifeStageFertile = 'MY.lifeStageFertile',
        LifeStageSterile = 'MY.lifeStageSterile',
        LifeStageTadpole = 'MY.lifeStageTadpole',
        LifeStageDead = 'MY.lifeStageDead',
        LifeStageAlive = 'MY.lifeStageAlive',
        LifeStageEmbryo = 'MY.lifeStageEmbryo',
        LifeStageSubadult = 'MY.lifeStageSubadult',
        LifeStageMature = 'MY.lifeStageMature'
    }
    export enum LineTransectObsTypeEnum {
        LineTransectObsTypeSong = 'MY.lineTransectObsTypeSong',
        LineTransectObsTypeOtherSound = 'MY.lineTransectObsTypeOtherSound',
        LineTransectObsTypeSeen = 'MY.lineTransectObsTypeSeen',
        LineTransectObsTypeSeenMale = 'MY.lineTransectObsTypeSeenMale',
        LineTransectObsTypeSeenFemale = 'MY.lineTransectObsTypeSeenFemale',
        LineTransectObsTypeFlyingOverhead = 'MY.lineTransectObsTypeFlyingOverhead',
        LineTransectObsTypeFlock = 'MY.lineTransectObsTypeFlock',
        LineTransectObsTypeFlockFlyingOverhead = 'MY.lineTransectObsTypeFlockFlyingOverhead',
        LineTransectObsTypeSeenPair = 'MY.lineTransectObsTypeSeenPair',
        LineTransectObsTypeSeenBrood = 'MY.lineTransectObsTypeSeenBrood',
        LineTransectObsTypeSeenNest = 'MY.lineTransectObsTypeSeenNest'
    }
    export enum LineTransectRouteFieldTypeEnum {
        LineTransectRouteFieldTypeInner = 'MY.lineTransectRouteFieldTypeInner',
        LineTransectRouteFieldTypeOuter = 'MY.lineTransectRouteFieldTypeOuter'
    }
    export enum NativeStatusEnum {
        Native = 'MY.native',
        NonNative = 'MY.nonNative'
    }
    export enum PlantLifeStageEnum {
        PlantLifeStageSterile = 'MY.plantLifeStageSterile',
        PlantLifeStageFertile = 'MY.plantLifeStageFertile',
        PlantLifeStageSeed = 'MY.plantLifeStageSeed',
        PlantLifeStageSprout = 'MY.plantLifeStageSprout',
        PlantLifeStageBud = 'MY.plantLifeStageBud',
        PlantLifeStageFlower = 'MY.plantLifeStageFlower',
        PlantLifeStageWitheredFlower = 'MY.plantLifeStageWitheredFlower',
        PlantLifeStageRipeningFruit = 'MY.plantLifeStageRipeningFruit',
        PlantLifeStageRipeFruit = 'MY.plantLifeStageRipeFruit',
        PlantLifeStageDeadSprout = 'MY.plantLifeStageDeadSprout',
        PlantLifeStageSubterranean = 'MY.plantLifeStageSubterranean',
        PlantLifeStageLivingPlant = 'MY.plantLifeStageLivingPlant',
        PlantLifeStageDeadPlant = 'MY.plantLifeStageDeadPlant'
    }
    export enum PlantStatusCodeEnum {
        MYPlantStatusCodeL = 'MY.MY.plantStatusCodeL',
        PlantStatusCodeA = 'MY.plantStatusCodeA',
        PlantStatusCodeAV = 'MY.plantStatusCodeAV',
        PlantStatusCodeAOV = 'MY.plantStatusCodeAOV',
        PlantStatusCodeAN = 'MY.plantStatusCodeAN',
        PlantStatusCodeANV = 'MY.plantStatusCodeANV',
        PlantStatusCodeANS = 'MY.plantStatusCodeANS',
        PlantStatusCodeT = 'MY.plantStatusCodeT',
        PlantStatusCodeTV = 'MY.plantStatusCodeTV',
        PlantStatusCodeTOV = 'MY.plantStatusCodeTOV',
        PlantStatusCodeTNV = 'MY.plantStatusCodeTNV',
        PlantStatusCodeTNS = 'MY.plantStatusCodeTNS',
        PlantStatusCodeV = 'MY.plantStatusCodeV',
        PlantStatusCodeOV = 'MY.plantStatusCodeOV',
        PlantStatusCodeN = 'MY.plantStatusCodeN',
        PlantStatusCodeNV = 'MY.plantStatusCodeNV',
        PlantStatusCodeNS = 'MY.plantStatusCodeNS',
        PlantStatusCodeE = 'MY.plantStatusCodeE',
        PlantStatusCodeTE = 'MY.plantStatusCodeTE',
        PlantStatusCodeTVE = 'MY.plantStatusCodeTVE',
        PlantStatusCodeTOVE = 'MY.plantStatusCodeTOVE',
        PlantStatusCodeTNVE = 'MY.plantStatusCodeTNVE',
        PlantStatusCodeTNSE = 'MY.plantStatusCodeTNSE',
        PlantStatusCodeTN = 'MY.plantStatusCodeTN',
        PlantStatusCodeTNE = 'MY.plantStatusCodeTNE',
        PlantStatusCodeR = 'MY.plantStatusCodeR',
        PlantStatusCodeC = 'MY.plantStatusCodeC',
        PlantStatusCodeH = 'MY.plantStatusCodeH',
        PlantStatusCodeG = 'MY.plantStatusCodeG',
        PlantStatusCodeF = 'MY.plantStatusCodeF'
    }
    export enum PreservationEnum {
        PreservationPinned = 'MY.preservationPinned',
        PreservationGlued = 'MY.preservationGlued',
        PreservationEthanol = 'MY.preservationEthanol',
        PreservationEthanolPure = 'MY.preservationEthanolPure',
        PreservationEthanol96 = 'MY.preservationEthanol96',
        PreservationEthanol80 = 'MY.preservationEthanol80',
        PreservationEthanol80Pure = 'MY.preservationEthanol80Pure',
        PreservationEthanol70 = 'MY.preservationEthanol70',
        PreservationEthanolDenatured = 'MY.preservationEthanolDenatured',
        PreservationFormalin = 'MY.preservationFormalin',
        PreservationEthanolFormalin = 'MY.preservationEthanolFormalin',
        PreservationGlycerol = 'MY.preservationGlycerol',
        PreservationLiquid = 'MY.preservationLiquid',
        PreservationEulan = 'MY.preservationEulan',
        PreservationSlide = 'MY.preservationSlide',
        PreservationSlideEuparal = 'MY.preservationSlideEuparal',
        PreservationSlidePolyviol = 'MY.preservationSlidePolyviol',
        PreservationSlideCanadaBalsam = 'MY.preservationSlideCanadaBalsam',
        PreservationCriticalPointDrying = 'MY.preservationCriticalPointDrying',
        PreservationGoldPlated = 'MY.preservationGoldPlated',
        PreservationFreezeDried = 'MY.preservationFreezeDried',
        PreservationFrozen = 'MY.preservationFrozen',
        PreservationDry = 'MY.preservationDry',
        PreservationStuffed = 'MY.preservationStuffed',
        PreservationParaffin = 'MY.preservationParaffin',
        PreservationPressed = 'MY.preservationPressed',
        PreservationLiving = 'MY.preservationLiving',
        PreservationCast = 'MY.preservationCast',
        PreservationEthanolExFormalin = 'MY.preservationEthanolExFormalin',
        PreservationMercuricChloride = 'MY.preservationMercuricChloride',
        PreservationBouinSolution = 'MY.preservationBouinSolution',
        PreservationPampelsFluid = 'MY.preservationPampelsFluid'
    }
    export enum ProvenanceEnum {
        ProvenanceUnknown = 'MY.provenanceUnknown',
        ProvenanceCultivated = 'MY.provenanceCultivated',
        ProvenanceCultivatedUnsure = 'MY.provenanceCultivatedUnsure',
        ProvenanceCultivatedPropagatedFromWildSource = 'MY.provenanceCultivatedPropagatedFromWildSource',
        ProvenanceWildSource = 'MY.provenanceWildSource',
        ProvenanceWildSourceUnsure = 'MY.provenanceWildSourceUnsure',
        ProvenanceEscapedCultivated = 'MY.provenanceEscapedCultivated',
        ProvenancePropagule = 'MY.provenancePropagule'
    }
    export enum PublicityRestrictionsEnum {
        PublicityRestrictionsPublic = 'MZ.publicityRestrictionsPublic',
        PublicityRestrictionsProtected = 'MZ.publicityRestrictionsProtected',
        PublicityRestrictionsPrivate = 'MZ.publicityRestrictionsPrivate'
    }
    export enum RecordBasisEnum {
        RecordBasisPreservedSpecimen = 'MY.recordBasisPreservedSpecimen',
        RecordBasisFossilSpecimen = 'MY.recordBasisFossilSpecimen',
        RecordBasisSubfossilSpecimen = 'MY.recordBasisSubfossilSpecimen',
        RecordBasisHumanObservation = 'MY.recordBasisHumanObservation',
        RecordBasisMachineObservation = 'MY.recordBasisMachineObservation',
        RecordBasisLivingSpecimen = 'MY.recordBasisLivingSpecimen',
        RecordBasisMicrobialSpecimen = 'MY.recordBasisMicrobialSpecimen',
        RecordBasisHumanObservationSeen = 'MY.recordBasisHumanObservationSeen',
        RecordBasisHumanObservationHeard = 'MY.recordBasisHumanObservationHeard',
        RecordBasisHumanObservationPhoto = 'MY.recordBasisHumanObservationPhoto',
        RecordBasisHumanObservationIndirect = 'MY.recordBasisHumanObservationIndirect',
        RecordBasisHumanObservationHandled = 'MY.recordBasisHumanObservationHandled',
        RecordBasisHumanObservationVideo = 'MY.recordBasisHumanObservationVideo',
        RecordBasisHumanObservationAudio = 'MY.recordBasisHumanObservationAudio',
        RecordBasisMachineObservationVideo = 'MY.recordBasisMachineObservationVideo',
        RecordBasisMachineObservationAudio = 'MY.recordBasisMachineObservationAudio',
        RecordBasisMachineObservationGeologger = 'MY.recordBasisMachineObservationGeologger',
        RecordBasisMachineObservationSatelliteTransmitter = 'MY.recordBasisMachineObservationSatelliteTransmitter',
        RecordBasisLiterature = 'MY.recordBasisLiterature'
    }
    export enum SamplingMethodEnum {
        SamplingMethodLight = 'MY.samplingMethodLight',
        SamplingMethodPitfall = 'MY.samplingMethodPitfall',
        SamplingMethodNet = 'MY.samplingMethodNet',
        SamplingMethodMalaise = 'MY.samplingMethodMalaise',
        SamplingMethodSoilsample = 'MY.samplingMethodSoilsample',
        SamplingMethodSweeping = 'MY.samplingMethodSweeping',
        SamplingMethodBait = 'MY.samplingMethodBait',
        SamplingMethodBaittrap = 'MY.samplingMethodBaittrap',
        SamplingMethodFeromonetrap = 'MY.samplingMethodFeromonetrap',
        SamplingMethodWindowtrap = 'MY.samplingMethodWindowtrap',
        SamplingMethodPantrap = 'MY.samplingMethodPantrap',
        SamplingMethodYellowpan = 'MY.samplingMethodYellowpan',
        SamplingMethodYellowWindowTrap = 'MY.samplingMethodYellowWindowTrap',
        SamplingMethodYellowtrap = 'MY.samplingMethodYellowtrap',
        SamplingMethodTrap = 'MY.samplingMethodTrap',
        SamplingMethodLightTrap = 'MY.samplingMethodLightTrap',
        SamplingMethodMistnet = 'MY.samplingMethodMistnet',
        SamplingMethodHand = 'MY.samplingMethodHand',
        SamplingMethodCarnet = 'MY.samplingMethodCarnet',
        SamplingMethodDropping = 'MY.samplingMethodDropping',
        SamplingMethodExovo = 'MY.samplingMethodExovo',
        SamplingMethodElarva = 'MY.samplingMethodElarva',
        SamplingMethodEpupa = 'MY.samplingMethodEpupa',
        SamplingMethodReared = 'MY.samplingMethodReared',
        SamplingMethodEclectortrap = 'MY.samplingMethodEclectortrap',
        SamplingMethodSifting = 'MY.samplingMethodSifting',
        SamplingMethodBoard = 'MY.samplingMethodBoard',
        SamplingMethodDrag = 'MY.samplingMethodDrag',
        SamplingMethodTriangleDrag = 'MY.samplingMethodTriangleDrag',
        SamplingMethodFishNet = 'MY.samplingMethodFishNet',
        SamplingMethodElectrofishing = 'MY.samplingMethodElectrofishing',
        SamplingMethodAngleFishing = 'MY.samplingMethodAngleFishing',
        SamplingMethodFishTrap = 'MY.samplingMethodFishTrap',
        SamplingMethodSeine = 'MY.samplingMethodSeine',
        SamplingMethodTrawling = 'MY.samplingMethodTrawling',
        SamplingMethodOther = 'MY.samplingMethodOther',
        SamplingMethodDiving = 'MY.samplingMethodDiving',
        SamplingMethodBeamTrawl = 'MY.samplingMethodBeamTrawl',
        SamplingMethodDigging = 'MY.samplingMethodDigging',
        SamplingMethodWashing = 'MY.samplingMethodWashing'
    }
    export enum SexEnum {
        SexM = 'MY.sexM',
        SexF = 'MY.sexF',
        SexW = 'MY.sexW',
        SexU = 'MY.sexU',
        SexN = 'MY.sexN',
        SexX = 'MY.sexX',
        SexE = 'MY.sexE',
        SexC = 'MY.sexC'
    }
    export enum SmellEnum {
        SmellNotSmelled = 'MY.smellNotSmelled',
        SmellNoSmelled = 'MY.smellNoSmelled',
        SmellWeak = 'MY.smellWeak',
        SmellModerate = 'MY.smellModerate',
        SmellStrong = 'MY.smellStrong'
    }
    export enum SubstrateClassificationEnum {
        SubstrateGround = 'MY.substrateGround',
        SubstrateGroundLowShrubs = 'MY.substrateGroundLowShrubs',
        SubstrateGroundLichens = 'MY.substrateGroundLichens',
        SubstrateGroundHerbs = 'MY.substrateGroundHerbs',
        SubstrateGroundMosses = 'MY.substrateGroundMosses',
        SubstrateGroundSphagnum = 'MY.substrateGroundSphagnum',
        SubstrateGroundGrassy = 'MY.substrateGroundGrassy',
        SubstrateGroundNeedleLitter = 'MY.substrateGroundNeedleLitter',
        SubstrateGroundLeafLitter = 'MY.substrateGroundLeafLitter',
        SubstrateGroundMixedLitter = 'MY.substrateGroundMixedLitter',
        SubstrateGroundSandySoil = 'MY.substrateGroundSandySoil',
        SubstrateGroundGravelSoil = 'MY.substrateGroundGravelSoil',
        SubstrateGroundClayeySoil = 'MY.substrateGroundClayeySoil',
        SubstrateGroundHeathHumus = 'MY.substrateGroundHeathHumus',
        SubstrateGroundMull = 'MY.substrateGroundMull',
        SubstrateGroundPeat = 'MY.substrateGroundPeat',
        SubstrateGroundBurnedSoil = 'MY.substrateGroundBurnedSoil',
        SubstrateLivingTree = 'MY.substrateLivingTree',
        SubstrateLivingTreeTrunk = 'MY.substrateLivingTreeTrunk',
        SubstrateLivingTreeBase = 'MY.substrateLivingTreeBase',
        SubstrateLivingTreeRoots = 'MY.substrateLivingTreeRoots',
        SubstrateLivingTreeBranch = 'MY.substrateLivingTreeBranch',
        SubstrateLivingTreeDeadBranch = 'MY.substrateLivingTreeDeadBranch',
        SubstrateLivingTreeDeadLimb = 'MY.substrateLivingTreeDeadLimb',
        SubstrateLivingTreeLeaf = 'MY.substrateLivingTreeLeaf',
        SubstrateLivingTreeNeedle = 'MY.substrateLivingTreeNeedle',
        SubstrateDeadWood = 'MY.substrateDeadWood',
        SubstrateDeadWoodStandingTreeTrunk = 'MY.substrateDeadWoodStandingTreeTrunk',
        SubstrateDeadWoodStandingTreeBranch = 'MY.substrateDeadWoodStandingTreeBranch',
        SubstrateDeadWoodStandingTreeBase = 'MY.substrateDeadWoodStandingTreeBase',
        SubstrateDeadWoodFallenTreeTrunk = 'MY.substrateDeadWoodFallenTreeTrunk',
        SubstrateDeadWoodFallenTreeBranch = 'MY.substrateDeadWoodFallenTreeBranch',
        SubstrateDeadWoodUpturnedRoots = 'MY.substrateDeadWoodUpturnedRoots',
        SubstrateDeadWoodDeadRoots = 'MY.substrateDeadWoodDeadRoots',
        SubstrateDeadWoodStump = 'MY.substrateDeadWoodStump',
        SubstrateDeadWoodFallenBranch = 'MY.substrateDeadWoodFallenBranch',
        SubstrateDeadWoodCone = 'MY.substrateDeadWoodCone',
        SubstrateDeadWoodTwigs = 'MY.substrateDeadWoodTwigs',
        SubstrateDeadWoodBark = 'MY.substrateDeadWoodBark',
        SubstrateDeadWoodSawdust = 'MY.substrateDeadWoodSawdust',
        SubstrateDeadWoodPieceOfWood = 'MY.substrateDeadWoodPieceOfWood',
        SubstrateDeadWoodLoggingResidue = 'MY.substrateDeadWoodLoggingResidue',
        SubstrateDeadWoodLog = 'MY.substrateDeadWoodLog',
        SubstrateDeadWoodDriftwood = 'MY.substrateDeadWoodDriftwood',
        SubstrateDeadWoodConstructionWood = 'MY.substrateDeadWoodConstructionWood',
        SubstrateDung = 'MY.substrateDung',
        SubstrateCompost = 'MY.substrateCompost',
        SubstrateLivingShoot = 'MY.substrateLivingShoot',
        SubstrateDeadShoot = 'MY.substrateDeadShoot',
        SubstrateLivingFungus = 'MY.substrateLivingFungus',
        SubstrateDeadFungus = 'MY.substrateDeadFungus',
        SubstrateLivingAnimal = 'MY.substrateLivingAnimal',
        SubstrateDeadAnimal = 'MY.substrateDeadAnimal',
        SubstrateRockSurface = 'MY.substrateRockSurface'
    }
    export enum SubstrateDecayStageEnum {
        SubstrateDecayStageEnum1 = 'MY.substrateDecayStageEnum1',
        SubstrateDecayStageEnum2 = 'MY.substrateDecayStageEnum2',
        SubstrateDecayStageEnum3 = 'MY.substrateDecayStageEnum3',
        SubstrateDecayStageEnum4 = 'MY.substrateDecayStageEnum4',
        SubstrateDecayStageEnum5 = 'MY.substrateDecayStageEnum5'
    }
    export enum TasteEnum {
        TasteNotTasted = 'MY.tasteNotTasted',
        TasteNoTaste = 'MY.tasteNoTaste',
        TasteWeak = 'MY.tasteWeak',
        TasteModerate = 'MY.tasteModerate',
        TasteStrong = 'MY.tasteStrong'
    }
    export enum TaxonConfidenceEnum {
        TaxonConfidenceSure = 'MY.taxonConfidenceSure',
        TaxonConfidenceUnsure = 'MY.taxonConfidenceUnsure',
        TaxonConfidenceSubspeciesUnsure = 'MY.taxonConfidenceSubspeciesUnsure'
    }
}
