
export interface UnitFact {
    autocompleteSelectedTaxonID?: string;

    /**
     * Non-negative integer
     */
    individualCountBiotopeA?: number;

    /**
     * Non-negative integer
     */
    individualCountBiotopeB?: number;

    /**
     * Non-negative integer
     */
    individualCountBiotopeC?: number;

    /**
     * Non-negative integer
     */
    individualCountBiotopeD?: number;

    /**
     * Non-negative integer
     */
    individualCountBiotopeE?: number;

    /**
     * Non-negative integer
     */
    individualCountBiotopeF?: number;

    /**
     * Non-negative integer
     */
    individualCountBiotopeG?: number;

    /**
     * Non-negative integer
     */
    individualCountBiotopeH?: number;

}
