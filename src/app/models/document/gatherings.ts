import { Units } from './units';
import { TaxonCensus } from './taxon-census';
import { GatheringFact } from './gathering-fact';
import { Geometry } from './geometry';

export interface Gatherings {

  /**
   * AFE grid name
   */
  aFEQuadrat?: string;

  /**
   * UTM grid name
   */
  uTMQuadrat?: string;

  /**
   * Separate multiple names with commas, from generic to specific. (E.g. Etelä-Suomen lääni, Uusimaa)
   */
  administrativeProvince?: string;

  /**
   * Altitude form sea level in meters, or range (E.g. 90, or 80-100). No GPS altitude here.
   */
  alt?: string;

  areaDetail?: string;

  /**
   * Write associated taxa names here, separated by a semicolon (;). E.g.: \\\"Betula pendula; Betula pubescens; Poaceae\\\".
   */
  associatedObservationTaxa?: string;

  associatedSpecimenTaxa?: string;

  /**
   * Formal abbreviation. For Finnish eliömaakunnat, use Finnish abbreviation.
   */
  biologicalProvince?: string;

  /**
   * Name of an expedition or such
   */
  collectingEventName?: string;

  controlActivitiesNotes?: string;

  /**
   * Informal notes about the coordinates
   */
  coordinateNotes?: string;

  /**
   * Maximum error of coordinates in meters
   */
  coordinateRadius?: string;

  /**
   * Use for NEW SPECIMENS: Where the coordinates have been acquired
   */
  coordinateSource?: Gatherings.CoordinateSourceEnum;

  /**
   * Examples of different coordinate styles are at https://wiki.helsinki.fi/display/digit/Entering+specimen+data
   */
  coordinateSystem?: Gatherings.CoordinateSystemEnum;

  coordinatesGridYKJ?: string;

  coordinatesVerbatim?: string;

  /**
   * Country name in English, or 2-letter country code, or name from label
   */
  country?: string;

  /**
   * County (piirikunta, kreivikunta etc.)
   */
  county?: string;

  dateBegin?: string;

  dateEnd?: string;

  /**
   * Date just as it appears on the label or other original source, no interpretation, gatheringErrors and all
   */
  dateVerbatim?: string;

  /**
   * Depth in meters, or range (E.g. 0.9, or 0.8-1.0)
   */
  depth?: string;

  dynamicProperties?: string;

  eventDate?: string;

  forestVegetationZone?: Gatherings.ForestVegetationZoneEnum;

  /**
   * instance of gatheringFact
   */
  gatheringFact?: GatheringFact;

  /**
   * QName for MZ.geometry
   */
  geometry?: Geometry;

  /**
   * Use for OLD SPECIMENS: What source was used to get coordinates from locality name
   */
  georeferenceSource?: Gatherings.GeoreferenceSourceEnum;

  habitat?: Array<string>;

  habitatAttributes?: Array<string>;

  /**
   * Formal habitat name or abbreviation. If several, separate with semicolons (E.g. 'OMT; OMaT')
   */
  habitatClassification?: string;

  /**
   * Informal description of the habitat
   */
  habitatDescription?: string;

  /**
   * If country is not known or not applicable, for example continent, ocean or large island
   */
  higherGeography?: string;

  /**
   * Unique ID for the object. This will be automatically generated.
   */
  id?: string;

  /**
   * QName for MM.image
   */
  images?: Array<string>;

  invasiveControlEffectiveness?: Gatherings.InvasiveControlEffectivenessEnum;

  /**
   * QName for MX.taxon
   */
  invasiveControlTaxon?: Array<string>;

  keywords?: Array<string>;

  /**
   * Latitude. For southern latitudes, use negative value.
   */
  latitude?: string;

  leg?: Array<string>;

  /**
   * Alkuperäislähteen käyttäjätunnus
   */
  legUserID?: Array<string>;

  /**
   * Leg just as it appears in the label or other original source, no interpretation, gatheringErrors and all
   */
  legVerbatim?: string;

  /**
   * Official name of the locality. Separate multiple names with commas, from generic to specific. No informal description here!
   */
  locality?: string;

  /**
   * Informal description of the exact locality, e.g. '5 km NE of city X, under stone bridge'
   */
  localityDescription?: string;

  /**
   * An unique identifier or code for the locality, if the locality has one (e.g. from SAPO-ontology)
   */
  localityID?: string;

  /**
   * Locality word-to-word as it appears on the label or other original source, gatheringErrors and all
   */
  localityVerbatim?: string;

  /**
   * Longitude. For western longitudes, use negative value.
   */
  longitude?: string;

  /**
   * Municipality, commune, town, city or civil parish
   */
  municipality?: string;

  /**
   * QName for MNP.namedPlace
   */
  namedPlaceID?: string;

  /**
   * Free-text notes
   */
  notes?: string;

  observationDays?: number;

  observationMinutes?: number;

  predominantTree?: Gatherings.PredominantTreeEnum;

  projectTitle?: string;

  province?: string;

  /**
   * PUBLIC: all data can be published; PROTECTED: exact locality is hidden; PRIVATE: most of the data is hidden. If blank means same as public
   */
  publicityRestrictions?: Gatherings.PublicityRestrictionsEnum;

  samplingMethod?: Gatherings.SamplingMethodEnum;

  samplingMethodNotes?: string;

  skipped?: boolean;

  substrate?: string;

  /**
   * Array of taxonCensus
   */
  taxonCensus?: Array<TaxonCensus>;

  temperature?: number;

  timeEnd?: string;

  timeStart?: string;

  trapCount?: number;

  /**
   * Array of units
   */
  units?: Array<Units>;

  weather?: string;

  /**
   * Geological information about gathering in wgs84 format
   */
  wgs84Geometry?: any;

  wgs84Latitude?: string;

  wgs84Longitude?: string;

}
export namespace Gatherings {
    export enum CoordinateSourceEnum {
        CoordinateSourceGps = 'MY.coordinateSourceGps',
        CoordinateSourcePeruskartta = 'MY.coordinateSourcePeruskartta',
        CoordinateSourcePapermap = 'MY.coordinateSourcePapermap',
        CoordinateSourceKotkamap = 'MY.coordinateSourceKotkamap',
        CoordinateSourceKarttapaikka = 'MY.coordinateSourceKarttapaikka',
        CoordinateSourceRetkikartta = 'MY.coordinateSourceRetkikartta',
        CoordinateSourceOther = 'MY.coordinateSourceOther',
        CoordinateSourceUnknown = 'MY.coordinateSourceUnknown'
    }
    export enum CoordinateSystemEnum {
        CoordinateSystemYkj = 'MY.coordinateSystemYkj',
        CoordinateSystemWgs84 = 'MY.coordinateSystemWgs84',
        CoordinateSystemWgs84dms = 'MY.coordinateSystemWgs84dms',
        CoordinateSystemKkj = 'MY.coordinateSystemKkj',
        CoordinateSystemEtrsTm35fin = 'MY.coordinateSystemEtrs-tm35fin',
        CoordinateSystemDd = 'MY.coordinateSystemDd',
        CoordinateSystemDms = 'MY.coordinateSystemDms'
    }
    export enum ForestVegetationZoneEnum {
        ForestVegetationZone1a = 'MY.forestVegetationZone1a',
        ForestVegetationZone1b = 'MY.forestVegetationZone1b',
        ForestVegetationZone2a = 'MY.forestVegetationZone2a',
        ForestVegetationZone2b = 'MY.forestVegetationZone2b',
        ForestVegetationZone3a = 'MY.forestVegetationZone3a',
        ForestVegetationZone3b = 'MY.forestVegetationZone3b',
        ForestVegetationZone3c = 'MY.forestVegetationZone3c',
        ForestVegetationZone4a = 'MY.forestVegetationZone4a',
        ForestVegetationZone4b = 'MY.forestVegetationZone4b',
        ForestVegetationZone4c = 'MY.forestVegetationZone4c',
        ForestVegetationZone4d = 'MY.forestVegetationZone4d'
    }
    export enum GeoreferenceSourceEnum {
        GeoreferenceSourceKotka = 'MY.georeferenceSourceKotka',
        GeoreferenceSourceKarttapaikka = 'MY.georeferenceSourceKarttapaikka',
        GeoreferenceSourcePaikkatietoikkuna = 'MY.georeferenceSourcePaikkatietoikkuna',
        GeoreferenceSourceKarjalankartat = 'MY.georeferenceSourceKarjalankartat',
        GeoreferenceSourceRetkikartta = 'MY.georeferenceSourceRetkikartta',
        GeoreferenceSourceGoogle = 'MY.georeferenceSourceGoogle',
        GeoreferenceSourcePeruskartta = 'MY.georeferenceSourcePeruskartta',
        GeoreferenceSourcePapermap = 'MY.georeferenceSourcePapermap',
        GeoreferenceSourceOtherpaper = 'MY.georeferenceSourceOtherpaper',
        GeoreferenceSourceOtherweb = 'MY.georeferenceSourceOtherweb',
        GeoreferenceSourceCatalogue = 'MY.georeferenceSourceCatalogue',
        GeoreferenceSourceBiogeomancer = 'MY.georeferenceSourceBiogeomancer',
        GeoreferenceSourceGeolocate = 'MY.georeferenceSourceGeolocate',
        GeoreferenceSourceOther = 'MY.georeferenceSourceOther',
        GeoreferenceSourceUnknown = 'MY.georeferenceSourceUnknown'
    }
    export enum InvasiveControlEffectivenessEnum {
        InvasiveControlEffectivenessFull = 'MY.invasiveControlEffectivenessFull',
        InvasiveControlEffectivenessPartial = 'MY.invasiveControlEffectivenessPartial',
        InvasiveControlEffectivenessNone = 'MY.invasiveControlEffectivenessNone',
        InvasiveControlEffectivenessNotFound = 'MY.invasiveControlEffectivenessNotFound'
    }
    export enum PredominantTreeEnum {
        _37819 = 'MX.37819',
        _37812 = 'MX.37812',
        _37992 = 'MX.37992',
        _38004 = 'MX.38004',
        _38590 = 'MX.38590',
        _38686 = 'MX.38686',
        _38563 = 'MX.38563',
        _38527 = 'MX.38527',
        _41344 = 'MX.41344',
        _38016 = 'MX.38016',
        _39331 = 'MX.39331',
        _37990 = 'MX.37990',
        _38008 = 'MX.38008',
        _38010 = 'MX.38010',
        _37975 = 'MX.37975',
        _37976 = 'MX.37976',
        _39122 = 'MX.39122'
    }
    export enum PublicityRestrictionsEnum {
        PublicityRestrictionsPublic = 'MZ.publicityRestrictionsPublic',
        PublicityRestrictionsProtected = 'MZ.publicityRestrictionsProtected',
        PublicityRestrictionsPrivate = 'MZ.publicityRestrictionsPrivate'
    }
    export enum SamplingMethodEnum {
        SamplingMethodLight = 'MY.samplingMethodLight',
        SamplingMethodPitfall = 'MY.samplingMethodPitfall',
        SamplingMethodNet = 'MY.samplingMethodNet',
        SamplingMethodMalaise = 'MY.samplingMethodMalaise',
        SamplingMethodSoilsample = 'MY.samplingMethodSoilsample',
        SamplingMethodSweeping = 'MY.samplingMethodSweeping',
        SamplingMethodBait = 'MY.samplingMethodBait',
        SamplingMethodBaittrap = 'MY.samplingMethodBaittrap',
        SamplingMethodFeromonetrap = 'MY.samplingMethodFeromonetrap',
        SamplingMethodWindowtrap = 'MY.samplingMethodWindowtrap',
        SamplingMethodPantrap = 'MY.samplingMethodPantrap',
        SamplingMethodYellowpan = 'MY.samplingMethodYellowpan',
        SamplingMethodYellowWindowTrap = 'MY.samplingMethodYellowWindowTrap',
        SamplingMethodYellowtrap = 'MY.samplingMethodYellowtrap',
        SamplingMethodTrap = 'MY.samplingMethodTrap',
        SamplingMethodLightTrap = 'MY.samplingMethodLightTrap',
        SamplingMethodMistnet = 'MY.samplingMethodMistnet',
        SamplingMethodHand = 'MY.samplingMethodHand',
        SamplingMethodCarnet = 'MY.samplingMethodCarnet',
        SamplingMethodDropping = 'MY.samplingMethodDropping',
        SamplingMethodExovo = 'MY.samplingMethodExovo',
        SamplingMethodElarva = 'MY.samplingMethodElarva',
        SamplingMethodEpupa = 'MY.samplingMethodEpupa',
        SamplingMethodReared = 'MY.samplingMethodReared',
        SamplingMethodEclectortrap = 'MY.samplingMethodEclectortrap',
        SamplingMethodSifting = 'MY.samplingMethodSifting',
        SamplingMethodBoard = 'MY.samplingMethodBoard',
        SamplingMethodDrag = 'MY.samplingMethodDrag',
        SamplingMethodTriangleDrag = 'MY.samplingMethodTriangleDrag',
        SamplingMethodFishNet = 'MY.samplingMethodFishNet',
        SamplingMethodElectrofishing = 'MY.samplingMethodElectrofishing',
        SamplingMethodAngleFishing = 'MY.samplingMethodAngleFishing',
        SamplingMethodFishTrap = 'MY.samplingMethodFishTrap',
        SamplingMethodSeine = 'MY.samplingMethodSeine',
        SamplingMethodTrawling = 'MY.samplingMethodTrawling',
        SamplingMethodOther = 'MY.samplingMethodOther',
        SamplingMethodDiving = 'MY.samplingMethodDiving',
        SamplingMethodBeamTrawl = 'MY.samplingMethodBeamTrawl',
        SamplingMethodDigging = 'MY.samplingMethodDigging',
        SamplingMethodWashing = 'MY.samplingMethodWashing'
    }
}
