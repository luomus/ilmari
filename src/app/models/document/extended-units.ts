import { Units } from './units';
import { ExtendedIdentifications } from './extended-identifications';
import { ExtendedUnitFact } from './extended-unit-fact';
import { ExtendedUnitGathering } from './extended-unit-gathering';

export interface ExtendedUnits extends Units {
  _root: string;
  _parent: string;
  _order: number;

  identifications?: ExtendedIdentifications[];
  unitFact?: ExtendedUnitFact;
  unitGathering?: ExtendedUnitGathering;
}
