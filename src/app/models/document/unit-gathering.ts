import { Geometry } from './geometry';

export interface UnitGathering {

    coordinatesGridYKJ?: string;

    dateBegin?: string;

    dateEnd?: string;

    /**
     * QName for MZ.geometry
     */
    geometry?: Geometry;

    /**
     * Informal description of the habitat
     */
    habitatDescription?: string;

    habitatIUCN?: string;

    /**
     * Unique ID for the object. This will be automatically generated.
     */
    id?: string;

    substrate?: string;

    /**
     * QName for MX.taxon
     */
    substrateSpeciesID?: string;

}
