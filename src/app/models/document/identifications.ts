export interface Identifications {

    /**
     * Write associated taxa names here, separated by a semicolon (;). E.g.: \\\"Betula pendula; Betula pubescens; Poaceae\\\".
     */
    associatedObservationTaxa?: string;

    author?: string;

    /**
     * Name of the identifier (person)
     */
    det?: string;

    detDate?: string;

    detMethod?: Identifications.DetMethodEnum;

    detVerbatim?: string;

    genusQualifier?: string;

    /**
     * Unique ID for the object. This will be automatically generated.
     */
    id?: string;

    identificationBasis?: Array<string>;

    identificationDate?: string;

    identificationNotes?: string;

    infraAuthor?: string;

    infraEpithet?: string;

    infraRank?: Identifications.InfraRankEnum;

    infrasubspecificSubdivision?: string;

    /**
     * This can be used to select one of the identifications as 'recommended', which is the used as default when displaying information about the specimen.
     */
    preferredIdentification?: string;

    /**
     * PUBLIC: all data can be published; PROTECTED: exact locality is hidden; PRIVATE: most of the data is hidden. If blank means same as public
     */
    publicityRestrictions?: Identifications.PublicityRestrictionsEnum;

    /**
     * Publication reference for the taxon concept, that was used in identification
     */
    sec?: string;

    sortOrder?: number;

    speciesQualifier?: string;

    taxon?: string;

    taxonID?: string;

    taxonRank?: Identifications.TaxonRankEnum;

    /**
     * Taxon name in original format (e.g. from the label), gatheringErrors and all
     */
    taxonVerbatim?: string;

}
export namespace Identifications {
    export enum DetMethodEnum {
        DetMethodFreshSample = 'MY.detMethodFreshSample',
        DetMethodMicroscopy = 'MY.detMethodMicroscopy',
        DetMethodPhoto = 'MY.detMethodPhoto'
    }
    export enum InfraRankEnum {
        InfraRankSsp = 'MY.infraRankSsp',
        InfraRankVar = 'MY.infraRankVar',
        InfraRankBeta = 'MY.infraRankBeta',
        InfraRankB = 'MY.infraRankB',
        InfraRankForma = 'MY.infraRankForma',
        InfraRankHybrid = 'MY.infraRankHybrid',
        InfraRankAnamorph = 'MY.infraRankAnamorph',
        InfraRankAggregate = 'MY.infraRankAggregate',
        InfraRankAberration = 'MY.infraRankAberration',
        InfraRankCultivar = 'MY.infraRankCultivar',
        InfraRankMorpha = 'MY.infraRankMorpha',
        InfraRankUnknown = 'MY.infraRankUnknown'
    }
    export enum PublicityRestrictionsEnum {
        PublicityRestrictionsPublic = 'MZ.publicityRestrictionsPublic',
        PublicityRestrictionsProtected = 'MZ.publicityRestrictionsProtected',
        PublicityRestrictionsPrivate = 'MZ.publicityRestrictionsPrivate'
    }
    export enum TaxonRankEnum {
        Superdomain = 'MX.superdomain',
        Domain = 'MX.domain',
        Kingdom = 'MX.kingdom',
        Subkingdom = 'MX.subkingdom',
        Superphylum = 'MX.superphylum',
        Superdivision = 'MX.superdivision',
        Phylum = 'MX.phylum',
        Division = 'MX.division',
        Subphylum = 'MX.subphylum',
        Subdivision = 'MX.subdivision',
        Superclass = 'MX.superclass',
        Class = 'MX.class',
        Subclass = 'MX.subclass',
        Infraclass = 'MX.infraclass',
        Parvclass = 'MX.parvclass',
        Superorder = 'MX.superorder',
        Order = 'MX.order',
        Suborder = 'MX.suborder',
        Infraorder = 'MX.infraorder',
        Parvorder = 'MX.parvorder',
        Superfamily = 'MX.superfamily',
        Family = 'MX.family',
        Subfamily = 'MX.subfamily',
        Tribe = 'MX.tribe',
        Subtribe = 'MX.subtribe',
        Supergenus = 'MX.supergenus',
        Genus = 'MX.genus',
        Subgenus = 'MX.subgenus',
        Section = 'MX.section',
        Subsection = 'MX.subsection',
        Series = 'MX.series',
        Subseries = 'MX.subseries',
        InfragenericTaxon = 'MX.infragenericTaxon',
        Aggregate = 'MX.aggregate',
        Species = 'MX.species',
        Nothospecies = 'MX.nothospecies',
        InfraspecificTaxon = 'MX.infraspecificTaxon',
        SubspecificAggregate = 'MX.subspecificAggregate',
        Subspecies = 'MX.subspecies',
        Nothosubspecies = 'MX.nothosubspecies',
        Variety = 'MX.variety',
        Subvariety = 'MX.subvariety',
        Form = 'MX.form',
        Subform = 'MX.subform',
        Hybrid = 'MX.hybrid',
        Anamorph = 'MX.anamorph',
        Ecotype = 'MX.ecotype',
        IntergenericHybrid = 'MX.intergenericHybrid',
        InfragenericHybrid = 'MX.infragenericHybrid',
        Cultivar = 'MX.cultivar',
        Group = 'MX.group',
        SpeciesAggregate = 'MX.speciesAggregate'
    }
}
