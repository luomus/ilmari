export interface TypeSpecimens {

    /**
     * Unique ID for the object. This will be automatically generated.
     */
    id?: string;

    /**
     * PUBLIC: all data can be published; PROTECTED: exact locality is hidden; PRIVATE: most of the data is hidden. If blank means same as public
     */
    publicityRestrictions?: TypeSpecimens.PublicityRestrictionsEnum;

    typeAuthor?: string;

    /**
     * Publication reference for original description or basionyme
     */
    typeBasionymePubl?: string;

    typeNotes?: string;

    typePubl?: string;

    typeSeriesID?: string;

    typeSpecies?: string;

    /**
     * Is this holotype, paratype, syntype etc...
     */
    typeStatus?: TypeSpecimens.TypeStatusEnum;

    typeSubspecies?: string;

    typeSubspeciesAuthor?: string;

    /**
     * Verification whether this really is a type?
     */
    typeVerification?: TypeSpecimens.TypeVerificationEnum;

    typif?: string;

    typifDate?: string;

}
export namespace TypeSpecimens {
    export enum PublicityRestrictionsEnum {
        PublicityRestrictionsPublic = 'MZ.publicityRestrictionsPublic',
        PublicityRestrictionsProtected = 'MZ.publicityRestrictionsProtected',
        PublicityRestrictionsPrivate = 'MZ.publicityRestrictionsPrivate'
    }
    export enum TypeStatusEnum {
        TypeStatusType = 'MY.typeStatusType',
        TypeStatusHolotype = 'MY.typeStatusHolotype',
        TypeStatusSyntype = 'MY.typeStatusSyntype',
        TypeStatusParatype = 'MY.typeStatusParatype',
        TypeStatusLectotype = 'MY.typeStatusLectotype',
        TypeStatusParalectotype = 'MY.typeStatusParalectotype',
        TypeStatusNeotype = 'MY.typeStatusNeotype',
        TypeStatusAllotype = 'MY.typeStatusAllotype',
        TypeStatusNeoallotype = 'MY.typeStatusNeoallotype',
        TypeStatusIsotype = 'MY.typeStatusIsotype',
        TypeStatusEpitype = 'MY.typeStatusEpitype',
        TypeStatusIsolectotype = 'MY.typeStatusIsolectotype',
        TypeStatusIsoepitype = 'MY.typeStatusIsoepitype',
        TypeStatusIsoneotype = 'MY.typeStatusIsoneotype',
        TypeStatusIsoparatype = 'MY.typeStatusIsoparatype',
        TypeStatusIsosyntype = 'MY.typeStatusIsosyntype',
        TypeStatusOriginalMaterial = 'MY.typeStatusOriginalMaterial',
        TypeStatusCotype = 'MY.typeStatusCotype',
        TypeStatusTopotype = 'MY.typeStatusTopotype',
        TypeStatusHomotype = 'MY.typeStatusHomotype',
        TypeStatusNo = 'MY.typeStatusNo',
        TypeStatusPossible = 'MY.typeStatusPossible',
        TypeStatusObscure = 'MY.typeStatusObscure',
        TypeStatusTypeStrain = 'MY.typeStatusTypeStrain',
        TypeStatusPathovarReferenceStrain = 'MY.typeStatusPathovarReferenceStrain'
    }
    export enum TypeVerificationEnum {
        TypeVerificationVerified = 'MY.typeVerificationVerified',
        TypeVerificationUnverified = 'MY.typeVerificationUnverified',
        TypeVerificationProbable = 'MY.typeVerificationProbable',
        TypeVerificationDoubtful = 'MY.typeVerificationDoubtful'
    }
}
