import { Gatherings } from './gatherings';
import { ExtendedUnits } from './extended-units';
import { ExtendedGatheringFact } from './extended-gathering-fact';

export interface ExtendedGatherings extends Gatherings {
  _root: string;
  _order: number;

  units?: ExtendedUnits[];
  gatheringFact?: ExtendedGatheringFact;
}
