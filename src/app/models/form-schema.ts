import { IdAware } from './id-aware';

export interface FormSchemaModel extends IdAware {

  id: string;

  language?: string;

  title: string;

  description: string;

  instructions?: {
    fi: string;
    en: string;
    sv: string;
  };

  uiSchema?: any;

  fields: any;
}
