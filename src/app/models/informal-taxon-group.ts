export interface InformalTaxonGroup {
  id: string;

  name: string;

  hasSubGroup?: Array<string|InformalTaxonGroup>;
}

export interface ExtendedInformalTaxonGroup extends InformalTaxonGroup {
  _root: number;
  _master: number;
}

export interface ExtendedInformalTaxonGroupQuery {
  _root: number;
}
