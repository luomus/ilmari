import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExportDbComponent } from './export-db/export-db.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: ExportDbComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
