import { Component, OnInit } from '@angular/core';
import { TaxonService } from '../../shared/service/taxon.service';
import { concatMap, switchMap, tap } from 'rxjs/operators';
import { from } from 'rxjs';
import { saveAs } from 'file-saver';

import * as JSZip from 'jszip';

@Component({
  selector: 'ilm-export-db',
  templateUrl: './export-db.component.html',
  styleUrls: ['./export-db.component.scss']
})
export class ExportDbComponent implements OnInit {

  constructor(
    private taxonService: TaxonService
  ) { }

  ngOnInit() {
  }

  export() {
    const zip = new JSZip();
    this.taxonService.getAll({belongsTo: ['MX.37600'], pageSize: 1000000, page: 1}).pipe(
      switchMap(data => from(data.taxa).pipe(
        concatMap(taxon => this.taxonService.get(taxon.id)),
        tap(taxon => zip.file(`${taxon.id}.json`, JSON.stringify(taxon)))
      ))
    ).subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        zip.generateAsync({type: 'blob'})
          .then(function (blob) {
            saveAs(blob, 'taxa.zip');
          });
      });

  }

  import() {

  }

}
