export const environment = {
  production: true,
  base: 'https://ilmari-dev.riihikoski.net',
  rootTaxon: 'MX.37600',
  mapsToken: 'AIzaSyAXsondK_Ruyf5NEDk_1Zy5IIB_LGb4L-M',
  viewer: 'https://dev.laji.fi/view?uri=%uri%&highlight=%highlight%',
  loginUrl: 'https://fmnh-ws-test.it.helsinki.fi/laji-auth/login?' +
    'target=KE.601&next=%2Fuser%2Flogin&allowUnapproved=false&redirectMethod=GET&offerPermanent=true',
  lajiApi: {
    base: 'https://apitest.laji.fi/v0/',
    token: 'sSV6YSTFvtQAZcRsdaWK99PGrfFWDI72wYy9jKs0hlgtDkGV85OIB8K5yYFPw4UF'
  },
  recognizeApi: 'https://proxy.riihikoski.net/',
  proxy: 'https://proxy.riihikoski.net/',
  defaults: {
    informalTaxonGroup: 'MVL.1'
  },
  masterGroups: [
    'MVL.1', // Linnut
    'MVL.2', // Nisäkkäät
    'MVL.31', // Perhoset
    'MVL.26', // Sammakkoeläimet ja matelijat
    'MVL.61', // Helttasienet
    'MVL.21', // Kasvit
    'MVL.27', // Kalat
    'MVL.40', // Nilviäiset
    'MVL.37', // Tuhatjalkaiset
    'MVL.39', // Äyriäiset
    'MVL.233', // Sienet ja jäkälät
    'MVL.232', // Hyönteiset ja hämähäkkieläimet
  ],
  forms: {
    trip: 'JX.519'
  }
};
